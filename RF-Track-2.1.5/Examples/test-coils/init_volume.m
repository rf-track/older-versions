function V = init_volume(Bz)
RF_Track;

%% Two coils
Cm = Coil(0.0, -Bz, 0.1); 
Cp = Coil(0.0, +Bz, 0.1);

%% Volume
V = Volume();

%add coils
step = 0.2; %separation between coils %m
 
for j = -16:2:16
  V.add(Cm, 0, 0, j*step);    
end

for j = -15:2:16
  V.add(Cp, 0, 0, j*step);    
end

% set boundaries
V.set_s0(-step);
V.set_s1(+step);
