addpath('../../');

pkg load image;

%% Load RF_Track

RF_Track;

setenv("DEBUG"); % triggers RF-Track debug information

%%%%%%%%%%%%%%

V_ele = 0.09;

Cooler.L = 3; % m
Cooler.B = 0.07; % T
Cooler.r0 = 25; % mm, electron beam radius

%% Electron beam parameters
Electrons.Ne = 8.27e13; % electron number density #/m^3
Electrons.V = V_ele; % 0.2% less than the ion speed
Electrons.gamma = 1 / sqrt(1 - Electrons.V**2); %
Electrons.P = RF_Track.electronmass * Electrons.V * Electrons.gamma;

Nx = 16;
Ny = 16;
Nz = 4;

E = ElectronCooler(Cooler.L, Cooler.r0/1e3, Cooler.r0/1e3);
E.set_Q(-1);
E.set_electron_mesh(Nx, Ny, Nz, Electrons.Ne, 0, 0, Electrons.V);
E.set_static_Bfield(0.0, 0.0, Cooler.B);
E.compute_cooling_force();

for force = { 'unmagnetized' , 'magnetized' }

    U = load(sprintf('cooling_force_%s.txt', force{}));

    Ur = sort(unique(U(:,1)));
    Ul = sort(unique(U(:,2)));
    Nr = length(Ur);
    Nl = length(Ul);
    
    Fr_int = reshape (U(:,3), Nl, Nr); % integrated force
    Fl_int = reshape (U(:,4), Nl, Nr);
    Fr_asy = reshape (U(:,5), Nl, Nr); % asymptotic force
    Fl_asy = reshape (U(:,6), Nl, Nr);
    Fr_rat = reshape (U(:,7), Nl, Nr); % asymptotic / integrated ratio
    Fl_rat = reshape (U(:,8), Nl, Nr);

    surfl(Ur(2:end), Ul, min(imsmooth(Fr_rat(:,2:end), "Gaussian", 1),2));
    shading flat;
    xlabel('U_\perp / \Delta_{\perp}', 'fontsize', 16);
    ylabel('U_{||} / \Delta_{\perp}', 'fontsize', 16);
    zlabel('F_{\perp_{(asymptotic)}} / F_{\perp_{(integrated)}}', 'fontsize', 16);
    axis([ min(Ur) max(Ur) min(Ul) max(Ul) ]);
    print -dpng -r200 tmp.png ; system(sprintf('anytopnm tmp.png | pnmcrop | pnmtopng > plot_Fr_%s_rat.png', force{}));

    surfl(Ur(2:end), Ul, min(imsmooth(Fl_rat(:,2:end), "Gaussian", 1),2));
    shading flat;
    xlabel('U_\perp / \Delta_{\perp}', 'fontsize', 16);
    ylabel('U_{||} / \Delta_{\perp}', 'fontsize', 16);
    zlabel('F_{{||}_{(asymptotic)}} / F_{{||}_{(integrated)}}', 'fontsize', 16);
    axis([ min(Ur) max(Ur) min(Ul) max(Ul) ]);
    print -dpng -r200 tmp.png ; system(sprintf('anytopnm tmp.png | pnmcrop | pnmtopng > plot_Fl_%s_rat.png', force{}));
    
    surfl(Ur, Ul, imsmooth(Fr_int, "Gaussian", 1)/1e6);
    shading flat;
    xlabel('U_\perp / \Delta_{\perp}', 'fontsize', 16);
    ylabel('U_{||} / \Delta_{\perp}', 'fontsize', 16);
    zlabel('F_{\perp_{(integrated)}} [10^6/c^2]', 'fontsize', 16);
    axis([ min(Ur) max(Ur) min(Ul) max(Ul) ]);
    print -dpng -r200 tmp.png ; system(sprintf('anytopnm tmp.png | pnmcrop | pnmtopng > plot_Fr_%s_int.png', force{}));

    surfl(Ur, Ul, imsmooth(Fl_int, "Gaussian", 1)/1e6);
    shading flat;
    xlabel('U_\perp / \Delta_{\perp}', 'fontsize', 16);
    ylabel('U_{||} / \Delta_{\perp}', 'fontsize', 16);
    zlabel('F_{{||}_{(integrated)}} [10^6/c^2]', 'fontsize', 16);
    axis([ min(Ur) max(Ur) min(Ul) max(Ul) ]);
    print -dpng -r200 tmp.png ; system(sprintf('anytopnm tmp.png | pnmcrop | pnmtopng > plot_Fl_%s_int.png', force{}));
    
    surfl(Ur, Ul, imsmooth(Fr_asy, "Gaussian", 1)/1e6);
    shading flat;
    xlabel('U_\perp / \Delta_{\perp}', 'fontsize', 16);
    ylabel('U_{||} / \Delta_{\perp}', 'fontsize', 16);
    zlabel('F_{\perp_{(asymptotic)}} [10^6/c^2]', 'fontsize', 16);
    axis([ min(Ur) max(Ur) min(Ul) max(Ul) ]);
    print -dpng -r200 tmp.png ; system(sprintf('anytopnm tmp.png | pnmcrop | pnmtopng > plot_Fr_%s_asy.png', force{}));

    surfl(Ur, Ul, imsmooth(Fl_asy, "Gaussian", 1)/1e6);
    shading flat;
    xlabel('U_\perp / \Delta_{\perp}', 'fontsize', 16);
    ylabel('U_{||} / \Delta_{\perp}', 'fontsize', 16);
    zlabel('F_{{||}_{(asymptotic)}} [10^6/c^2]', 'fontsize', 16);
    axis([ min(Ur) max(Ur) min(Ul) max(Ul) ]);
    print -dpng -r200 tmp.png ; system(sprintf('anytopnm tmp.png | pnmcrop | pnmtopng > plot_Fl_%s_asy.png', force{}));


end
