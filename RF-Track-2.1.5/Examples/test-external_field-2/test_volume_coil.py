import matplotlib.pyplot as plt
import RF_Track as RFT
import numpy as np

# Two coils
Cm = RFT.ExternalField(0, './coil 0 -1 0.2')
Cp = RFT.ExternalField(0, './coil 0 +1 0.2')

# Volume
V = RFT.Volume()

# set boundaries
V.set_s0(-1.0)
V.set_s1(+1.0)

# place the two coils in the volume
V.add(Cm, 0, 0, -0.5)
V.add(Cp, 0, 0,  0.5)

# plot Bz
S  = np.linspace(V.get_s0(), V.get_s1(), 100) # m
Bz = np.empty_like(S)
for i in range(100):
    E,B = V.get_field(0, 0, S[i] * 1e3, 0)
    Bz[i] = B[2]

plt.plot(S, Bz)
plt.xlabel('$S$ [m]')
plt.ylabel('$B_z$ [T]')
plt.savefig('test.pdf')
