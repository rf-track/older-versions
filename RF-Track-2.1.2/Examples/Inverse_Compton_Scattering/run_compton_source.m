%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             %
% Data taken from ThomX's TDR %
%                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RF_Track;

%% Define beam-laser IP parameters
L_FP = 0.01; % m, length of the IP region
IP_beta = 0.1; % m, beta star
IP_Lstar = L_FP/2; % m
X_angle = 2; % deg, crossing angle

%% Define beam parameters
Pref = 50; % MeV/c
Pspread = 3; % permil, momentum spread
sigma_z = 5 * RF_Track.ps; % mm/c
Q_pC = 1000; % pC, bunch charge

%% Define Twiss parameters
Twiss = Bunch6d_twiss();
Twiss.emitt_x = 5; % mm.mrad, normalized emittances
Twiss.emitt_y = 5; % mm.mrad
Twiss.emitt_z = (sigma_z) * (Pspread) * (Pref/RF_Track.electronmass); % mm.mrad
Twiss.alpha_x = IP_Lstar / IP_beta; % converging beam
Twiss.alpha_y = IP_Lstar / IP_beta; %
Twiss.beta_x = IP_beta + IP_Lstar**2 / IP_beta; % m
Twiss.beta_y = IP_beta + IP_Lstar**2 / IP_beta; % m
Twiss.beta_z = (sigma_z) / (Pspread); % m

%% Create the bunch
B0 = Bunch6d(RF_Track.electronmass, Q_pC*RF_Track.pC, -1, Pref, Twiss, 10000);
E0 = B0.get_phase_space();

%% Define laser-beam IP region
FP = LaserBeam(); % Fabry-Pérot resonator
FP.pulse_energy = 28; % mJ, laser pulse energy
FP.pulse_length = 5; % ps, laser pulse length
FP.wavelength = 1030; % nm, laser wavelength
FP.set_direction(sind(180-X_angle), 0, cosd(180-X_angle)); % 2 degrees crossing angle
FP.length = L_FP;
FP.set_position(IP_Lstar); % m
FP.R = 0.035; % mm, laser beam radius at waist, Gaussian profile
FP.M2 = 1; %
FP.min_number_of_gammas_per_slice = 10;
FP.set_nsteps(20);

%% Define lattice
L = Lattice();
L.append(FP);

%% Tracking
tic
B1 = L.track(B0);
toc

%% Post-processing
M1 = B1.get_phase_space('%x %xp %y %yp %t %Pc %m %N');
M1_w = M1(:,7)==0.0; % pick the photons
E1 = M1(~M1_w,:); % electrons
P1 = M1( M1_w,:); % gammas

% back track photons to the IP
L_ = L_FP-IP_Lstar; % m
P1(:,1) -= L_ .* P1(:,2);
P1(:,3) -= L_ .* P1(:,4);
P1(:,5) -= L_ * 1e3;

%% Total number of gammas produced
Nw_RFT = sum(P1(:,8))

%% Analytic estimate based on the geometric luminosity
N_e = Q_pC * RF_Track.pC; % e
Nw = FP.pulse_energy * FP.wavelength * 5034116567542.7; % mJ / (h c / nm) = 5034116567542.7
sigma_x = sqrt(IP_beta * Twiss.emitt_x * RF_Track.electronmass / Pref); % mm, electron beam at IP
sigma_y = sqrt(IP_beta * Twiss.emitt_y * RF_Track.electronmass / Pref); % mm, electron beam at IP
sigma_Z = hypot(FP.pulse_length * RF_Track.ps, sigma_z) * 1e3; % um
sigma_X = hypot(hypot(FP.R*1e3, sigma_x*1e3)*cosd(X_angle/2), sigma_Z*sind(X_angle/2)); % um
sigma_Y = hypot(FP.R*1e3, sigma_y*1e3); % um
cs_T = 66; % fm^2, Thomson cross section

Nw_geom = cosd(X_angle/2) * N_e * Nw / (2*pi * (sigma_X*1e9) * (sigma_Y*1e9)) * cs_T

printf('Nw_RFT / Nw_geom = %g\n', Nw_RFT / Nw_geom);

%% Plots
angle = atan(hypot(P1(:,2), P1(:,4)) / 1e3) * 1e3; % mrad
figure(1)
clf
subplot(2,2,1);
[H,X] = whist(P1(:,6) * 1e3, P1(:,8), 100);
X0000 = X;
H /= X(2)-X(1); 
stairs(X,H/1e6, 'linewidth', 1)
hold on
[H,X] = whist(P1(angle<5,6) * 1e3, P1(angle<5,8), X);
H /= X(2)-X(1); 
stairs(X,H/1e6, 'linewidth', 1)
[H,X] = whist(P1(angle<1,6) * 1e3, P1(angle<1,8), X);
H /= X(2)-X(1); 
stairs(X,H/1e6, 'linewidth', 1)
xlabel('E'' [keV]');
ylabel('dN / dE'' [10^6 ph / (keV bx)]');
h = legend('full angle', '5 mrad', '1 mrad');
legend (h, 'location', 'northwest');
legend boxoff
set(gca, 'fontsize', 6)

% cut large-angle photons
angle = hypot(P1(:,2), P1(:,4));
P1(angle>30,:) = [];
angle(angle>30) = [];

%%%
subplot(2,2,2)
[H,Xa,Ya] = whist2d([ angle P1(:,6)*1e3 ], P1(:,8), 64, 64, Nw_RFT / 1e6);
H /= (Xa(2) - Xa(1)) * (Ya(2) - Ya(1));
h = pcolor(Xa,Ya,H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
xlabel('\theta [mrad]');
ylabel('E [keV]');
h = colorbar;
set(h, 'fontsize', 6);
set(get(h,'label'),'string','10^6 ph / (mrad keV bx)','fontsize', 6);
set(gca, 'fontsize', 6);


%%%
subplot(2,2,3)
[H,Xa,Ya] = whist2d(P1(:,[ 1 2 ]), P1(:,8), 64, 64, Nw_RFT / 1e6);
H /= (Xa(2) - Xa(1)) * (Ya(2) - Ya(1));
h = pcolor(Xa,Ya,H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
xlabel('X [mm]');
ylabel('X'' [mrad]');
h = colorbar;
set(h, 'fontsize', 6);
set(get(h,'label'),'string','10^6 ph / (mm mrad bx)','fontsize', 6);
set(gca, 'fontsize', 6);


subplot(2,2,4)
[H,Xa,Ya] = whist2d(P1(:,[ 2 4 ]), P1(:,8), 64, 64, Nw_RFT / 1e6);
H /= (Xa(2) - Xa(1)) * (Ya(2) - Ya(1));
h = pcolor(Xa,Ya,H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
xlabel('X'' [mrad]');
ylabel('Y'' [mrad]');
h = colorbar;
set(h, 'fontsize', 6);
set(get(h, 'label'), 'string','10^6 ph / (mrad^2 bx)','fontsize', 6);
set(gca, 'fontsize', 6);

print -dpng -r200 plot_Xrays.png

figure(2)
clf
subplot(2,2,1)
[H,Xa,Ya] = whist2d([ P1(:,5)/RF_Track.ps P1(:,1) ], P1(:,8), 64, 64, Nw_RFT / 1e6);
H /= (Xa(2) - Xa(1)) * (Ya(2) - Ya(1));
h = pcolor(Xa,Ya,H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
xlabel('t [ps]');
ylabel('X [mm]');
h = colorbar;
set(h, 'fontsize', 6);
set(get(h,'label'),'string','10^6 ph / (ps mm bx)','fontsize', 6);
set(gca, 'fontsize', 6);

subplot(2,2,2)
[H,Xa,Ya] = whist2d([ P1(:,5)/RF_Track.ps P1(:,2) ], P1(:,8), 64, 64, Nw_RFT / 1e6);
H /= (Xa(2) - Xa(1)) * (Ya(2) - Ya(1));
h = pcolor(Xa,Ya,H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
xlabel('t [ps]');
ylabel('X'' [mrad]');
h = colorbar;
set(h, 'fontsize', 6);
set(get(h,'label'),'string','10^6 ph / (ps mrad bx)','fontsize', 6);
set(gca, 'fontsize', 6);

subplot(2,2,3)
[H,Xa,Ya] = whist2d([ P1(:,5)/RF_Track.ps P1(:,6)*1e3 ], P1(:,8), 64, 64, Nw_RFT / 1e6);
H /= (Xa(2) - Xa(1)) * (Ya(2) - Ya(1));
h = pcolor(Xa,Ya,H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
xlabel('t [ps]');
ylabel('E [keV]');
h = colorbar;
set(h, 'fontsize', 6);
set(get(h,'label'),'string','10^6 ph / (ps keV bx)','fontsize', 6);
set(gca, 'fontsize', 6);

subplot(2,2,4)
[H,Xa,Ya] = whist2d([ P1(:,1) P1(:,6)*1e3 ], P1(:,8), 64, 64, Nw_RFT / 1e6);
H /= (Xa(2) - Xa(1)) * (Ya(2) - Ya(1));
h = pcolor(Xa,Ya,H);
set(h, 'EdgeColor', 'none');
axis([ Xa(1) Xa(end) Ya(1) Ya(end) ]);
xlabel('X [mm]');
ylabel('E [keV]');
h = colorbar;
set(h, 'fontsize', 6);
set(get(h,'label'),'string','10^6 ph / (mm keV bx)','fontsize', 6);
set(gca, 'fontsize', 6);

print -dpng -r200 plot_Xrays_t.png
