#include <iostream>
#include <string>
#include <cmath>
#include <gsl/gsl_sf_ellint.h>

int main(int argc, char *argv[] )
{
  if (argc!=4) {
    std::cerr << "usage: ./coil L B0 R\n\n"
      " L: element length [m]\n"
      " B0: magnetic field at the center of the coil [T]\n"
      " R: coil radius [m]\n\n";
    return 1;
  }
  const double length = std::stod(argv[1]); // m
  const double B0 = std::stod(argv[2]); // T
  const double R = std::stod(argv[3]); // m
  double x, y, z, t; // mm
  while (std::cin >> x >> y >> z >> t) {
    double Ex = 0.0, Ey = 0.0, Ez = 0.0;
    double Bx = 0.0, By = 0.0, Bz = 0.0;
    if (R==0.0) {
      std::cout << "0 0 0 0 0 0\n";
    } else {
      const double r = hypot(x,y); // mm
      const double alpha = r/(R*1e3); // #
      // See also CLIC note 465 https://cds.cern.ch/record/492189?ln=en
      // Bz(z) := B0*R^3/(z^2+R^2)^(3/2)
      // Br(r,z):=-r/2*('diff(B(z),z)-r**2/8*'diff(B(z),z,3)+r**4/192*'diff(B(z),z,5));
      // Bz(r,z):=B(z)-r^2/4*('diff(B(z),z,2)-r^2/16*('diff(B(z),z,4))+r^4/576*('diff(B(z),z,6)))
      // and https://tiggerntatie.github.io/emagnet/offaxis/iloopoffaxis.htm
      auto B = [&] (double z_ /* mm */ ) {
	const double beta = z_/(R*1e3); // #
	const double gamma = z_/r; // #
	const double sqrt_Q = hypot(1+alpha, beta); // #
	const double Q = (1+alpha)*(1+alpha) + beta*beta; // #
	const double k = 2*sqrt(alpha) / sqrt_Q;
	const double K = gsl_sf_ellint_Kcomp(k, GSL_PREC_DOUBLE);
	const double E = gsl_sf_ellint_Ecomp(k, GSL_PREC_DOUBLE);
	const double B0_ = B0/M_PI/sqrt_Q; // T
	const double Bz = B0_*      (E * (1-alpha*alpha-beta*beta) / (Q-4*alpha) + K); // T
	const double Br = B0_*gamma*(E * (1+alpha*alpha+beta*beta) / (Q-4*alpha) - K); // T
	return std::pair<double,double>(Br, Bz);
      };
      const auto B_ = B(z - 0.5*length*1e3);
      double Br = B_.first; // T
      double Bz = B_.second; // T
      double Bx, By;
      if (r==0.0) {
	Bx = By = 0.0;
      } else {
	Bx = Br*x/r;
	By = Br*y/r;
      }
      std::cout << "0 0 0 " << Bx << ' ' << By << ' ' << Bz << std::endl;
    }
  }
  std::cerr << "exiting coil\n";
  return 0;
}
