%% prepare for test
system('make coil');

RF_Track;

%% Two coils
Cm = ExternalField(0, "./coil 0 -1 0.2");
Cp = ExternalField(0, "./coil 0 +1 0.2");

%% Volume
V = Volume();

% set boundaries
V.set_s0(-1.0);
V.set_s1(+1.0);

% add the two coils
V.add(Cm, 0, 0, -0.5);
V.add(Cp, 0, 0,  0.5);

%% plot Bz
S_axis = linspace(V.get_s0(), V.get_s1(), 100); % m
Bz = [];
for S = S_axis
    [E,B] = V.get_field(0, 0, S * 1e3, 0);
    Bz = [ Bz ; B(3) ];
end

plot(S_axis, Bz);
xlabel('S [m]');
ylabel('B_z [T]');
