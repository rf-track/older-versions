addpath('../..');

RF_Track;

%for odeint_algorithm = { "leapfrog" , "analytic" , "rk2" , "rkf45", "rk4imp" } 
for odeint_algorithm = { "rk2" , "rkf45", "rk4imp" } 

    C = TestCoils(0.4, ... % m, length
                  2.98749651718870, ... % T, on-axis field
                  0.1); % m, radius
    
    C.set_nsteps(10);
    C.set_odeint_algorithm(odeint_algorithm{1});
    
    L = Lattice();
    L.append(C);
    L.append(C);
    L.append(C);
    L.append(C);
    
    %% beam
    I = imread('CERN_logo.png');
    [r,c] = find(I(:,:,1)>0);
    O_ = zeros(size(r,1),1);
    Pz_ = 64 * ones(size(r,1),1);

    B0 = Bunch6d(RF_Track.electronmass, 1, -1, [ (c-64)/64 O_ (64-r)/64 O_ O_ Pz_ ]);
    M0 = B0.get_phase_space();

    tic
    for i=1:100
        B0 = L.track(B0);
    end
    toc

    M = B0.get_phase_space();
    save(sprintf("%s.dat", odeint_algorithm{1}), '-text', 'M');
end

figure(1)
clf
hold on
M = B0.get_phase_space();
scatter(M0(:,1), M0(:,3),'r');
scatter(M(:,1), M(:,3),'k');

figure(2)
clf
hold on
M = B0.get_phase_space();
scatter(M0(:,2), M0(:,4),'r');
scatter(M(:,2), M(:,4),'k');

figure(3)
clear Bz
for i=1:int32(C.get_length()*1e3); [E,B] = C.get_field(0,0,i,0); Bz(i) = B(3); end;
plot(Bz);

figure(4)
clear Br
for i=1:int32(100); [E,B] = C.get_field(0,i,100,0); Br(i) = B(2); end;
plot(Br);