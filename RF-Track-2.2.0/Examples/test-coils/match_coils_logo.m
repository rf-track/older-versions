RF_Track;

function [M, B1] = merit(Bz)
    RF_Track;
    %% beam
    %I = imread('CERN_logo.png');
    %[r,c] = find(I(:,:,1)>0);

    I = imread('cat.png');
    [r,c] = find(I(:,:,1)<250);

    I_ = ones(size(r,1),1);
    O_ = zeros(size(r,1),1);
    Pz_ = 64 * I_;

    %% tracking
    T = TrackingOptions();

    T.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45', 'rk8pd'
    T.odeint_epsabs = 1e-13;
    T.odeint_epsrel = 1e-13;

    T.open_boundaries = false;

    T.dt_mm = 10; % mm/c
    
    V = init_volume(Bz);
    
    %% beam
    step = 0.2; % m
    B0 = Bunch6dT(RF_Track.electronmass, 1, -1, [ (c-64)/6400 O_ (64-r)/6400 O_ (-step*1e3*I_) Pz_ ]);
    M0 = B0.get_phase_space("%X %Px %Y %Py %S %Pz");

    for n=1:4
        V.track(B0, T);
        B1 = V.get_bunch_at_s1();
        M1 = B1.get_phase_space("%X %Px %Y %Py %S %Pz");
        M1(:,5) = -step*1e3*I_; % mm
        B0 = Bunch6dT(RF_Track.electronmass, 1, -1, M1);
    end		

    M = norm(M1(:,1:4) - M0(:,1:4))
    
    figure(1)
    clf; hold on
    scatter(M0(:,1), M0(:,3), 'r')
    scatter(M1(:,1), M1(:,3))
    xlabel('x [mm]');
    ylabel('y [mm]');
    drawnow

    figure(2)
    clf; hold on
    scatter(M0(:,2), M0(:,4), 'r')
    scatter(M1(:,2), M1(:,4))
    xlabel("x' [mrad]");
    ylabel("y' [mrad]");
    drawnow
    
endfunction

%{
T = [];
for Bz = linspace(0, 3.1, 30)
    [M,B1] = merit(Bz);
    T = [ T ; Bz M ];
    figure(4)
    plot(T(:,1), T(:,2));
    drawnow
end
figure(3)
plot(T(:,1), T(:,2));
%}

format long
O = optimset('TolFun', 1e-16, 'TolX', 1e-8);
Bz = fminbnd(@merit, 2.8, 3.2, O)