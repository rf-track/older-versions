%% load RF-Track
RF_Track;

%% Define reference particle, and rigidity
Part.mass = RF_Track.electronmass; % MeV/c^2
Part.P = 100; % MeV/c
Part.Q = -1; % e

%% Bunch
N = 10000;
X = randn(N,1);
Y = randn(N,1);
O = zeros(N,1);
I = ones (N,1);
B0 = Bunch6dT(Part.mass, 0.0, Part.Q, [ X O Y O O I*Part.P ]);

%% Volume
A = Absorber(1.0, 'air');
A.disable_log_term();

V = Volume();
V.add(A, 0, 0, 0, 'entrance');

figure(1)
clf ; hold on
for nsteps = [ 10 50 100 500 1000 ]

    O = TrackingOptions();
    O.dt_mm = 10; % mm/c
    O.tt_dt_mm = V.get_length() * 1e3 / nsteps; % mm/c
    O.cfx_dt_mm = V.get_length() * 1e3 / nsteps; % mm/c

    tic
    B1 = V.track(B0, O);
    T = V.get_transport_table('%mean_S %sigma_X %sigma_Y');
    toc

    plot(T(:,1)/1e3, T(:,2), '*-', 'displayname', sprintf('nsteps = %d', nsteps));
    axis([ 0 1 0 5 ])
    xlabel('S [m]');
    ylabel('\sigma_X [mm]');
    title('Volume', 'fontsize', 12);
    legend('location', 'northwest');
    grid on
    drawnow
end
pause