#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../../');

%% Load RF_Track
RF_Track;

%% define bunch
Qb = 1e12;
Ne = 50000;
Lb = 6; % bunch length [mm]

randn("seed", 1234);
rand ("seed", 5678);

Pz = 1; % MeV/c

B_pos = [ 0.1 * randn(Ne,1), ... % x
	  0.0 * randn(Ne,1), ... % Px
	  0.1 * randn(Ne,1), ... % y
	  0.0 * randn(Ne,1), ... % Py
	  (rand(Ne,1)-0.5)*Lb, ... % S
	  Pz * ones(Ne,1),  ... % Pz
	  RF_Track.electronmass * ones(Ne,1), ... % mass
	  -1 * ones(Ne,1), ...   % Q
	  Qb / Ne * ones(Ne,1) ]; % N

B_pos(1, [ 1 3 5 9 ] ) = [ -5  3 0 0 ];
B_pos(2, [ 1 3 5 9 ] ) = [  5  3 0 0 ];
B_pos(3, [ 1 3 5 9 ] ) = [  5 -3 0 0 ];
B_pos(4, [ 1 3 5 9 ] ) = [ -5 -3 0 0 ];

% example 1
SC = SpaceCharge_PIC_FreeSpace(32,32,32);

% example 2
SC = SpaceCharge_PIC_LongCylinder(32,32,32);
SC.set_aperture(10e-3); % 10 mm

% example 3
SC = SpaceCharge_PIC_HorizontalPlates(32,32,32);
SC.set_half_gap(3e-3); % 10 mm

B0 = Bunch6dT(B_pos);
B0.set_coasting(Lb);

time_per_frame = 0.0;
dt = 0.15;
SC.compute_force(B0);

[Ef,Bf] = SC.get_fields();
L = Ef.get_length()/2;

Rx = -5:0.5:5;
Ry = -3:0.5:3;
[X,Y] = meshgrid(Rx,Ry);

Ex = Ey = zeros(size(X));
Bx = By = zeros(size(X));

for i=1:length(Ry)
  for j=1:length(Rx)
    [E,B] = Bf.get_field(X(i,j), Y(i,j), L/2, 0);
    Bx(i,j) = B(1);
    By(i,j) = B(2);
    [E,B] = Ef.get_field(X(i,j), Y(i,j), L/2, 0);
    Ex(i,j) = E(1);
    Ey(i,j) = E(2);
  end
end


En = hypot(Ex,Ey);
Ex ./= En;
Ey ./= En;


Bn = hypot(Bx,By);
Bx ./= Bn;
By ./= Bn;



figure(1)
quiver(Rx,Ry,Bx,By)
axis([ min(Rx) max(Rx) min(Ry)-0.1 max(Ry)+0.1 ])
title('Magnetic field')
xlabel('x [mm]')
ylabel('y [mm]')
print -dpng plot_Bfield.png

figure(2)
quiver(Rx,Ry,Ex,Ey)
axis([ min(Rx) max(Rx) min(Ry)-0.1 max(Ry)+0.1 ])
title('Electric field')
xlabel('x [mm]')
ylabel('y [mm]')
print -dpng plot_Efield.png

