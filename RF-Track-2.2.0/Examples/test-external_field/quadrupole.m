G = 1.2; % T/m, gradient
while length(I = fscanf(stdin, '%lf %lf %lf %lf', 4))
    Bx = G * I(2) / 1000; %  convert Y from mm to m
    By = G * I(1) / 1000; %  convert X from mm to m
    fprintf(stdout, ' %.17g', [ 0 0 0 Bx By 0 ]);
    fprintf(stdout, '\n');
end