/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <sstream>
#include <cstdlib>
#include <cstdio>
#include <thread>
#include <fftw3.h>
#include <gsl/gsl_version.h>

#include "RF_Track.hh"
#include "for_all.hh"
#include "bunch6d.hh"
#include "constants.hh"
#include "lorentz_boost.hh"
#include "space_charge_pic.hh"
#include "relativistic_velocity_addition.hh"

namespace RFT {
  const std::string version = RF_TRACK_VERSION;
  const size_t max_number_of_threads = std::thread::hardware_concurrency();
  size_t number_of_threads = std::thread::hardware_concurrency();
  SpaceCharge *SC_engine;
  const double clight = C_LIGHT; // m/s
  const double muonmass = 105.6583745; // MeV/c/c
  const double protonmass = PMASS; // MeV/c/c
  const double electronmass = EMASS; // MeV/c/c
  const double s = 299792458000; // mm/c
  const double ms = 299792458; // mm/c
  const double us = 299792.458; // mm/c
  const double ns = 299.792458; // mm/c
  const double ps = 0.299792458; // mm/c
  const double fs = 0.000299792458; // mm/c
  const double pC = 6241509.074460763136; // e
  const double nC = 6241509074.460763136; // e
  const double uC = 6241509074460.763136; // e
  const double mC = 6241509074460763.136; // e
  const double C = 6241509074460763136; // e
  StaticVector<3> relativistic_velocity_addition(const StaticVector<3> &u, const StaticVector<3> &v ) { return ::relativistic_velocity_addition(u, v); }
  FourVector lorentz_boost(const StaticVector<3> &v, const FourVector &x ) { return ::lorentz_boost(v, x ); }
  StaticVector<4> lorentz_boost(const StaticVector<3> &v, const StaticVector<4> &x ) { return ::lorentz_boost(v, x ); }
  StaticMatrix<4,4> lorentz_boost_matrix(const StaticVector<3> &v ) { return ::lorentz_boost_matrix(v); }
  gsl_rng *rng = nullptr;
  void rng_set(const char *name )
  {
    const gsl_rng_type *T = nullptr;
    if (!strcmp(name,"taus2")) T = gsl_rng_taus2;
    else if(!strcmp(name,"mt19937")) T = gsl_rng_mt19937;
    else if(!strcmp(name,"gfsr4")) T = gsl_rng_gfsr4;
    else if(!strcmp(name,"ranlxs0")) T = gsl_rng_ranlxs0;
    else if(!strcmp(name,"ranlxs1")) T = gsl_rng_ranlxs1;
    else if(!strcmp(name,"ranlxs2")) T = gsl_rng_ranlxs2;
    else if(!strcmp(name,"mrg")) T = gsl_rng_mrg;
    else if(!strcmp(name,"ranlux")) T = gsl_rng_ranlux;
    else if(!strcmp(name,"ranlux389")) T = gsl_rng_ranlux389;
    else if(!strcmp(name,"ranlxd1")) T = gsl_rng_ranlxd1;
    else if(!strcmp(name,"ranlxd2")) T = gsl_rng_ranlxd2;  
    if (T) {
      if (RFT::rng)
	gsl_rng_free (RFT::rng);
      RFT::rng = gsl_rng_alloc(T);
    } else {
      std::cerr << "error: unknown random number generator '" << name << "'\n";
    }
  }
  void rng_set_seed(unsigned long int s )
  {
    if (RFT::rng)
      gsl_rng_set(RFT::rng, s);
  }
  const char *rng_get() { return gsl_rng_name(rng); }

}

static void finalize_rf_track()
{
  if (RFT::rng)
    gsl_rng_free (RFT::rng);
  fftw_cleanup_threads();
}

void init_rf_track()
{
  // splash message
  std::ostringstream str;
  str << "\nRF-Track, version " << RFT::version << '\n'
      << "\nCopyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly\n"
      << "granted are reserved. See the COPYRIGHT file at the top-level directory\n"
      << "of the source code distribution.\n\n"
      << "Author and contact:\n"
      << " Andrea Latina <andrea.latina@cern.ch>\n"
      << " BE-ABP Group\n"
      << " CERN\n"
      << " CH-1211 GENEVA 23\n"
      << " SWITZERLAND\n\n"
      << "RF-Track was compiled with GSL-" GSL_VERSION " and " << fftw_version << "\n\n"
      << "This is free software; see the source code for copying conditions.\n"
      << "There is ABSOLUTELY NO WARRANTY; not even for MERCHANTABILITY or\n"
      << "FITNESS FOR A PARTICULAR PURPOSE.\n";
  puts(str.str().c_str());
  // init random number generator
  gsl_rng_env_setup();
  RFT::rng = gsl_rng_alloc (gsl_rng_default);
  // init fft
  fftw_init_threads();
  RFT::SC_engine = new SpaceCharge_PIC_FreeSpace(32,32,32);
  atexit(finalize_rf_track);
  // disable GSL error handler
  gsl_set_error_handler_off();
}
