addpath('../..')

%% Load RF-Track
RF_Track;

%% Bunch parameters
mass = RF_Track.electronmass; % MeV/c^2
charge = -1; % single-particle charge, in units of e
population = 1e10; % number of real particles per bunch
Pc = 5; % reference momentum, MeV/c
B_rho = Pc / charge; % MV/c, reference rigidity

%% FODO cell paramters
Lcell = 2; % m
Lquad = 0; % m
Ldrift = Lcell/2 - Lquad; % m
mu = 90; % deg
k1L = sind(mu/2) / (Lcell/4); % 1/m
strength = k1L * B_rho; % MeV/m

% Setup the elements
Qf = Quadrupole(Lquad/2, strength/2);
QD = Quadrupole(Lquad, -strength);
Dr = Drift(Ldrift);
Dr.set_tt_nsteps(100);
Dr.set_odeint_algorithm('rk2');

% Setup the lattice
FODO = Lattice();
FODO.append(Qf);
FODO.append(Dr);
FODO.append(QD);
FODO.append(Dr);
FODO.append(Qf);

%% Define Twiss parameters
Twiss = Bunch6d_twiss();
Twiss.emitt_x = 0.001; % mm.mrad, normalized emittances
Twiss.emitt_y = 0.001; % mm.mrad
Twiss.alpha_x = 0.0;
Twiss.alpha_y = 0.0;
Twiss.beta_x = Lcell * (1 + sind(mu/2)) / sind(mu); % m
Twiss.beta_y = Lcell * (1 - sind(mu/2)) / sind(mu); % m

%% Create the bunch
B0 = Bunch6d(mass, population, charge, Pc, Twiss, 10000);

%% Perform tracking
B1 = FODO.track(B0);

%% Retrieve the Twiss plot and the phase space
T = FODO.get_transport_table('%S %beta_x %beta_y');
M = B1.get_phase_space('%x %xp %y %yp');

%% Make plots
figure(1);
clf
hold on
plot(T(:,1), T(:,2), 'b-', 'linewidth', 2);
plot(T(:,1), T(:,3), 'r-', 'linewidth', 2);
lgnd = legend({ '\beta_x ', '\beta_y ' });
xlabel('S [m]');
ylabel('\beta [m]');
legend boxoff
legend boxon
set(lgnd, 'fontsize', 14);
axis([ 0 2 ]);
set(gca, 'linewidth', 2, 'fontsize', 14);
box
print -dpng plot_beta.png

figure(2)
clf
hold on
scatter(M(:,1), M(:,2), '*');
xlabel('x [mm]');
ylabel('x'' [mrad]');
set(gca, 'linewidth', 2, 'fontsize', 14);
box
print -dpng plot_xxp.png
