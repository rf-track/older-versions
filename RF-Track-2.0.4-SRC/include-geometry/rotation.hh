/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef rotation_hh
#define rotation_hh

#include <iostream>
#include <iomanip>

#include "axis.hh"
#include "quaternion.hh"
#include "fftw_complex.hh"
#include "static_matrix.hh"

class Rotation : public Quaternion {
  enum TAIT_BRYAN {
    PHI   = 0, // phi, or alpha, or yaw angle, rotation angle around Z-axis [0..2pi]
    ALPHA = 0, // 
    YAW   = 0, // 
    THETA = 1, // theta, or beta, or pitch, or elevation angle, rotation angle around Y-axis [0..pi]
    BETA  = 1, // 
    PITCH = 1, // 
    PSI   = 2, // psi, or gamma, or roll, or azimuthal angle, rotation angle around X-axis [0..2pi]
    GAMMA = 2, // 
    ROLL  = 2  // 
  };
public:
  explicit Rotation(const Quaternion &q ) : Quaternion(q) {}
  explicit Rotation(const Vector3d &angle ) : Quaternion(exp(Quaternion(0.0, angle/2))) {}
  Rotation(double angle, const Axis &axis ) : Quaternion(cos(angle/2), sin(angle/2) * axis) {}
  Rotation(double yaw /* Z */, double pitch /* Y */, double roll /* X */ ) : Quaternion(Rotation(yaw, Axis_Z) * Rotation(pitch, Axis_Y) * Rotation(roll, Axis_X)) {}
  Rotation() : Quaternion(1.0, 0.0, 0.0, 0.0) {}
  Rotation(const Axis &s_, const Axis &t_ ) // rotates s into t
  {
    const Vector3d u = cross(s_, t_);
    const double sin2phi = norm(u);
    if (sin2phi != 0.0) {
      double cos2phi = dot(s_, t_);
      // calculates sinphi and cosphi using half-angle relations 
      double sinphi = sqrt((1. - cos2phi) / 2.);
      double cosphi = sqrt((1. + cos2phi) / 2.);
      s = cosphi;
      v = sinphi * (u / sin2phi);
    } else {
      s = 1.;
      v = Vector3d(0., 0., 0.);
    }
  }
  ~Rotation() = default;
  
  Vector3d operator * (const Vector3d &v ) const { const Quaternion &q = *this; return (q * v * conj(q)).v; }
  friend Vector3d operator / (const Vector3d &a, const Rotation &b ) { return inverse(b) * a; }
  
  StaticVector<3,fftwComplex> operator * (const StaticVector<3,fftwComplex> &v ) const
  {
    const Quaternion &q = *this;
    Vector3d
      v_real(v[0].real, v[1].real, v[2].real),
      v_imag(v[0].imag, v[1].imag, v[2].imag);    
    v_real = (q * v_real * conj(q)).v;
    v_imag = (q * v_imag * conj(q)).v;
    return StaticVector<3,fftwComplex>(fftwComplex(v_real[0], v_imag[0]),
				       fftwComplex(v_real[1], v_imag[1]),
				       fftwComplex(v_real[2], v_imag[2]));
  }
  Rotation operator * (const Rotation &r ) const { const Quaternion &q1 = *this, q2 = r; return Rotation(q1*q2); }
  StaticMatrix<3,3> get_rotation_matrix() const
  {
    return StaticMatrix<3,3>(1 - 2 * (v[1] * v[1] + v[2] * v[2]),   2 * (v[0] * v[1] - s * v[2]),	   2 * (v[0] * v[2] + s * v[1]),
			     2 * (v[0] * v[1] + s * v[2]), 	1 - 2 * (v[0] * v[0] + v[2] * v[2]),       2 * (v[1] * v[2] - s * v[0]),
			     2 * (v[0] * v[2] - s * v[1]),	    2 * (v[1] * v[2] + s * v[0]),      1 - 2 * (v[0] * v[0] + v[1] * v[1]));
  }

  friend Rotation inverse(const Rotation &a ) { return Rotation(conj(a)); }

  friend std::ostream &operator << (std::ostream &stream, const Rotation &r )
  {
    return stream << std::fixed
		  << "X = " << r * Axis_X << std::endl
		  << "Y = " << r * Axis_Y << std::endl
		  << "Z = " << r * Axis_Z << std::endl;
  }


};

#endif /* rotation_hh */
