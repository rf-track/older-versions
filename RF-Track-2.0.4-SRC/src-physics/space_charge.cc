/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <vector>
#include "RF_Track.hh"
#include "numtools.hh"
#include "for_all.hh"
#include "relativistic_velocity_addition.hh"

StaticVector<3> SpaceCharge::change_reference_frame(const Bunch6d &bunch, const ParticleSelector &selector )
{
  const size_t Nparticles = bunch.size();
 
  // manage pre-allocated memory
  position.resize(Nparticles);
  velocity.resize(Nparticles);
 
  // change the reference frame
  const auto v_ref = bunch.get_average_Vx_Vy_Vz();
  const auto inv_gamma_ref = sqrt(1.0 - dot(v_ref, v_ref));
  const auto gamma_ref = 1.0 / inv_gamma_ref;
  const auto n = normalize(v_ref);
  const double t0 = bunch.get_t_mean(); // mm/c, time such that <Z> = 0
  auto change_reference_frame_parallel = [&] (size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      const auto &p = bunch[i];
      if (selector(p)) {
        auto v = p.get_Vx_Vy_Vz(); // velocity in the lab's frame
        auto r = StaticVector<3>(p.x, p.y, 0.0) + (t0 - p.t) * v;
	if (bunch.coasting()) {
	  r[2] = fmod(r[2] + bunch.coasting()/2, bunch.coasting());
	  if (r[2]<0.0)
	    r[2] += bunch.coasting();
	  r[2] -= bunch.coasting()/2;
	}
	position[i] = r + (gamma_ref - 1.0) * dot(r, n) * n;
	velocity[i] = relativistic_velocity_addition(-v_ref, v);
      }
    }
  };
  const size_t Nthreads = RFT::number_of_threads;
  for_all(Nthreads, Nparticles, change_reference_frame_parallel);

  return v_ref;
}

StaticVector<3> SpaceCharge::change_reference_frame(const Bunch6dT &bunch, const ParticleSelector &selector )
{
  const size_t Nparticles = bunch.size();
  
  // manage pre-allocated memory
  position.resize(Nparticles);
  velocity.resize(Nparticles);
 
  // change the reference frame
  const auto v_ref = bunch.get_average_Vx_Vy_Vz();
  const auto inv_gamma_ref = sqrt(1.0 - dot(v_ref, v_ref));
  const auto gamma_ref = 1.0 / inv_gamma_ref;
  const auto n = normalize(v_ref);
  auto change_reference_frame_parallel = [&] (size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      const auto &p = bunch[i];
      if (selector(p)) {
        auto v = p.get_Vx_Vy_Vz(); // velocity in the lab's frame
        auto r = StaticVector<3>(p.X, p.Y, p.S); // position in the lab's frame
	if (bunch.coasting()) {
	  r[2] = fmod(r[2] + bunch.coasting()/2, bunch.coasting());
	  if (r[2]<0.0)
	    r[2] += bunch.coasting();
	  r[2] -= bunch.coasting()/2;
	}
	position[i] = r + (gamma_ref - 1.0) * dot(r, n) * n;
	velocity[i] = relativistic_velocity_addition(-v_ref, v);
      }
    }
  };
  const size_t Nthreads = RFT::number_of_threads;
  for_all(Nthreads, Nparticles, change_reference_frame_parallel);
 
  return v_ref;
}
