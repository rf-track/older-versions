/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "RF_Track.hh"

#include "relativistic_velocity_addition.hh"
#include "lorentz_boost.hh"
#include "numtools.hh"
#include "space_charge_p2p.hh"

template <typename BUNCH_TYPE>
void SpaceCharge_P2P::compute_force_(MatrixNd &force, const BUNCH_TYPE &bunch, const ParticleSelector &selector )
{
  const size_t Nthreads = RFT::number_of_threads;
  const size_t Nparticles = bunch.size();

  // compute coordinates in bunch's reference frame
  const auto v_ref = change_reference_frame(bunch, selector);
  const double inv_gamma_ref = sqrt(1.0 - dot(v_ref, v_ref));
  const double gamma_ref = 1.0 / inv_gamma_ref;
  const auto n = v_ref / norm(v_ref);

  // compute forces
  auto compute_p2p_force_parallel = [&](size_t thread, size_t start, size_t end ) {
    auto &cumulative_F = F_all[thread];
    cumulative_F.resize(Nparticles);
    for (size_t i=0; i<Nparticles; i++) {
      if (bunch[i]) {
	cumulative_F[i] = StaticVector<3>(0.0);
      }
    }
    for (size_t i=0, k=0; i<Nparticles-1; i++) {
      for (size_t j=i+1; j<Nparticles; j++, k++) {
	if (k>=start) {
	  if (k==end) goto end;
	  const auto &P_i = bunch[i];
	  const auto &P_j = bunch[j];
	  if (selector(P_i) && selector(P_j)) {
	    const auto Q_ij = P_i.Q * P_j.Q;
	    if (Q_ij!=0.0) {
	      if (P_i.N!=0.0 || P_j.N!=0.0) {
	        const auto r_ij = position[i] - position[j];
	        // compute the force in the two-particle frame, then go back to lab frame
	        if (dot(r_ij, r_ij) != 0.0) {
		  const auto &v_i = velocity[i];
		  const auto &v_j = velocity[j];
		  const double norm_r_ij = norm(r_ij);
		  const auto F_ij = Q_ij * (r_ij + cross(v_i, cross(v_j, r_ij))) / (norm_r_ij * norm_r_ij * norm_r_ij); // Coulomb force + Biot and Savart law
		  // cumulate forces for particles i and j
		  cumulative_F[i] +=  F_ij * P_j.N;
		  cumulative_F[j] += -F_ij * P_i.N;
		}
	      }
	    }
	  }
	}
      }
    }
  end:;
  };
  F_all.resize(Nthreads);
  const size_t Nindex_pairs = (Nparticles*(Nparticles-1))/2;
  const size_t effective_Nthreads = for_all(Nthreads, Nindex_pairs, compute_p2p_force_parallel);

  // merge the data from different threads and store the result in F_all[0]
  for (size_t thread=1; thread<effective_Nthreads; thread++) {
    const auto &F_thread = F_all[thread];
    for (size_t i=0; i<Nparticles; i++) {
      F_all[0][i] += F_thread[i];
    }
  }

  // convert force into MeV/m
  // You have: e*e / 4 / pi / epsilon0 / mm / mm
  // You want: MeV/m
  // e*e / 4 / pi / epsilon0 / mm / mm = 1.43996448504452e-09 MeV/m
  // e*e / 4 / pi / epsilon0 / mm / mm = (1 / 694461572.063762) MeV/m
  force.resize(Nparticles, 3);
  for (size_t i=0; i<Nparticles; i++) {
    if (selector(bunch[i])) {
      const StaticVector<3> &F = F_all[0][i];
      // bring the force back to the lab frame
      const auto &v = velocity[i];
      const double inv_denom = 1.0 / (1.0 + dot(v_ref, v));
      auto Flab = (v_ref * dot(F, v) * gamma_ref + n * dot(F, n) * (gamma_ref - 1.0) + F) * inv_denom * inv_gamma_ref;
      // convert force into MeV/m
      // e*e / 4 / pi / epsilon0 / mm / mm = 1.43996448504452e-09 MeV/m
      // e*e / 4 / pi / epsilon0 / mm / mm = (1 / 694461572.063762) MeV/m	
      Flab /= 694461572.063762; // MeV/m
      force[i][0] = Flab[0];
      force[i][1] = Flab[1];
      force[i][2] = Flab[2];
    } else {
      force[i][0] = 0.0;
      force[i][1] = 0.0;
      force[i][2] = 0.0;
    }
  }
}

// template specializations

template void SpaceCharge_P2P::compute_force_(MatrixNd &force, const Bunch6d &bunch, SpaceCharge::ParticleSelector const& );
template void SpaceCharge_P2P::compute_force_(MatrixNd &force, const Bunch6dT &bunch, SpaceCharge::ParticleSelector const& );
