/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <functional>
#include <algorithm>
#include <set>

#include "RF_Track.hh"

#include "relativistic_velocity_addition.hh"
#include "lorentz_boost.hh"
#include "numtools.hh"
#include "space_charge_pic.hh"

#include "greens_function.hh"
#include "greens_functions/coulomb_long_cylinder.hh"
#include "greens_functions/coulomb_horizontal_plates.hh"

template <typename GREENS_FUNCTION>
SpaceCharge_PIC<GREENS_FUNCTION>::~SpaceCharge_PIC()
{
  if (p1) fftw_destroy_plan(p1);
  if (p2) fftw_destroy_plan(p2);
  if (p3) fftw_destroy_plan(p3);
}

template <typename GREENS_FUNCTION>
SpaceCharge_PIC<GREENS_FUNCTION> &SpaceCharge_PIC<GREENS_FUNCTION>::operator = (const SpaceCharge_PIC &sc )
{
  if (this != &sc) {
    if (p1) fftw_destroy_plan(p1);
    if (p2) fftw_destroy_plan(p2);
    if (p3) fftw_destroy_plan(p3);
  }
  init_pic(sc.Nx, sc.Ny, sc.Nz);
  return *this;
}

template <typename GREENS_FUNCTION>
bool SpaceCharge_PIC<GREENS_FUNCTION>::init_pic(size_t Nx_, size_t Ny_, size_t Nz_ )
{
  Nx = Nx_ ? Nx_ : 16;
  Ny = Ny_ ? Ny_ : 16;
  Nz = Nz_ ? Nz_ : 16;

  p1 = p2 = p3 = nullptr;

  // for padding
  size_t Nxx = Nx<<1;
  size_t Nyy = Ny<<1;
  size_t Nzz = Nz<<1;

  // preallocate meshes
  mesh_rho = fftwMesh3d(Nxx, Nyy, Nzz); // charge density, and also Jx, Jy, Jz
  mesh_G   = fftwMesh3d(Nxx, Nyy, Nzz); // Green's function

  mesh_rho_hat    = fftwComplexMesh3d(Nxx, Nyy, Nz+1); // charge density, and also Jx, Jy, Jz
  mesh_G_hat      = fftwComplexMesh3d(Nxx, Nyy, Nz+1); // Green's function
  mesh_smooth_hat = fftwComplexMesh3d(Nxx, Nyy, Nz+1); // smoothing function

  // output of the SC caclulation
  mesh_A.resize(Nx, Ny, Nz);

  // initialize FFTW
  const size_t Nthreads = RFT::number_of_threads;
  fftw_plan_with_nthreads(Nthreads);

  // create fftw plans
  if ((p1 = fftw_plan_dft_r2c_3d(Nxx, Nyy, Nzz, mesh_G.data(), (fftw_complex *)mesh_G_hat.data(), FFTW_ESTIMATE))) {
    if ((p2 = fftw_plan_dft_r2c_3d(Nxx, Nyy, Nzz, mesh_rho.data(), (fftw_complex *)mesh_rho_hat.data(), FFTW_ESTIMATE))) {
      if ((p3 = fftw_plan_dft_c2r_3d(Nxx, Nyy, Nzz, (fftw_complex *)mesh_rho_hat.data(), mesh_rho.data(), FFTW_ESTIMATE))) {
	set_smooth(smooth_radius);
	return true;
      } fftw_destroy_plan(p2); p2=nullptr;
    } fftw_destroy_plan(p1); p1=nullptr;
  }
  return false;
}

template <typename GREENS_FUNCTION>
void SpaceCharge_PIC<GREENS_FUNCTION>::set_smooth(const double radius ) // units of mesh cells, default = 3
{
  smooth_radius = radius;
  if (smooth_radius != 0.0) {
    size_t Nxx = Nx<<1;
    size_t Nyy = Ny<<1;
    size_t Nzz = Nz<<1;
    // use mesh_rho to temporarily store the smooth mesh
    mesh_rho = 0.0;
    const auto sqr = [] (double x ) { return x*x; };
    for (size_t i = 0; i<Nx; i++) {
      for (size_t j = 0; j<Ny; j++) {
	for (size_t k = 0; k<Nz; k++) {
	  double weight = exp(-(sqr(double(i)/smooth_radius) + sqr(double(j)/smooth_radius) + sqr(double(k)/smooth_radius)));
	  if (i==0 && j==0 && k==0)
	    mesh_rho.elem(0,0,0) = weight;
	  else {
	    mesh_rho.elem(i,j,k) = weight;
	    if (k!=0) {
	      mesh_rho.elem(i,j,Nzz-k) = weight;
	      if (j!=0) {
		mesh_rho.elem(i,Nyy-j,Nzz-k) = weight;
		if (i!=0) mesh_rho.elem(Nxx-i,Nyy-j,Nzz-k) = weight;
	      }
	      if (i!=0) mesh_rho.elem(Nxx-i,j,Nzz-k) = weight;
	    }
	    if (j!=0) {
	      mesh_rho.elem(i,Nyy-j,k) = weight;
	      if (i!=0) mesh_rho.elem(Nxx-i,Nyy-j,k) = weight;
	    }
	    if (i!=0) mesh_rho.elem(Nxx-i,j,k) = weight;
	  }
	}
      }
    }
    mesh_rho /= mesh_rho.sum();
    fftw_execute(p2); // mesh_rho.data() -> mesh_rho_hat.data()
    mesh_smooth_hat = mesh_rho_hat;
  } else {
    mesh_smooth_hat = 1.0;
  }
}

template <typename GREENS_FUNCTION>
template <typename BUNCH_TYPE>
void SpaceCharge_PIC<GREENS_FUNCTION>::compute_force_(MatrixNd &force, const BUNCH_TYPE &bunch, const ParticleSelector &selector )
{
  const size_t Nparticles = bunch.size();
  force.resize(Nparticles, 3);
  if (Nparticles == 0)
    return;

  if (Nparticles == 1) {
    force = 0;
    return;
  }

  const size_t Nthreads = RFT::number_of_threads;
  const size_t Nxx = Nx<<1;
  const size_t Nyy = Ny<<1;

  // compute coordinates in bunch's reference frame
  const auto v_ref = change_reference_frame(bunch, selector);
  const double inv_gamma_ref = sqrt(1.0 - dot(v_ref, v_ref));
  const double gamma_ref = 1.0 / inv_gamma_ref;
  const auto n = normalize(v_ref);
  
  // count how many good particles are in the bunch
  {
    size_t good_particles = 0;
    for (size_t i=0; i<bunch.size(); i++) {
      const auto &p = bunch[i];
      if (selector(p))
	good_particles++;
    }
    if (good_particles == 0) {
      force = 0;
      return;
    }
  }
  // find bounding box to compute Green's function, excluding some particles charge on each side
  auto min_ =  StaticVector<3>(std::numeric_limits<double>::infinity());
  auto max_ = -StaticVector<3>(std::numeric_limits<double>::infinity());
  for (size_t i=0; i<Nparticles; i++) {
    const auto &particle = bunch[i];
    if (selector(particle)) {
      if (position[i][0] < min_[0]) min_[0] = position[i][0];
      if (position[i][1] < min_[1]) min_[1] = position[i][1];
      if (position[i][2] < min_[2]) min_[2] = position[i][2];
      if (position[i][0] > max_[0]) max_[0] = position[i][0];
      if (position[i][1] > max_[1]) max_[1] = position[i][1];
      if (position[i][2] > max_[2]) max_[2] = position[i][2];
    }
  }
  
  // return if the box has zero size
  if (min_ == max_) {
    force = 0;
    return;
  }

  const double S0 = gsl_isnan(S_mirror) ? GSL_NAN : (S_mirror * gamma_ref); // position of the mirror in the moving frame
  const double S1 = [&] () -> double { // mm, in the moving frame
    if (!gsl_isnan(S0)) {
      const double L = max_[2]-min_[2]; // mm, bunch length, in the moving frame
      if (S0 + L*5 >= min_[2]) {
	min_[2] = 2*S0 - max_[2]; // mm. in the moving frame
	return max_[2]; // mm. in the moving frame
      }
    }
    return GSL_NAN;
  } ();

  // compute Green's function if bounding box has changed
  auto dim = max_ - min_;
  const double dim_min = std::max(dim[0], std::max(dim[1], dim[2])) / 1e3;
  dim[0] = std::max(dim[0], dim_min);
  dim[1] = std::max(dim[1], dim_min);
  dim[2] = std::max(dim[2], dim_min);
  mesh.x0 = min_[0];
  mesh.y0 = min_[1];
  mesh.z0 = min_[2];
  mesh.hx = dim[0] / double(Nx-1);
  mesh.hy = dim[1] / double(Ny-1);
  mesh.hz = dim[2] / double(Nz-1);
  GreensFunction::compute_mesh(greens_function, mesh_G, mesh.hx, mesh.hy, mesh.hz, gamma_ref * bunch.coasting()); // 1/mm
  fftw_execute(p1); // 1/mm

  auto _H = StaticVector<3>(double(Nx-1) / dim[0],
			    double(Ny-1) / dim[1],
			    double(Nz-1) / dim[2]);

  // assign functions (four times to compute rho, Jx, Jy, Jz)
  std::vector<std::function<double(double,int)>> assign_functions(4);
  assign_functions[0] = []  (double charge, int i ) { return charge; };
  assign_functions[1] = [&] (double charge, int i ) { return charge * velocity[i][0]; };
  assign_functions[2] = [&] (double charge, int i ) { return charge * velocity[i][1]; };
  assign_functions[3] = [&] (double charge, int i ) { return charge * velocity[i][2]; };

  std::vector<std::function<double(double,int)>> assign_functions_mirror(4); // mirror charges
  assign_functions_mirror[0] = []  (double charge, int i ) { return -charge; };
  assign_functions_mirror[1] = [&] (double charge, int i ) { return -charge *  velocity[i][0]; };
  assign_functions_mirror[2] = [&] (double charge, int i ) { return -charge *  velocity[i][1]; };
  assign_functions_mirror[3] = [&] (double charge, int i ) { return -charge * -velocity[i][2]; };

  // loop to compute the electric potential, Ax, Ay, Az
  for (size_t func_id=0; func_id<4; func_id++) { // func_id:= 0: rho; 1: Jx; 2: Jy; 3: Jz
    const auto &assign_function        = assign_functions       [func_id];
    const auto &assign_function_mirror = assign_functions_mirror[func_id];
    // distribute charge to mesh_rho
    mesh_rho = 0.0;
    for (size_t i=0; i<Nparticles; i++) {
      const auto &particle = bunch[i];
      if (selector(particle)) {
        if (particle.Q!=0.0 && particle.N!=0.0) {
	  const auto r = position[i] - min_;
	  if (r[0]>=0.0 && r[0]<=dim[0] && r[1]>=0.0 && r[1]<=dim[1] && r[2]>=0.0 && r[2]<=dim[2]) {
	    const auto charge = particle.Q * particle.N;
	    const auto assigned_charge = assign_function(charge, i);
	    mesh_rho.add_value(r[0]*_H[0], r[1]*_H[1], r[2]*_H[2], assigned_charge);
	    // mirror charges
	    if (!gsl_isnan(S1)) {
	      if (position[i][2] >= S0 && position[i][2] <= S1) { // range subjected to have mirror charges
		const auto r_S_mirror = S1 - position[i][2];
		if (r_S_mirror>=0.0 && r_S_mirror<=dim[2]) {
		  const auto assigned_charge_mirror = assign_function_mirror(charge, i);
		  mesh_rho.add_value(r[0]*_H[0], r[1]*_H[1], r_S_mirror*_H[2], assigned_charge_mirror);
		}
	      }
	    }
	  }
	}
      }
    }
    fftw_execute(p2); // mesh_rho.data() -> mesh_rho_hat.data()

    // convolution
    for (size_t i=0; i<Nxx*Nyy*(Nz+1); i++) { mesh_rho_hat.data()[i] *= mesh_smooth_hat.data()[i] * mesh_G_hat.data()[i]; } // 1/mm

    // inverse Fourier Transform to find the potential
    fftw_execute(p3);
    mesh_rho /= 8.0*double(Nx*Ny*Nz);

    auto copy_mesh = [&] (const fftwMesh3d &from, size_t to ) {
      for (size_t i=0; i<Nx; i++)
	for (size_t j=0; j<Ny; j++)
	  for (size_t k=0; k<Nz; k++)
	    mesh_A.elem(i,j,k)[to] = from.elem(i,j,k);
    };

    copy_mesh(mesh_rho, func_id);
  }     

  // compute force for each particle
  if (gamma_ref>1000) _H[2] = 0.0;
  auto compute_force_parallel = [&] (size_t thread, size_t start, size_t end ) {
    for (size_t i=start; i<end; i++) {
      const auto &particle = bunch[i];
      if (selector(particle)) {
	auto r = position[i] - min_;
	if (r[0]>=0.0 && r[0]<=dim[0] &&
	    r[1]>=0.0 && r[1]<=dim[1] &&
	    r[2]>=0.0 && r[2]<=dim[2]) { // inside the bounding box
	  const auto &v = velocity[i];
	  const auto q = particle.Q;
	  r[0] *= _H[0];
	  r[1] *= _H[1];
	  r[2] *= _H[2];
	  // Lorentz force
	  const auto dA_dx = mesh_A.deriv_x(r[0], r[1], r[2])*_H[0]; // 1/mm^2
	  const auto dA_dy = mesh_A.deriv_y(r[0], r[1], r[2])*_H[1]; // 1/mm^2
	  const auto dA_dz = mesh_A.deriv_z(r[0], r[1], r[2])*_H[2]; // 1/mm^2
	  const auto dPhi_dx = dA_dx[0];
	  const auto dPhi_dy = dA_dy[0];
	  const auto dPhi_dz = dA_dz[0];
	  const auto dAx_dy = dA_dy[1];
	  const auto dAx_dz = dA_dz[1];
	  const auto dAy_dx = dA_dx[2];
	  const auto dAy_dz = dA_dz[2];
	  const auto dAz_dx = dA_dx[3];
	  const auto dAz_dy = dA_dy[3];
	  const auto E = -StaticVector<3>(dPhi_dx, dPhi_dy, dPhi_dz); // 1/mm^2
	  const auto B =  StaticVector<3>(dAz_dy-dAy_dz, dAx_dz-dAz_dx, dAy_dx-dAx_dy); // 1/mm^2
	  const auto F = q * (E + cross(v, B)); // 1/mm^2
	  const double inv_denom = 1.0 / (1.0 + dot(v_ref, v));
	  auto Flab = (v_ref * dot(F, v) * gamma_ref + n * dot(F, n) * (gamma_ref - 1.0) + F) * inv_denom * inv_gamma_ref;
	  // const auto Elab = gamma_ref*(E-cross(v_ref,B))-gamma_ref*gamma_ref/(1+gamma_ref)*dot(v_ref,E)*v_ref;
	  // const auto Blab = gamma_ref*(B+cross(v_ref,E))-gamma_ref*gamma_ref/(1+gamma_ref)*dot(v_ref,B)*v_ref;
	  // auto Flab = q * (Elab + cross(particle.get_Vx_Vy_Vz(), Blab)); // 1/mm^2
	  // convert force into MeV/m
	  // e*e / epsilon0 / mm / mm = 1.809512739058424e-08 MeV/m
	  // e*e / epsilon0 / mm / mm = (1 / 55263495.99065812) MeV/m
	  Flab /= 55263495.99065812; // MeV/m
	  force[i][0] = Flab[0];
	  force[i][1] = Flab[1];
	  force[i][2] = Flab[2];
	}
      } else {
	force[i][0] = 0.0;
	force[i][1] = 0.0;
	force[i][2] = 0.0;
      }
    }
  };
  for_all(Nthreads, Nparticles, compute_force_parallel);
}

template <typename GREENS_FUNCTION>
std::pair<Static_Electric_FieldMap,Static_Magnetic_FieldMap> SpaceCharge_PIC<GREENS_FUNCTION>::get_fields() const
{
  // e / epsilon0 / mm = (1 / 55.2634959906581) V*mm/m
  // e / epsilon0 / mm / c = (1 / 16567579300.7125) T*mm
  Mesh3d Phi(Nx,Ny,Nz); // V*mm/m
  Mesh3d Ax(Nx,Ny,Nz); // T*mm
  Mesh3d Ay(Nx,Ny,Nz); // T*mm
  Mesh3d Az(Nx,Ny,Nz); // T*mm
  Mesh3d PhiM = Mesh3d(Nx, Ny, Nz, 0.0); // T*mm
  auto copy_mesh = [&] (size_t from, Mesh3d &to ) {
    for (size_t i=0; i<Nx; i++)
      for (size_t j=0; j<Ny; j++)
	for (size_t k=0; k<Nz; k++)
	  to.elem(i,j,k) = mesh_A.elem(i,j,k)[from];
  };
  copy_mesh(0, Phi);
  copy_mesh(1, Ax);
  copy_mesh(2, Ay);
  copy_mesh(3, Az);
  Phi /= 55.2634959906581; // V*mm/m
  Ax /= 16567579300.7125; // T*mm
  Ay /= 16567579300.7125; // T*mm
  Az /= 16567579300.7125; // T*mm
  return std::pair<Static_Electric_FieldMap,Static_Magnetic_FieldMap>(Static_Electric_FieldMap(Phi,
											       mesh.x0/1e3, mesh.y0/1e3, // m
											       mesh.hx/1e3, mesh.hy/1e3, mesh.hz/1e3, // m
											       (Nz-1)*mesh.hz/1e3),
								      Static_Magnetic_FieldMap(Ax, Ay, Az, PhiM,
											       mesh.x0/1e3, mesh.y0/1e3, // m
											       mesh.hx/1e3, mesh.hy/1e3, mesh.hz/1e3, // m
											       (Nz-1)*mesh.hz/1e3)); // length m
}

template <typename GREENS_FUNCTION>
Static_Electric_FieldMap SpaceCharge_PIC<GREENS_FUNCTION>::get_electric_field() const
{
  // e / epsilon0 / mm = (1 / 55.2634959906581) V*mm/m
  Mesh3d Phi(Nx,Ny,Nz); // V*mm/m
  auto copy_mesh = [&] (size_t from, Mesh3d &to ) {
    for (size_t i=0; i<Nx; i++)
      for (size_t j=0; j<Ny; j++)
	for (size_t k=0; k<Nz; k++)
	  to.elem(i,j,k) = mesh_A.elem(i,j,k)[from];
  };
  copy_mesh(0, Phi);
  Phi /= 55.2634959906581; // V*mm/m
  return Static_Electric_FieldMap(Phi,
				  mesh.x0/1e3, mesh.y0/1e3, // m
				  mesh.hx/1e3, mesh.hy/1e3, mesh.hz/1e3, // m
				  (Nz-1)*mesh.hz/1e3);
}

template <typename GREENS_FUNCTION>
Static_Magnetic_FieldMap SpaceCharge_PIC<GREENS_FUNCTION>::get_magnetic_field() const
{
  // e / epsilon0 / mm / c = (1 / 16567579300.7125) T*mm
  Mesh3d Ax(Nx,Ny,Nz); // T*mm
  Mesh3d Ay(Nx,Ny,Nz); // T*mm
  Mesh3d Az(Nx,Ny,Nz); // T*mm
  Mesh3d PhiM = Mesh3d(Nx, Ny, Nz, 0.0); // T*mm
  auto copy_mesh = [&] (size_t from, Mesh3d &to ) {
    for (size_t i=0; i<Nx; i++)
      for (size_t j=0; j<Ny; j++)
	for (size_t k=0; k<Nz; k++)
	  to.elem(i,j,k) = mesh_A.elem(i,j,k)[from];
  };
  copy_mesh(1, Ax);
  copy_mesh(2, Ay);
  copy_mesh(3, Az);
  Ax /= 16567579300.7125; // T*mm
  Ay /= 16567579300.7125; // T*mm
  Az /= 16567579300.7125; // T*mm
  return Static_Magnetic_FieldMap(Ax, Ay, Az, PhiM,
				  mesh.x0/1e3, mesh.y0/1e3, // m
				  mesh.hx/1e3, mesh.hy/1e3, mesh.hz/1e3, // m
				  (Nz-1)*mesh.hz/1e3); // length m
}

// template specializations

template class SpaceCharge_PIC<GreensFunction::IntegratedCoulomb>;
template class SpaceCharge_PIC<GreensFunction::IntegratedCoulomb_LongCylinder>;
template class SpaceCharge_PIC<GreensFunction::IntegratedCoulomb_HorizontalPlates>;
