#!/bin/bash
G=1.2; # T/m, gradient
while read X Y Z T
do
    Bx=$(echo "scale=15; $G * $Y / 1000" | bc); # convert $Y from mm to m
    By=$(echo "scale=15; $G * $X / 1000" | bc); # convert $X from mm to m
    echo 0 0 0 $Bx $By 0 # return Ex Ey Ez Bx By Bz
done


