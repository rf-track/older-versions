RF_Track;

N_electrons = 1e6; % e, number of particles in the distribution
Q = -1; % e, charge of one electron

N = 1000000; % number of simulated macro particles

radius = 1; % mm, radius of the sphere
rvals = 2*rand(N,1)-1;
elevation = asin(rvals); % rad
azimuth = 2*pi*rand(N,1); % rad
radii = radius*rand(N,1).^(1/3); % mm
[X, Y, Z] = sph2cart(azimuth, elevation, radii);

%% Create a beam
O = zeros(N,1);
B = [ X O Y O Z O ];
B0 = Bunch6dT(RF_Track.electronmass, N_electrons, Q, B);

figure(1)
clf
hold on
LGND = '';

for NN = [ 32 48 64 80 128 ] 
    Nx = NN; 
    Ny = NN;
    Nz = NN;

    SC = SpaceCharge_PIC_FreeSpace(Nx,Ny,Nz);
    F = SC.compute_force(B0);
    E = SC.get_electric_field(); %  V/m

    Er = [];
    Xa = linspace(-radius * 0.99, radius * 0.99, 100); % mm
    for X = Xa
        [E_, B_] = E.get_field(X, 0, radius, 0.0);
        Er = [ Er ; E_(1) ]; % V/m
    end
    plot(Xa,Er/1e3,'linewidth',2);
    LGND = [ LGND ; sprintf('PIC %dx%dx%d  ', Nx, Ny, Nz) ];
    
    drawnow;
end

%% analytic formula
rmax = radius; % mm

Er = [];
Ra = linspace(-rmax, rmax, 100); % mm
for r = Ra
    if r < radius
        % e / 4 pi epsilon0 mm^2 = 0.0014399645 V/m
        E = 0.0014399645 * Q * N_electrons * r / radius^3; % V/m
    else
        % e / 4 pi epsilon0 mm^2 = 0.0014399645 V/m
        E = 0.0014399645 * Q * N_electrons / r^2; % V/m
    end
    Er = [ Er ; E ];
end


plot(Ra, Er/1e3,'linewidth',2);
title('Radial electric field of a uniformly charged sphere');
xlabel('r [mm]')
ylabel('E_r [kV/m]')
LGND = [ LGND ; 'Analytic formula' ];
legend(cellstr(LGND));
grid on;
print -dpng plot_Er.png
