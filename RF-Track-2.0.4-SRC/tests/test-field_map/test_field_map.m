setenv("DEBUG"); % triggers RF-Track debug information

%% load RF-Track
addpath('../../')
RF_Track;

%% load the field and create the lattice
load('field.dat.gz');

S = Static_Magnetic_FieldMap(M.Bx, M.By, M.Bz, ...
                             M.xa(1), M.ya(1), ...
                             M.xa(2) - M.xa(1), ...
                             M.ya(2) - M.ya(1), ...
                             M.za(2) - M.za(1), M.za(end));

i0 = 21; % 21 = central axis (5 is interesting)
j0 = 21; % 21 = central axis (5 is interesting)

T = []; for z=M.za' ;
    [E,B] = S.get_field(M.xa(i0)*1e3, M.ya(j0)*1e3, z*1e3, 0.0);
    T = [ T ; B(1) ];
end
Tf = []; for k0=1:length(M.za) ; Tf = [ Tf ; M.Bx(i0,j0,k0) ]; end

figure(1);
clf;
set(gca, "linewidth", 4)
plot(M.za, T*10000, "linewidth", 2);
hold on
plot(M.za, Tf*10000, 'r-', "linewidth", 2)
axis([ 0 3 -1 1 ]);
xlabel('s [m]')
ylabel('Bx [gauss]')
legend('interpolated ', 'field map');
print -dpng plot_Bx.png

T = []; for z=M.za'
    [E,B] = S.get_field(M.xa(i0)*1e3, M.ya(j0)*1e3, z*1e3, 0.0);
    T = [ T ; B(2) ];
end
Tf = []; for k0=1:length(M.za) ; Tf = [ Tf ; M.By(i0,j0,k0) ]; end

figure(2);
clf;
set(gca, "linewidth", 4)
plot(M.za, T*10000, "linewidth", 2);
hold on
plot(M.za, Tf*10000, 'r-', "linewidth", 2)
xlabel('s [m]')
ylabel('By [gauss]')
legend('interpolated ', 'field map');
print -dpng plot_By.png

T = []; for z=M.za' ;
    [E,B] = S.get_field(M.xa(i0)*1e3, M.ya(j0)*1e3, z*1e3, 0.0);
    T = [ T ; B(3) ];
end
Tf = []; for k0=1:length(M.za) ; Tf = [ Tf ; M.Bz(i0,j0,k0) ]; end

figure(3);
clf;
set(gca, "linewidth", 4)
plot(M.za, T*10000, "linewidth", 2);
hold on
plot(M.za, Tf*10000, 'r-', "linewidth", 2)
xlabel('s [m]')
ylabel('Bz [gauss]')
legend('interpolated ', 'field map');
print -dpng plot_Bz.png

if false
for k0=880 %1:10:1761
    k0
    Tx = zeros(40,40);
    Ty = zeros(40,40);
    Tz = zeros(40,40);
    Tfx = zeros(40,40);
    Tfy = zeros(40,40);
    Tfz = zeros(40,40);
    for i0=1:40
        for j0=1:40
            [E,B] = S.get_field(M.xa(i0)*1e3, M.ya(j0)*1e3, M.za(k0)*1e3, 0.0);
            Tfz(i0,j0) = M.Bx(i0,j0,k0);
            Tfy(i0,j0) = M.By(i0,j0,k0);
            Tfz(i0,j0) = M.Bz(i0,j0,k0);
            Tx (i0,j0) = B(1);
            Ty (i0,j0) = B(2);
            Tz (i0,j0) = B(3);
        end
    end
    figure(4)
    surf(Tx); legend('Bx (interpolated)');
    figure(5)
    surf(Ty); legend('By (interpolated)');
    figure(6)
    surf(Tz); legend('Bz (interpolated)');
    
    figure(7)
    surf(Tfx); legend('Bx (field map)');
    figure(8)
    surf(Tfy); legend('By (field map)');
    figure(9)
    surf(Tfz); legend('Bz (field map)');
end

T(800)
Tf(800)
Tf(800)/T(800)


Bx = reshape(M.Bx, M.Nx*M.Ny*M.Nz, 1, 1);
By = reshape(M.By, M.Nx*M.Ny*M.Nz, 1, 1);
Bz = reshape(M.Bz, M.Nx*M.Ny*M.Nz, 1, 1);

C = cov([ Bx By Bz ]);

[V,L] = eig(C)
end