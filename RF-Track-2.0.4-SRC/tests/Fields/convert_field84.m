#! /opt/local/bin/octave -q --persist

addpath('../../../tools');

%%%%%%%%%%%%%%%% field maps

ExEyEz_file='Efield84.fld.gz';
HxHyHz_file='Hfield84.fld.gz';
Output_file='Structure_84.dat.gz';

%%%%%%%%%%%%%%%% global variables

global Bunch Structure

%%%%%%%%%%%%%%%% bunch parameters

Bunch.mass0 = 0.938272046; % GeV/c/c
Bunch.ekinetic0 = 76e-3; % GeV
Bunch.etotal0 = Bunch.ekinetic0 + Bunch.mass0; % GeV
Bunch.pc0 = sqrt(Bunch.etotal0**2 - Bunch.mass0**2); % GeV
Bunch.beta0 = Bunch.pc0 / Bunch.etotal0

%%%%%%%%%%%%%%%% structure parameters

Structure.frequency = 2.9985e9; % Hz
Structure.direction = 1.0; % -1 == backward travelling structure
Structure.P_map = 1; % W design input power
Structure.P_actual = 1; % W actual input power
Structure.beta = Bunch.beta0;
%Structure.beam_loading = @(z) 1.0;
beam_loading = @(z) 1.0;

%%%%%%%%%%%%%%%% some constants

mu0 = 1.2566371e-06; % 4 pi 1e-7 H/m
clight = 299792458; % m/s

%%%%%%%%%%%%%%%% load files and eliminate Nan's

TE=load(ExEyEz_file);
TH=load(HxHyHz_file);
TE(isnan(TE))=0.0;
TH(isnan(TH))=0.0;

%%%%%%%%%%%%%%%%

Structure.xa=sort(unique(TE(:,1)));
Structure.ya=sort(unique(TE(:,2)));
Structure.za=sort(unique(TE(:,3)));
Nx=length(Structure.xa);
Ny=length(Structure.ya);
Nz=length(Structure.za);
Structure.Nx = Nx;
Structure.Ny = Ny;
Structure.Nz = Nz;
Structure.Ex = zeros(Nx,Ny,Nz);
Structure.Ey = zeros(Nx,Ny,Nz);
Structure.Ez = zeros(Nx,Ny,Nz);
Structure.Bx = zeros(Nx,Ny,Nz);
Structure.By = zeros(Nx,Ny,Nz);
Structure.Bz = zeros(Nx,Ny,Nz);

%%%%%%%%%%%%%%%% prepare the output field maps

for n=1:rows(TE)
    i = find(Structure.xa==TE(n,1));
    j = find(Structure.ya==TE(n,2));
    k = find(Structure.za==TE(n,3));
    _beam_loading = beam_loading(Structure.za(k));
    Structure.Ex(i,j,k) = complex(TE(n,4),TE(n,5)) * _beam_loading;
    Structure.Ey(i,j,k) = complex(TE(n,6),TE(n,7)) * _beam_loading;
    Structure.Ez(i,j,k) = complex(TE(n,8),TE(n,9)) * _beam_loading;
    Structure.Bx(i,j,k) = mu0 * complex(TH(n,4),TH(n,5)) * _beam_loading;
    Structure.By(i,j,k) = mu0 * complex(TH(n,6),TH(n,7)) * _beam_loading;
    Structure.Bz(i,j,k) = mu0 * complex(TH(n,8),TH(n,9)) * _beam_loading;
end

function ekinetic = trackEnergy(phi0)
    global Bunch Structure
    i = find(Structure.xa==0);
    j = find(Structure.ya==0);
    ekinetic = Bunch.ekinetic0;
    etotal = Bunch.etotal0;
    beta = Bunch.beta0;
    pc = Bunch.pc0; % GeV
    clight = 299792458; % m/s
    dz = (Structure.za(end) - Structure.za(1)) / (length(Structure.za) - 1);
    t = 0;
    for k = 1:Structure.Nz
        F = Structure.Ez(i,j,k) * exp(J * (phi0 + 2*pi*Structure.direction*Structure.frequency*t));
        %%%%% energy gain
        momentum_gain = real(F) * dz / beta / 1e9;
        pc += momentum_gain;
        etotal = sqrt(pc*pc + Bunch.mass0**2); % GeV
        ekinetic = etotal - Bunch.mass0; % GeV
        beta = pc / etotal;
        t += dz / beta / clight;
    end
endfunction

PLOT = [];
%for phi0=0:0.01:2*pi
%    PLOT = [ PLOT ; phi0 trackEnergy(phi0) - Bunch.ekinetic0  ];
%end

for phi0=0:0.01:2*pi
    PLOT = [ PLOT ; phi0 trackEnergy(phi0) ];
end


plot(PLOT(:,1), PLOT(:,2))

figure

phi0_range = PLOT(find(PLOT(:,2) >= 0.999 * max(PLOT(:,2))),1);
phi0 = fminbnd(inline('-trackEnergy(x)'), phi0_range(1), phi0_range(end));
phi0_deg = rad2deg(phi0)

% we locate ourselves at the origin
i = find(Structure.xa==0);
j = find(Structure.ya==0);
clight = 299792458; % m/s
ekinetic = Bunch.ekinetic0;
etotal = Bunch.etotal0;
beta = Bunch.beta0;
pc = Bunch.pc0; % GeV
Average_Field = 0;
F = zeros(Structure.Nz,1);
dz = (Structure.za(end) - Structure.za(1)) / (length(Structure.za) - 1);
scale_factor = sqrt(Structure.P_actual / Structure.P_map); % W design input power
t = 0;
for k = 1:Structure.Nz
    F(k) = Structure.Ez(i,j,k) * exp(J*(phi0 + 2*pi*Structure.direction*Structure.frequency*t)) * scale_factor;
    Average_Field += F(k);
    %%%%% energy gain
    momentum_gain = real(F(k)) * dz / beta / 1e9;
    pc += momentum_gain;
    etotal = sqrt(pc*pc + Bunch.mass0**2); % GeV
    ekinetic = etotal - Bunch.mass0; % GeV
    beta = pc / etotal;
    t += dz / beta / clight;
end
t_final = t
Average_Field = Average_Field / Structure.Nz;
phi_rad = arg(Average_Field);
phi_deg = rad2deg(phi_rad);
fprintf(stderr, "Final kinetic energy =  %g GeV\n", ekinetic);
fprintf(stderr, "Avg. Field =  %g + %gi MV/m\n", real(Average_Field)*1e-6, imag(Average_Field)*1e-6);
fprintf(stderr, "phi =  %g rad\n", phi_rad);
fprintf(stderr, "phi =  %g deg\n", phi_deg);
Average_Field *= exp(J * -phi_rad);
F .*= exp(J * -phi_rad);
fprintf(stderr, "Avg. Gradient =  %g + %gi MV/m\n", real(Average_Field)*1e-6, imag(Average_Field)*1e-6);

plot(Structure.za, real(F));

%%%%%%%%%%%%%%%% prepares for saving the data on disk

Structure.Ex *= exp(J * phi0);
Structure.Ey *= exp(J * phi0);
Structure.Ez *= exp(J * phi0);

Structure.Bx *= exp(J * phi0);
Structure.By *= exp(J * phi0);
Structure.Bz *= exp(J * phi0);

%%%%%%%%%%%%%%%% saves the fields on disk

save('-binary', '-zip', Output_file, 'Structure')
