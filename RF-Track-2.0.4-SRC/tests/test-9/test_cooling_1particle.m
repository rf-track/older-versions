#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../../');

%% Load RF_Track
RF_Track;

randn("seed", 1234);
rand("seed", 5678);

L = 3; % m
sigma_x = 50; % mm

Proton.N_particles = 3e8;
Proton.K = 46.2; % MeV
Proton.E = Proton.K + RF_Track.protonmass; % MeV
Proton.P = 308.6; % MeV/c
%Proton.P = sqrt(Proton.E**2 - RF_Track.protonmass**2); % MeV
Proton.P_spread = 0.25 / (2*sqrt(2*log(2))); % 0.25 percent FWHM
Proton.v = Proton.P / Proton.E; % c
Proton.emitt_x = 40; % mm.mrad geometrical
Proton.emitt_y = 40; % mm.mrad geometrical
Proton.sigma_x = 0.8 * sigma_x; % mm
Proton.sigma_y = 0.8 * sigma_x; % mm
Proton.sigma_xp = Proton.emitt_x / Proton.sigma_x; % mrad
Proton.sigma_yp = Proton.emitt_y / Proton.sigma_x; % mrad

N_proton   = 100;

%%
Pz = Proton.P * (100 + Proton.P_spread * randn(N_proton,1)) / 100.0; % MeV/c
xp = 0*randn(N_proton,1) * Proton.sigma_xp; % mrad
yp = 0*randn(N_proton,1) * Proton.sigma_yp; % mrad

%Pz = P ./ sqrt(xp.*xp + yp.*yp + 1e6) * 1e3; % MeV/c
Px = xp .* Pz / 1e3; % MeV/c
Py = yp .* Pz / 1e3; % MeV/c
x = (1 - 2*rand(N_proton,1)) * Proton.sigma_x;
y = (1 - 2*rand(N_proton,1)) * Proton.sigma_y;
z = rand(N_proton,1) * L * 1e3;
%Pz = ones(N_proton,1) * Proton.P;

B_proton = [
	    x, Px, ...
	    y, Py, ...
	    z, Pz, ...
	    ones(N_proton,1) * RF_Track.protonmass, ...  % mass
	    30 * ones(N_proton,1), ...                        % Q
	    ones(N_proton,1) * Proton.N_particles / N_proton % N
];

Bpro = Bunch6dT(B_proton);

%%%%%%%%%%%%%%

L = 1; % m
sigma_x = 50; % mm
Proton.P = 308.6; % MeV/c

RF_Track.number_of_threads = 1;

Bpro = Bunch6d([ 0 10 0 0 0 Proton.P RF_Track.protonmass 30 1 ]);
%Bpro = Bunch6d(B_proton);

%Electron.v = Bpro.get_phase_space("%Vz");
Electron.v = 0.3;

E = ElectronCooler("uniform", L, sigma_x/1e3, sigma_x/1e3, 1e17, Electron.v);
E.set_nsteps(1);

L = Lattice();
L.append(E);

tic
Bpro_ = L.track(Bpro);
toc

format long

printf("P_initial = %.16g\t%.16g\t%.16g\n", Bpro.get_phase_space("%Px %Py %Pz"));
printf("P_final   = %.16g\t%.16g\t%.16g\n", Bpro_.get_phase_space("%Px %Py %Pz"));

printf("K_initial = %.16g\n", mean(Bpro.get_phase_space("%K")));
printf("K_final   = %.16g\n", mean(Bpro_.get_phase_space("%K")));
