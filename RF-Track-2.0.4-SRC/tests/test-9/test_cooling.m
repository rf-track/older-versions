#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../../');

%% Load RF_Track
RF_Track;

randn("seed", 1234);
rand("seed", 5678);

%%%%%%%%%%%%%%

RF_Track.number_of_threads = 1;

L = 10; % m
sigma_x = 50; % mm

Proton.N_particles = 3e8;
Proton.mass = RF_Track.protonmass; % MeV/c^2
%Proton.P = 8.6; % MeV/c
%Proton.P_spread = 0.25 / (2*sqrt(2*log(2))); % 0.25 percent FWHM
%Proton.E = hypot(Proton.mass, Proton.P); % MeV
%Proton.V = Proton.P / Proton.E; % c
%Proton.K = Proton.E - Proton.mass; % MeV

Electron.V = 0.5;
Proton.V = Electron.V + 0.00936192;
Proton.gamma = 1/sqrt(1 - Proton.V**2);
Proton.P = Proton.mass * Proton.gamma * Proton.V;

N_proton = 1;

%%
x = 0 * (1 - 2*rand(N_proton,1)) * sigma_x;
y = 0 * (1 - 2*rand(N_proton,1)) * sigma_x;
z = 0;

Pz = Proton.P; % MeV/c
xp = 0; % mrad
yp = 0; % mrad

%Pz = ones(N_proton,1) * Proton.P;

B_proton = [
	    x, xp, ...
	    y, yp, ...
	    z, Pz, ...
	    Proton.mass, ...      % mass
	    50*ones(N_proton,1), ...                       % Q
	    ones(N_proton,1) * Proton.N_particles / N_proton % N
];

Nx = 32;
Ny = 32;
Nz = 1;

B0 = Bunch6d(B_proton);

E = ElectronCooler(L*Proton.gamma/Proton.V, sigma_x/1e3, sigma_x/1e3);
E.set_Q(-1);
E.set_nsteps(Nz);
E.set_electron_mesh_template(1e17, 0, 0, Electron.V, Nx, Ny);
E.set_static_Bfield(0.0, 0.0, 0.0);

L = Lattice();
L.append(E);

tic
B1 = L.track(B0);
toc

printf("K_initial = %.16g\n", mean(B0.get_phase_space("%K")));
printf("K_final   = %.16g\n", mean(B1.get_phase_space("%K")));

bi = mean(B0.get_phase_space("%Vz"));
bf = mean(B1.get_phase_space("%Vz"));

gi = 1/sqrt(1-bi*bi);
gf = 1/sqrt(1-bf*bf);

t_Mandelstam = 1+(bi*bf-1)*gi*gf

printf("dK = %.16g\n", mean(B1.get_phase_space("%K")) - mean(B0.get_phase_space("%K")));

figure(1);
clf
scatter3(B1.get_phase_space("%x"), B1.get_phase_space("%y"), B1.get_phase_space("%Pz"))
hold on
scatter3(B0.get_phase_space("%x"), B0.get_phase_space("%y"), B0.get_phase_space("%Pz"), 'r')
xlabel('X [mm]');
ylabel('Y [mm]');
zlabel('K [MeV]');
