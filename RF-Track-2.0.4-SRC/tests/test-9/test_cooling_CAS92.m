#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../../');

clear all

%% Load RF_Track
RF_Track;

%RF_Track.number_of_threads = 1;

randn("seed", 1234);
rand("seed", 5678);

%%%%%%%%%%%%%%

Cooler.L = 3; % m
Cooler.B = 0.0455; % T
Cooler.r0 = 25; % mm

Electron.Ne = 1e17; %%%8.27e13; % electron number density #/m^3
Electron.K = 0.026929; % MeV
Electron.E = RF_Track.electronmass + Electron.K; % MeV
Electron.P = sqrt(Electron.E**2 - RF_Track.electronmass**2);
Electron.V = Electron.P / Electron.E

Proton.Np = 5e9; % total number of protons
Proton.P = 308.6; % MeV/c
Proton.E = sqrt(RF_Track.protonmass**2 + Proton.P**2);
Proton.V = Proton.P / Proton.E;
Proton.P_spread = 0.25 / (2*sqrt(2*log(2))) % 0.25 percent FWHM

N_particles = 10000;

sigma_x = Cooler.r0;

%%
x = 0.1 * (1 - 2*rand(N_particles,1)) * sigma_x;
y = 0.1 * (1 - 2*rand(N_particles,1)) * sigma_x;
z = 0 * rand(N_particles,1) * Cooler.L * 1e3;

Pz = Proton.P * (100 + Proton.P_spread * randn(N_particles,1)) / 100.0; % MeV/c
xp = randn(N_particles,1) * 0; % mrad
yp = randn(N_particles,1) * 0; % mrad

B_proton = [
	    x, xp, ...
	    y, yp, ...
	    z, Pz, ...
	    ones(N_particles,1) * RF_Track.protonmass, ...  % mass
	    ones(N_particles,1), ...                        % Q
	    ones(N_particles,1) * Proton.Np / N_particles   % N
];

B0 = Bunch6d(B_proton);

B{1} = B0;

Nx = 64;
Ny = 64;
Nz = 10;

E = ElectronCooler(Cooler.L, Cooler.r0/1e3, Cooler.r0/1e3);
E.set_Q(-1);
E.set_nsteps(Nz);
E.set_electron_mesh_template(Electron.Ne, 0, 0, Electron.V, Nx, Ny);
E.set_static_Bfield(0.0, 0.0, Cooler.B);

L = Lattice();
L.append(E);

for i=1:100
  tic
  B{i+1} = L.track(B{i});
  toc
end

B1 = B{end};

format long

printf("K_initial = %.16g\n", mean(B0.get_phase_space("%K")));
printf("K_final   = %.16g\n", mean(B1.get_phase_space("%K")));

figure(1);
clf
scatter3(B1.get_phase_space("%x"), B1.get_phase_space("%y"), B1.get_phase_space("%Pz"))
hold on
scatter3(B0.get_phase_space("%x"), B0.get_phase_space("%y"), B0.get_phase_space("%Pz"), 'r')
