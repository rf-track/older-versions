#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../../');

%% Load RF_Track
RF_Track;

Zp = +30;
Ze = -1;

Mp = RF_Track.protonmass;
Me = RF_Track.electronmass;

b = 0.0005; % impact parameter

B_pro = [ -1 5 0 0 0 0 Mp Zp 1 ];
B_ele = [  0 0 b 0 0 0 Me Ze 1 ]

N_ele = 20;
B_ele = repmat(B_ele, N_ele, 1);
B_ele(:,1) = linspace(-2, 2, N_ele);
tmp = B_ele;
tmp(:,3) = -b*ones(N_ele,1);
B_ele = [ B_ele ; tmp ];

%B_ele(1,:) = [];

%B_ele = repmat(B_ele, 100, 1);
%B_ele(:,1) = linspace(-0.5, 0.5, 100);

B0 = Bunch6dT( [ B_pro ; B_ele ] );
B1 = Bunch6dT( [ B_pro ; B_ele ] );

Vp = B0.get_phase_space("%Vx")(1);
Kp = B0.get_phase_space("%K")(1);

ne = 1e10; %% electron density #/m**3

%% from Conte pag 248
%% l_min: mm = 2*e*e / 4/ pi/ epsilon0 / MeV / 347230786031.881
%% l_debye: sqrt(MeV*epsilon0/e/e*m**3) = 7433942156.80067 mm

LC = 10; % coulomb logarighm
l_min_mm = abs(Zp * Ze) / Me / Vp**2 / 347230786031.881 % [mm] minimum impact distnace
l_debye_mm = sqrt(Kp/ne/abs(Zp*Ze)) * 7433942156.80067 % [mm] debye length
l_screening_mm = l_min_mm * exp(10)

SC = SpaceCharge_P2P();

format long

dt = 1; % mm

Tp = [];
Te = [];

TP = [];

figure(1)

K0 = B1.get_phase_space("%K");
K = [];
%FT = zeros(2,3);
do

  %%
  M = B1.get_phase_space("%X %Y %K");

  Tp = [ Tp ; M(1,:) ];
  Te = [ Te ; M(2,:) ];

  %% Solenoid
  %F .+= repmat(M(:,1), 1, 3) .* cross(M(:,2:4), repmat([ 0 0 RF_Track.clight*Solenoid.field ], N_proton + N_electron, 1)) ./ 1e6; % MeV/m

  %% Compute space-charge force
  F = SC.compute_force(B1);
  %% FT += F;
  F(1,2:3) = 0.0;
  F(2:end,1:3) = 0.0; %% keeps the electrons still
  
  %% Apply force
  B1.apply_force(F, dt);

  %% Work done
  dX = B1.get_phase_space("%Vx %Vy %Vz")(1,:) * dt;
  dx = dX(1); % dx horizontal direction
  x0 = M(1,1) - dx/2;

  %% Analytic estimate
  % e*e/4/pi/epsilon0 /mm = 1.4399645e-09 keV
  dW_keV = -dot(F(1,:), dX); % MeV/m * mm = keV
  dW_keV_analytic = -abs(Zp*Ze) * (atan2(x0+dx,b)-atan2(x0,b))/(2*b) * 1.4399645e-09; % keV
  dW_keV_analytic_radial = -abs(Zp*Ze) * ((x0+dx)/(b*sqrt((x0+dx)^2+b^2))-x0/(b*sqrt(x0^2+b^2))) * 1.4399645e-09; % keV

  K = [ K ; dW_keV dW_keV_analytic dW_keV_analytic_radial F(1,:) ];
  
  plot(Tp(:,1), Tp(:,2), 'r*-', Te(:,1), Te(:,2), 'b*');
  axis([ -1.1 1.1 -0.002 0.002 ]);
  xlabel('X [mm]');
  ylabel('Y [mm]');
  drawnow

  E = B1.get_phase_space("%E");
  P = B1.get_phase_space("%Px %Py %Pz");
  Pp = P(1,:);
  Pe = P(2,:);

  r_norm = norm(M(1,:) .- M(2,:));
  
  Vpot = abs(Zp*Ze) / r_norm / 6.9446157e+11; % MeV
  
  Vcom = (Pp + Pe) / (E(1) + E(2));

  V = B1.get_phase_space("%Vx %Vy %Vz");
  Vp = relativistic_velocity_addition(-Vcom, V(1,:));
  Ve = relativistic_velocity_addition(-Vcom, V(2,:));

  me = RF_Track.electronmass;
  mp = RF_Track.protonmass;

  Pp_com = mp * Vp;
  Pe_com = me * Ve;

  Ep_com = sqrt(mp*mp + dot(Pp_com,Pp_com));
  Ee_com = sqrt(me*me + dot(Pe_com,Pe_com));

  Kp_com = Ep_com - mp;
  Ke_com = Ee_com - me;
  
  Ptot = Pp + Pe;
  Mtot = mp + me;
  Etot = sqrt(Mtot*Mtot + dot(Ptot,Ptot));

  TP = [ TP ; sum(E) Etot norm(sum(P)) ];
  
until M(1,1) >= 1

save -text TP.dat TP

figure(2)
ax=plotyy(1:size(Te,1), Te(:,3), 1:size(Tp,1), Tp(:,3));
xlabel ("t [step]");
ylabel (ax(2), "K_p [MeV]");
ylabel (ax(1), "K_e [MeV]");

printf("proton_energy_loss = %g MeV\n", Tp(1,3) - Tp(end,3));
printf("relative_proton_energy_loss = %g %%\n", (Tp(1,3) - Tp(end,3)) / Tp(1,3) * 100);

disp('Energy loss in keV: numerical | analytical 2d | analytical radial : ')

sum(abs(K(:,1:3)))

Before = B0.get_phase_space("%Px %Py %Pz %K")(1,:)
After = B1.get_phase_space("%Px %Py %Pz %K")(1,:)
After - Before

figure(3)
clf
hold on
plot(cumsum(K(:,1)), 'r-;numerical;');
plot(cumsum(K(:,2)), 'b-;analytical 2d;');
plot(cumsum(K(:,3)), 'g-;analytical radial;');
xlabel ("t [step]");
ylabel ("dK [keV]");
