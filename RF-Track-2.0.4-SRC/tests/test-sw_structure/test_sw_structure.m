RF_Track;

%% create standing wave structure
a1 = 1.0; % V/m max gradient
freq = 12e9; % Hz
lambda = RF_Track.clight / freq; % m, wavelength
l_cell = lambda / 3 + 0.020; % m, length of the full SW cell
n_cells = -3; % number of cells              
             % a negative sign indicates a start from the beginning of the cell
             % a positive sign indicates a start from the middle of the cell

SW = SW_Structure(a1, freq, l_cell, n_cells);

%% plot Ez along the structure, in time
T_period = RF_Track.s / freq; % mm/c
T_axis = linspace(0, T_period, 64); % mm/c
Z_axis = linspace(0, SW.get_length()*1e3, 64); % mm
for t=T_axis
    E_ = [];
    B_ = [];
    for z=Z_axis
        [E,B] = SW.get_field(0, 0, z, t);
        E_ = [ E_ E ];
        B_ = [ B_ B ];
    end
    plot(Z_axis, E_(3,:));
    title(sprintf('t = %2.f%% of period', t*100/T_period));
    xlabel('Z [mm]');
    ylabel('E_z [V/m]');
    axis([ 0 Z_axis(end) -a1*1.2 a1*1.2 ]);
    drawnow;
end


