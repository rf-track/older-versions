RF_Track;

%% create full structure
a0 = 1.0; % V/m, principal Fourier coefficient, of TW structure
a1 = 1.0; % V/m, principal Fourier coefficient, of SW structure
freq = 12e9; % Hz, frequency
lambda = RF_Track.clight / freq; % m, wavelength
ph_adv = 2*pi/3; % radian, phase advance per cell
l_sw_cell = lambda / 3 + 0.020; % m, length of the SW full cell
n_tw_cells = +3; % number of TW cells
                 % a negative sign indicates a start from the beginning of the cell
                 % a positive sign indicates a start from the middle of the cell

% define the accelerating structure
AS = Lattice();
AS.append(SW_Structure(a1,    freq, l_sw_cell, -0.5));       % half SW, entrance coupler
AS.append(TW_Structure(a0, 0, freq, ph_adv,    n_tw_cells)); % TW part
AS.append(SW_Structure(a1,    freq, l_sw_cell, +0.5));       % half SW, exit coupler

% place the AS in the volume
V = Volume();
V.add(AS, 0, 0, 0);

%% plot Ez along the structure, in time
T_period = RF_Track.s / freq; % mm/c
T_axis = linspace(0, T_period, 100); % mm/c
Z_axis = linspace(0, V.get_length()*1e3, 1000); % mm
for t=T_axis
    E_ = [];
    B_ = [];
    for z=Z_axis
        [E,B] = V.get_field(0, 0, z, t);
        E_ = [ E_ E ];
        B_ = [ B_ B ];
    end
    plot(Z_axis, E_(3,:));
    title(sprintf('t = %2.f%% of period', t*100/T_period));
    xlabel('Z [mm]');
    ylabel('E_z [V/m]');
    axis([ 0 Z_axis(end) -a0*1.2 a0*1.2 ]);
    drawnow;
end