addpath('../..');

RF_Track;

C = TestCoils(0.4, ... % m, length
              0.4, ... % T, on-axis field
              0.1); % m, radius

C.set_nsteps(40);
C.set_odeint_algorithm("rk2");

L = Lattice();
L.append(C);

%{
O = TrackingOpts();
O.dt_mm = 1 ; % mm/c
O.nsteps = 0; % if 0, tracks all the way to the end of the lattice
O.space_charge_nsteps = 0; % if != 0, applies a space charge kick every space_charge_nsteps steps
O.backtrack_at_entrance = false; % bring the foremost particle to S=0 before strarting to track
O.odeint_algorithm = "bsimp"; % pick your favorite algorithm
%}

%% beam
I = imread('CERN_logo.png');
[r,c] = find(I(:,:,1)>0);
O_ = zeros(size(r,1),1);
Pz_ = 1280 * ones(size(r,1),1);

B0 = Bunch6d(RF_Track.electronmass, 1, -1, [ (c-64) O_ (64-r) O_ O_ Pz_ ] / 128);
% B0 = Bunch6dT(RF_Track.electronmass, 1, -1, [ 1 0 0 0 0 100 ]);
M0 = B0.get_phase_space();

fid = popen("ffmpeg -y -r 25 -i - -vcodec libx264 movie2_xy.mkv", "w");

for i=1:40
    B0 = L.track(B0);
    % B0 = L.track(B0, O); % bunch 6dT
    % M = B0.get_phase_space();
    % M = B0.get_phase_space("%X %Px %Y %Py %Pz");
    % B0 = Bunch6dT(RF_Track.electronmass, 1, -1, [ M(1) M(2) M(3)
    % M(4) 0 M(5) ]);
    M = B0.get_phase_space();
    scatter(M(:,1), M(:,3),'k');
    axis([ -1 1 -1 1 ]);
    print -dpng pic.png
    fid2 = fopen('pic.png', 'r');
    buffer = fread(fid2);
    fwrite(fid, buffer);
    fclose(fid2);
end

fclose(fid);

figure(1)
clf
hold on
M = B0.get_phase_space();
scatter(M0(:,1), M0(:,3),'r');
scatter(M(:,1), M(:,3),'k');

figure(2)
clf
hold on
M = B0.get_phase_space();
scatter(M0(:,2), M0(:,4),'r');
scatter(M(:,2), M(:,4),'k');

figure(3)
clear Bz
for i=1:int32(C.get_length()*1e3); [E,B] = C.get_field(0,0,i,0); Bz(i) = B(3); end;
plot(Bz);

figure(4)
clear Br
for i=1:int32(100); [E,B] = C.get_field(0,i,100,0); Br(i) = B(2); end;
plot(Br);