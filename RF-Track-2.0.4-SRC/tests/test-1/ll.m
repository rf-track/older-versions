L.append(Drift(1.898));
Q = Quadrupole(0.1, Bunch.Pc, 2.17177 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.03));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.01));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.03));
Q = Quadrupole(0.1, Bunch.Pc, -1.69973 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.023));
L.append(Drift(0.3));
L.append(Drift(0.56));
L.append(Drift(0.023));
Q = Quadrupole(0.1, Bunch.Pc, 1.6159 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.03));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.01));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.03));
Q = Quadrupole(0.1, Bunch.Pc, -1.53905 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.023));
L.append(Drift(0.3));
L.append(Drift(0.4478));

L.append(Drift(0.1));
L.append(Drift(0.1));
L.append(Drift(0.4893));
L.append(Drift(0.3));
L.append(Drift(0.023));
Q = Quadrupole(0.1, Bunch.Pc, 0.9326674735 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.03));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.01));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.03));
Q = Quadrupole(0.1, Bunch.Pc, 0 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.023));
L.append(Drift(0.86));
L.append(Drift(0.3));
L.append(Drift(0.023));
Q = Quadrupole(0.1, Bunch.Pc, -0.9326674735 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.03));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.01));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.03));
Q = Quadrupole(0.1, Bunch.Pc, 0 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.023));
L.append(Drift(0.86));
L.append(Drift(0.3));
L.append(Drift(0.023));
Q = Quadrupole(0.1, Bunch.Pc, 0.9326674735 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.03));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.01));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.03));
Q = Quadrupole(0.1, Bunch.Pc, 0 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.023));
L.append(Drift(0.616));
L.append(Drift(0.3));
L.append(Drift(0.023));
Q = Quadrupole(0.1, Bunch.Pc, 0 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.03));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.01));
# WARNING: CORRECTOR needs to be defined, not an RF-Track's element
L.append(Drift(0.037));
L.append(Drift(0.03));
Q = Quadrupole(0.1, Bunch.Pc, -0.9326674735 / 0.1);
Q.set_aperture(30e-3);
L.append(Q);
L.append(Drift(0.1699));


