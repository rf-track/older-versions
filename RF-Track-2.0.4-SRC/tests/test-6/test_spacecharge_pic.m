#! /opt/local/bin/octave -q --persist

setenv("GNUTERM","X11");
addpath('../../');

%% Load RF_Track

RF_Track;

global Bunch
Bunch.mass = 938.272046; % MeV/c/c
Bunch.population = 1e9; % nb. of particles per bunch
Bunch.charge = 1; % units of e+

Nparticles = 100000;
randn("seed", 12345);
T = [ randn(Nparticles,5) 5000 * ones(Nparticles,1) ];
T(:,2) = 0;
T(:,4) = 0;
B0 = Bunch6d(Bunch.mass, ...
             Bunch.population, ...
             Bunch.charge, T);

SC = SpaceCharge_PIC_FreeSpace(64,64,128);

tic
for i=1:50
    F = SC.compute_force(B0);
    B0.apply_force(F, 0.5);
    %T0 = B0.get_phase_space("%X %Y %Z");
    %scatter3(T0(:,1), T0(:,2), T0(:,3));
    %axis([ -20 20 -20 20 -10 10]);
    %drawnow
    %SC.apply_spacecharge_drift(B0, 0.5);
end
time_per_frame = toc/50

A0 = B0.get_phase_space("%x %xp %y %yp %z %K");
save -text "output.dat" A0

T0 = B0.get_phase_space("%X %Y %Z");
scatter3(T0(:,1), T0(:,2), T0(:,3));
axis([ -20 20 -20 20 -10 10]);
drawnow
