RF_Track;

%% 1 GeV proton
Pz = 1000; % MeV/c
B0 = Bunch6dT([ 0 0 0 0 0 Pz RF_Track.protonmass +1 ]);

%% drift with By != 0.0
rho = 10; % m, bending radius
By = Pz * 1e6 / rho  / RF_Track.clight; % T
Brho = By * rho; % T*m

D = Drift(0.5);
D.set_static_Bfield(0, By, 0);

%% 3D tracking
T = TrackingOptions();
T.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45', 'rk8pd'
T.odeint_epsabs = 1e-6;
T.dt_mm = 1; % mm/c
T.open_boundaries = false;

V = Volume();
V.add(D, 0, 0, 0);

V.track(B0, T);
B1 = V.get_bunch_at_s1();
M1 = B1.get_phase_space()

%% Transfer map
B0 = Bunch6d([ 0 0 0 0 0 Pz RF_Track.protonmass +1 ]);

B = SBend(0.5);
B.set_Brho(Brho);
B.set_Bfield(By);
B.set_h(0.0);
A = B.get_angle()
B.set_E1(A/2);
B.set_E2(A/2);

L = Lattice();
L.append(B);

B1 = L.track(B0);
M1 = B1.get_phase_space()
