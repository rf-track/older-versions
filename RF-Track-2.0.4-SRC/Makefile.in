PREFIX = @prefix@

SWIG = swig

GSL_CONFIG = @GSL_CONFIG@
GSL_CFLAGS = $(shell  $(GSL_CONFIG) --cflags) -DHAVE_INLINE
GSL_LDFLAGS =  $(shell  $(GSL_CONFIG) --libs)

FFTW_LDFLAGS = @FFTW_LIB@ -lfftw3_threads -lfftw3

PYTHON_BIN = @PYTHON_BIN@
PYTHON_CONFIG = @PYTHON_CONFIG@
NUMPY_INCLUDES = -I`$(PYTHON_BIN) -c "import numpy ; print(numpy.get_include())"`
PYTHON_CFLAGS = $(filter-out -Wstrict-prototypes,  $(shell $(PYTHON_CONFIG) --cflags)) $(NUMPY_INCLUDES)
PYTHON_LDFLAGS = $(shell $(PYTHON_CONFIG) --ldflags --embed)
MKOCTFILE = @MKOCTFILE@

MKOCTFILE = mkoctfile

include Makefile.objs

LDFLAGS = -lm $(FFTW_LDFLAGS) $(GSL_LDFLAGS)
CXXFLAGS = @DEFAULT_CXXFLAGS@ $(INCLUDE_DIRS) $(GSL_CFLAGS)

all: @make_all@

install: all
	@install_octave@
	@install_python@

# PYTHON

RF_Track.py: _RF_Track.so

_RF_Track.so: src/RF_Track_python_wrap.o $(OBJS)
	@echo "Creating Python module ..."
	@$(CXX) -shared src/RF_Track_python_wrap.o $(OBJS) $(LDFLAGS) $(PYTHON_LDFLAGS) -o _RF_Track.so

src/RF_Track_python_wrap.o: src/RF_Track_python_wrap.cc
	@$(CXX) $(GSL_CFLAGS) $(PYTHON_CFLAGS) $(CXXFLAGS) src/RF_Track_python_wrap.cc -c -o src/RF_Track_python_wrap.o


# OCTAVE

RF_Track.oct: src/RF_Track_octave_wrap.o $(OBJS)
	@echo "Creating Octave module ..."
	@$(MKOCTFILE) $(OBJS) src/RF_Track_octave_wrap.o $(LDFLAGS) -Wno-unused-function -v -o RF_Track.oct

src/RF_Track_octave_wrap.o: src/RF_Track_octave_wrap.cc
	@$(MKOCTFILE) src/RF_Track_octave_wrap.cc -Wno-unused-function $(CXXFLAGS) -c -o src/RF_Track_octave_wrap.o 

# SWIG

swig:
	@echo "Running SWIG-Python ..."
	@$(SWIG) -c++ -python -module RF_Track -o src/RF_Track_python_wrap.cc $(INCLUDE_DIRS) include/RF_Track.i
	@mv src/RF_Track.py RF_Track.py
	@echo "Running SWIG-Octave ..."
	@$(SWIG) -c++ -octave -globals . -module RF_Track -o src/RF_Track_octave_wrap.cc $(INCLUDE_DIRS) include/RF_Track.i

# OBJECTS

src/RF_Track.o: src/RF_Track.cc $(shell find . -name \*.hh)
	@$(CXX) src/RF_Track.cc $(CXXFLAGS) $(GSL_CFLAGS) -c -o $@

src*/%.o : src*/%.cc include*/%.hh
	@echo "Compiling $< ..."
	@$(CXX) $(CXXFLAGS) $(GSL_CFLAGS) $< -c -o $@

src*/%.o : src*/%.cc
	@echo "Compiling $< ..."
	@$(CXX) $(CXXFLAGS) $(GSL_CFLAGS) $< -c -o $@

src-physics/space_charge.o: include-physics/space_charge*.hh
src-physics/space_charge_pic.o: include-physics/space_charge*.hh include-math/greens_functions/*.hh

# CLEAN

clean:
	@rm -f $(OBJS)\
 src/RF_Track_python_wrap.o\
 src/RF_Track_octave_wrap.o\
 RF_Track.oct\
 _RF_Track.so

cleanall: clean
	@rm -f src/RF_Track_octave_wrap.cc src/RF_Track_python_wrap.cc
