/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>

#include "RF_Track.hh"
#include "mesh3d_lint.hh"
#include "vector_field_div_free.hh"

VectorField_divFree::VectorField_divFree(const Mesh3d &Bx,
					 const Mesh3d &By,
					 const Mesh3d &Bz,
					 double x0_, double y0_,  // mm
					 double hx_, double hy_, double hz_ ) : x0(x0_), // mm
										y0(y0_), // mm
										hx(hx_), // mm
										hy(hy_), // mm
										hz(hz_)  // mm
{
  set_Vx_Vy_Vz(Bx,By,Bz);
}

void VectorField_divFree::set_Vx_Vy_Vz(const Mesh3d &Bx, const Mesh3d &By, const Mesh3d &Bz )
{
  const int
    Nx = Bx.size1(),
    Ny = Bx.size2(),
    Nz = Bx.size3();

  mesh_Ax.resize(Nx,Ny,Nz);
  mesh_Ay.resize(Nx,Ny,Nz);
  mesh_Az.resize(Nx,Ny,Nz);

  const Mesh3d_LINT &Bx_ = Bx;
  const Mesh3d_LINT &By_ = By;
  const Mesh3d_LINT &Bz_ = Bz;
  
  auto anti_curl_parallel = [&] (size_t thread, size_t start, size_t end ) {
    for (size_t i=start; i<end; i++) {
      for (int j=0; j<Ny; j++) {
	for (int k=0; k<Nz; k++) {
	  const auto func = [&] (double t ) {
	    const double tx = t*i, ty = t*j, tz = t*k;
	    StaticVector<3> B_vec;
	    B_vec[0] = Bx_(tx,ty,tz); // T
	    B_vec[1] = By_(tx,ty,tz); // T
	    B_vec[2] = Bz_(tx,ty,tz); // T
	    StaticVector<3> tr_vec;
	    tr_vec[0] = tx*hx; // mm
	    tr_vec[1] = ty*hy; // mm
	    tr_vec[2] = tz*hz; // mm
	    return cross(B_vec, tr_vec); // T*mm
	  };
	  const StaticVector<3> &A = [&] () {
	    CumulativeKahanSum<StaticVector<3>> Integral = StaticVector<3>(0.0);
	    const size_t N = std::max(size_t(ceil(sqrt(i*i+j*j+k*k))),size_t(3));
	    for (size_t n = 1; n < N-1; n++) { // trapezoidal rule
	      Integral += 2.0 * func(double(n) / double(N-1));
	    }
	    Integral += func(1.0);
	    return Integral / (2*N);
	  } ();
	  mesh_Ax.elem(i,j,k) = A[0];
	  mesh_Ay.elem(i,j,k) = A[1];
	  mesh_Az.elem(i,j,k) = A[2];
	}
      }
    }
  };
  for_all(RFT::number_of_threads, Nx, anti_curl_parallel);
}

StaticMatrix<3,3> VectorField_divFree::jacobian(double x, double y, double z ) const // x,y,z [mm]
{
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Ax.size1()-1;
  int height_ = mesh_Ax.size2()-1;
  int length_ = mesh_Ax.size3()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_ || z<0.0 || z>length_)
    return StaticMatrix<3,3>(0.0);
  const double inv_hxhx = 1.0/(hx*hx);
  const double inv_hxhy = 1.0/(hx*hy);
  const double inv_hxhz = 1.0/(hx*hz);
  const double inv_hyhy = 1.0/(hy*hy);
  const double inv_hyhz = 1.0/(hy*hz);
  const double inv_hzhz = 1.0/(hz*hz);
  const auto d2Ax_dxy = mesh_Ax.deriv2_xy(x,y,z)*inv_hxhy; //
  const auto d2Ax_dyz = mesh_Ax.deriv2_yz(x,y,z)*inv_hyhz; //
  const auto d2Ax_dxz = mesh_Ax.deriv2_xz(x,y,z)*inv_hxhz; //
  const auto d2Ax_dy2 = mesh_Ax.deriv2_y2(x,y,z)*inv_hyhy; //
  const auto d2Ax_dz2 = mesh_Ax.deriv2_z2(x,y,z)*inv_hzhz; //
  const auto d2Ay_dxy = mesh_Ay.deriv2_xy(x,y,z)*inv_hxhy; //
  const auto d2Ay_dxz = mesh_Ay.deriv2_xz(x,y,z)*inv_hxhz; //
  const auto d2Ay_dyz = mesh_Ay.deriv2_yz(x,y,z)*inv_hyhz; //
  const auto d2Ay_dx2 = mesh_Ay.deriv2_x2(x,y,z)*inv_hxhx; //
  const auto d2Ay_dz2 = mesh_Ay.deriv2_z2(x,y,z)*inv_hzhz; //
  const auto d2Az_dxy = mesh_Az.deriv2_xy(x,y,z)*inv_hxhy; //
  const auto d2Az_dxz = mesh_Az.deriv2_xz(x,y,z)*inv_hxhz; //
  const auto d2Az_dyz = mesh_Az.deriv2_yz(x,y,z)*inv_hyhz; //
  const auto d2Az_dx2 = mesh_Az.deriv2_x2(x,y,z)*inv_hxhx; //
  const auto d2Az_dy2 = mesh_Az.deriv2_y2(x,y,z)*inv_hyhy; //
  return StaticMatrix<3,3>(d2Az_dxy-d2Ay_dxz, d2Az_dy2-d2Ay_dyz, d2Az_dyz-d2Ay_dz2,
			   d2Ax_dxz-d2Az_dx2, d2Ax_dyz-d2Az_dxy, d2Ax_dz2-d2Az_dxz,
			   d2Ay_dx2-d2Ax_dxy, d2Ay_dxy-d2Ax_dy2, d2Ay_dxz-d2Ax_dyz); //
}

StaticVector<3> VectorField_divFree::operator() (double x /* mm */, double y /* mm */, double z /* mm */  ) const
{
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Ax.size1()-1;
  int height_ = mesh_Ax.size2()-1;
  int length_ = mesh_Ax.size3()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_ || z<0.0 || z>length_)
    return StaticVector<3>(0.0); 
  const auto dAx_dy = mesh_Ax.deriv_y(x,y,z)/hy; //
  const auto dAx_dz = mesh_Ax.deriv_z(x,y,z)/hz; //
  const auto dAy_dx = mesh_Ay.deriv_x(x,y,z)/hx; //
  const auto dAy_dz = mesh_Ay.deriv_z(x,y,z)/hz; //
  const auto dAz_dx = mesh_Az.deriv_x(x,y,z)/hx; //
  const auto dAz_dy = mesh_Az.deriv_y(x,y,z)/hy; //
  return StaticVector<3>(dAz_dy-dAy_dz, dAx_dz-dAz_dx, dAy_dx-dAx_dy); //
}
