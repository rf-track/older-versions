/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>

#include "RF_Track.hh"
#include "scalar_field.hh"

ScalarField::ScalarField(const Mesh3d &Phi,
			 double x0_, double y0_,  // mm
			 double hx_, double hy_, double hz_ ) : mesh_Phi(Phi),
								x0(x0_), // mm
								y0(y0_), // mm
								hx(hx_), // mm
								hy(hy_), // mm
								hz(hz_)  // mm
{}
double ScalarField::operator() (double x /* mm */, double y /* mm */, double z /* mm */  ) const
{
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Phi.size1()-1;
  int height_ = mesh_Phi.size2()-1;
  int length_ = mesh_Phi.size3()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_ || z<0.0 || z>length_)
    return 0.0;
  return mesh_Phi(x,y,z); //
}

StaticVector<3> ScalarField::grad(double x, double y, double z ) const // x,y,z [mm]
{
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Phi.size1()-1;
  int height_ = mesh_Phi.size2()-1;
  int length_ = mesh_Phi.size3()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_ || z<0.0 || z>length_)
    return StaticVector<3>(0.0);
  return StaticVector<3>(mesh_Phi.deriv_x(x,y,z)/hx,
			 mesh_Phi.deriv_y(x,y,z)/hy,
			 mesh_Phi.deriv_z(x,y,z)/hz); //
}
