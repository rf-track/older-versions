/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "greens_functions/coulomb.hh"
#include "mesh3d.hh"
#include "fftw_mesh3d.hh"

namespace GreensFunction {
  template <>
  void compute_mesh(IntegratedCoulomb greens_function, fftwMesh3d &mesh_G, double dx, double dy, double dz, double coasting )
  {
    // Computes the Green's function, with 8-fold mirroring 
    const size_t Nthreads = RFT::number_of_threads;
    const size_t Nx = mesh_G.size1();
    const size_t Ny = mesh_G.size2();
    const size_t Nz = mesh_G.size3();
    const size_t Nxx = Nx>>1;
    const size_t Nyy = Ny>>1;
    const size_t Nzz = Nz>>1;
    static Mesh3d F; // to avoid continuous reallocations
    if (coasting==0.0) {
      F.resize(Nxx+2, Nyy+2, Nzz+2); // indefinite integral
      // indefinite integral: integrate(integrate(integrate(1/sqrt(x^2+y^2+(g*z)^2),x),y),z)/(dx*dy*dz);
      auto Func = [] (double x, double y, double z ) {
	if (x==0.0 && y==0.0) return 0.0;
	if (y==0.0 && z==0.0) return 0.0;
	if (z==0.0 && x==0.0) return 0.0;
	const double xy = x*y;
	const double zx = z*x;
	const double yz = y*z;
	const double x2 = x*x;
	const double y2 = y*y;
	const double z2 = z*z;
	if (x==0.0) return 0.5*yz*log(y2+z2);
	if (y==0.0) return 0.5*zx*log(z2+x2);
	if (z==0.0) return 0.5*xy*log(x2+y2);
	const double r = hypot(x, y, z);
	const double rx = r*x;
	const double ry = r*y;
	const double rz = r*z;
	return xy*log(z+r) + yz*log(x+r) + zx*log(y+r) - 0.5 * (x2*atan(yz/rx) + y2*atan(zx/ry) + z2*atan(xy/rz));
      };
      const double hdx = 0.5*dx;
      const double hdy = 0.5*dy;
      const double hdz = 0.5*dz;
      auto compute_F_parallel = [&] (size_t thread, size_t start, size_t end ) -> void {
	for (size_t i=start; i<end; i++) {
	  const double x0 = i*dx;
	  for (size_t j=0; j<F.size2(); j++) {
	    const double y0 = j*dy;
	    for (size_t k=0; k<F.size3(); k++) {
	      const double z0 = k*dz;
	      F.elem(i,j,k) = Func(x0-hdx, y0-hdy, z0-hdz) / (4*M_PI*dx*dy*dz);
	    }
	  }
	}
      };
      for_all(Nthreads, F.size1(), compute_F_parallel);
    }
    auto compute_greens_function_parallel = [&] (size_t thread, size_t start, size_t end ) -> void {
      for (size_t i=start; i<end; i++) {
	for (size_t j=0; j<=Nyy; j++) {
	  for (size_t k=0; k<=Nzz; k++) {
	    const double elem_ijk = [&] () {
	      if (coasting==0.0) {
		return
		+F.elem(i+1, j+1, k+1)
		+F.elem(i,   j,   k+1)
		+F.elem(i,   j+1, k)
		+F.elem(i+1, j,   k)
		-F.elem(i,   j+1, k+1)
		-F.elem(i+1, j,   k+1)
		-F.elem(i+1, j+1, k)
		-F.elem(i,   j,   k);
	      }
	      const double x0 = i*dx;
	      const double y0 = j*dy;
	      const double z0 = k*dz;
	      return greens_function(coasting,x0,y0,z0,dx,dy,dz);
	    } ();
	    mesh_G.elem(i, j, k) = elem_ijk;
	    if (i!=Nxx) {
	      mesh_G.elem(Nx-i, j, k) = elem_ijk;
	      if (j!=Nyy) {
		mesh_G.elem(Nx-i, Ny-j, k) = elem_ijk;
		if (k!=Nzz) {
		  mesh_G.elem(Nx-i, Ny-j, Nz-k) = elem_ijk;
		}
	      }
	      if (k!=Nzz) {
		mesh_G.elem(Nx-i, j, Nz-k) = elem_ijk;
	      }
	    }
	    if (j!=Nyy) {
	      mesh_G.elem(i, Ny-j, k) = elem_ijk;
	      if (k!=Nzz) {
		mesh_G.elem(i, Ny-j, Nz-k) = elem_ijk;
	      }
	    }
	    if (k!=Nzz) {
	      mesh_G.elem(i, j, Nz-k) = elem_ijk;
	    }
	  }
	}
      }
    };
    for_all(Nthreads, Nxx+1, compute_greens_function_parallel);
  }
}
