/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "stats.hh"

double stats_wcovariance_m(const double w[], size_t wstride,
			   const double data1[], const size_t stride1,
			   const double data2[], const size_t stride2,
			   const size_t n,
			   const double mean1,
			   const double mean2 )
{
  double sum = 0.0;
  double wsum = 0.0;
  double w2sum = 0.0;
  for (size_t i=0; i<n; i++) {
    sum += w[0] * (data1[0] - mean1) * (data2[0] - mean2);
    wsum += w[0];
    w2sum += w[0] * w[0];
    data1 += stride1;
    data2 += stride2;
    w += wstride;
  }
  // this expression is consistent with gsl_stats_wvariance(), see: https://www.gnu.org/software/gsl/doc/html/statistics.html#c.gsl_stats_wvariance
  return wsum * sum / (wsum * wsum - w2sum);
}
