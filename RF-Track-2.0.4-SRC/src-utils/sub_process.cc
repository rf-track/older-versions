/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <algorithm>
#include <unistd.h>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <iostream>

#include <sys/socket.h>
#include <signal.h>
#include <stdarg.h>

#include "sub_process.hh"

SubProcess::SubProcess(const char *command )
{
  int send_pipe[2];
  int recv_pipe[2];
  pipe(send_pipe);
  pipe(recv_pipe);
  if ((PID=fork())<0) {
    fputs("can't fork\n", ::stderr);
    exit(-1);
  } else if (PID==0) { // child
    dup2(send_pipe[0], STDIN_FILENO);
    dup2(recv_pipe[1], STDOUT_FILENO);
    ::close(send_pipe[0]);
    ::close(send_pipe[1]);
    ::close(recv_pipe[0]);
    ::close(recv_pipe[1]);
    execl("/bin/sh", "/bin/sh", "-c", command, NULL);
    _exit(0);
  }
  // parent
  send_fd = send_pipe[1];
  recv_fd = recv_pipe[0];
  ::close(send_pipe[0]);
  ::close(recv_pipe[1]);
}

SubProcess::~SubProcess()
{
  if (PID) {
    ::close(send_fd);
    ::close(recv_fd);
    ::kill(PID, SIGKILL);
  }
}
