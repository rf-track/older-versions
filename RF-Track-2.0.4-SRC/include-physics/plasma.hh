/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef plasma_hh
#define plasma_hh

#include <unordered_map>
#include <algorithm>

#include "RF_Track.hh"
#include "mesh3d.hh"
#include "particle.hh"
#include "particle_key.hh"
#include "static_matrix.hh"

#include "scalar_field.hh"
#include "vector_field.hh"

#include "static_electric_field_map.hh"
#include "static_magnetic_field_map.hh"

class Plasma {

  double mass; // [MeV/c/c] plasma's particle mass
  double Q; // [e+] plasma's particle charge

  double D_ele_r; // c, transverse velocity spread, in the rest frame of the electrons
  double D_ele_l; // c, longitudinal velocity spread, in the rest frame of the electrons

  // TO BE USED
  ScalarField plasma_density;  // #/m^3, number density
  VectorField plasma_velocity; // c, velocity
  // END OF TO BE USED
  
public:

  struct Cell : public StaticVector<4> {
    // double ne; // #/m^3, number density
    // StaticVector<3> P; // MeV/c, momentum
  public:
    Cell() = default;
    Cell(double x ) : StaticVector<4>(x) {}
    Cell(const StaticVector<4> &v ) : StaticVector<4>(v) {}
    Cell(double a, double b, double c, double d ) : StaticVector<4>(a,b,c,d) {}
    Cell(const double &number_density, const StaticVector<3> &P ) : StaticVector<4>(number_density, P[0], P[1], P[2]) {}
    void set_momentum(const StaticVector<3> &P ) { (*this)[1] = P[0]; (*this)[2] = P[1]; (*this)[3] = P[2]; }
    void set_velocity(double mass, const StaticVector<3> &V ) { const double gamma =  1.0 / sqrt(1.0 - dot(V,V)); set_momentum(mass * gamma * V); }
    StaticVector<3>   get_velocity(double mass ) const /* c */ { const auto &P = get_momentum(); return P / hypot(mass, P); }
    StaticVector<3>   get_momentum() const /* MeV/c /m^3 */ { return StaticVector<3>((*this)[1], (*this)[2], (*this)[3]); }
    StaticVector<3>   get_momentum_density() const /* MeV/c /m^3 */ { return (*this)[0] * get_momentum(); }
    double            get_energy(double mass ) const /* c */ { const auto &P = get_momentum(); return hypot(mass, P); }
    double            get_debye_length(double mass, double kbT /* eV */ ) const; // m, in the reference frame of the electrons
    double            get_number_density() const /* #/m^3 */ { return (*this)[0]; }
    double            get_plasma_parameter(double mass, double kbT /* eV */ ) const { const double L = get_debye_length(mass, kbT); return get_number_density()*L*L*L; }
  };

  static Cell null_cell;

private:

    // mesh
  TMesh3d_CINT<Cell>
    plasma, // current plasma
    plasma_next; // buffer for next step
  
  double rx, ry, length; // mm (radiusx, radiusy, and length)
  StaticVector<3> static_Bfield; // [T] static magnetic field embedding the RF_FieldMap (e.g. solenoid)  (should this really be here?)

protected:

  size_t size1() const { return plasma.size1(); }
  size_t size2() const { return plasma.size2(); }

  StaticVector<3> get_mesh_coordinates(double x, double y, double z ) const;
  StaticVector<3> get_mesh_coordinates(const StaticVector<3> &r ) const { return get_mesh_coordinates(r[0],r[1],r[2]); }

  Cell   get_state(double x /* mm */, double y /* mm */, double z /* mm */ ) const;
  Cell   get_state_bnd(double x /* mm */, double y /* mm */, double z /* mm */ ) const;
  Cell   get_state(const StaticVector<3> &r_mesh /* mm */ ) const { return get_state(r_mesh[0], r_mesh[1], r_mesh[2]); }
  Cell   get_state_bnd(const StaticVector<3> &r_mesh /* mm */ ) const { return get_state_bnd(r_mesh[0], r_mesh[1], r_mesh[2]); }
  double get_debye_length(const Cell &cell ) const /* m */ { const double kbT = get_temperature(); return cell.get_debye_length(mass, kbT); }
  double get_plasma_parameter(const Cell &cell ) const /* m */ { const double kbT = get_temperature(); return cell.get_plasma_parameter(mass, kbT); }
  double get_average_debye_length() const; // m
  double get_average_plasma_parameter() const; // #
  StaticVector<3> get_average_velocity() const; // c
  
public:

  Plasma(double length = 0.0 /* m */, double rx = 0.0 /* m */, double ry = 0.0 /* m */, double ne = 0.0 /* #/m**3 */, double Vz=0.0 /* c */, double D_ele_r=0.0 /* c */, double D_ele_l=0.0 /* c */ );
  virtual ~Plasma() = default;

  std::pair<Static_Electric_FieldMap,Static_Magnetic_FieldMap> get_self_fields() const;

  double get_rx() const { return rx/1e3; } // m
  double get_ry() const { return ry/1e3; } // m
  double get_area() const { const double area = rx*ry/1e6; return M_PI*area; } // m^2
  void set_radii(double rx_, double ry_ ) { rx = rx_*1e3; ry = ry_*1e3; } // m
  inline bool is_point_inside_area(double x /* mm */, double y /* mm */ ) const { const double rx_ = rx*rx, ry_ = ry*ry; return rx_*y*y + ry_*x*x < rx_*ry_; }
  
  void set_Q(double Q_ /* e+ */ ) { Q = Q_; }
  void set_mass(double mass_ /* MeV */ ) { mass = mass_; }
  void set_length(double l ) { length = l*1e3; }
  virtual void set_temperature(double kb_T_r /* eV */, double kb_T_l /* eV */ ); // set transverse and longitudinal temperatures, in the rest frame of the electrons
  void set_plasma_mesh(size_t Nx, size_t Ny, size_t Nz, double density /* #/m^3 */, double Vx /* c */, double Vy /* c */, double Vz /* c */ );
  void set_plasma_mesh(size_t Nz, const MatrixNd &density /* #/m^3 */, const MatrixNd &Vx /* c */, const MatrixNd &Vy /* c */, const MatrixNd &Vz /* c */ );
  void set_plasma_mesh(const Mesh3d &density /* #/m^3 */, const Mesh3d &Vx /* c */, const Mesh3d &Vy /* c */, const Mesh3d &Vz /* c */ );
  void set_nsteps(size_t nsteps );
  
  double get_Q() const /* e+ */ { return Q; }
  double get_mass() const /* MeV */ { return mass; }
  double get_length() const /* m */ { return length/1e3; }
  double get_density(double x /* mm */, double y /* mm */, double z /* mm */ ) const { return get_state(x,y,z).get_number_density(); } // #/m^3
  double get_D_ele_r() const { return D_ele_r; } // c, velocity spread, in the rest frame of the electrons
  double get_D_ele_l() const { return D_ele_l; } // c, velocity spread, in the rest frame of the electrons
  double get_temperature() const { return 1e6*get_mass()*(2*D_ele_r*D_ele_r+D_ele_l*D_ele_l)/3.0; } // eV, average temperature
  double get_debye_length(double x /* mm */, double y /* mm */, double z /* mm */ ) const /* m */ { const double kbT = get_temperature(); return get_state(x,y,z).get_debye_length(mass, kbT); }
  double get_plasma_parameter(double x /* mm */, double y /* mm */, double z /* mm */ ) const /* m */ { const double kbT = get_temperature(); return get_state(x,y,z).get_plasma_parameter(mass, kbT); }
  StaticVector<3> get_velocity(double x /* mm */, double y /* mm */, double z /* mm */ ) const { return get_state(x,y,z).get_velocity(mass); } // c
  StaticVector<3> get_current_density(double x /* mm */, double y /* mm */, double z /* mm */ ) const // A/m^2
  {
    // e*c/m^3 = 4.803204505713468e-11 A/m^2
    // e*c/m^3 = (1 / 20819434167.55394) A/m^2
    return Q * get_density(x,y,z) * get_velocity(x,y,z) / 20819434167.55394;
  }
  Mesh3d get_density_mesh() const;

  const StaticVector<3> &get_static_Bfield() const { return static_Bfield; }
  void set_static_Bfield(double Bx, double By, double Bz ) { static_Bfield = std::array<double,3>{ { Bx, By, Bz } }; }
  
  // tracking
  void project();
  void advect(double dt_mm ); // dt [mm/c]
  void apply_momentum_through_dt(const TMesh3d<StaticVector<3>> &dP, double dt_mm ); // dP [MeV/c]; dt [mm/c] mesh
  void apply_momentum_through_dS(const TMesh3d<StaticVector<3>> &dP, double dS_mm ); // dP [MeV/c]; dS [mm] mesh

};

#endif /* plasma_hh */
