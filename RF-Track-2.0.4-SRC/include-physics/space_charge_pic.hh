/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef space_charge_pic_hh
#define space_charge_pic_hh

#include "space_charge.hh"
#include "mesh3d.hh"
#include "fftw_mesh3d.hh"
#include "static_electric_field_map.hh"
#include "static_magnetic_field_map.hh"
#include "greens_functions/coulomb.hh"

template <typename GREENS_FUNCTION>
class SpaceCharge_PIC : public SpaceCharge {
protected:
  
  // particle-in-cell method
  size_t Nx, Ny, Nz;
  
  fftwMesh3d mesh_rho; // charge density
  fftwMesh3d mesh_G;   // Green's function

  fftwComplexMesh3d mesh_rho_hat; // fft of the charge density
  fftwComplexMesh3d mesh_G_hat; // fft of the Green's function
  fftwComplexMesh3d mesh_smooth_hat; // fft transform of the smooth function

  TMesh3d_CINT<StaticVector<4>> mesh_A; // electromagnetic four-potential
  struct {
    double x0, y0, z0;
    double hx, hy, hz;
  } mesh;
  
  fftw_plan p1, p2, p3;

  bool init_pic(size_t Nx, size_t Ny, size_t Nz );

  GREENS_FUNCTION greens_function;
  
  template <typename BUNCH_TYPE>
  void compute_force_(MatrixNd &force, const BUNCH_TYPE &bunch, const ParticleSelector &selector );

  double S_mirror = GSL_NAN; // mm
  double smooth_radius = 3.0; // mesh cells
  
public:

  SpaceCharge_PIC(size_t Nx_=16, size_t Ny_=16, size_t Nz_=16 ) { init_pic(Nx_, Ny_, Nz_ ); }
  SpaceCharge_PIC(const SpaceCharge_PIC &sc ) { init_pic(sc.Nx, sc.Ny, sc.Nz); }
  ~SpaceCharge_PIC();

  SpaceCharge_PIC &operator = (const SpaceCharge_PIC &sc );

  // compute the force [MeV/m] and return it as a Nx3 matrix
  void compute_force(MatrixNd &force, const Bunch6d  &bunch, const ParticleSelector &selector = ParticleSelector()) override { compute_force_(force, bunch, selector); }
  void compute_force(MatrixNd &force, const Bunch6dT &bunch, const ParticleSelector &selector = ParticleSelector()) override { compute_force_(force, bunch, selector); }

  std::pair<Static_Electric_FieldMap,Static_Magnetic_FieldMap> get_fields() const;
  Static_Electric_FieldMap get_electric_field() const;
  Static_Magnetic_FieldMap get_magnetic_field() const;

  void set_mirror(double S_mirror_ /* m */ ) { S_mirror = S_mirror_ * 1e3; }
  void set_smooth(const double radius ); // units of mesh cells, default = 3

  double get_mirror() const { return S_mirror / 1e3; } // m
  double get_smooth() const { return smooth_radius; } // units of mesh cells, default = 3
  
};

// template specialization
typedef SpaceCharge_PIC<GreensFunction::IntegratedCoulomb> SpaceCharge_PIC_FreeSpace;

#endif /* space_charge_pic_hh */
