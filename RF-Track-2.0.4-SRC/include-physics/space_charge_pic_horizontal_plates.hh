/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef space_charge_pic_horizontal_plates_hh
#define space_charge_pic_horizontal_plates_hh

#include "space_charge_pic.hh"
#include "greens_functions/coulomb_horizontal_plates.hh"

class SpaceCharge_PIC_HorizontalPlates : public SpaceCharge_PIC<GreensFunction::IntegratedCoulomb_HorizontalPlates> {
public:
  
  SpaceCharge_PIC_HorizontalPlates(size_t Nx_=1, size_t Ny_=1, size_t Nz_=1, double h=1.0 /* m */  ) : SpaceCharge_PIC<GreensFunction::IntegratedCoulomb_HorizontalPlates>(Nx_, Ny_, Nz_) { greens_function.h = h*1e3; }

  void set_half_gap(double h /* m */ ) { greens_function.h = h*1e3; }
  double get_half_gap() const { return greens_function.h/1e3; } // m
  
};

#endif /* space_charge_pic_horizontal_plates_hh */
