/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef lorentz_boost_hh
#define lorentz_boost_hh

#include "static_matrix.hh"
#include "four_vector.hh"

namespace {
  // L = lorentz_boost_matrix(v);
  // e.g. A' = L * A; returns the four-vector A as seen from a frame A' moving with velocity "v" w.r.t. A
  StaticMatrix<4,4> lorentz_boost_matrix(const StaticVector<3> &v )
  {
    const double v_sqr = dot(v,v);
    const double gamma = 1.0 / sqrt(1.0-v_sqr);
    const double gamma_m1 = gamma - 1.0;
    StaticMatrix<4,4> M;
    M[0][0] = gamma;
    M[0][1] = M[1][0] = -gamma*v[0];
    M[0][2] = M[2][0] = -gamma*v[1];
    M[0][3] = M[3][0] = -gamma*v[2];
    if (v_sqr>0.0) {
      M[1][1] = 1.0 + gamma_m1 * v[0]*v[0] / v_sqr;
      M[2][2] = 1.0 + gamma_m1 * v[1]*v[1] / v_sqr;
      M[3][3] = 1.0 + gamma_m1 * v[2]*v[2] / v_sqr;
      M[1][2] = M[2][1] = gamma_m1 * v[0]*v[1] / v_sqr;
      M[1][3] = M[3][1] = gamma_m1 * v[0]*v[2] / v_sqr;
      M[2][3] = M[3][2] = gamma_m1 * v[1]*v[2] / v_sqr;
    } else {
      M[1][1] = M[2][2] = M[3][3] = 1.0;
      M[1][2] = M[2][1] = M[1][3] = M[3][1] = M[2][3] = M[3][2] = 0.0;
    }
    return M;  
  }
  // A' = lorentz_boost(v, A);
  // returns the four-vector A as seen from a frame A' moving with velocity "v" w.r.t. A
  StaticVector<4> lorentz_boost(const StaticVector<3> &v, const StaticVector<4> &x )
  {
    const double v_sqr = dot(v,v);
    if (v_sqr == 0.0)
      return x;
    const double gamma = 1.0 / sqrt(1.0-v_sqr);
    const StaticVector<3> x_vec = StaticVector<3>(x[1], x[2], x[3]);
    const double dot_v_x_vec = dot(v, x_vec);
    const double ret0 = gamma * (x[0] - dot_v_x_vec);
    const StaticVector<3> ret_vec = x_vec + (gamma - 1.0) * dot_v_x_vec * v  / v_sqr - gamma * v * x[0];
    return StaticVector<4>(ret0, ret_vec[0], ret_vec[1], ret_vec[2]);
  }
  // A' = lorentz_boost(v, A);
  // returns the four-vector A as seen from a frame A' moving with velocity "v" w.r.t. A
  FourVector lorentz_boost(const StaticVector<3> &v, const FourVector &x )
  {
    const double v_sqr = dot(v,v);
    if (v_sqr == 0.0)
      return x;
    const double gamma = 1.0 / sqrt(1.0-v_sqr);
    const StaticVector<3> x_vec = StaticVector<3>(x[1], x[2], x[3]);
    const double dot_v_x_vec = dot(v, x_vec);
    const double ret0 = gamma * (x[0] - dot_v_x_vec);
    const StaticVector<3> ret_vec = x_vec + (gamma - 1.0) * dot_v_x_vec * v  / v_sqr - gamma * v * x[0];
    return FourVector(ret0, ret_vec[0], ret_vec[1], ret_vec[2]);
  }
}

#endif /* lorentz_boost.hh */
