/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef compton_hh
#define compton_hh

#include <limits>
#include "four_momentum.hh"
#include "constants.hh"

class Compton {
  
  // precomputed variables
  double K, _integrated_y_min, _total, x, lambda, P;
  
  Compton() = default;

protected:

  double diff(double y ) const { // differential cross section # Landau (86.15)
    auto sqr = [] (double x) { return x*x; };
    const double X0 = K*(sqr(1/x-1/y)+1/x-1/y+(x/y+y/x)/4)/sqr(x); // fm^2, Klein-Nishina, Landau
    if (lambda==0||P==0.0)
      return X0;
    const double r = (2*y)/(x*(1-2*y));
    const double X1 = K*(1-2*r)*r*(1-y); // fm^2, polarization, TESLA TDR Part VI page 29
    return X0 + lambda*P*X1;
  }

  double integrated(double y ) const { // indefinite integral
    auto sqr = [] (double x) { return x*x; };
    const double x2 = sqr(x), y2 = sqr(y);
    const double X0 = K*((x/4-2/x-1)*log(y)+y2/(8*x)+(1/x+1/x2)*y-1/y)/x2; // fm^2
    if (lambda==0||P==0.0)
      return X0;
    const double X1 = K*((x+2)*log(1/(2*y-1))/2+1/(2*y-1)+((x+2)*y-x)*y)/2/x2; // fm^2, polarization
    return X0 + lambda*P*X1;
  }

public:

  // relativistic invariants
  double y_min, y_max; // see Landau

  Compton(const FourMomentum &p /* massive particle */, const FourMomentum &k /* photon */, double lambda=0.0 /* helicity of the electron */, double P=0.0 /* polarization of the photon */ ) : lambda(lambda), P(P) {
    const double m2 = sqr(p); // MeV^2
    const double s = sqr(p+k); // Mandelstam variable
    K = 8 * M_PI * 2.0734979 / m2; // fm^2, (e^2/ 4 pi epsilon0 MeV )**2 = 2.0734979 fm^2
    x = s / m2 - 1; // # Landau (86.15)
    y_min = x / (1+x);
    y_max = x;
    _integrated_y_min = integrated(y_min); // with no polarization : K*((1-x/4+2/x)*log1p(1/x)-1+y_min/(8*(x+1)))/x2; // fm^2
    _total = integrated(y_max) - _integrated_y_min; ////// (K/4)*((1-(4+8/x)/x)*log1p(x)+8/x+0.5-0.5/sqr(1+x))/x; // fm^2
#ifdef DEBUG
    std::cerr << "\n s = " << s << "\n";
    std::cerr << " x = (s-m^2) / m^2 = " << x << "\n";
    std::cerr << " y_min = " << y_min << "\n";
    std::cerr << " y_max = " << y_max << "\n\n";
    std::cerr << " integrated_min = " << _integrated_y_min << std::endl;
    std::cerr << " integrated_min = " << integrated(y_min) << std::endl;
    std::cerr << " integrated total = " << integrated(y_max) - _integrated_y_min << std::endl;
    std::cerr << " integrated total = " << integrated(y_max) - integrated(y_min) << std::endl;
    std::cerr << " analytic total cross section = " << _total << " fm^2\n\n";
#endif
  }
  
  double total() const { // total cross section # Landau (86.16)
    return _total; // fm^2
  }
  
  double integral(double y ) const { // Y in ymin..ymax
    return integrated(y) - _integrated_y_min; // fm^2
  }
  
};

#endif /* compton_hh */
