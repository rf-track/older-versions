/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef constants_hh
#define constants_hh

#include <cmath>

/** physical constants from http://physics.nist.gov/cuu/Constants/index.html */
#define EMASS 0.51099895000 /** electron mass [MeV/c/c] */
#define PMASS 938.2720813 /** proton mass [MeV/c/c] */
#define C_LIGHT 299792458.0 /** velocity of light [m/s] */

#endif /* constants_hh */
