/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef four_momentum_hh
#define four_momentum_hh

#include <limits>
#include "four_vector.hh" 
#include "static_vector.hh"

class FourMomentum : public FourVector {
  double mass_sqr = 0.0; // MeV/c^2, rest mass, -1 means not initialized
public:
  FourMomentum(double mass, const StaticVector<3> &P ) : mass_sqr(mass*mass)
  {
    v[0] = hypot(mass, P[0], P[1], P[2]);
    v[1] = P[0];
    v[2] = P[1];
    v[3] = P[2];
  }
  FourMomentum(double mass, double Px, double Py, double Pz ) : mass_sqr(mass*mass)
  {
    v[0] = hypot(mass, Px, Py, Pz);
    v[1] = Px;
    v[2] = Py;
    v[3] = Pz;
  }
  FourMomentum(const FourVector &p ) : FourVector(p) { mass_sqr = v[0]*v[0] - v[1]*v[1] - v[2]*v[2] - v[3]*v[3]; }
  FourMomentum() = default;
  ~FourMomentum() = default;

  friend double sqr(const FourMomentum &p ) { return p.mass_sqr; }

  double get_mass() const { return sqrt(mass_sqr); }
  
  double get_gamma() const { return mass_sqr==0.0 ? (v[0]==0.0 ? 0 : std::numeric_limits<double>::infinity()) : (v[0] / sqrt(mass_sqr)); }
  double get_beta() const { return mass_sqr==0.0 ? (v[0]==0.0 ? 0 : 1) : sqrt(1-mass_sqr/(v[0]*v[0])); } // c
  double get_beta_gamma() const { return mass_sqr==0.0 ? (v[0]==0.0 ? 0 : std::numeric_limits<double>::infinity()) : sqrt(v[0]*v[0]/mass_sqr - 1); }

  StaticVector<3> get_Px_Py_Pz() const { return StaticVector<3>(v[1], v[2], v[3]); } // MeV/c
  StaticVector<3> get_Vx_Vy_Vz() const // units of c
  {
    if (mass_sqr==0.0 && v[0]==0.0) return StaticVector<3>(0.0);
    return get_Px_Py_Pz() / v[0]; // c
  }

  friend double operator * (const FourMomentum &a, const FourMomentum &b )
  {
    if (&a==&b) return a.mass_sqr;
    return a[0]*b[0]-a[1]*b[1]-a[2]*b[2]-a[3]*b[3];
  }
  
};

#endif /* four_momentum_hh */
