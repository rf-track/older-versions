/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef thomson_hh
#define thomson_hh

#include <limits>
#include "four_momentum.hh"
#include "constants.hh"

class Thomson {
  
  // precomputed variables
  double K, _integrated_theta_min, _total;
  
protected:

  double diff(double theta ) const { // differential cross section
    const double cos_theta = cos(theta);
    return K*(cos_theta*cos_theta+1); // fm^2
  }

  double integrated(double theta ) const { // indefinite integral
    const double cos_theta = cos(theta);
    return -K*(cos_theta*cos_theta/3+1)*cos_theta; // fm^2
  }

public:

  double theta_min, theta_max;

  Thomson(const FourMomentum &p /* massive particle */, const FourMomentum &k /* photon */ ) {
    K = M_PI * 2.0734979 / sqr(p); // fm^2, (e^2/ 4 pi epsilon0 MeV )**2 = 2.0734979 fm^2
    theta_min = 0;
    theta_max = M_PI;
    _integrated_theta_min = integrated(theta_min); // fm^2
    _total = integrated(theta_max) - _integrated_theta_min;
    /*
      std::cerr << " theta_min = " << theta_min << "\n";
      std::cerr << " theta_max = " << theta_max << "\n\n";
      std::cerr << " integrated_min = " << _integrated_theta_min << std::endl;
      std::cerr << " integrated_min = " << integrated(theta_min) << std::endl;
      std::cerr << " integrated total = " << integrated(theta_max) - _integrated_theta_min << std::endl;
      std::cerr << " integrated total = " << integrated(theta_max) - integrated(theta_min) << std::endl;
      std::cerr << " analytic total cross section = " << _total << " fm^2\n\n";
    */
  }
  
  double total() const { // total cross section # Landau (86.16)
    return _total; // fm^2
  }
  
  double integral(double y ) const { // Y in ymin..ymax
    return integrated(y) - _integrated_theta_min; // fm^2
  }
  
};

#endif /* thomson_hh */
