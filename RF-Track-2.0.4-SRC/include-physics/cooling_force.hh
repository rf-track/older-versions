/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef cooling_force_hh
#define cooling_force_hh

#include <utility>
#include "mesh2d.hh"

class CoolingForce {

  // auxiliary variables
  TMesh2d_LINT<StaticVector<2>> f_unmagnetized; // radial, longitudinal
  double Ur_min, Ur_max, Ur_step;
  double Ul_min, Ul_max, Ul_step;

  TMesh2d_LINT<StaticVector<2>> f_magnetized; // radial, longitudinal
  double Mr_min, Mr_max, Mr_step;
  double Ml_min, Ml_max, Ml_step;

  double D_ele_r, D_ele_l;

public:

  CoolingForce() = default;
  CoolingForce(double D_ele_r_ /* c */, double D_ele_l_ /* c */ ) { init_cooling_force(D_ele_r_, D_ele_l_); }
  
  double get_D_ele_r() const { return D_ele_r; } // c, velocity spread, in the rest frame of the electrons
  double get_D_ele_l() const { return D_ele_l; } // c, velocity spread, in the rest frame of the electrons
  
  void init_cooling_force(double D_ele_r /* c */, double D_ele_l /* c */ );

  std::pair<double,double> cooling_force_unmagnetized(double Ur, double Ul ); // return radial, longitudinal
  std::pair<double,double> cooling_force_magnetized(double Ur, double Ul ); // return radial, longitudinal

};

#endif /* cooling_force_hh */
