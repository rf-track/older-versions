/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef space_charge_hh
#define space_charge_hh

#include "bunch6d.hh"
#include "bunch6dt.hh"


class SpaceCharge {
public:

  virtual ~SpaceCharge() = default;

  struct ParticleSelector {
    virtual bool operator () (const Particle  &p ) const { return bool(p); }
    virtual bool operator () (const ParticleT &p ) const { return bool(p); }
    virtual ~ParticleSelector() = default;
  };
  
  virtual void compute_force(MatrixNd &force, const Bunch6d &bunch, const ParticleSelector &selector = ParticleSelector()) = 0;
  virtual void compute_force(MatrixNd &force, const Bunch6dT &bunch, const ParticleSelector &selector = ParticleSelector()) = 0;

  // for SWIG users
  MatrixNd compute_force(const Bunch6d &bunch ) { MatrixNd force; compute_force(force, bunch, ParticleSelector()); return force; }
  MatrixNd compute_force(const Bunch6dT &bunch ) { MatrixNd force; compute_force(force, bunch, ParticleSelector()); return force; }
  
protected:

  // coordinates in co-moving reference frame
  std::vector<StaticVector<3>> position;
  std::vector<StaticVector<3>> velocity;

  StaticVector<3> change_reference_frame(const Bunch6d &bunch, const ParticleSelector &selector );
  StaticVector<3> change_reference_frame(const Bunch6dT &bunch, const ParticleSelector &selector );
  
};

#endif /* space_charge_hh */
