/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* my_std_vector.i */

%{
#include <vector>
%}

#if defined(SWIGOCTAVE)

// Octave Matrix -> C++ std::vector<double>
%typemap(in) const std::vector<double> & {
  const Matrix &matrix = $input.matrix_value();
  $1 = new std::vector<double>(matrix.rows() * matrix.columns());
  int k=0;
  for (int i=0; i<matrix.rows(); i++)
    for (int j=0; j<matrix.columns(); j++)
      (*$1)[k++] = matrix(i,j);
 }

%typemap(freearg) const std::vector<double> & {
  if ($1) delete $1;
 }

%typemap(typecheck) const std::vector<double> & {
  octave_value obj = $input;
  $1 = false;
  if (obj.is_real_scalar() || obj.is_real_matrix()) {
    const Matrix &matrix = obj.matrix_value();
    if (matrix.rows()==1 || matrix.columns()==1) {
      $1 = true;
    }
  }
 }

// C++ std::vector<double> -> Octave Matrix (as a return value)
%typemap(out) std::vector<double> {
  Matrix ret($1.size(), 1);
  for (size_t i=0; i<$1.size(); i++)
    ret(i,0) = $1[i];
  $result = octave_value(ret);
 }

%typemap(out) const std::vector<double> & {
  Matrix ret((*$1).size(), 1);
  for (size_t i=0; i<(*$1).size(); i++)
    ret(i,0) = (*$1)[i];
  $result = octave_value(ret);
 }

// C++ std::vector<double> -> Octave Matrix (as an output argument)
%typemap(in, numinputs=0) std::vector<double> & (std::vector<double> temp ) {
  $1 = &temp;
 }

%typemap(argout) std::vector<double> & {
  Matrix ret((*$1).size(), 1);
  for (size_t i=0; i<(*$1).size(); i++)
    ret(i,0) = (*$1)[i];
  $result->append(ret);
 }

%typemap(freearg,noblock=1) std::vector<double> & {
 }
#endif

#if defined(SWIGPYTHON)
%{
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#define is_array(a) ((a) && PyArray_Check((PyArrayObject *)a))
%}

%init %{
  import_array();
  %}

// NumPy Matrix -> C++ std::vector<double>
%typemap(in) const std::vector<double> & {
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 1, 2);
    npy_intp strides[2] = { 0, 0 };
    npy_intp rows, cols;
    if (PyArray_NDIM(array) == 1) {
      rows = 1;
      cols = PyArray_DIMS(array)[0];
      strides[0] = 0;
      strides[1] = PyArray_STRIDES(array)[0];
    } else {
      rows = PyArray_DIMS(array)[0];
      cols = PyArray_DIMS(array)[1];
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = PyArray_STRIDES(array)[1];
    }
    char *data = PyArray_BYTES(array);
    $1 = new std::vector<double>(rows * cols);
    int k=0;
    for (int i=0; i<rows; i++)
      for (int j=0; j<cols; j++)
        (*$1)[k++] = *(double *)(data + i*strides[0] + j*strides[1]);
    Py_DECREF(array);
  } else {
    $1 = new std::vector<double>(1);
    (*$1)[0] = PyFloat_AsDouble($input);
    Py_DECREF($input);
  }
 }

%typemap(freearg) const std::vector<double> & {
  if ($1) delete $1;
 }

%typemap(typecheck) const std::vector<double> & {
  $1 = false;
  if (is_array($input) || PyFloat_Check($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 1, 2);
    if (PyArray_NDIM(array) == 1) {
      $1 = true;
    }
  }
 }

%typemap(argout,noblock=1) const std::vector<double> & {
 }

// C++ std::vector<double> -> NumPy Matrix (as a return value)
%typemap(out) std::vector<double> {
  npy_intp dimensions[2] = { (npy_intp)$1.size(), npy_intp(1) };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (npy_intp i=0; i<dimensions[0]; i++)
    *(double*)(data + i*strides[0]) = $1[i];
  $result = PyArray_Return(res);
 }

%typemap(out) const std::vector<double> & {
  npy_intp dimensions[2] = { npy_intp((*$1).size()), npy_intp(1) };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (npy_intp i=0; i<dimensions[0]; i++)
    *(double*)(data + i*strides[0]) = (*$1)[i];
  $result = PyArray_Return(res);
}

// C++ std::vector<double> -> NumPy Matrix (as an output argument)
%typemap(in, numinputs=0) std::vector<double> & (std::vector<double> temp ) {
  $1 = &temp;
 }

%typemap(argout) std::vector<double> & {
  npy_intp dimensions[2] = { npy_intp($1->rows()), npy_intp(1) };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (npy_intp i=0; i<dimensions[0]; i++)
      *(double*)(data + i*strides[0]) = (*$1)[i];
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res));
 }

%typemap(freearg,noblock=1) std::vector<double> & {
 }

#endif
