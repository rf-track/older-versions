/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* RF_Track.i */

%include <std_shared_ptr.i>
%include <std_string.i>

%include "my_std_vector.i"
%include "matrixnd.i"
%include "vectornd.i"
%include "static_vector_3.i"
%include "static_vector_4.i"
%include "static_matrix_33.i"
%include "static_matrix_44.i"
%include "static_matrix_66.i"
%include "mesh1d.i"
%include "mesh2d.i"
%include "mesh3d.i"

%shared_ptr(Volume)
%shared_ptr(Lattice)
%shared_ptr(Element)
%shared_ptr(Coil)
%shared_ptr(Drift)
%shared_ptr(SBend)
%shared_ptr(Quadrupole)
%shared_ptr(GenericField)
%shared_ptr(Static_Magnetic_FieldMap)
%shared_ptr(Static_Electric_FieldMap)
%shared_ptr(BroadbandDielectricStructure)
%shared_ptr(Static_Magnetic_FieldMap_1d<Mesh1d_CINT>)
%shared_ptr(Static_Magnetic_FieldMap_1d<Mesh1d_LINT>)
%shared_ptr(Static_Magnetic_FieldMap_2d<TMesh2d_CINT<StaticVector<2>>>)
%shared_ptr(Static_Magnetic_FieldMap_2d<TMesh2d_LINT<StaticVector<2>>>)
%shared_ptr(RF_FieldMap<TMesh3d_CINT<StaticVector<3,fftwComplex>>>)
%shared_ptr(RF_FieldMap<TMesh3d_LINT<StaticVector<3,fftwComplex>>>)
%shared_ptr(RF_FieldMap_2d<TMesh2d_CINT<StaticVector<2,fftwComplex>>>)
%shared_ptr(RF_FieldMap_2d<TMesh2d_LINT<StaticVector<2,fftwComplex>>>)
%shared_ptr(RF_FieldMap_1d<ComplexMesh1d_CINT>)
%shared_ptr(RF_FieldMap_1d<ComplexMesh1d_LINT>)
%shared_ptr(AdiabaticMatchingDevice)
%shared_ptr(ElectronCooler)
%shared_ptr(ExternalField)
%shared_ptr(TransferLine)
%shared_ptr(SW_Structure)
%shared_ptr(TW_Structure)

%shared_ptr(Plasma)
%shared_ptr(Aperture)
%shared_ptr(Parallel_ODE_Solver)

%{
#include <utility>
#include "RF_Track.hh"
#include "lattice.hh"
#include "volume.hh"
#include "element.hh"
#include "generic_field.hh"
#include "drift.hh"
#include "sbend.hh"
#include "quadrupole.hh"
#include "transfer_line.hh"
#include "bunch6dt.hh"
#include "bunch6d.hh"
#include "particle.hh"
#include "static_electric_field_map.hh"
#include "static_magnetic_field_map.hh"
#include "static_magnetic_field_map_1d.hh"
#include "static_magnetic_field_map_2d.hh"
#include "sw_structure.hh"
#include "tw_structure.hh"
#include "rf_field_map.hh"
#include "rf_field_map_1d.hh"
#include "rf_field_map_2d.hh"
#include "plasma.hh"
#include "space_charge.hh"
#include "space_charge_p2p.hh"
#include "space_charge_pic.hh"
#include "space_charge_pic_long_cylinder.hh"
#include "space_charge_pic_horizontal_plates.hh"
#include "electron_cooler.hh"
#include "adiabatic_matching_device.hh"
#include "external_field.hh"
#include "broadband_dielectric_structure.hh"
#include "coil.hh"
%}

%ignore RFT::rng;
%ignore SpaceCharge::compute_force(const Bunch6d &, MatrixNd & );
%ignore SpaceCharge::compute_force(const Bunch6dT &, MatrixNd & );

#if defined(SWIGOCTAVE)
%typemap(out) std::pair<Static_Electric_FieldMap,Static_Magnetic_FieldMap> {
  std::shared_ptr<  Element > *Eptr = new std::shared_ptr<  Element >(std::make_shared<Static_Electric_FieldMap>($1.first));
  std::shared_ptr<  Element > *Bptr = new std::shared_ptr<  Element >(std::make_shared<Static_Magnetic_FieldMap>($1.second));
  octave_value_list ret;
  ret.append(SWIG_NewPointerObj(SWIG_as_voidptr(Eptr), SWIGTYPE_p_std__shared_ptrT_Element_t, SWIG_POINTER_OWN));
  ret.append(SWIG_NewPointerObj(SWIG_as_voidptr(Bptr), SWIGTYPE_p_std__shared_ptrT_Element_t, SWIG_POINTER_OWN));
  $result = octave_value(ret);
 }

%typemap(out) std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> {
  ComplexMatrix E(3,1);
  ComplexMatrix B(3,1);
  E(0) = Complex(($1).first[0]);
  E(1) = Complex(($1).first[1]);
  E(2) = Complex(($1).first[2]);
  B(0) = Complex(($1).second[0]);
  B(1) = Complex(($1).second[1]);
  B(2) = Complex(($1).second[2]);
  octave_value_list ret;
  ret.append(E);
  ret.append(B);
  $result = octave_value(ret);
 }

%typemap(out) std::pair<StaticVector<3>, StaticVector<3>> {
  Matrix E(3,1);
  Matrix B(3,1);
  E(0) = ($1).first[0];
  E(1) = ($1).first[1];
  E(2) = ($1).first[2];
  B(0) = ($1).second[0];
  B(1) = ($1).second[1];
  B(2) = ($1).second[2];
  octave_value_list ret;
  ret.append(E);
  ret.append(B);
  $result = octave_value(ret);
 }

%typemap(out) std::pair<StaticMatrix<3,3>, StaticMatrix<3,3>> {
  Matrix E(3,3);
  Matrix B(3,3);
  for (size_t i=0; i<3; i++) {
    for (size_t j=0; j<3; j++) {
      E(i,j) = ($1).first[i][j];
      B(i,j) = ($1).second[i][j];
    }
 }
  octave_value_list ret;
  ret.append(E);
  ret.append(B);
  $result = octave_value(ret);
 }
#endif

#if defined(SWIGPYTHON)
%typemap(out) std::pair<Static_Electric_FieldMap,Static_Magnetic_FieldMap> {
  std::shared_ptr<  Element > *Eptr = new std::shared_ptr<  Element >(std::make_shared<Static_Electric_FieldMap>($1.first));
  std::shared_ptr<  Element > *Bptr = new std::shared_ptr<  Element >(std::make_shared<Static_Magnetic_FieldMap>($1.second));
  $result = SWIG_Python_AppendOutput($result, SWIG_NewPointerObj(SWIG_as_voidptr(Eptr), SWIGTYPE_p_std__shared_ptrT_Element_t, SWIG_POINTER_OWN));
  $result = SWIG_Python_AppendOutput($result, SWIG_NewPointerObj(SWIG_as_voidptr(Bptr), SWIGTYPE_p_std__shared_ptrT_Element_t, SWIG_POINTER_OWN));
 }

%typemap(out) std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> {
  npy_intp dimensions[1] = { 3 };
  PyArrayObject *res1 = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, NPY_CDOUBLE);
  PyArrayObject *res2 = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, NPY_CDOUBLE);
  npy_intp *strides = PyArray_STRIDES(res1);
  char *data1 = PyArray_BYTES(res1);
  char *data2 = PyArray_BYTES(res2);
  for (int i=0; i<3; i++) {
    (*(npy_cdouble*)(data1 + i*strides[0])).real = ($1).first[i].real;
    (*(npy_cdouble*)(data1 + i*strides[0])).imag = ($1).first[i].imag;
    (*(npy_cdouble*)(data2 + i*strides[0])).real = ($1).second[i].real;
    (*(npy_cdouble*)(data2 + i*strides[0])).imag = ($1).second[i].imag;
  }
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res1));
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res2));
 }

%typemap(out) std::pair<StaticVector<3>, StaticVector<3>> {
  npy_intp dimensions[1] = { 3 };
  PyArrayObject *res1 = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, NPY_DOUBLE);
  PyArrayObject *res2 = (PyArrayObject *)PyArray_SimpleNew(1, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res1);
  char *data = PyArray_BYTES(res1);
  *(double*)(data + 0*strides[0]) = ($1).first[0];
  *(double*)(data + 1*strides[0]) = ($1).first[1];
  *(double*)(data + 2*strides[0]) = ($1).first[2];
  data = PyArray_BYTES(res2);
  *(double*)(data + 0*strides[0]) = ($1).second[0];
  *(double*)(data + 1*strides[0]) = ($1).second[1];
  *(double*)(data + 2*strides[0]) = ($1).second[2];
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res1));
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res2));
 }

%typemap(out) std::pair<StaticMatrix<3,3>, StaticMatrix<3,3>> {
  npy_intp dimensions[2] = { 3, 3 };
  PyArrayObject *res1 = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  PyArrayObject *res2 = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res1);
  char *data1 = PyArray_BYTES(res1);
  char *data2 = PyArray_BYTES(res2);
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      *(double*)(data1 + i*strides[0] + j*strides[1]) = ($1).first[i][j];
      *(double*)(data2 + i*strides[0] + j*strides[1]) = ($1).second[i][j];
    }
  }
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res1));
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res2));
 }
#endif

%rename(is_valid) Particle::operator bool;
%rename(is_valid) ParticleT::operator bool;
%rename(__assign__) SpaceCharge::operator=;
%ignore Lattice::operator[]; 
%ignore Vector::operator[]; 
%ignore Bunch6d::operator[]; 
%ignore Bunch6dT::operator[]; 
%ignore init_rf_track;

%include "parallel_ode_solver.hh"
%include "tracking_options.hh"
%include "aperture.hh"
%include "RF_Track.hh"
%include "element.hh"
%include "generic_field.hh"
%include "drift.hh"
%include "sbend.hh"
%include "transfer_line.hh"
%include "quadrupole.hh"
%include "particle.hh"
%include "bunch6dt.hh"
%include "bunch6d.hh"
%include "bunch6d_info.hh"
%include "bunch6dt_info.hh"
%include "bunch6d_twiss.hh"
%include "static_electric_field_map.hh"
%include "static_magnetic_field_map.hh"
%include "static_magnetic_field_map_1d.hh"
%include "static_magnetic_field_map_2d.hh"
%include "sw_structure.hh"
%include "tw_structure.hh"
%include "rf_field_map.hh"
%include "rf_field_map_1d.hh"
%include "rf_field_map_2d.hh"
%include "adiabatic_matching_device.hh"
%include "external_field.hh"
%include "transport_table.hh"
%include "lattice.hh"
%include "volume.hh"
%include "plasma.hh"

%include "space_charge.hh"
%include "space_charge_p2p.hh"
%include "space_charge_pic.hh"
%include "electron_cooler.hh"
%include "broadband_dielectric_structure.hh"
%include "coil.hh"

%template(RF_FieldMap) RF_FieldMap<TMesh3d_CINT<StaticVector<3,fftwComplex>>>;
%template(RF_FieldMap_LINT) RF_FieldMap<TMesh3d_LINT<StaticVector<3,fftwComplex>>>;

%template(RF_FieldMap_1d) RF_FieldMap_1d<ComplexMesh1d_CINT>;
%template(RF_FieldMap_1d_LINT) RF_FieldMap_1d<ComplexMesh1d_LINT>;

%template(RF_FieldMap_2d) RF_FieldMap_2d<TMesh2d_CINT<StaticVector<2,fftwComplex>>>;
%template(RF_FieldMap_2d_LINT) RF_FieldMap_2d<TMesh2d_LINT<StaticVector<2,fftwComplex>>>;

%template(Static_Magnetic_FieldMap_1d) Static_Magnetic_FieldMap_1d<Mesh1d_CINT>;
%template(Static_Magnetic_FieldMap_1d_LINT) Static_Magnetic_FieldMap_1d<Mesh1d_LINT>;

%template(Static_Magnetic_FieldMap_2d) Static_Magnetic_FieldMap_2d<TMesh2d_CINT<StaticVector<2>>>;
%template(Static_Magnetic_FieldMap_2d_LINT) Static_Magnetic_FieldMap_2d<TMesh2d_LINT<StaticVector<2>>>;

%template(SpaceCharge_PIC_FreeSpace) SpaceCharge_PIC<GreensFunction::IntegratedCoulomb>;
%template(SpaceCharge_PIC_LongCylinder_T) SpaceCharge_PIC<GreensFunction::IntegratedCoulomb_LongCylinder>;
%template(SpaceCharge_PIC_HorizontalPlates_T) SpaceCharge_PIC<GreensFunction::IntegratedCoulomb_HorizontalPlates>;

%include "space_charge_pic_long_cylinder.hh"
%include "space_charge_pic_horizontal_plates.hh"

%extend Lattice {
  std::shared_ptr<Element> __brace__(int j) {
    if (j>=1 && j <= int($self->size()))
      return $self->operator[](j-1);
#if defined(SWIGOCTAVE)
    error("index out of range");
#elif defined(SWIGPYTHON)
    std::cerr << "error: index out of range\n";
#endif
    return NULL;
  }
 };

%extend Bunch6d {
  Particle __brace__(int j) {
    if (j>=1 && j <= int($self->size()))
      return $self->get_particle(j-1);
#if defined(SWIGOCTAVE)
    error("index out of range");
#elif defined(SWIGPYTHON)
    std::cerr << "error: index out of range\n";
#endif
    return Particle();
  }
 };

%extend Bunch6dT {
  ParticleT __brace__(int j) {
    if (j>=1 && j <= int($self->size()))
      return $self->get_particle(j-1);
#if defined(SWIGOCTAVE)
    error("index out of range");
#elif defined(SWIGPYTHON)
    std::cerr << "error: index out of range\n";
#endif
    return ParticleT();
  }
 };

%init %{
  init_rf_track();
  %}
