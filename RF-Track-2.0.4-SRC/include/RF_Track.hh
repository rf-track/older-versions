/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef rf_track_hh
#define rf_track_hh

#include <cstddef>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics.h>

#include "space_charge.hh"
#include "static_matrix.hh"
#include "four_vector.hh"
#include "for_all.hh"
#include "hypot.hh"

#define RF_TRACK_VERSION "2.0.4"

namespace RFT {
  extern const std::string version;
  extern const size_t max_number_of_threads;
  extern size_t number_of_threads;
  extern SpaceCharge *SC_engine;
  // constants
  extern const double clight; // m/s
  extern const double muonmass; // MeV/c^2
  extern const double protonmass; // MeV/c^2
  extern const double electronmass; // MeV/c^2
  extern const double s, ms, us, ns, ps, fs; // mm/c
  extern const double C, mC, uC, nC, pC; // e
  // useful functions
  extern StaticVector<3> relativistic_velocity_addition(const StaticVector<3> &u, const StaticVector<3> &v );
  extern StaticVector<4> lorentz_boost(const StaticVector<3> &v, const StaticVector<4> &x );
  extern FourVector lorentz_boost(const StaticVector<3> &v, const FourVector &x );
  extern StaticMatrix<4,4> lorentz_boost_matrix(const StaticVector<3> &v );
  // random number generator
  extern gsl_rng *rng;
  extern void rng_set(const char *name );
  extern void rng_set_seed(unsigned long int s );
  extern const char *rng_get();
}

extern void init_rf_track();

#endif /* rf_track_hh */
