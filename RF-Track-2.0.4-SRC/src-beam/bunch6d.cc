/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <cmath>
#include <assert.h>
#include <cstdio>
#include <sstream>
#include <limits>
#include <unordered_map>

#include "RF_Track.hh"
#include "stats.hh"
#include "bunch6d.hh"
#include "bunch6dt.hh"
#include "for_all.hh"
#include "matrixnd.hh"
#include "numtools.hh"
#include "get_token.hh"
#include "conversion.hh"
#include "particle_key.hh"
#include "relativistic_velocity_addition.hh"
#include "file_stream.hh"
#include "quadratic_equation.hh"

Bunch6d::Bunch6d(double mass_, double population_, double charge_, const MatrixNd &X ) : S(0.0)
{
  if (X.columns()==6) {
    set_phase_space(X);
    const double particles_per_macroparticle = population_ / X.rows();
    for (auto &particle: particles) {
      particle.mass = mass_;
      particle.Q = charge_;
      particle.N = particles_per_macroparticle;
    }
  } else {
    throw "Bunch6d() requires a 6-column matrix for its initialization\n";
  }
}

Bunch6d::Bunch6d(double mass_, double population_, double charge_, double Pc_, Bunch6d_twiss T, size_t N, double sigma_cut ) : particles(N), S(0.0)
{
  if (N==0)
    return;

  const double beta_gamma = Pc_ / mass_;
  const double beta = beta_gamma/sqrt(beta_gamma*beta_gamma+1);
  double alpha_beta_t = 0.0;
  if (T.sigma_z==0.0 && T.sigma_d==0.0) {
    T.sigma_z = sqrt(T.beta_z * T.emitt_z / beta_gamma); // mm
    if (T.beta_z!=0.0) {
      T.sigma_d = sqrt(T.emitt_z / beta_gamma / T.beta_z); // mrad
      alpha_beta_t = T.alpha_z / T.beta_z;
    }
  }

  auto ran_gaussian_n_sigma = [] (double sigma_cut ) {
    if (sigma_cut<=0.0)
      return gsl_ran_gaussian(RFT::rng, 1.0);
    double t;
    while (fabs(t=gsl_ran_gaussian(RFT::rng, 1.0))>sigma_cut) {}
    return t;
  };

  auto create_phase_space_t = [&] (double beta, double alpha, double emitt ) {
    MatrixNd M(N,2);
    if (beta == 0.0) {
      M = 0.0;
    } else {
      const double sigma = sqrt(beta * emitt / beta_gamma); // mm
      const double sigma_p = sqrt(emitt / beta_gamma / beta); // mrad
      M[0][0] = 0.0; // mm
      M[0][1] = 0.0; // mrad
      CumulativeKahanSum<double> sum[2] = { 0.0 };
      for (size_t i = 1; i < N; i++) {
	double u = ran_gaussian_n_sigma(sigma_cut);
	double v = ran_gaussian_n_sigma(sigma_cut);
	sum[0] += (M[i][0] = sigma * u); // mm
	sum[1] += (M[i][1] = (-alpha * u + v) * sigma_p); // mrad
      }
      if (N>1) {
	const double a = sum[0] / double(N-1);
	const double b = sum[1] / double(N-1);
	for (size_t i = 1; i < N; i++) {
	  M[i][0] -= a;
	  M[i][1] -= b;
	}
      }
      double cov[2][2];
      auto A = (gsl_matrix *)(M);
      auto a = gsl_matrix_column (A, 0);
      auto b = gsl_matrix_column (A, 1);
      cov[0][0] = gsl_stats_covariance(a.vector.data, a.vector.stride, a.vector.data, a.vector.stride, a.vector.size);
      cov[0][1] = cov[1][0] = gsl_stats_covariance(a.vector.data, a.vector.stride, b.vector.data, b.vector.stride, a.vector.size);
      cov[1][1] = gsl_stats_covariance(b.vector.data, b.vector.stride, b.vector.data, b.vector.stride, b.vector.size);
      const double e = sqrt(cov[0][0]*cov[1][1]-cov[1][0]*cov[0][1]);
      if (e!=0.0) {
	M *= sqrt(emitt / beta_gamma) / sqrt(e);
      }
    }
    return M;
  };

  auto create_phase_space_l = [&] (double sigma_t, double sigma_d ) {
    MatrixNd M(N,2);
    M[0][0] = 0.0; // mm/c
    M[0][1] = 0.0; // permill
    CumulativeKahanSum<double> sum[2] = { 0.0 };
    for (size_t i = 1; i < N; i++) {
      if (sigma_t>=0) sum[0] += (M[i][0] = sigma_t * ran_gaussian_n_sigma(sigma_cut)); // mm/c
      else            sum[0] += (M[i][0] = sigma_t * gsl_ran_flat(RFT::rng, -0.5, 0.5)); // mm/c
      if (sigma_d>=0) sum[1] += (M[i][1] = sigma_d * ran_gaussian_n_sigma(sigma_cut)); // permill
      else            sum[1] += (M[i][1] = sigma_d * gsl_ran_flat(RFT::rng, -0.5, 0.5)); // permill
    }
    if (N>1) {
      const double a = sum[0] / double(N-1);
      const double b = sum[1] / double(N-1);
      for (size_t i = 1; i < N; i++) {
	M[i][0] -= a;
	M[i][1] -= b;
      }
    }
    return M;
  };

  MatrixNd Mx = create_phase_space_t(T.beta_x, T.alpha_x, T.emitt_x);
  MatrixNd My = create_phase_space_t(T.beta_y, T.alpha_y, T.emitt_y);
  MatrixNd Mz = create_phase_space_l(T.sigma_z / beta , T.sigma_d);
  
  const double particles_per_macroparticle = population_ / N;
  for (size_t i=0; i<N; i++) {
    const double
      d  = Mz[i][1] - alpha_beta_t * Mz[i][0], // 1000
      x  = Mx[i][0] + T.disp_x  * Mz[i][1], // mm
      y  = My[i][0] + T.disp_y  * Mz[i][1], // mm
      t  = Mz[i][0] + T.disp_z  * Mz[i][1], // mm
      xp = Mx[i][1] + T.disp_xp * Mz[i][1], // mrad
      yp = My[i][1] + T.disp_yp * Mz[i][1], // mrad
      Pc = Pc_ * (1e3 + d) / 1e3; // MeV/c
    Particle &particle = particles[i];
    // transport each particle to the plane S=0 (i.e. via a drift by -z);
    particle.x  = x;
    particle.xp = xp;
    particle.y  = y;
    particle.yp = yp;
    particle.t  = t;
    particle.Pc = Pc;
    particle.mass = mass_;
    particle.Q = charge_;
    particle.N = particles_per_macroparticle;
  }
}

Bunch6d::Bunch6d(const Bunch6dT &bunchT, const char *select )
{
  const bool all = strncmp(select, "all", 4) == 0;
  S = bunchT.get_S_mean(); // m
  const double S_mm = S*1e3;
  if (all) {
    particles.resize(bunchT.size());
  } else {
    size_t count = 0;
    for (size_t i=0; i<bunchT.size(); i++) {
      if (bunchT.t >= bunchT[i].t0)
	count++;
    }
    particles.resize(count);
  }
  size_t i = 0;
  for (size_t n=0; n<bunchT.size(); n++) {
    const auto &particleT = bunchT[n];
    if (all || bunchT.t>=particleT.t0) {
      const double
	dS = particleT.S - S_mm, // mm
	E  = particleT.get_total_energy(), // MeV
	xp = particleT.Px / particleT.Pz, // rad
	yp = particleT.Py / particleT.Pz, // rad
	Vz = particleT.Pz / E, // c
	x  = particleT.X - dS * xp, // mm
	y  = particleT.Y - dS * yp, // mm
	dt = -dS / Vz; // mm/c
      Particle &particle = particles[i++];
      particle.x  = x;
      particle.y  = y;
      particle.xp = xp * 1e3;
      particle.yp = yp * 1e3;
      particle.t  = bunchT.t + dt;
      particle.Pc = particleT.get_Pc();
      particle.mass = particleT.mass;
      particle.Q = particleT.Q;
      particle.N = particleT.N;
      if (!particleT) {
	particle.lost_at(particleT.S/1e3);
      }
    }
  }
}

Bunch6d::Bunch6d(const MatrixNd &X ) : S(0.0)
{
  if (X.columns()==8 || X.columns()==9) {
    bool has_N = X.columns()==9;
    particles.resize(X.rows());
    for (size_t i=0; i<X.rows(); i++) {
      Particle &particle = particles[i];
      const double
	x    = X[i][0], // mm
	xp   = X[i][1], // mrad
	y    = X[i][2], // mm
	yp   = X[i][3], // mrad
	t    = X[i][4], // mm/c
	Pc   = X[i][5], // MeV/c
	mass = X[i][6], // MeV/c/c
	Q    = X[i][7], // e+
	N    = has_N ? X[i][8] : 1.0;
      particle.mass = mass;
      particle.Q  = Q;
      particle.N  = N;
      particle.x  = x;
      particle.xp = xp;
      particle.y  = y;
      particle.yp = yp;
      particle.t  = t;
      particle.Pc = Pc;
    }
  } else {
    std::cerr << "error: Bunch6d() requires an 8 or 9-column matrix as an input\n";
  }
}

StaticVector<3> Bunch6d::get_center_of_mass_Vx_Vy_Vz() const
{
  StaticVector<3> Psum(0.0);
  double Esum = 0.0;
  for (const auto &particle : particles) {
    if (particle) {
      Psum += particle.N * particle.get_Px_Py_Pz();
      Esum += particle.N * particle.get_total_energy();
    }
  }
  return Esum != 0.0 ? Psum / Esum : StaticVector<3>(0.0);
}

StaticVector<3> Bunch6d::get_average_Vx_Vy_Vz() const
{
  StaticVector<3> Vsum(0.0);
  double Nsum = 0.0;
  for (const auto &particle : particles) {
    if (particle) {
      Vsum += particle.N * particle.get_Vx_Vy_Vz();
      Nsum += particle.N;
    }
  }
  return Nsum != 0.0 ? Vsum / Nsum : StaticVector<3>(0.0);
}

Particle Bunch6d::get_average_particle() const
{
  Particle p(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  if (!particles.empty()) {
    CumulativeKahanSum<double> sum[9];
    size_t ngood = 0;
    for (const Particle &particle : particles) {
      if (particle) {
	sum[0] += particle.x;
	sum[1] += particle.xp;
	sum[2] += particle.y;
	sum[3] += particle.yp;
	sum[4] += particle.t;
	sum[5] += particle.Pc;
	sum[6] += particle.mass;
	sum[7] += particle.Q;
	sum[8] += particle.N;
	ngood++;
      }
    }
    if (ngood!=0) {
      p.x  = sum[0] / ngood;
      p.xp = sum[1] / ngood;
      p.y  = sum[2] / ngood;
      p.yp = sum[3] / ngood;
      p.t  = sum[4] / ngood;
      p.Pc = sum[5] / ngood;
      p.mass = sum[6] / ngood;
      p.Q  = sum[7] / ngood;
      p.N  = sum[8] / ngood;
    }
  }
  return p;
}

Particle Bunch6d::get_reference_particle() const
{
  if (particles[0])
    return particles[0]; // MeV/c
  std::cerr << "warning: the beam's first particle was lost, using the beam centroid as reference particle.\n";
  return get_average_particle();
}

double Bunch6d::get_reference_momentum() const
{
  return get_reference_particle().Pc; // MeV/c
}

Particle Bunch6d::get_average_particle_t0(double dS ) const // returns the average particle at t = t0 (that is when <z> = dS [m] )
{
  Particle p(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  if (!particles.empty()) {
    CumulativeKahanSum<double> sum[8];
    const double t0 = get_t_mean(dS); // mm/c
    size_t ngood = 0;
    for (const Particle &particle : particles) {
      if (particle) {
	const double dt = particle.t - t0;
	auto const &v = particle.get_Vx_Vy_Vz();
	sum[0] += particle.x - dt * v[0];
	sum[1] += particle.xp;
	sum[2] += particle.y - dt * v[1];
	sum[3] += particle.yp;
	sum[4] += particle.Pc;
	sum[5] += particle.mass;
	sum[6] += particle.Q;
	sum[7] += particle.N;
	ngood++;
      }
    }
    if (ngood!=0) {
      p.x  = sum[0] / ngood;
      p.xp = sum[1] / ngood;
      p.y  = sum[2] / ngood;
      p.yp = sum[3] / ngood;
      p.Pc = sum[4] / ngood;
      p.mass = sum[5] / ngood;
      p.Q  = sum[6] / ngood;
      p.N  = sum[7] / ngood;
      p.t  = t0;
    }
  }
  return p;
}

StaticVector<3> Bunch6d::get_bunch_temperature()
{
  // accumulate velocoty variances discriminating by ParticleKey (mass, charge)
  std::unordered_map<ParticleKey,Weighted_incremental_variance,ParticleKeyHasher,ParticleKeyEquals> K_variance;
  {
    for (auto const &particle : particles) {
      if (particle) {
	const auto key = ParticleKey{ particle.mass, particle.Q };
	const auto P = particle.get_Px_Py_Pz();
	const auto K = StaticVector<3>(P[0]*P[0], P[1]*P[1], P[2]*P[2]) / (2*particle.mass);
	K_variance[key].append(K, particle.N);
      }
    }
  }
  
  // sums the temperatures Tx, Ty, Tz, for each ParticleKey
  StaticVector<3> retval(0.0);
  for (auto const &vv : K_variance) {
    retval += vv.second.variance();
  }

  return 1e6*retval; // eV
}

double Bunch6d::get_t_mean(double dS ) const // returns the time such that <Z> = dS [m]
{
  CumulativeKahanSum<double> sum_t_Vz;
  CumulativeKahanSum<double> sum_Vz;
  for (auto const &particle: particles) {
    if (particle) {
      const auto v = particle.get_Vx_Vy_Vz();
      sum_t_Vz += particle.t * v[2] + dS * 1e3;
      sum_Vz += v[2];
    }
  }
  return sum_Vz != 0.0 ? sum_t_Vz / sum_Vz : 0.0;
}

double Bunch6d::get_t_min() const
{
  double t_min = std::numeric_limits<double>::infinity();
  for (auto const &particle: particles) {
    if (particle) {
      if (particle.t<t_min) {
	t_min = particle.t;
      }
    }
  }
  return t_min;
}

double Bunch6d::get_t_max() const
{
  double t_max = -std::numeric_limits<double>::infinity();
  for (auto const &particle: particles) {
    if (particle) {
      if (particle.t>t_max) {
	t_max = particle.t;
      }
    }
  }
  return t_max;
}

void Bunch6d::set_phase_space(const MatrixNd &X ) // takes %x %xp %y %yp %t %Pc (larger %t means later arrival)
{
  if (X.columns()==6) {
    size_t N = X.rows();
    particles.resize(N);
    for (size_t i=0; i<N; i++) {
      const double
	&x  = X[i][0], // mm
	&xp = X[i][1], // mrad
	&y  = X[i][2], // mm
	&yp = X[i][3], // mrad
	&t  = X[i][4], // mm/c
	&Pc = X[i][5]; // MeV
      Particle &particle = particles[i];
      particle.x  = x;
      particle.xp = xp;
      particle.y  = y;
      particle.yp = yp;
      particle.t  = t;
      particle.Pc = Pc;
    }
  } else {
    std::cerr << "Bunch6d::set_phase_space() requires a 6-column matrix as an input\n";
  }
}

Bunch6d_info Bunch6d::get_info() const
{
  double *w = new double[particles.size()];
  size_t wsum = 0;
  assert(w);
  double r2max = 0.0;
  for (size_t i=0; i<particles.size(); i++) {
    const Particle &particle = particles[i];
    if (particle) {
      w[i] = 1.0;
      wsum++;
      double r2 = particle.x*particle.x + particle.y*particle.y;
      if (r2>r2max)
	r2max = r2;
    } else {
      w[i] = 0.0;
    }
  }
  constexpr size_t particle_stride = sizeof(Particle) / sizeof(double);
  const double rmax = sqrt(r2max);
  const Particle P0 = get_average_particle();
  const auto V0 = P0.get_Vx_Vy_Vz();
  const double var_x = gsl_stats_wvariance_m(w, 1, &particles[0].x, particle_stride, particles.size(), P0.x); // mm**2
  const double var_y = gsl_stats_wvariance_m(w, 1, &particles[0].y, particle_stride, particles.size(), P0.y); // mm**2
  const double var_t = gsl_stats_wvariance_m(w, 1, &particles[0].t, particle_stride, particles.size(), P0.t); // [mm/c]**2
  const double var_xp = gsl_stats_wvariance_m(w, 1, &particles[0].xp, particle_stride, particles.size(), P0.xp); // mrad**2
  const double var_yp = gsl_stats_wvariance_m(w, 1, &particles[0].yp, particle_stride, particles.size(), P0.yp); // mrad**2
  const double var_P = gsl_stats_wvariance_m(w, 1, &particles[0].Pc, particle_stride, particles.size(), P0.Pc); // (MeV/c)**2
  const double var_z = var_t * V0[2] * V0[2]; // mm**2
  const double beta0_gamma0 = P0.Pc / P0.mass;
  Bunch6d_info info;
  info.S = S;
  info.mean_x = P0.x; // mm
  info.mean_y = P0.y; // mm
  info.mean_t = P0.t; // mm/c
  info.mean_P = P0.Pc;  // average momentum in MeV
  info.mean_E = P0.get_total_energy(); // average total energy in MeV
  info.mean_xp = P0.xp; // mrad
  info.mean_yp = P0.yp; // mrad
  info.mean_K = info.mean_E - P0.mass; // average kinetic energy in MeV
  if (wsum>1) {
    info.sigma_x = sqrt(var_x); // mm
    info.sigma_y = sqrt(var_y); // mm
    info.sigma_z = sqrt(var_z); // mm
    info.sigma_t = sqrt(var_t); // mm/c
    info.sigma_xp = sqrt(var_xp); // mrad
    info.sigma_yp = sqrt(var_yp); // mrad
    info.sigma_xxp = stats_wcovariance_m(w, 1, &particles[0].x, particle_stride, &particles[0].xp, particle_stride, particles.size(), P0.x, P0.xp); // mm*mrad
    info.sigma_yyp = stats_wcovariance_m(w, 1, &particles[0].y, particle_stride, &particles[0].yp, particle_stride, particles.size(), P0.y, P0.yp); // mm*mrad
    info.sigma_zP  = stats_wcovariance_m(w, 1, &particles[0].t, particle_stride, &particles[0].Pc, particle_stride, particles.size(), P0.t, P0.Pc) * V0[2]; // mm*MeV
    info.sigma_P = gsl_stats_wsd_m(w, 1, &particles[0].Pc, particle_stride, particles.size(), P0.Pc); // momentum spread in MeV
    info.sigma_E = P0.Pc / info.mean_E * info.sigma_P; // kinetic-energy spread in MeV
    const double sigma_xP = stats_wcovariance_m(w, 1, &particles[0].x, particle_stride, &particles[0].Pc, particle_stride, particles.size(), P0.x, P0.Pc); // mm*MeV/c
    const double sigma_yP = stats_wcovariance_m(w, 1, &particles[0].y, particle_stride, &particles[0].Pc, particle_stride, particles.size(), P0.y, P0.Pc); // mm*MeV/c
    const double sigma_xpP = stats_wcovariance_m(w, 1, &particles[0].xp, particle_stride, &particles[0].Pc, particle_stride, particles.size(), P0.xp, P0.Pc); // mrad*MeV/c
    const double sigma_ypP = stats_wcovariance_m(w, 1, &particles[0].yp, particle_stride, &particles[0].Pc, particle_stride, particles.size(), P0.yp, P0.Pc); // mrad*MeV/c
    auto sqr = [] (double x ) { return x*x; };
    info.disp_x = info.sigma_P ? sigma_xP * P0.Pc / sqr(info.sigma_P) / 1e3 : 0.0; // m
    info.disp_y = info.sigma_P ? sigma_yP * P0.Pc / sqr(info.sigma_P) / 1e3 : 0.0; // m
    info.disp_xp = info.sigma_P ? sigma_xpP * P0.Pc / sqr(info.sigma_P) / 1e3 : 0.0; // rad
    info.disp_yp = info.sigma_P ? sigma_ypP * P0.Pc / sqr(info.sigma_P) / 1e3 : 0.0; // rad
    MatrixNd sigma_4d(4,4);
    sigma_4d[0][0] = var_x; // mm**2
    sigma_4d[0][1] = sigma_4d[1][0] = info.sigma_xxp; // mm*mrad
    sigma_4d[0][2] = sigma_4d[2][0] = stats_wcovariance_m(w, 1, &particles[0].x, particle_stride, &particles[0].y,  particle_stride, particles.size(), P0.x, P0.y); // mm**2
    sigma_4d[0][3] = sigma_4d[3][0] = stats_wcovariance_m(w, 1, &particles[0].x, particle_stride, &particles[0].yp, particle_stride, particles.size(), P0.x, P0.yp); // mm*mrad
    sigma_4d[1][1] = var_xp; // mrad**2
    sigma_4d[2][1] = sigma_4d[1][2] = stats_wcovariance_m(w, 1, &particles[0].y, particle_stride, &particles[0].xp, particle_stride, particles.size(), P0.y, P0.xp); // mm*mrad
    sigma_4d[3][1] = sigma_4d[1][3] = stats_wcovariance_m(w, 1, &particles[0].yp, particle_stride, &particles[0].xp,  particle_stride, particles.size(), P0.yp, P0.xp); // mrad**2
    sigma_4d[2][2] = var_y; // mm*mm
    sigma_4d[2][3] = sigma_4d[3][2] = info.sigma_yyp; // mm*mrad
    sigma_4d[3][3] = var_yp; // mrad**2
    info.emitt_4d = beta0_gamma0 * pow(det(sigma_4d), 0.25); // mm*mrad, 4d normalized emittance
    info.emitt_x = sqrt(var_x * var_xp - info.sigma_xxp * info.sigma_xxp); // emittance mm * mrad
    info.emitt_y = sqrt(var_y * var_yp - info.sigma_yyp * info.sigma_yyp); // emittance mm * mrad
    info.emitt_z = sqrt(var_z * var_P  - info.sigma_zP  * info.sigma_zP) * 1e3 / P0.Pc; // emittance mm * permille
    info.alpha_x = -info.sigma_xxp / info.emitt_x;
    info.alpha_y = -info.sigma_yyp / info.emitt_y;
    info.alpha_z = -info.sigma_zP / info.emitt_z / P0.Pc / 1e3;
    info.beta_x = var_x / info.emitt_x; // mm/mrad = m
    info.beta_y = var_y / info.emitt_y; // mm/mrad = m
    info.beta_z = var_z / info.emitt_z; // mm/permille = m
    info.emitt_x *= beta0_gamma0; // normalised emittance mm * mrad
    info.emitt_y *= beta0_gamma0; // normalised emittance mm * mrad
    info.emitt_z *= beta0_gamma0; // normalised emittance mm * permille
  } else {
    info.sigma_x = 0.0;
    info.sigma_y = 0.0;
    info.sigma_z = 0.0;
    info.sigma_t = 0.0;
    info.sigma_xp = 0.0;
    info.sigma_yp = 0.0;
    info.sigma_xxp = 0.0;
    info.sigma_yyp = 0.0;
    info.emitt_4d = 0.0;
    info.sigma_zP = 0.0;
    info.sigma_P = 0.0;
    info.sigma_E = 0.0;
    info.emitt_x = 0.0;
    info.emitt_y = 0.0;
    info.emitt_z = 0.0;
    info.alpha_x = 0.0;
    info.alpha_y = 0.0;
    info.alpha_z = 0.0;
    info.beta_x = 0.0;
    info.beta_y = 0.0;
    info.beta_z = 0.0;
  }
  info.rmax = rmax;
  info.transmission = get_ngood() * 100.0 / particles.size();
  delete []w;
  return info;
}

MatrixNd Bunch6d::get_phase_space(const char *fmt, const char *select ) const
{
  MatrixNd retval;
  if (particles.size()==0)
    return retval;
  struct _ref_par {
    double Pc;
    double t;
  } reference_particle = [&] () {
    if (particles[0]) {
      return _ref_par {
	particles[0].Pc,  // MeV/c
	particles[0].t // mm/c
      };
    }
    std::cerr << "warning: the beam's first particle was lost, using the beam centroid as reference particle.\n";
    return _ref_par {
      get_average_particle().Pc, // MeV/c
      get_t_max() // mm/c
    };
  }();
  const bool all = strncmp(select, "all", 4) == 0;
  size_t rows = all ? particles.size() : get_ngood();
  size_t cols = 0; // will be initialized later
  bool error_msg = false;
  size_t i = 0;
  for (const Particle &particle : particles) {
    const bool particle_ok = bool(particle);
    if (all || particle_ok) {
      std::vector<double> values;
      for(const char *ptr0 = fmt, *ptr = strchr(ptr0, '%'); ptr; ptr0 = ptr, ptr = strchr(ptr0, '%')) {
	/// print any constants in 'fmt', if present
	{
	  char *endptr;
	  std::string nptr(ptr0, size_t(ptr-ptr0));
	  double value = strtod(nptr.c_str(), &endptr);
	  if (value!=0.0 || endptr!=nptr.c_str()) {
	    values.push_back(value);
	  }
	}
	ptr++;
	double value = 0.0;
	const auto p = particle.get_px_py_pz(reference_particle.Pc);
	const auto P = particle.get_Px_Py_Pz();
	const auto v = particle.get_Vx_Vy_Vz();
	const double K[3] = {
	  hypot(P[0], particle.mass) - particle.mass,
	  hypot(P[1], particle.mass) - particle.mass,
	  hypot(P[2], particle.mass) - particle.mass
	};
	const double dt = particle.t - reference_particle.t; // [mm/c] delay to be at S=S0 [s] (dt < 0: head of the bunch)
	if (get_token(&ptr, "deg@")) {
	  char *endptr;
	  double frequency = strtod(ptr, &endptr);
	  if (endptr == ptr) {
	    if (!error_msg) {
	      std::cerr << "error: modifier %deg requires to specify a frequency, in MHz, e.g. %deg@2998.5\n";
	      error_msg = true;
	    }
	    continue;
	  }
	  value = fmod(particle.t * frequency * 1e3 * 360.0 / C_LIGHT, 360.0); // (t / 1e3 / C_LIGHT = seconds) * (frequency * 1e6 = MHz) * (360 = deg)
	  ptr = endptr;
	}
	else if (get_token(&ptr, "Vx")) value = v[0]; // c
	else if (get_token(&ptr, "Vy")) value = v[1]; // c
	else if (get_token(&ptr, "Vz")) value = v[2]; // c
	else if (get_token(&ptr, "Px")) value = P[0]; // MeV/c
	else if (get_token(&ptr, "Py")) value = P[1]; // MeV/c
	else if (get_token(&ptr, "Pz")) value = P[2]; // MeV/c
	else if (get_token(&ptr, "px")) value = p[0]; // mrad
	else if (get_token(&ptr, "py")) value = p[1]; // mrad
	else if (get_token(&ptr, "pz")) value = p[2]; // mrad
	else if (get_token(&ptr, "Kx")) value = K[0]; // MeV
	else if (get_token(&ptr, "Ky")) value = K[1]; // MeV
	else if (get_token(&ptr, "Kz")) value = K[2]; // MeV
	else if (get_token(&ptr, "Pc")) value = particle.Pc; // MeV
	else if (get_token(&ptr, "pt")) value = particle.get_pt(reference_particle.Pc) * 1e3; // permille
	else if (get_token(&ptr, "dt")) value = dt; // delay to be at S=S0 [mm/c] ; c * (t-reference_particle.t)
	else if (get_token(&ptr, "xp")) value = particle.xp; // mrad
	else if (get_token(&ptr, "yp")) value = particle.yp; // mrad
	else if (get_token(&ptr, "zp")) value = 1e3; // mrad
	else if (get_token(&ptr, 'x')) value = particle.x; // [mm] 'x' 'y' and 't' are the coordinates being tracked
	else if (get_token(&ptr, 'y')) value = particle.y; // [mm] that is, on a plane at S = bunch.S
	else if (get_token(&ptr, 'z')) value = 0.0; // [mm] that is, on a plane at S = bunch.S
	else if (get_token(&ptr, 't')) value = particle.t; // t is the proper time of each particle [mm/c]
	else if (get_token(&ptr, 'X')) value = particle.x - dt * v[0]; // [mm] 'X' 'Y' 'Z' are the particle coordinates
	else if (get_token(&ptr, 'Y')) value = particle.y - dt * v[1]; // [mm] in space at t = reference_particle.t
	else if (get_token(&ptr, 'Z')) value = -dt * v[2]; // [mm]
	else if (get_token(&ptr, 'd')) value = particle.get_delta(reference_particle.Pc) * 1e3; // permille
	else if (get_token(&ptr, 'K')) value = particle.get_kinetic_energy(); // MeV
	else if (get_token(&ptr, 'E')) value = particle.get_total_energy(); // MeV
	else if (get_token(&ptr, 'S')) value = S*1e3; // mm
	else if (get_token(&ptr, 'm')) value = particle.mass; // [MeV/c/c] rest mass
	else if (get_token(&ptr, 'Q')) value = particle.Q; // [e+] charge of each particle
	else if (get_token(&ptr, 'N')) value = particle.N; // number of particles per macroparticle
	else if (get_token(&ptr, 'P')) value = particle.Pc; // MeV/c
	else if (get_token(&ptr, 'V')) value = norm(v); // c
	else {
	  if (!error_msg) {
	    const char *next = strpbrk(ptr, "% \n\t");
	    std::string id = next ? std::string(ptr, next-ptr) : ptr;
	    std::cerr << "error: unknown identifier '%" << id << "'\n";
	    error_msg = true;
	  }
	  continue;
	}
	if (!particle_ok)
	  value = GSL_NAN;
	values.push_back(value);
      }
      if (cols == 0) {
	cols = values.size();
	retval.resize(rows, cols);
      }
      std::copy(values.begin(), values.end(), retval[i]);
      i++;
    }
  }
  return retval;
}

MatrixNd Bunch6d::get_lost_particles() const
{
  MatrixNd ret(get_nlost(), 10);
  size_t i = 0;
  for (auto const &particle : particles) {
    if (!particle) {
      ret[i][0] = particle.x;
      ret[i][1] = particle.xp;
      ret[i][2] = particle.y;
      ret[i][3] = particle.yp;
      ret[i][4] = particle.t;
      ret[i][5] = particle.Pc;
      ret[i][6] = particle.S_lost*1e3; // mm
      ret[i][7] = particle.mass;
      ret[i][8] = particle.Q;
      ret[i][9] = particle.N;
      i++;
    }
  }
  return ret;
}

MatrixNd Bunch6d::get_lost_particles_mask() const
{
  MatrixNd ret(particles.size(), 1);
  size_t i = 0;
  for (auto const &particle : particles) {
    ret[i++][0] = particle ? 0.0 : 1.0;
  }
  return ret;
}

size_t Bunch6d::get_ngood() const
{
  size_t ngood = 0;
  for (auto const &particle : particles) {
    if (particle) {
      ngood++;
    }
  }
  return ngood;
}

size_t Bunch6d::get_nlost() const
{
  return particles.size() - get_ngood();
}

void Bunch6d::apply_force(const MatrixNd &force, double dS_mm )
{
  auto apply_force_parallel = [&](size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      auto &particle = particles[i];
      if (particle) {
	const auto F = StaticVector<3>(force[i]); // MeV/m
	const auto v = particle.get_Vx_Vy_Vz(); // c
	const auto a = (F - dot(v,F)*v) / (particle.get_total_energy() * 1e3); // c^2/mm
	// dS = v*dt + 1/2*a*dt^2 : given dS must find dt
	const double dt_mm = quadratic_equation(0.5*a[2], v[2], -dS_mm)[0]; // mm/c
	const auto dP = F * (dt_mm / 1e3); // MeV/c
	const auto P = particle.get_Px_Py_Pz() + dP; // MeV/c
	// update particle
	particle.x += particle.xp * dS_mm / 1e3 + 0.5 * a[0] * dt_mm * dt_mm; // mm
	particle.y += particle.yp * dS_mm / 1e3 + 0.5 * a[1] * dt_mm * dt_mm; // mm
	particle.t += dt_mm; // mm/c
	particle.xp = P[0] / P[2] * 1e3; // mrad
	particle.yp = P[1] / P[2] * 1e3; // mrad
	particle.Pc = norm(P); // MeV
      }
    }
  };
  const size_t Nthreads = RFT::number_of_threads;
  const size_t Nparticles = particles.size();
  for_all(Nthreads, Nparticles, apply_force_parallel);

  // final step
  S += dS_mm / 1e3;
}

void Bunch6d::kick(const MatrixNd &force, double dS_mm )
{
  auto apply_force_parallel = [&](size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      auto &particle = particles[i];
      if (particle) {
	const auto F = StaticVector<3>(force[i]); // MeV/m
	const auto v = particle.get_Vx_Vy_Vz(); // c
	const auto a = (F - dot(v,F)*v) / (particle.get_total_energy() * 1e3); // c^2/mm
	// dS = v*dt + 1/2*a*dt^2 : given dS must find dt
	const double dt_mm = quadratic_equation(0.5*a[2], v[2], -dS_mm)[0]; // mm/c
	const auto dP = F * (dt_mm / 1e3); // MeV/c
	const auto P = particle.get_Px_Py_Pz() + dP; // MeV/c
	// update particle
	particle.xp = P[0] / P[2] * 1e3; // mrad
	particle.yp = P[1] / P[2] * 1e3; // mrad
	particle.Pc = norm(P); // MeV
      }
    }
  };
  const size_t Nthreads = RFT::number_of_threads;
  const size_t Nparticles = particles.size();
  for_all(Nthreads, Nparticles, apply_force_parallel);
}

void Bunch6d::drift(double dS_mm ) // dS is in mm
{
  const double dS = dS_mm / 1e3;
  auto drift_parallel = [&](size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      auto &particle = particles[i];
      if (particle) {
	const auto v = particle.get_Vx_Vy_Vz(); // c
	const double dt_mm = dS_mm / v[2]; // mm/c
	particle.x += particle.xp * dS; // mm
	particle.y += particle.yp * dS; // mm
	particle.t += dt_mm; // mm/c
      }
    }
  };
  const size_t Nthreads = RFT::number_of_threads;
  const size_t Nparticles = particles.size();
  for_all(Nthreads, Nparticles, drift_parallel);

  // final step
  S += dS;
}

bool Bunch6d::load(const char *filename )
{
  File_IStream file(filename);
  if (file) {
    std::string rf_track_version;
    file >> rf_track_version >> S >> coasting_ >> particles;
  }
  return file;
}

bool Bunch6d::save(const char *filename ) const
{
  File_OStream file(filename);
  if (file)
    file << std::string(RF_TRACK_VERSION) << S << coasting_ << particles;
  return file;
}

bool Bunch6d::save_as_dst_file(const char *filename, double frequency_MHz ) const
{
  if (frequency_MHz!=0.0) {
    if (FILE *file = fopen(filename, "w")) {
      std::ostringstream fmt;
      fmt << "%x %xp %y %yp %deg@" << frequency_MHz << " %K";
      MatrixNd bunch = Bunch6d::get_phase_space(fmt.str().c_str());
      int Np = bunch.rows();
      double Ib = 0.0; // beam current [mA]
      fputc(0xfd,file);
      fputc(0x50,file);
      fwrite(&Np, sizeof(int), 1, file);
      fwrite(&Ib, sizeof(double), 1, file);
      fwrite(&frequency_MHz, sizeof(double), 1, file);
      fputc(0x54,file);
      for (int i=0; i<Np; i++) {
	double P[6] = {
	  bunch[i][0] / 10.0,    // cm
	  bunch[i][1] / 1000.0,  // rad
	  bunch[i][2] / 10.0,    // cm
	  bunch[i][3] / 1000.0,  // rad
	  deg2rad(bunch[i][4]),  // rad
	  bunch[i][5]            // MeV
	};
	fwrite(P, sizeof(double), 6, file);
      }
      fwrite(&particles[0].mass, sizeof(double), 1, file);
      fclose(file);
      return true;
    } else std::cerr << "error: couldn't open file\n";
  } else std::cerr << "error: frequency must be != 0.0\n";
  return false;
}

bool Bunch6d::save_as_sdds_file(const char *filename, const char *description ) const
{
  if (FILE *fout = fopen(filename, "w")) {
    const char *desc = description ? description : "This file was created by RF-Track " RF_TRACK_VERSION " (Andrea Latina <andrea.latina@cern.ch>)";
    fputs("SDDS1 \n",fout);
    fprintf(fout,"&description text=\"%s\", &end\n", desc); 
    fputs("&parameter name=prps, format_string=\%s, type=string, &end \n",fout);
    fputs("&parameter name=t0,symbol=t0,units=ns,description=\"reference time\", type=double, &end \n",fout);
    fputs("&parameter name=z0,symbol=z0,units=m,description=\"reference position\", type=double, &end \n",fout);
    fputs("&parameter name=p0,symbol=P0,units=MeV/c,description=\"reference momentum\", type=double, &end \n",fout);
    fputs("&parameter name=Q,symbol=Q,units=nC,description=\"total charge\", type=double, &end \n",fout);
    fputs("&column name=x,symbol=x,units=mm,description=\"horizontal position\", type=double, &end \n",fout);
    fputs("&column name=xp,symbol=x',units=mrad,description=\"horizontal slope\", type=double, &end \n",fout); 
    fputs("&column name=y,symbol=y,units=mm,description=\"vertical position\", type=double, &end \n",fout);
    fputs("&column name=yp,symbol=y',units=mrad,description=\"vertical slope\", type=double, &end \n",fout);
    fputs("&column name=z,symbol=z,units=mm,description=\"longitudinal position\", type=double, &end \n",fout);
    fputs("&column name=E,symbol=E,units=MeV,description=\"energy\", type=double, &end \n",fout);
    fputs("&column name=t,symbol=t,units=s,description=\"time\", type=double, &end \n",fout);
    fputs("&column name=px,symbol=Px,units=MeV/c,description=\"horizontal momentum\", type=double, &end \n",fout); 
    fputs("&column name=py,symbol=Py,units=MeV/c,description=\"vertical momentum\", type=double, &end \n",fout);
    fputs("&column name=pz,symbol=Pz,units=MeV/c,description=\"longitudinal momentum\", type=double, &end \n",fout);
    fputs("&column name=p,symbol=P,units=MeV/c,description=\"total momentum\", type=double, &end \n",fout);
    fputs("&column name=ID, description=\"particle index\",type=double, &end \n",fout);
    fputs("&data mode=ascii, &end\n",fout);
    fputs("! page number 1\n",fout);
    fprintf(fout,"\"%s\"\n",desc);
    double total_charge = 0.0;
    std::for_each(particles.begin(), particles.end(), [&](auto &p){ if (p) { total_charge += p.N * p.Q; }});
    const Particle Pref = get_reference_particle();
    fprintf(fout,"%16.9e\n",Pref.t / RFT::ns); // ns
    fprintf(fout,"%16.9e\n",S); // m
    fprintf(fout,"%16.9e\n",Pref.Pc); // MeV/c
    fprintf(fout,"%16.9e\n",total_charge / RFT::nC); // nC
    fprintf(fout,"         %ld\n", get_ngood());
    int index = 0;
    for (const auto &p: particles) {
      if (p) {
	const auto &P = p.get_Px_Py_Pz();
	fprintf(fout, "%16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %d\n",
		p.x, p.xp,
		p.y, p.yp,
		S*1e3,
		p.get_total_energy(),
		p.t / RFT::s,
		P[0], P[1], P[2],
		p.Pc,
		index++);
      }
    }
    fclose(fout);
    return true;
  } else std::cerr << "error: couldn't open file\n";
  return false;
}
