/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <assert.h>
#include <cstdio>
#include <sstream>
#include <limits>
#include <unordered_map>

#include "RF_Track.hh"
#include "stats.hh"
#include "bunch6d.hh"
#include "bunch6dt.hh"
#include "for_all.hh"
#include "matrixnd.hh"
#include "numtools.hh"
#include "get_token.hh"
#include "conversion.hh"
#include "particle_key.hh"
#include "relativistic_velocity_addition.hh"
#include "file_stream.hh"

Bunch6dT::Bunch6dT(double mass_, double population_, double charge_, const MatrixNd &X ) : t(0.0)
{
  if (X.columns()==6) {
    set_phase_space(X);
    const double particles_per_macroparticle = population_ / X.rows();
    for (auto &particle: particles) {
      particle.mass = mass_;
      particle.Q = charge_;
      particle.N = particles_per_macroparticle;
    }
  } else {
    throw "Bunch6dT() requires a 6-column matrix for its initialization\n";
  }
}

Bunch6dT::Bunch6dT(double mass_, double population_, double charge_, double Pc_, Bunch6d_twiss T, size_t N, double sigma_cut ) : particles(N), t(0.0)
{
  if (N==0)
    return;

  const double beta_gamma = Pc_ / mass_;
  double alpha_beta_z = 0.0;
  if (T.sigma_z==0.0 && T.sigma_d==0.0) {
    T.sigma_z = sqrt(T.beta_z * T.emitt_z / beta_gamma); // mm
    if (T.beta_z!=0.0) {
      T.sigma_d = sqrt(T.emitt_z / beta_gamma / T.beta_z); // mrad
      alpha_beta_z = T.alpha_z / T.beta_z;
    }
  }

  auto ran_gaussian_n_sigma = [] (double sigma_cut ) {
    if (sigma_cut<=0.0)
      return gsl_ran_gaussian(RFT::rng, 1.0);
    double t;
    while (fabs(t=gsl_ran_gaussian(RFT::rng, 1.0))>sigma_cut) {}
    return t;
  };

  auto create_phase_space_t = [&] (double beta, double alpha, double emitt ) {
    MatrixNd M(N,2);
    if (beta == 0.0) {
      M = 0.0;
    } else {
      const double sigma = sqrt(beta * emitt / beta_gamma); // mm
      const double sigma_p = sqrt(emitt / beta_gamma / beta); // mrad
      M[0][0] = 0.0; // mm
      M[0][1] = 0.0; // mrad
      CumulativeKahanSum<double> sum[2] = { 0.0 };
      for (size_t i = 1; i < N; i++) {
	double u = ran_gaussian_n_sigma(sigma_cut);
	double v = ran_gaussian_n_sigma(sigma_cut);
	sum[0] += (M[i][0] = sigma * u); // mm
	sum[1] += (M[i][1] = (-alpha * u + v) * sigma_p); // mrad
      }
      if (N>1) {
	const double a = sum[0] / double(N-1);
	const double b = sum[1] / double(N-1);
	for (size_t i = 1; i < N; i++) {
	  M[i][0] -= a;
	  M[i][1] -= b;
	}
      }
      double cov[2][2];
      auto A = (gsl_matrix *)(M);
      auto a = gsl_matrix_column (A, 0);
      auto b = gsl_matrix_column (A, 1);
      cov[0][0] = gsl_stats_covariance(a.vector.data, a.vector.stride, a.vector.data, a.vector.stride, a.vector.size);
      cov[0][1] = cov[1][0] = gsl_stats_covariance(a.vector.data, a.vector.stride, b.vector.data, b.vector.stride, a.vector.size);
      cov[1][1] = gsl_stats_covariance(b.vector.data, b.vector.stride, b.vector.data, b.vector.stride, b.vector.size);
      const double e = sqrt(cov[0][0]*cov[1][1]-cov[1][0]*cov[0][1]);
      if (e!=0.0) {
	M *= sqrt(emitt / beta_gamma) / sqrt(e);
      }
    }
    return M;
  };

  auto create_phase_space_l = [&] (double sigma_z, double sigma_d ) {
    MatrixNd M(N,2);
    M[0][0] = 0.0; // mm
    M[0][1] = 0.0; // permill
    CumulativeKahanSum<double> sum[2] = { 0.0 };
    for (size_t i = 1; i < N; i++) {
      if (sigma_z>=0) sum[0] += (M[i][0] = sigma_z * ran_gaussian_n_sigma(sigma_cut)); // mm
      else            sum[0] += (M[i][0] = sigma_z * gsl_ran_flat(RFT::rng, -0.5, 0.5)); // mm
      if (sigma_d>=0) sum[1] += (M[i][1] = sigma_d * ran_gaussian_n_sigma(sigma_cut)); // permill
      else            sum[1] += (M[i][1] = sigma_d * gsl_ran_flat(RFT::rng, -0.5, 0.5)); // permill
    }
    if (N>1) {
      double a = sum[0] / double(N-1);
      double b = sum[1] / double(N-1);
      for (size_t i = 1; i < N; i++) {
	M[i][0] -= a;
	M[i][1] -= b;
      }
    }
    return M;
  };

  MatrixNd Mx = create_phase_space_t(T.beta_x, T.alpha_x, T.emitt_x);
  MatrixNd My = create_phase_space_t(T.beta_y, T.alpha_y, T.emitt_y);
  MatrixNd Mz = create_phase_space_l(T.sigma_z, T.sigma_d);
  
  const double particles_per_macroparticle = population_ / N;
  for (size_t i=0; i<N; i++) {
    const double
      d  = Mz[i][1] - alpha_beta_z * Mz[i][0], // 1000
      x  = Mx[i][0] + T.disp_x  * Mz[i][1], // mm
      y  = My[i][0] + T.disp_y  * Mz[i][1], // mm
      z  = Mz[i][0] + T.disp_z  * Mz[i][1], // mm
      xp = Mx[i][1] + T.disp_xp * Mz[i][1], // mrad
      yp = My[i][1] + T.disp_yp * Mz[i][1], // mrad
      Pc = Pc_ * (1e3 + d) / 1e3, // MeV/c
      Px = Pc_ * xp / 1e3, // MeV/c
      Py = Pc_ * yp / 1e3, // MeV/c
      Pz = (Pc_ > 0 ? 1 : -1) * sqrt(Pc*Pc - Px*Px -Py*Py); // MeV/c
    ParticleT &particle = particles[i];
    particle.X = x; // mm
    particle.Y = y; // mm
    particle.S = z; // mm
    particle.Px = Px; // MeV/c
    particle.Py = Py; // MeV/c
    particle.Pz = Pz; // MeV/c
    particle.mass = mass_;
    particle.Q = charge_;
    particle.N = particles_per_macroparticle;
  }
}

Bunch6dT::Bunch6dT(const Bunch6d &b ) : particles(b.size()), t(std::numeric_limits<double>::infinity())
{
  for (size_t i=0; i<b.size(); i++) {
    const Particle &particle = b[i];
    const auto t0 = particle.t; // mm/c
    const auto P = particle.get_Px_Py_Pz(); // MeV/c
    ParticleT &particleT = particles[i];
    particleT.X  = particle.x; // mm
    particleT.Y  = particle.y; // mm
    particleT.S  = b.S * 1e3; // mm
    particleT.Px = P[0]; // MeV/c
    particleT.Py = P[1]; // MeV/c
    particleT.Pz = P[2]; // meV/c
    particleT.mass = particle.mass;
    particleT.Q = particle.Q;
    particleT.N = particle.N;
    particleT.t0 = t0;
    if (t > t0) t = t0;
    if (!particle) {
      particleT.lost_at(t0); // mm/c
    }
  }
}

Bunch6dT::Bunch6dT(const MatrixNd &data )
{
  if (data.columns()==8 || data.columns()==9 || data.columns()==10) {
    const bool has_N  = data.columns()>=9;
    const bool has_t0 = data.columns()>=10;
    t = has_t0 ? std::numeric_limits<double>::infinity() : 0.0; // mm/c
    particles.resize(data.rows());
    for (size_t i=0; i<data.rows(); i++) {
      ParticleT &particle = particles[i];
      // "Native" phase space
      const double
	X    = data[i][0], // mm
	Px   = data[i][1], // MeV/c
	Y    = data[i][2], // mm
	Py   = data[i][3], // MeV/c
	S    = data[i][4], // mm
	Pz   = data[i][5], // MeV/c
	mass = data[i][6], // MeV/c/c
	Q    = data[i][7], // e+
	N    = has_N  ? data[i][8] : 1.0,
	t0   = has_t0 ? data[i][9] : 0.0; // mm/c
      particle.mass = mass;
      particle.Q  = Q;
      particle.N  = N;
      particle.X  = X;
      particle.Y  = Y;
      particle.S  = S;
      particle.Px = Px;
      particle.Py = Py;
      particle.Pz = Pz;
      particle.t0 = t0;
      if (t0 < t) t = t0;
    }
    if (t==std::numeric_limits<double>::infinity())
      t = 0.0;
  } else {
    throw "Bunch6dT::Bunch6dT() requires a 8-, 9-, or 10-column matrix as an input\n";
  }
}

void Bunch6dT::set_phase_space(const MatrixNd &X ) // takes %X %Px %Y %Py %S %Pz (larger %S means bunch head)
{
  if (X.columns()==6) {
    size_t N = X.rows();
    particles.resize(N);
    for (size_t i=0; i<N; i++) {
      const double
	&x  = X[i][0], // mm
	&Px = X[i][1], // MeV/c
	&y  = X[i][2], // mm
	&Py = X[i][3], // MeV/c
	&S  = X[i][4], // mm
	&Pz = X[i][5]; // MeV/c
      ParticleT &particle = particles[i];
      particle.X  = x;
      particle.Px = Px;
      particle.Y  = y;
      particle.Py = Py;
      particle.S  = S;
      particle.Pz = Pz;
    }
  } else {
    throw "Bunch6dT::set_phase_space() requires a 6-column matrix as an input\n";
  }
}

StaticVector<3> Bunch6dT::get_center_of_mass_Vx_Vy_Vz() const
{
  StaticVector<3> Psum(0.0);
  double Esum = 0.0;
  for (const auto &particle : particles) {
    if (particle) {
      Psum += particle.N * particle.get_Px_Py_Pz();
      Esum += particle.N * particle.get_total_energy();
    }
  }
  return Psum / Esum;
}

StaticVector<3> Bunch6dT::get_average_Vx_Vy_Vz() const
{
  StaticVector<3> Vsum(0.0);
  double Nsum = 0.0;
  for (const auto &particle : particles) {
    if (particle) {
      Vsum += particle.N * particle.get_Vx_Vy_Vz();
      Nsum += particle.N;
    }
  }
  return Nsum != 0.0 ? Vsum / Nsum : StaticVector<3>(0.0);
}

ParticleT Bunch6dT::get_average_particle() const
{
  ParticleT p(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  if (!particles.empty()) {
    CumulativeKahanSum<double> sum[9];
    size_t ngood = 0;
    for (const ParticleT &particle : particles) {
      if (particle) {
        sum[0] += particle.X;
        sum[1] += particle.Px;
        sum[2] += particle.Y;
        sum[3] += particle.Py;
        sum[4] += particle.S;
        sum[5] += particle.Pz;
	sum[6] += particle.mass;
        sum[7] += particle.Q;
        sum[8] += particle.N;
	ngood++;
      }
    }
    if (ngood!=0) {
      p.X  = sum[0] / ngood;
      p.Px = sum[1] / ngood;
      p.Y  = sum[2] / ngood;
      p.Py = sum[3] / ngood;
      p.S  = sum[4] / ngood;
      p.Pz = sum[5] / ngood;
      p.mass = sum[6] / ngood;
      p.Q  = sum[7] / ngood;
      p.N  = sum[8] / ngood;
    }
  }
  return p;
}

ParticleT Bunch6dT::get_reference_particle() const
{
  if (particles[0])
    return particles[0]; // MeV/c
  std::cerr << "warning: the beam's first particle was lost, using the beam centroid as reference particle.\n";
  return get_average_particle();
}

double Bunch6dT::get_reference_momentum() const
{
  return get_reference_particle().get_Pc(); // MeV/c
}

StaticVector<3> Bunch6dT::get_bunch_temperature()
{
  // accumulate velocoty variances discriminating by ParticleKey (mass, charge)
  std::unordered_map<ParticleKey,Weighted_incremental_variance,ParticleKeyHasher,ParticleKeyEquals> K_variance;
  {
    for (auto const &particle : particles) {
      if (particle) {
	const auto key = ParticleKey{ particle.mass, particle.Q };
	const auto P = particle.get_Px_Py_Pz();
	const auto K = StaticVector<3>(P[0]*P[0], P[1]*P[1], P[2]*P[2]) / (2*particle.mass);
	K_variance[key].append(K, particle.N);
      }
    }
  }
  
  // sums the temperatures Tx, Ty, Tz, for each ParticleKey
  StaticVector<3> retval(0.0);
  for (auto const &vv : K_variance) {
    retval += vv.second.variance();
  }

  return 1e6*retval; // eV
}

double Bunch6dT::get_S_mean(const double dt ) const // m, returns S0 such that <dt> = <(S-S0)/Vz> = dt
{
  CumulativeKahanSum<double> sum_S_iVz;
  CumulativeKahanSum<double> sum_iVz;
  for (auto const &particle: particles) {
    if (particle) {
      const double Vz = particle.get_Vx_Vy_Vz()[2];
      sum_S_iVz += particle.S / Vz; // mm/c
      sum_iVz += 1.0 / Vz; // 1/c
    }
  }
  if (sum_iVz != 0.0 && gsl_finite(sum_iVz))
    return (sum_S_iVz - dt) / sum_iVz / 1e3; // m
  return 0.0;
}

double Bunch6dT::get_S_min() const // m
{
  double S_min_mm = std::numeric_limits<double>::infinity();
  for (auto const &particle: particles) {
    if (particle) {
      if (particle.S<S_min_mm) {
	S_min_mm = particle.S;
      }
    }
  }
  return S_min_mm / 1e3;
}

double Bunch6dT::get_S_max() const // m
{
  double S_max_mm = -std::numeric_limits<double>::infinity();
  for (auto const &particle: particles) {
    if (particle) {
      if (particle.S>S_max_mm) {
	S_max_mm = particle.S;
      }
    }
  }
  return S_max_mm / 1e3;
}

Bunch6dT_info Bunch6dT::get_info() const
{
  double *w = new double[particles.size()];
  size_t wsum = 0;
  assert(w);
  double r2max = 0.0;
  for (size_t i=0; i<particles.size(); i++) {
    const ParticleT &particle = particles[i];
    if (particle && t>=particle.t0) {
      w[i] = 1.0;
      wsum++;
      double r2 = particle.X*particle.X + particle.Y*particle.Y;
      if (r2>r2max)
	r2max = r2;
    } else {
      w[i] = 0.0;
    }
  }
  constexpr size_t particle_stride = sizeof(ParticleT) / sizeof(double);
  const double rmax = sqrt(r2max);
  const ParticleT P0 = get_average_particle();
  const double var_X = gsl_stats_wvariance_m(w, 1, &particles[0].X, particle_stride, particles.size(), P0.X); // mm**2
  const double var_Y = gsl_stats_wvariance_m(w, 1, &particles[0].Y, particle_stride, particles.size(), P0.Y); // mm**2
  const double var_Z = gsl_stats_wvariance_m(w, 1, &particles[0].S, particle_stride, particles.size(), P0.S); // [mm/c]**2
  const double var_Px = gsl_stats_wvariance_m(w, 1, &particles[0].Px, particle_stride, particles.size(), P0.Px); // mrad**2
  const double var_Py = gsl_stats_wvariance_m(w, 1, &particles[0].Py, particle_stride, particles.size(), P0.Py); // mrad**2
  const double var_Pz = gsl_stats_wvariance_m(w, 1, &particles[0].Pz, particle_stride, particles.size(), P0.Pz); // (MeV/c)**2
  Bunch6dT_info info;
  info.t = t;
  info.mean_X = P0.X; // mm
  info.mean_Y = P0.Y; // mm
  info.mean_S = P0.S; // mm
  info.mean_Px = P0.Px; // MeV/c
  info.mean_Py = P0.Py; // MeV/c
  info.mean_Pz = P0.Pz; // MeV/c
  info.mean_E = P0.get_total_energy(); // average total energy in MeV
  info.mean_K = info.mean_E - P0.mass; // average kinetic energy in MeV
  if (wsum>1) {
    info.sigma_X = sqrt(var_X); // mm
    info.sigma_Y = sqrt(var_Y); // mm
    info.sigma_Z = sqrt(var_Z); // mm
    info.sigma_Py = sqrt(var_Px); // MeV/c
    info.sigma_Px = sqrt(var_Py); // MeV/c
    info.sigma_Pz = sqrt(var_Pz); // MeV/c
    info.sigma_XPx = stats_wcovariance_m(w, 1, &particles[0].X, particle_stride, &particles[0].Px, particle_stride, particles.size(), P0.X, P0.Px); // mm*MeV/c
    info.sigma_YPy = stats_wcovariance_m(w, 1, &particles[0].Y, particle_stride, &particles[0].Py, particle_stride, particles.size(), P0.Y, P0.Py); // mm*MeV/c
    info.sigma_ZPz = stats_wcovariance_m(w, 1, &particles[0].S, particle_stride, &particles[0].Pz, particle_stride, particles.size(), P0.S, P0.Pz); // mm*MeV/c
    info.sigma_E = P0.get_Pc() / info.mean_E * (info.sigma_Px + info.sigma_Py + info.sigma_Pz); // kinetic-energy spread in MeV
    const double sigma_XPz = stats_wcovariance_m(w, 1, &particles[0].X, particle_stride, &particles[0].Pz, particle_stride, particles.size(), P0.X, P0.Pz); // mm*MeV/c
    const double sigma_YPz = stats_wcovariance_m(w, 1, &particles[0].Y, particle_stride, &particles[0].Pz, particle_stride, particles.size(), P0.Y, P0.Pz); // mm*MeV/c
    const double sigma_PxPz = stats_wcovariance_m(w, 1, &particles[0].Px, particle_stride, &particles[0].Pz, particle_stride, particles.size(), P0.Px, P0.Pz); // MeV/c*MeV/c
    const double sigma_PyPz = stats_wcovariance_m(w, 1, &particles[0].Py, particle_stride, &particles[0].Pz, particle_stride, particles.size(), P0.Py, P0.Pz); // MeV/c*MeV/c
    auto sqr = [] (double x ) { return x*x; };
    info.disp_x = info.sigma_Pz ? sigma_XPz * P0.Pz / sqr(info.sigma_Pz) / 1e3 : 0.0; // m
    info.disp_y = info.sigma_Pz ? sigma_YPz * P0.Pz / sqr(info.sigma_Pz) / 1e3 : 0.0; // m
    info.disp_xp = info.sigma_Pz ? sigma_PxPz / sqr(info.sigma_Pz) : 0.0; // rad
    info.disp_yp = info.sigma_Pz ? sigma_PyPz / sqr(info.sigma_Pz) : 0.0; // rad
    MatrixNd sigma_4d(4,4);
    sigma_4d[0][0] = var_X; // mm**2
    sigma_4d[0][1] = sigma_4d[1][0] = info.sigma_XPx; // mrad**2
    sigma_4d[0][2] = sigma_4d[2][0] = stats_wcovariance_m(w, 1, &particles[0].X, particle_stride, &particles[0].Y,  particle_stride, particles.size(), P0.X, P0.Y); // mm**2
    sigma_4d[0][3] = sigma_4d[3][0] = stats_wcovariance_m(w, 1, &particles[0].X, particle_stride, &particles[0].Py, particle_stride, particles.size(), P0.X, P0.Py); // mm*MeV/c
    sigma_4d[1][1] = var_Px; // (MeV/c)**2
    sigma_4d[2][1] = sigma_4d[1][2] = stats_wcovariance_m(w, 1, &particles[0].Y, particle_stride, &particles[0].Px, particle_stride, particles.size(), P0.Y, P0.Px); // mm*MeV/c
    sigma_4d[3][1] = sigma_4d[1][3] = stats_wcovariance_m(w, 1, &particles[0].Py, particle_stride, &particles[0].Px,  particle_stride, particles.size(), P0.Py, P0.Px); // (MeV/c)**2
    sigma_4d[2][2] = var_Y; // mm*mm
    sigma_4d[2][3] = sigma_4d[3][2] = info.sigma_YPy; // mm*MeV/c
    sigma_4d[3][3] = var_Py; // (MeV/c)**2
    info.emitt_4d = 1e3 * pow(det(sigma_4d), 0.25) / P0.mass; // mm*mrad, 4d normalized emittance
    info.emitt_x = sqrt(var_X * var_Px - info.sigma_XPx * info.sigma_XPx) * 1e3 / P0.get_Pc(); // geometric emittance mm * mrad
    info.emitt_y = sqrt(var_Y * var_Py - info.sigma_YPy * info.sigma_YPy) * 1e3 / P0.get_Pc(); // geometric emittance mm * mrad
    info.emitt_z = sqrt(var_Z * var_Pz - info.sigma_ZPz * info.sigma_ZPz) * 1e3 / P0.get_Pc(); // geometric emittance mm * permille
    info.alpha_x = -info.sigma_XPx / info.emitt_x / P0.get_Pc() * 1e3;
    info.alpha_y = -info.sigma_YPy / info.emitt_y / P0.get_Pc() * 1e3;
    info.alpha_z = -info.sigma_ZPz / info.emitt_z / P0.get_Pc() * 1e3;
    info.beta_x = var_X / info.emitt_x; // mm/mrad = m
    info.beta_y = var_Y / info.emitt_y; // mm/mrad = m
    info.beta_z = var_Z / info.emitt_z; // mm/permille = m
    const double beta0_gamma0 = P0.get_Pc() / P0.mass;
    info.emitt_x *= beta0_gamma0; // normalised emittance mm * mrad
    info.emitt_y *= beta0_gamma0; // normalised emittance mm * mrad
    info.emitt_z *= beta0_gamma0; // normalised emittance mm * permille
  } else {
    info.sigma_X = 0.0;
    info.sigma_Y = 0.0;
    info.sigma_Z = 0.0;
    info.sigma_Py = 0.0;
    info.sigma_Px = 0.0;
    info.sigma_Pz = 0.0;
    info.sigma_XPx = 0.0;
    info.sigma_YPy = 0.0;
    info.emitt_4d = 0.0;
    info.sigma_ZPz = 0.0;
    info.sigma_E = 0.0;
    info.emitt_x = 0.0;
    info.emitt_y = 0.0;
    info.emitt_z = 0.0;
    info.alpha_x = 0.0;
    info.alpha_y = 0.0;
    info.alpha_z = 0.0;
    info.beta_x = 0.0;
    info.beta_y = 0.0;
    info.beta_z = 0.0;
  }
  info.rmax = rmax;
  info.transmission = get_ngood() * 100.0 / particles.size();
  delete []w;
  return info;
}

MatrixNd Bunch6dT::get_phase_space(const char *fmt, const char *select ) const
{
  MatrixNd retval;
  if (particles.size()==0)
    return retval;
  struct _ref_par {
    double Pc; // MeV/c
    double S; // mm
  } reference_particle = [&] () {
    if (particles[0]) {
      return _ref_par {
	particles[0].get_Pc(), // MeV/c
	particles[0].S // mm
      };
    }
    std::cerr << "warning: the beam's first particle was lost, using the beam centroid as reference particle.\n";
    return _ref_par {
      get_average_particle().get_Pc(), // MeV/c
      get_S_min() * 1e3 // mm
    };
  }();
  const bool all = strncmp(select, "all", 4) == 0;
  size_t rows = all ? particles.size() : get_ngood();
  size_t cols = 0; // will be initialized later
  bool error_msg = false;
  size_t i = 0;
  for (const ParticleT &particle : particles) {
    const bool particle_ok = particle;
    if (all || particle_ok) {
      std::vector<double> values;
      for(const char *ptr0 = fmt, *ptr = strchr(ptr0, '%'); ptr; ptr0 = ptr, ptr = strchr(ptr0, '%')) {
	/// print any constants in 'fmt', if present
	{
	  char *endptr;
	  std::string nptr(ptr0, size_t(ptr-ptr0));
	  double value = strtod(nptr.c_str(), &endptr);
	  if (value!=0.0 || endptr!=nptr.c_str()) {
	    values.push_back(value);
	  }
	}
	ptr++;
	double value = 0.0;
	const auto p = particle.get_px_py_pz(reference_particle.Pc);
	const auto v = particle.get_Vx_Vy_Vz();
	const double K[3] = {
	  hypot(particle.Px, particle.mass) - particle.mass,
	  hypot(particle.Py, particle.mass) - particle.mass,
	  hypot(particle.Pz, particle.mass) - particle.mass
	};
	const double Z = particle.S - reference_particle.S; // Z position mm
	const double xp = particle.Px * 1e3 / particle.Pz; // mrad
	const double yp = particle.Py * 1e3 / particle.Pz; // mrad
	const double dt = -Z / v[2]; // mm/c
	if (get_token(&ptr, "deg@")) {
	  char *endptr;
	  double frequency = strtod(ptr, &endptr);
	  if (endptr == ptr) {
	    if (!error_msg) {
	      std::cerr << "warning: modifier %deg requies to specify a frequency, in MHz, e.g. %deg@2998.5\n";
	      error_msg = true;
	    }
	    continue;
	  }
	  value = fmod(dt / 1e3 / C_LIGHT * frequency * 1e6 * 360.0, 360.0);
	  ptr = endptr;
	}
	else if (get_token(&ptr, "Vx")) value = v[0]; // [c]
	else if (get_token(&ptr, "Vy")) value = v[1]; // [c]
	else if (get_token(&ptr, "Vz")) value = v[2]; // [c]
	else if (get_token(&ptr, "Px")) value = particle.Px; // [MeV/c]
	else if (get_token(&ptr, "Py")) value = particle.Py; // [MeV/c]
	else if (get_token(&ptr, "Pz")) value = particle.Pz; // [MeV/c]
	else if (get_token(&ptr, "px")) value = p[0]; // [mrad]
	else if (get_token(&ptr, "py")) value = p[1]; // [mrad]
	else if (get_token(&ptr, "pz")) value = p[2]; // [mrad]
	else if (get_token(&ptr, "Kx")) value = K[0]; // MeV
	else if (get_token(&ptr, "Ky")) value = K[1]; // MeV
	else if (get_token(&ptr, "Kz")) value = K[2]; // MeV
	else if (get_token(&ptr, "Pc")) value = particle.get_Pc(); // MeV
	else if (get_token(&ptr, "pt")) value = particle.get_pt(reference_particle.Pc) * 1e3; // permille
	else if (get_token(&ptr, "dt")) value = dt; // delay to be at S = reference_particle.S [mm/c]
	else if (get_token(&ptr, "xp")) value = xp; // [mrad]
	else if (get_token(&ptr, "yp")) value = yp; // [mrad]
	else if (get_token(&ptr, "zp")) value = 1e3; // [mrad]
	else if (get_token(&ptr, "t0")) value = particle.t0; // [mm/c]
	else if (get_token(&ptr, 'x')) value = particle.X + dt * v[0]; // [mm] 'x' 'y' and 't' are the coordinates being tracked
	else if (get_token(&ptr, 'y')) value = particle.Y + dt * v[1]; // [mm] that is, on a plane at S = bunch.S
	else if (get_token(&ptr, 'z')) value = reference_particle.S; // [mm] that is, on a plane at S = reference_particle.S
	else if (get_token(&ptr, 't')) value = t; // t is the proper time of each particle [mm/c]
	else if (get_token(&ptr, 'X')) value = particle.X; // [mm] 'X' 'Y' 'Z' are the particle coordinates
	else if (get_token(&ptr, 'Y')) value = particle.Y; // [mm]
	else if (get_token(&ptr, 'Z')) value = Z; // [mm]
	else if (get_token(&ptr, 'd')) value = particle.get_delta(reference_particle.Pc) * 1e3; // permille
	else if (get_token(&ptr, 'K')) value = particle.get_kinetic_energy(); // MeV
	else if (get_token(&ptr, 'E')) value = particle.get_total_energy(); // MeV
	else if (get_token(&ptr, 'S')) value = particle.S; // mm
	else if (get_token(&ptr, 'm')) value = particle.mass; // [MeV/c/c] rest mass
	else if (get_token(&ptr, 'Q')) value = particle.Q; // [e+] charge of each particle
	else if (get_token(&ptr, 'N')) value = particle.N; // number of particles per macroparticle
	else if (get_token(&ptr, 'P')) value = particle.get_Pc(); // MeV/c
	else if (get_token(&ptr, 'V')) value = norm(v); // c
	else {
	  if (!error_msg) {
	    const char *next = strpbrk(ptr, "% \n\t");
	    std::string id = next ? std::string(ptr, next-ptr) : ptr;
	    std::cerr << "warning: unknown identifier '%" << id << "'\n";
	    error_msg = true;
	  }
	  continue;
	}
	if (!particle_ok)
	  value = GSL_NAN;
	values.push_back(value);
      }
      if (cols == 0) {
	cols = values.size();
	retval.resize(rows, cols);
      }
      std::copy(values.begin(), values.end(), retval[i]);
      i++;
    }
  }
  return retval;
}

MatrixNd Bunch6dT::get_lost_particles() const
{
  MatrixNd ret(get_nlost(), 10);
  size_t i = 0;
  for (auto const &particle : particles) {
    if (!particle) {
      ret[i][0] = particle.X; // mm
      ret[i][1] = particle.Px; // MeV/c
      ret[i][2] = particle.Y; // mm
      ret[i][3] = particle.Py; // MeV/c
      ret[i][4] = particle.S; // mm
      ret[i][5] = particle.Pz; // MeV/c
      ret[i][6] = particle.t_lost; // mm/c
      ret[i][7] = particle.mass;
      ret[i][8] = particle.Q;
      ret[i][9] = particle.N;
      i++;
    }
  }
  return ret;
}

MatrixNd Bunch6dT::get_lost_particles_mask() const
{
  MatrixNd ret(particles.size(), 1);
  size_t i = 0;
  for (auto const &particle : particles) {
    ret[i++][0] = particle ? 0.0 : 1.0;
  }
  return ret;
}

size_t Bunch6dT::get_ngood() const
{
  size_t ngood = 0;
  for (auto const &particle : particles) {
    if (particle) {
      ngood++;
    }
  }
  return ngood;
}

size_t Bunch6dT::get_nlost() const
{
  return particles.size() - get_ngood();
}

void Bunch6dT::apply_force(const MatrixNd &force, double dt_mm ) // dt is in mm/c (1 mm = ~3.33 ps)
{
  const size_t Nthreads = RFT::number_of_threads;
  auto apply_force_parallel = [&](size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      auto &particle = particles[i];
      if (particle) {
	const auto F = StaticVector<3>(force[i]); // MeV/m
	const auto v = particle.get_Vx_Vy_Vz(); // c
	const auto a = (F - dot(v,F)*v) / (particle.get_total_energy() * 1e3); // c^2/mm
	const auto dX = (v + 0.5 * a * dt_mm) * dt_mm; // mm
	const auto dP = F * (dt_mm / 1e3); // MeV/c
	// update particle
	particle.X += dX[0]; // mm
	particle.Y += dX[1]; // mm
	particle.S += dX[2]; // mm
	particle.Px += dP[0]; // MeV/c
	particle.Py += dP[1]; // MeV/c
	particle.Pz += dP[2]; // MeV/c
      }
    }
  };
  const size_t Nparticles = particles.size();
  for_all(Nthreads, Nparticles, apply_force_parallel);

  // final step
  t += dt_mm;
}

void Bunch6dT::kick(const MatrixNd &force, double dt_mm ) // dt is in mm/c (1 mm = ~3.33 ps)
{
  const size_t Nthreads = RFT::number_of_threads;
  auto apply_force_parallel = [&](size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      auto &particle = particles[i];
      if (particle) {
	const auto F = StaticVector<3>(force[i]); // MeV/m
	const auto dP = F * (dt_mm / 1e3); // MeV/c
	// update particle
	particle.Px += dP[0]; // MeV/c
	particle.Py += dP[1]; // MeV/c
	particle.Pz += dP[2]; // MeV/c
      }
    }
  };
  const size_t Nparticles = particles.size();
  for_all(Nthreads, Nparticles, apply_force_parallel);
}

void Bunch6dT::drift(double dt_mm ) // dt is in mm/c (1 mm = ~3.33 ps)
{
  const size_t Nthreads = RFT::number_of_threads;
  auto drift_parallel = [&](size_t thread, size_t start, size_t end ) -> void {
    for (size_t i=start; i<end; i++) {
      auto &particle = particles[i];
      if (particle) {
	const auto v = particle.get_Vx_Vy_Vz(); // MeV
	particle.X += v[0] * dt_mm; // mm
	particle.Y += v[1] * dt_mm; // mm
	particle.S += v[2] * dt_mm; // mm
      }
    }
  };
  const size_t Nparticles = particles.size();
  for_all(Nthreads, Nparticles, drift_parallel);

  // final step
  t += dt_mm;
}

bool Bunch6dT::load(const char *filename )
{
  File_IStream file(filename);
  if (file) {
    std::string rf_track_version;
    file >> rf_track_version >> t >> coasting_ >> particles;
  }
  return file;
}

bool Bunch6dT::save(const char *filename ) const
{
  File_OStream file(filename);
  if (file)
    file << std::string(RF_TRACK_VERSION) << t << coasting_ << particles;
  return file;
}

bool Bunch6dT::save_as_dst_file(const char *filename, double frequency_MHz ) const
{
  if (frequency_MHz!=0.0) {
    if (FILE *file = fopen(filename, "w")) {
      std::ostringstream fmt;
      fmt << "%x %xp %y %yp %deg@" << frequency_MHz << " %K";
      MatrixNd bunch = Bunch6dT::get_phase_space(fmt.str().c_str());
      int Np = bunch.rows();
      double Ib = 0.0; // beam current [mA]
      fputc(0xfd,file);
      fputc(0x50,file);
      fwrite(&Np, sizeof(int), 1, file);
      fwrite(&Ib, sizeof(double), 1, file);
      fwrite(&frequency_MHz, sizeof(double), 1, file);
      fputc(0x54,file);
      for (int i=0; i<Np; i++) {
	double P[6] = {
	  bunch[i][0] / 10.0,    // cm
	  bunch[i][1] / 1000.0,  // rad
	  bunch[i][2] / 10.0,    // cm
	  bunch[i][3] / 1000.0,  // rad
	  deg2rad(bunch[i][4]),  // rad
	  bunch[i][5]            // MeV
	};
	fwrite(P, sizeof(double), 6, file);
      }
      fwrite(&particles[0].mass, sizeof(double), 1, file);
      fclose(file);
      return true;
    } else std::cerr << "error: couldn't open file\n";
  } else std::cerr << "error: frequency must be != 0.0\n";
  return false;
}

bool Bunch6dT::save_as_sdds_file(const char *filename, const char *description ) const
{
  if (FILE *fout = fopen(filename, "w")) {
    const char *desc = description ? description : "This file was created by RF-Track " RF_TRACK_VERSION " (Andrea Latina <andrea.latina@cern.ch>)";
    fputs("SDDS1 \n",fout);
    fprintf(fout,"&description text=\"%s\", &end\n", desc); 
    fputs("&parameter name=prps, format_string=\%s, type=string, &end \n",fout);
    fputs("&parameter name=t0,symbol=t0,units=ns,description=\"reference time\", type=double, &end \n",fout);
    fputs("&parameter name=z0,symbol=z0,units=m,description=\"reference position\", type=double, &end \n",fout);
    fputs("&parameter name=p0,symbol=P0,units=MeV/c,description=\"reference momentum\", type=double, &end \n",fout);
    fputs("&parameter name=Q,symbol=Q,units=nC,description=\"total charge\", type=double, &end \n",fout);
    fputs("&column name=x,symbol=x,units=mm,description=\"horizontal position\", type=double, &end \n",fout);
    fputs("&column name=xp,symbol=x',units=mrad,description=\"horizontal slope\", type=double, &end \n",fout); 
    fputs("&column name=y,symbol=y,units=mm,description=\"vertical position\", type=double, &end \n",fout);
    fputs("&column name=yp,symbol=y',units=mrad,description=\"vertical slope\", type=double, &end \n",fout);
    fputs("&column name=z,symbol=z,units=mm,description=\"longitudinal position\", type=double, &end \n",fout);
    fputs("&column name=E,symbol=E,units=MeV,description=\"energy\", type=double, &end \n",fout);
    fputs("&column name=t,symbol=t,units=s,description=\"time\", type=double, &end \n",fout);
    fputs("&column name=px,symbol=Px,units=MeV/c,description=\"horizontal momentum\", type=double, &end \n",fout); 
    fputs("&column name=py,symbol=Py,units=MeV/c,description=\"vertical momentum\", type=double, &end \n",fout);
    fputs("&column name=pz,symbol=Pz,units=MeV/c,description=\"longitudinal momentum\", type=double, &end \n",fout);
    fputs("&column name=p,symbol=P,units=MeV/c,description=\"total momentum\", type=double, &end \n",fout);
    fputs("&column name=ID, description=\"particle index\",type=double, &end \n",fout);
    fputs("&data mode=ascii, &end\n",fout);
    fputs("! page number 1\n",fout);
    fprintf(fout,"\"%s\"\n",desc);
    double total_charge = 0.0;
    std::for_each(particles.begin(), particles.end(), [&](auto &p){ if (p) { total_charge += p.N * p.Q; }});
    const ParticleT Pref = get_reference_particle();
    fprintf(fout,"%16.9e\n",t / RFT::ns); // ns
    fprintf(fout,"%16.9e\n",Pref.S/1e3); // m
    fprintf(fout,"%16.9e\n",Pref.get_Pc()); // MeV/c
    fprintf(fout,"%16.9e\n",total_charge / RFT::nC); // nC
    fprintf(fout,"         %ld\n", get_ngood());
    int index = 0;
    for (const auto &p: particles) {
      if (p) {
	const auto &angles = p.get_xp_yp_zp();
	fprintf(fout, "%16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %16.9e %d\n",
		p.X, angles[0],
		p.Y, angles[1],
		p.S,
		p.get_total_energy(),
		t / RFT::s,
		p.Px, p.Py, p.Pz,
		p.get_Pc(),
		index++);
      }
    }
    fclose(fout);
    return true;
  } else std::cerr << "error: couldn't open file\n";
  return false;
}
