/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef bunch6d_hh
#define bunch6d_hh

#include <vector>
#include <utility>

#include "matrixnd.hh"
#include "particle.hh"
#include "constants.hh"
#include "bunch6d_twiss.hh"
#include "bunch6d_info.hh"

// input phase space matrix X must containt 6 columns t x [mm] xp [mrad] y [mm] yp [mrad] z [mm] Pc [MeV]

class Bunch6dT;
class Bunch6d {
  std::vector<Particle> particles; ///< particles;
  double coasting_ = 0.0; // mm, period or 0.0 for bunched beam (default)
public:
  double S; ///< m, longitudinal position along the accelerator
  Bunch6d(size_t N=0 ) : particles(N), S(0.0) {}
  Bunch6d(double mass, double population, double charge, const MatrixNd &X );
  Bunch6d(double mass, double population, double charge, double Pref, Bunch6d_twiss T, size_t N, double sigma_cut = 0.0 );
  Bunch6d(const Bunch6dT &b, const char *select = "all" ); // select == "all" all particles, "exist" only those that exist 
  Bunch6d(const MatrixNd &X );

  const Particle &operator[] (size_t pos ) const { return particles[pos]; }
  Particle &operator[] (size_t pos ) { return particles[pos]; }
  
  void clear() { S = coasting_ = 0.0; particles.clear(); }
  
  size_t size() const { return particles.size(); }

  void append(const Bunch6d &b ) { particles.reserve(particles.size() + b.size()); particles.insert(end(particles), begin(b.particles), end(b.particles)); }
  
  double coasting() const { return coasting_; }
  void set_coasting(double period /* mm */ ) { coasting_ = period; }

  //void set_population(double _population ) { population = _population; }
  //void set_mass(double _mass ) { mass = _mass; }
  void set_phase_space(const MatrixNd &X );
  void set_S(double S_ ) { S = S_; }

  const Particle &get_particle(size_t i ) const { return particles[i]; }
  Particle &get_particle(size_t i ) { return particles[i]; }
  double get_t_mean(double dS=0.0 ) const; // mm/c, returns the time such that <Z> = dS (dS in meters)
  double get_t_min() const; // mm/c
  double get_t_max() const; // mm/c
  double get_S() const { return S; }
  //double get_mass() const { return mass; }
  //double get_population() const { return population; }
  StaticVector<3> get_center_of_mass_Vx_Vy_Vz() const;
  StaticVector<3> get_average_Vx_Vy_Vz() const;
  Particle get_average_particle_t0(double dS=0.0 ) const; // returns the average particle such that <Z> = dS (dS in meters )
  Particle get_average_particle() const;
  Particle get_reference_particle() const;
  double get_reference_momentum() const;

  StaticVector<3> get_bunch_temperature(); // eV
  
  MatrixNd get_phase_space(const char *fmt = "%x %xp %y %yp %t %Pc", const char *select = "good" ) const; // "good" or "all"
  MatrixNd get_lost_particles() const;
  MatrixNd get_lost_particles_mask() const;
  size_t get_ngood() const;
  size_t get_nlost() const; // Nparticles - get_ngood()

  Bunch6d_info get_info() const;

  void apply_force(const MatrixNd &force, double dS_mm ); // force is a "Nparticles x 3-column" matrix in MeV/m; dS is in mm
  void kick(const MatrixNd &force, double dS_mm ); // force is a "Nparticles x 3-column" matrix in MeV/m; dS is in mm
  void drift(double dS_mm ); // force is a "Nparticles x 3-column" matrix in MeV/m; dS is in mm
  
  bool load(const char *filename );
  bool save(const char *filename ) const;
  bool save_as_dst_file(const char *filename, double frequency_MHz ) const;
  bool save_as_sdds_file(const char *filename, const char *description = NULL ) const;

};

#endif /* bunch6d_hh */
