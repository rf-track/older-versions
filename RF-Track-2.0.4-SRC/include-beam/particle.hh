/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef particle_hh
#define particle_hh

#include <cmath>
#include <gsl/gsl_nan.h>
#include <gsl/gsl_sys.h>

#include "hypot.hh"
#include "stream.hh"
#include "static_vector.hh"

struct Particle { // particle in space
  double mass; ///< [MeV/c/c]
  double Q;  ///< [e+] charge of each particle
  double N;  ///< [#] number of particles per macroparticle
  double x;  ///< [mm]
  double xp; ///< Px/Pz [mrad]
  double y;  ///< [mm]
  double yp; ///< Py/Pz [mrad]
  double t;  ///< [mm/c] proper time of each particle
  double Pc; ///< [MeV/c] total momentum (can be negative)
  double S_lost; ///< [m] if S_lost isn't Nan then the particle is lost at location S_lost
  Particle() : S_lost(GSL_NAN) {}
  Particle(double x_, double xp_, double y_, double yp_, double t_, double Pc_ ) : x(x_), xp(xp_), y(y_), yp(yp_), t(t_), Pc(Pc_), S_lost(GSL_NAN) {}
  operator bool() const { return gsl_isnan(S_lost); }
  void lost_at(double S ) { S_lost = S; }
  double get_Pc_sqr() const { return Pc*Pc; };
  double get_Pc() const { return Pc; }
  double get_total_energy_sqr() const { return mass*mass + Pc*Pc; } // MeV**2
  double get_total_energy() const { return hypot(mass,Pc); } // MeV
  double get_kinetic_energy() const { return get_total_energy() - mass; } // MeV
  double get_delta_plus_1(double P0c ) const { return Pc / P0c; } // #
  double get_delta(double P0c ) const { return (Pc - P0c) / P0c; } // #
  double get_beta_sqr() const { double Pc_sqr = get_Pc_sqr(); return Pc_sqr / (mass*mass + Pc_sqr); } // #
  double get_beta() const { return fabs(Pc) / get_total_energy(); } // #
  double get_gamma_sqr() const { return get_total_energy_sqr() / (mass*mass); }
  double get_gamma() const { return get_total_energy() / mass; } // #
  // pt: rewrite of { return (get_total_energy() - sqrt(mass*mass + P0c*P0c)) / P0c; } to avoid cancellation
  double get_pt(double P0c ) const { const double Pz = 1e3 * Pc / hypot(1e3, xp, yp); return (Pz+P0c) * (Pz-P0c) / P0c / (get_total_energy() + hypot(mass, P0c)); } // #
  StaticVector<3> get_xp_yp_zp() const { return StaticVector<3>(xp, yp, 1e3); } // mrad
  StaticVector<3> get_Vx_Vy_Vz() const { double Vz_div_1000 = get_beta() / hypot(1e3, xp, yp); return StaticVector<3>(Vz_div_1000*xp, Vz_div_1000*yp, Vz_div_1000*1e3); } // c
  StaticVector<3> get_Px_Py_Pz() const { const double Pz_div_1000 = Pc / hypot(1e3, xp, yp); return StaticVector<3>(Pz_div_1000*xp, Pz_div_1000*yp, Pz_div_1000*1e3); } // MeV/c
  StaticVector<3> get_px_py_pz(double P0c ) const { return 1e3*get_Px_Py_Pz()/P0c; } // mrad
  StaticVector<4> get_four_momentum() const { const auto &P = get_Px_Py_Pz(); return StaticVector<4>(get_total_energy(), P[0], P[1], P[2]); } // MeV/c
  friend OStream &operator<<(OStream &stream, const Particle &p ) { return stream << p.mass << p.Q << p.N << p.x << p.xp << p.y << p.yp << p.t << p.Pc << p.S_lost; }
  friend IStream &operator>>(IStream &stream, Particle &p ) { return stream >> p.mass >> p.Q >> p.N >> p.x >> p.xp >> p.y >> p.yp >> p.t >> p.Pc >> p.S_lost; }

};

struct ParticleT { // particle in time
  double mass; ///< [MeV/c/c]
  double Q;  ///< [e+] charge of each particle
  double N;  ///< [#] number of particles per macroparticle
  double X;  ///< [mm]
  double Px; ///< [MeV/c]
  double Y;  ///< [mm]
  double Py; ///< [MeV/c]
  double S;  ///< [mm]
  double Pz; ///< [MeV/c]
  double t0; ///< [mm/c] creation time
  double t_lost; ///< if !NAN, particle was lost at time t_lost
  ParticleT() : t0(0.0), t_lost(GSL_NAN) {}
  ParticleT(double X_, double xp_, double Y_, double yp_, double Z_, double Pc_ ) : X(X_), Y(Y_), S(Z_), t0(0.0), t_lost(GSL_NAN)
  {
    const double Pz_div_1000 = Pc_ / hypot(xp_, yp_, 1e3); // 1e-3 * MeV/c
    Px = Pz_div_1000 * xp_; // MeV/c
    Py = Pz_div_1000 * yp_; // MeV/c
    Pz = Pz_div_1000 * 1e3; // MeV/c
  }
  operator bool() const { return gsl_isnan(t_lost); }
  void lost_at(double t ) { t_lost = t; }
  double get_Pc_sqr() const { return Px*Px + Py*Py + Pz*Pz; }
  double get_Pc() const { return hypot(Px, Py, Pz); }
  double get_total_energy_sqr() const { return mass*mass + get_Pc_sqr(); } // MeV**2
  double get_total_energy() const { return sqrt(get_total_energy_sqr()); } // MeV
  double get_kinetic_energy() const { return get_total_energy() - mass; } // MeV
  double get_delta_plus_1(double P0c ) const { return get_Pc() / P0c; } // #
  double get_delta(double P0c ) const { return (get_Pc() - P0c) / P0c; } // #
  double get_beta_sqr() const { return get_Pc_sqr() / get_total_energy_sqr(); } // #
  double get_beta() const { return sqrt(get_beta_sqr()); } // #
  double get_gamma_sqr() const { return get_total_energy_sqr() / (mass*mass); }
  double get_gamma() const { return get_total_energy() / mass; } // #
  // pt: rewrite of { return (get_total_energy() - sqrt(mass*mass + P0c*P0c)) / P0c; } to avoid cancellation
  double get_pt(double P0c ) const { return (Pz+P0c) * (Pz-P0c) / P0c / (get_total_energy() + hypot(mass, P0c)); } // #
  StaticVector<3> get_xp_yp_zp() const { return 1e3 * StaticVector<3>(Px/Pz, Py/Pz, 1.0); } // mrad
  StaticVector<3> get_Vx_Vy_Vz() const { return StaticVector<3>(Px, Py, Pz) / get_total_energy(); } // c
  StaticVector<3> get_Px_Py_Pz() const { return StaticVector<3>(Px, Py, Pz);  } // MeV/c
  StaticVector<3> get_px_py_pz(double P0c ) const { return 1e3 * get_Px_Py_Pz() / P0c; } // mrad
  StaticVector<4> get_four_momentum() const { return StaticVector<4>(get_total_energy(), Px, Py, Pz); } // MeV/c
  friend OStream &operator<<(OStream &stream, const ParticleT &p ) { return stream << p.mass << p.Q << p.N << p.X << p.Px << p.Y << p.Py << p.S << p.Pz << p.t_lost; }
  friend IStream &operator>>(IStream &stream, ParticleT &p ) { return stream >> p.mass >> p.Q >> p.N >> p.X >> p.Px >> p.Y >> p.Py >> p.S >> p.Pz >> p.t_lost; }
};

#endif /* particle_hh */
