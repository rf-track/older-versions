/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef bunch6d_twiss_hh
#define bunch6d_twiss_hh

struct Bunch6d_twiss {
  double emitt_x; // mm.mrad normalised emittance
  double emitt_y; // mm.mrad normalised emittance
  double emitt_z; // mm.permille
  double alpha_x; 
  double alpha_y;
  double alpha_z;
  double beta_x;  // m
  double beta_y;  // m
  double beta_z;  // m
  double sigma_z = 0.0; // mm
  double sigma_d = 0.0; // 0.1%
  double disp_x;  // m
  double disp_xp; // rad
  double disp_y;  // m
  double disp_yp; // rad
  double disp_z;  // m
  Bunch6d_twiss() : emitt_x(0.0), emitt_y(0.0), emitt_z(0.0),
		    alpha_x(0.0), alpha_y(0.0), alpha_z(0.0),
		    beta_x(1.0), beta_y(1.0), beta_z(1.0),
		    disp_x(0.0), disp_xp(0.0),
		    disp_y(0.0), disp_yp(0.0), disp_z(0.0) {}
};

#endif /* bunch6d_twiss_hh */
