/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef element_hh
#define element_hh

#include <utility>
#include <memory>
#include <limits>
#include <cmath>

#include "transport_table.hh"
#include "static_matrix.hh"
#include "aperture.hh"
#include "matrixnd.hh"
#include "bunch6d.hh"
#include "frame.hh"
// class Lattice;
class Element : public Aperture {
  Frame frame; // mm, rad misalignment
  size_t nsteps; /// number of tracking steps
  size_t sc_nsteps; /// number of space-charge steps (default 0)
  size_t tt_nsteps; /// number of transport-table steps (default 0)
protected:
  virtual void track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t number_of_steps, size_t thread ) const {}; // track one particle
  virtual void track0_initialize(Bunch6d &bunch ) {} // initalize
  virtual void track0_finalize(Bunch6d &bunch ) {} // finalize
public:
  Element(size_t nsteps_ = 1 ) : nsteps(nsteps_), sc_nsteps(0), tt_nsteps(0) {}
  virtual ~Element() = default;
  virtual std::shared_ptr<Element> clone() const = 0;
  virtual double get_length() const = 0;
  const Frame &get_frame() const { return frame; };
  void set_offsets(double x0, double y0, double z0, // m
		   double roll, double pitch, double yaw, // rad
		   const std::string &reference = "entrance" );
  void set_offsets(double x0, double y0, double z0, // m
		   const std::string &reference = "entrance" );
  void set_angles(double roll, double pitch, double yaw, // rad
		  const std::string &reference = "entrance" );
  void zero_offsets() { set_offsets(0.0, 0.0, 0.0, 0.0, 0.0, 0.0); }
  size_t get_nsteps() const { return nsteps; }
  size_t get_sc_nsteps() const { return sc_nsteps; }
  virtual void set_tt_nsteps(size_t tt_nsteps_ ) { tt_nsteps = tt_nsteps_; }
  virtual void set_sc_nsteps(size_t sc_nsteps_ ) { sc_nsteps = sc_nsteps_; }
  virtual void set_nsteps(size_t nsteps_ ) { nsteps = nsteps_; }
  virtual std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t ) { return std::pair<StaticVector<3>, StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0)); } // x,y,z [mm] ; t [mm/c]
  virtual std::pair<StaticMatrix<3,3>, StaticMatrix<3,3>> get_field_jacobian(double x, double y, double z, double t ); // x,y,z [mm] t [mm/c], returns V/m/mm, and T/mm
  virtual TransportTable track(Bunch6d &bunch );
};

#endif /* element_hh */
