/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef static_magnetic_field_map_hh
#define static_magnetic_field_map_hh

#include <complex>
#include <mutex>
#include <array>

#include "mesh3d.hh"
#include "element.hh"
#include "rbf_stencil.hh"
#include "parallel_ode_solver.hh"

// StaticMagneticField in vacuum
class StaticMagneticField : public Element, public Parallel_ODE_Solver {
  Mesh3d mesh_Ex; // V/m
  Mesh3d mesh_Ey; // V/m
  Mesh3d mesh_Ez; // V/m
  Mesh3d mesh_Bx; // T
  Mesh3d mesh_By; // T
  Mesh3d mesh_Bz; // T
  RBF_Stencil rbf_interpolate;
  StaticVector<3> B0; // T
  // cartesian bounding box
  double x0, y0, z0, z1; // mm
  double hx, hy, hz; // mm
  void track0_initialize(Bunch6d &bunch ); // initialize
  void track0_finalize(); // finalize
  void track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t number_of_steps, size_t thread  ) const;
  // auxiliary variables for GSL ODE integration
  std::vector<gsl_odeiv2_system> sys;
  mutable bool gsl_error;
public:
  StaticMagneticField(const Mesh3d &Ex = DefaultMesh3d,
	      const Mesh3d &Ey = DefaultMesh3d,
	      const Mesh3d &Ez = DefaultMesh3d,
	      const Mesh3d &Bx = DefaultMesh3d,
	      const Mesh3d &By = DefaultMesh3d,
	      const Mesh3d &Bz = DefaultMesh3d,
	      double x0 = 0.0, double y0 = 0.0, // m
	      double hx = 1.0, double hy = 1.0, double hz = 1.0, // m
	      double length = -1.0); // if -1 takes the default : (Ez.size()-1) * hz
  ~StaticMagneticField() = default;
  virtual StaticMagneticField *clone() const { return new StaticMagneticField(*this); }
  double get_length() const { return (z1-z0)/1e3; } // return m
  double get_hx() const { return hx/1e3; } // return m
  double get_hy() const { return hy/1e3; } // return m
  double get_hz() const { return hz/1e3; } // return m
  double get_nx() const { return mesh_Ex.size1(); }
  double get_ny() const { return mesh_Ex.size2(); }
  double get_nz() const { return mesh_Ex.size3(); }
  double get_x0() const { return x0/1e3; } // return m
  double get_y0() const { return y0/1e3; } // return m
  double get_z0() const { return z0/1e3; } // return m
  double get_x1() const { return (x0 + (mesh_Ex.size1()-1)*hx)/1e3; } // return m
  double get_y1() const { return (y0 + (mesh_Ex.size2()-1)*hy)/1e3; } // return m
  double get_z1() const { return z1/1e3; } // return m
  void set_hx(double hx_ ) { hx = hx_*1e3; } // accepts m
  void set_hy(double hy_ ) { hy = hy_*1e3; } // accepts m
  void set_hz(double hz_ ) { hz = hz_*1e3; } // accepts m
  void set_x0(double x0_ ) { x0 = x0_*1e3; } // accepts m
  void set_y0(double y0_ ) { y0 = y0_*1e3; } // accepts m
  void set_z0(double z0_ ) { z0 = z0_*1e3; } // accepts m
  void set_z1(double z1_ ) { z1 = z1_*1e3; } // accepts m
  void set_length(double length ); // accepts m
  void set_Ex_Ey_Ez(const Mesh3d &Ex,
		    const Mesh3d &Ey,
		    const Mesh3d &Ez );
  void set_Bx_By_Bz(const Mesh3d &Bx,
		    const Mesh3d &By,
		    const Mesh3d &Bz );
  double get_divB(double x, double y, double z ) const;
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t ); // x,y,z [mm] ; t [mm/c]
  std::pair<StaticMatrix<3,3>, StaticMatrix<3,3>> get_field_jacobian(double x, double y, double z, double t ); // x,y,z [mm] t [mm/c], returns V/m/mm, and T/mm

};

#endif /* static_magnetic_field_map_hh */
