/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef transport_table_hh
#define transport_table_hh

#include <list>

#include "bunch6d.hh"
#include "bunch6dt.hh"

class TransportTable {
  std::list<Bunch6d_info> t_table;
  std::list<Bunch6dT_info> t_tableT;
public:
  MatrixNd get_transport_table(const char *fmt = "%S %mean_x %mean_y %emitt_x %emitt_y" ) const;
  void append_bunch_info(const Bunch6d_info &info ) { t_table.push_back(info); }
  void append_bunch_info(const Bunch6dT_info &info ) { t_tableT.push_back(info); }
  void append_bunch_info(const Bunch6d &bunch ) { if (bunch.get_ngood()>=1) append_bunch_info(bunch.get_info()); }
  void append_bunch_info(const Bunch6dT &bunch ) { if (bunch.get_ngood()>=1) append_bunch_info(bunch.get_info()); }
  void clear() { t_table.clear(); t_tableT.clear(); }
  void append(TransportTable t ) { t_table.splice(t_table.end(), t.t_table); }

};

#endif /* transport_table_hh */


