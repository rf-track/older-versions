/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef external_field_hh
#define external_field_hh

#include <string>
#include <vector>
#include <memory>

#include "generic_field.hh"
#include "sub_process.hh"

class ExternalField : public GenericField {
  std::string external_command;
  double length; // m
  size_t index;
  struct _SubProcess {
    std::shared_ptr<SubProcess> process;
    char rbuffer[1024]; // recv buffer
    char sbuffer[1024]; // send buffer
    _SubProcess(const char *cmd ) { process = std::make_shared<SubProcess>(cmd); }
  };
  std::vector<_SubProcess> processes;
  StaticVector<3> static_Efield; // [V/m] static electric field embedding the Drift (e.g. Wein filter)
  StaticVector<3> static_Bfield; // [T] static magnetic field embedding the Drift (e.g. solenoid)
  ExternalField() = default;
public:
  ExternalField(double length, /* m */
		std::string external_command, /* string */
		size_t number_of_threads = 0);
  virtual std::shared_ptr<Element> clone() const override { return std::make_shared<ExternalField>(*this); }
  const StaticVector<3> &get_static_Efield() const { return static_Efield; }
  const StaticVector<3> &get_static_Bfield() const { return static_Bfield; } // T
  size_t get_number_of_threads() const { return processes.size(); }
  double get_length() const override { return length; } // m
  void set_static_Efield(double Ex, double Ey, double Ez ) { static_Efield = StaticVector<3>(Ex, Ey, Ez); }
  void set_static_Bfield(double Bx, double By, double Bz ) { static_Bfield = StaticVector<3>(Bx, By, Bz); }
  void set_length(double length_ ) { length = length_; } // m
  void set_number_of_threads(size_t n );
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t ) override; // x,y,z [mm] ; t [mm/c]

};

#endif /* external_field_hh */
