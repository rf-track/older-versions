/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef sw_structure_hh
#define sw_structure_hh

#include <mutex>

#include "generic_field.hh"

class SW_Structure : public GenericField {
  double z0, z1; // mm
  double omega;  // radian/(mm/c)
  double Lcell; // mm, cell length
  std::vector<double> coeffs; // V/m, the 0-th element is a1
  std::vector<double> qn; // 1/mm
  std::vector<double> coeffs1; // V/m
  std::vector<double> coeffs2; // T/mm
  std::vector<bool> ordinary_bessel; // whether it should use modified or ordinary bessel functions
  double number_of_cells; //
  double phi0;        // [rad] user-set phase w.r.t. to crest (default = 0.0, on crest acceleration)
  bool   t0_is_set;  // t0 has been set
  double t0;         // [mm/c] time offset (default = delay to accelerate the first bunch on crest)
  StaticVector<3> static_Bfield; // [T] static magnetic field embedding the Drift (e.g. solenoid)
  void track0_initialize(Bunch6d &bunch ) override
  {
    if (!t0_is_set) {
      t0 = bunch.get_t_min();
      t0_is_set = true;
    }
    GenericField::track0_initialize(bunch);
  }
public:
  SW_Structure(const std::vector<double> &coeffs_, double frequecy, double cell_length, double number_of_cells );
  SW_Structure() = default;
  virtual std::shared_ptr<Element> clone() const override { return std::make_shared<SW_Structure>(*this); }
  double get_length() const override { return (z1-z0)/1e3; } // return m
  double get_cell_length() const { return Lcell/1e3; } // m
  double get_z0() const { return z0/1e3; } // return m
  double get_z1() const { return z1/1e3; } // return m
  double get_t0() const { return t0; } // mm/c
  double get_phi() const { return phi0; }
  double get_phid() const { return 180.0 * phi0 / M_PI; }
  const std::vector<double> &get_coefficients() const { return coeffs; } // V/m (?)
  double get_frequency() const { return  omega * (C_LIGHT*1e3) / (2.0 * M_PI); } // Hz
  const StaticVector<3> &get_static_Bfield() const { return static_Bfield; }
  void set_static_Bfield(double Bx, double By, double Bz ) { static_Bfield = StaticVector<3>(Bx, By, Bz); }
  void set_z0(double z0_ ) { z0 = z0_*1e3; } // accepts m
  void set_z1(double z1_ ) { z1 = z1_*1e3; } // accepts m
  void set_length(double length_ ); // accepts m
  void set_phi(double p ) { phi0 = p; }
  void set_phid(double p ) { phi0 = M_PI * p / 180.0; }
  void set_t0(double t ) { t0_is_set = true; t0 = t; }
  void unset_t0() { t0_is_set = false; t0 = 0.0; }
  void set_frequency(double frequency ) {  omega = 2.0 * M_PI * frequency / (C_LIGHT*1e3); }
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t )  override; // x,y,z [mm] ; t [mm/c]

};

#endif /* sw_structure_hh */
