/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef coil_hh
#define coil_hh

#undef B0

#include "generic_field.hh"

class Coil : public GenericField { // one coil
  double length, B0, R; // m, T, m
  StaticVector<3> static_Efield; // V/m, static electric field embedding the element
  StaticVector<3> static_Bfield; // T, static magnetic field embedding the element
public:
  Coil() : length(0.0), B0(0.0), R(0.0), static_Efield(0.0), static_Bfield(0.0) {}
  Coil(double length_, /* m */
       double B0_, /* T */
       double R_ /* m */ ) : length(length_), B0(B0_), R(R_), static_Efield(0.0), static_Bfield(0.0) {}
  virtual std::shared_ptr<Element> clone() const override { return std::make_shared<Coil>(*this); }
  const StaticVector<3> &get_static_Efield() const { return static_Efield; }
  const StaticVector<3> &get_static_Bfield() const { return static_Bfield; } // T
  double get_length() const override { return length; } // m
  double get_R() const { return R; } // m
  double get_B0() const { return B0; } // T
  void set_static_Efield(double Ex, double Ey, double Ez ) { static_Efield = StaticVector<3>(Ex, Ey, Ez); }
  void set_static_Bfield(double Bx, double By, double Bz ) { static_Bfield = StaticVector<3>(Bx, By, Bz); }
  void set_length(double length_ ) { length = length_; } // m
  void set_R(double R_ ) { R = R_; } // m
  void set_B0(double B0_ ) { B0 = B0_; } // T
  void set_R_and_current(double R_ /* m */ , double I_ /* A */ )
  {
    // A * mu0 / m = 1.256637061435917e-06 T
    // A * mu0 / m = (1 / 795774.7154594767) T
    R = R_;
    B0 = I_ / (2. * R_ * 795774.7154594767); // T
  }
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t ) override; // x,y,z [mm] ; t [mm/c]

};

#endif /* coil_hh */
