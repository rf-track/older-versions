/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef volume_hh
#define volume_hh

#include <memory>
#include <vector>
#include <array>
#include <list>

#include "frame.hh"
#include "lattice.hh"
#include "element.hh"
#include "matrixnd.hh"
#include "bunch6dt.hh"
#include "space_charge.hh"
#include "transport_table.hh"
#include "tracking_options.hh"
#include "parallel_ode_solver.hh"

class Volume : public Aperture {
  struct Element_3d {
    Frame frame; // mm, rad
    std::shared_ptr<Element> element_ptr;
    // auxilary variables
    double length_mm; // mm
    StaticMatrix<3,3> rotation_matrix;
    Element_3d() = default;
    Element_3d(const Frame &f, std::shared_ptr<Element> p ) : frame(f), element_ptr(p), length_mm(p->get_length()*1e3), rotation_matrix(f.get_axes().get_rotation_matrix()) {}
    //Element_3d(const Element_3d &e ) : frame(e.frame), element_ptr(e.element_ptr->clone()), length_mm(e.length_mm), rotation_matrix(e.rotation_matrix) {}
    Element_3d clone() const { return Element_3d(frame, element_ptr->clone()); }
  };
  double s0 = 0.0, s1 = 0.0; // m, end and start points  
  std::vector<Element_3d> elements;
  // OUTPUT
  TransportTable transport_table;
  Bunch6d bunch6d_at_s0_mm;
  Bunch6d bunch6d_at_s1_mm;
  // ODEINT
  struct Params {
    double mass;
    double charge;
    double s0_mm, s1_mm; // mm, end and start points
    const Volume *self; // volume aperture
  };
  static int func(double t_mm, const double Y[], double dY[], void *params_ptr );
  static int jac(double t_mm, const double Y[], double *dfdy, double dfdt[], void *params_ptr );
public:
  std::shared_ptr<Element> operator[] (size_t i ) { return elements[i].element_ptr; }
  std::shared_ptr<const Element> operator[] (size_t i ) const { return elements[i].element_ptr; }
  std::shared_ptr<Volume> clone() const { return std::make_shared<Volume>(*this); }
  Volume &operator = (const Lattice &lattice );
  ~Volume() = default;
  void add_ref(std::shared_ptr<Element> element_ptr,
	       double x0, double y0, double z0, // m
	       const std::string &reference = "entrance" ) { add_ref(element_ptr, x0, y0, z0, 0.0, 0.0, 0.0, reference); }
  void add_ref(std::shared_ptr<Element> element_ptr,
	       double x0, double y0, double z0, // m
	       double roll /* Z */, double pitch /* X */, double yaw /* Y */, // rad
	       const std::string &reference = "entrance" ); // reference = entrance, center, exit
  void add(std::shared_ptr<const Element> element_ptr,
   	   double x0, double y0, double z0, // m
	   const std::string &reference = "entrance" ) { add_ref(element_ptr->clone(), x0, y0, z0, 0.0, 0.0, 0.0, reference); }
  void add(std::shared_ptr<const Element> element_ptr,
   	   double x0, double y0, double z0, // m
   	   double roll /* Z */, double pitch /* X */, double yaw /* Y */, // rad
	   const std::string &reference = "entrance" ) { add_ref(element_ptr->clone(), x0, y0, z0, roll, pitch, yaw, reference); }
  void add(std::shared_ptr<const Lattice> lattice_ptr,
	   double x0, double y0, double z0, // m
	   const std::string &reference = "entrance" ) { add(lattice_ptr, x0, y0, z0, 0.0, 0.0, 0.0, reference); }
  void add(std::shared_ptr<const Lattice> lattice_ptr,
   	   double x0, double y0, double z0, // m
	   double roll /* Z */, double pitch /* X */, double yaw /* Y */, // rad
	   const std::string &reference = "entrance" ); // reference = entrance, center, exit
  void add(std::shared_ptr<const Volume> volume_ptr,
    	   double x0, double y0, double z0, // m
   	   const std::string &reference = "entrance" ) { add(volume_ptr, x0, y0, z0, 0.0, 0.0, 0.0, reference); }
  void add(std::shared_ptr<const Volume> volume_ptr,
    	   double x0, double y0, double z0, // m
    	   double roll /* Z */, double pitch /* X */, double yaw /* Y */, // rad
   	   const std::string &reference = "entrance" ); // reference = entrance, center, exit
  size_t size() const { return elements.size(); }
  Bunch6dT track(Bunch6dT bunch, const TrackingOptions &T = TrackingOptions());
  MatrixNd get_transport_table(const char *fmt = "%S %mean_x %mean_y %emitt_x %emitt_y" ) const { return transport_table.get_transport_table(fmt); }
  Bunch6d get_bunch_at_s0() const { return bunch6d_at_s0_mm; }
  Bunch6d get_bunch_at_s1() const { return bunch6d_at_s1_mm; }
  double get_s0() const { return s0; } // m
  double get_s1() const { return s1; } // m
  double get_length() const { return s1-s0; } // m
  void set_s0(double l ) { s0 = l; } // m
  void set_s1(double l ) { s1 = l; } // m
  void set_length(double l ) { s1 = s0  + l; } // m
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t ) const;
  
};

#endif /* volume_hh */
