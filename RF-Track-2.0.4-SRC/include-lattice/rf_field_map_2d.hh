/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef rf_field_map_2d_hh
#define rf_field_map_2d_hh

#include <complex>
#include <mutex>

#include "fftw_complex.hh"
#include "generic_field.hh"
#include "mesh2d.hh"

template <typename COMPLEX_MESH_2D>
class RF_FieldMap_2d : public GenericField {
  COMPLEX_MESH_2D Efield; // V/m
  COMPLEX_MESH_2D Bfield; // T
  size_t Nz, Nr; // size of the mesh
  bool disable_Efield, disable_Bfield; //
  double hr, hz; // mm
  double z0, z1; // mm
  // RF field
  double omega;  // rad/(mm/c)
  double direction;  // +1, 0, -1 ; negative for backward travelling wave structure, 0: static field
  double P_map;      // W, map input power
  double P_actual;   // W, actual input power
  fftwComplex scale_factor_and_phase; // = std::polar(sqrt(P_actual / P_map), phi);
  double phi;        // [rad] user-set phase w.r.t. to crest (default = 0.0, on crest acceleration)
  bool   t0_is_set;  // t0 has been set
  double t0;         // [mm/c] time offset (default = delay to accelerate the first bunch on crest)
  StaticVector<3> static_Bfield; // [T] static magnetic field embedding the Drift (e.g. solenoid)
  void track0_initialize(Bunch6d &bunch ) override
  {
    if (!t0_is_set) {
      t0 = bunch.get_t_min();
      t0_is_set = true;
    }
    GenericField::track0_initialize(bunch);
  }
public:
  RF_FieldMap_2d(const ComplexMesh2d &Er,
		 const ComplexMesh2d &Ez,
		 const ComplexMesh2d &Br,
		 const ComplexMesh2d &Bz,
		 double hr, double hz, // m
		 double length, // m, if -1 takes the default : Ez.size() * hz
		 double freq, // in Hz
		 double direction, // +1 = forward travelling, -1 = backward travelling
		 double P_map = 1.0, // W
		 double P_actual = 1.0 ); // W
  RF_FieldMap_2d(double Er, double Ez, // means zero field
		 const ComplexMesh2d &Br,
		 const ComplexMesh2d &Bz,
		 double hr, double hz, // m
		 double length, // m, if -1 takes the default : Ez.size() * hz
		 double freq, // in Hz
		 double direction, // +1 = forward travelling, -1 = backward travelling
		 double P_map = 1.0, // W
		 double P_actual = 1.0 ); // W
  RF_FieldMap_2d(const ComplexMesh2d &Er,
		 const ComplexMesh2d &Ez,
		 double Br, double Bz, // means zero field
		 double hr, double hz, // m    // or h_rho (m), h_theta (rad) and hz
		 double length, // m, if -1 takes the default : Ez.size() * hz
		 double freq, // in Hz
		 double direction, // +1 = forward travelling, -1 = backward travelling
		 double P_map = 1.0, // W
		 double P_actual = 1.0 ); // W
  RF_FieldMap_2d();
  virtual std::shared_ptr<Element> clone() const override { return std::make_shared<RF_FieldMap_2d>(*this); }
  double get_length() const override { return (z1-z0)/1e3; } // return m
  double get_hr() const { return hr/1e3; } // return m
  double get_hz() const { return hz/1e3; } // return m
  double get_nr() const { return Nr; }
  double get_nz() const { return Nz; }
  double get_t0() const { return t0; } // mm/c
  double get_phi() const { return phi; }
  double get_phid() const { return 180.0 * phi / M_PI; }
  double get_P_map() const { return P_map; }
  double get_P_actual() const { return P_actual; }
  double get_frequency() const { return (C_LIGHT*1e3) * omega / (2.0*M_PI ); } // Hz
  double get_direction() const { return direction; }
  const StaticVector<3> &get_static_Bfield() const { return static_Bfield; }
  void set_static_Bfield(double Bx, double By, double Bz ) { static_Bfield = StaticVector<3>(Bx, By, Bz); }
  void set_hr(double hr_ ) { hr = hr_*1e3; } // accepts m
  void set_hz(double hz_ ) { hz = hz_*1e3; } // accepts m
  void set_z0(double z0_ ) { z0 = z0_*1e3; } // accepts m
  void set_z1(double z1_ ) { z1 = z1_*1e3; } // accepts m
  void set_length(double length_ ); // accepts m
  void set_phi(double p ) { phi = p; scale_factor_and_phase = std::polar(sqrt(P_actual / P_map), phi); }
  void set_phid(double p ) { phi = M_PI * p / 180.0; scale_factor_and_phase = std::polar(sqrt(P_actual / P_map), phi); }
  void set_t0(double t ) { t0_is_set = true; t0 = t; }
  void unset_t0() { t0_is_set = false; t0 = 0.0; }
  void set_frequency(double f ) { omega = 2.0 * M_PI * (f / double(C_LIGHT*1e3)); }
  void set_direction(double d ) { direction = d == 0.0 ? 0 : (d > 0.0 ? +1 : -1); }
  void set_P_map(double P ) { P_map = P; scale_factor_and_phase = std::polar(sqrt(P_actual / P_map), phi); }
  void set_P_actual(double P ) { P_actual = P; scale_factor_and_phase = std::polar(sqrt(P_actual / P_map), phi); }
  std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> get_field_complex(double x, double y, double z, double t ); // x,y,z [mm] ; t [mm/c]
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t )  override; // x,y,z [mm] ; t [mm/c]

};

// template specialization
typedef RF_FieldMap_2d<TMesh2d_LINT<StaticVector<2,fftwComplex>>> RF_FieldMap_2d_LINT;
typedef RF_FieldMap_2d<TMesh2d_CINT<StaticVector<2,fftwComplex>>> RF_FieldMap_2d_CINT;

#endif /* rf_field_map_2d_hh */
