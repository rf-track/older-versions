/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef lattice_hh
#define lattice_hh

#include <memory>
#include <vector>
#include <array>
#include <list>

#include "element.hh"
#include "matrixnd.hh"
#include "transport_table.hh"

class Lattice {
  std::vector<std::shared_ptr<Element>> elements;
  std::list<std::array<double,3>> bpm_readings;
  TransportTable transport_table;
  friend class Element;
  void append_bpm_readings(double s, double x, double y ) { bpm_readings.push_back(std::array<double,3>{{s, x, y}}); }
  // ODEINT
  struct Params {
    double mass;
    double charge;
    const std::vector<double> *S_array_mm; // array element ends in mm
    const std::vector<std::shared_ptr<Element>> *elements; // from lattice
  };
public:
  Lattice(const Lattice &lattice );
  Lattice() = default;
  ~Lattice() = default;
  Lattice &operator=(const Lattice &lattice );
  std::shared_ptr<const Element> operator[] (size_t i ) const { return elements[i]; }
  std::shared_ptr<Element> operator[] (size_t i ) { return elements[i]; }
  std::shared_ptr<Lattice> clone() const { return std::make_shared<Lattice>(*this); }
  void append(const Lattice &l ) { const size_t l_size = l.elements.size(); for (size_t i=0; i<l_size; i++) append(l.elements[i]->clone()); }
  void append_ref(Lattice &l ) { const size_t l_size = l.elements.size(); for (size_t i=0; i<l_size; i++) append_ref(l.elements[i]); }
  void append(std::shared_ptr<const Element> element_ptr ) { elements.push_back(element_ptr->clone()); }
  void append_ref(std::shared_ptr<Element> element_ptr ) { elements.push_back(element_ptr); }
  MatrixNd get_transport_table(const char *fmt = "%S %mean_x %mean_y %emitt_x %emitt_y" ) const { return transport_table.get_transport_table(fmt); }
  size_t size() const { return elements.size(); }
  Bunch6d track(Bunch6d bunch );
  double get_length() const;
  MatrixNd get_bpm_readings() const;
};

#endif /* lattice_hh */
