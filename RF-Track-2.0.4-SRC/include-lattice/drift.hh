/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef drift_hh
#define drift_hh

#include "generic_field.hh"

class Drift : public GenericField {
  double length; // m
  StaticVector<3> static_Efield; // [V/m] static electric field embedding the Drift (e.g. Wein filter)
  StaticVector<3> static_Bfield; // [T] static magnetic field embedding the Drift (e.g. solenoid)
public:
  Drift(double length_ = 0.0 /* m */ ) : length(length_), static_Efield{{{ 0.0, 0.0, 0.0 }}}, static_Bfield{{{ 0.0, 0.0, 0.0 }}} {}
  std::shared_ptr<Element> clone() const override { return std::make_shared<Drift>(*this); }
  const StaticVector<3> &get_static_Efield() const { return static_Efield; }
  const StaticVector<3> &get_static_Bfield() const { return static_Bfield; }
  double get_length() const override { return length; } // return m
  void set_static_Efield(double Ex, double Ey, double Ez ) { static_Efield = StaticVector<3>(Ex, Ey, Ez); }
  void set_static_Bfield(double Bx, double By, double Bz ) { static_Bfield = StaticVector<3>(Bx, By, Bz); }
  void set_length(double length_ ) { length = length_; } // m
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t ) override; // x,y,z [mm] ; t [mm/c]

};

#endif /* drift_hh */
