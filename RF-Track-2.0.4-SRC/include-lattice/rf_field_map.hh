/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef rf_field_map_hh
#define rf_field_map_hh

#include <complex>
#include <mutex>

#include "fftw_complex.hh"
#include "generic_field.hh"
#include "mesh3d_lint.hh"
#include "mesh3d_cint.hh"

template <typename COMPLEX_MESH_3D>
class RF_FieldMap : public GenericField {
  COMPLEX_MESH_3D Efield; // V/m
  COMPLEX_MESH_3D Bfield; // T
  size_t Nx, Ny, Nz; // size of the mesh
  bool disable_Efield, disable_Bfield; //
  double xa, ya; // mm
  double hx, hy, hz; // mm
  bool cylindrical; //
  // cartesian bounding box
  double x0, y0, z0, z1, width, height; // mm
  // cylindrical maps
  double rho_max_sqr; // mm^2 max radius squared
  // RF field
  double omega;  // rad/(mm/c)
  double direction;  // +1, 0, -1 ; negative for backward travelling wave structure, 0: static field
  double P_map;      // W, map input power
  double P_actual;   // W, actual input power
  fftwComplex scale_factor_and_phase; // = std::polar(sqrt(P_actual / P_map), phi);
  double phi;        // [rad] user-set phase w.r.t. to crest (default = 0.0, on crest acceleration)
  bool   t0_is_set;  // t0 has been set
  double t0;         // [mm/c] time offset (default = delay to accelerate the first bunch on crest)
  void init_bounding_box();
  StaticVector<3> static_Bfield; // [T] static magnetic field embedding the Drift (e.g. solenoid)
  void track0_initialize(Bunch6d &bunch ) override
  {
    if (!t0_is_set) {
      t0 = bunch.get_t_min();
      t0_is_set = true;
    }
    GenericField::track0_initialize(bunch);
  }
public:
  RF_FieldMap(const ComplexMesh3d &Ex,
	      const ComplexMesh3d &Ey,
	      const ComplexMesh3d &Ez,
	      const ComplexMesh3d &Bx,
	      const ComplexMesh3d &By,
	      const ComplexMesh3d &Bz,
	      double xa, double ya, // m    // or rho_min, theta_min
	      double hx, double hy, double hz, // m    // or h_rho (m), h_theta (rad) and hz
	      double length, // m, if -1 takes the default : Ez.size() * hz
	      double freq, // in Hz
	      double direction, // +1 = forward travelling, -1 = backward travelling
	      double P_map = 1.0, // W
	      double P_actual = 1.0 ); // W
  RF_FieldMap(double Ex, double Ey, double Ez, // means zero field
	      const ComplexMesh3d &Bx,
	      const ComplexMesh3d &By,
	      const ComplexMesh3d &Bz,
	      double xa, double ya, // m    // or rho_min, theta_min
	      double hx, double hy, double hz, // m    // or h_rho (m), h_theta (rad) and hz
	      double length, // m, if -1 takes the default : Ez.size() * hz
	      double freq, // in Hz
	      double direction, // +1 = forward travelling, -1 = backward travelling
	      double P_map = 1.0, // W
	      double P_actual = 1.0 ); // W
  RF_FieldMap(const ComplexMesh3d &Ex,
	      const ComplexMesh3d &Ey,
	      const ComplexMesh3d &Ez,
	      double Bx, double By, double Bz, // means zero field
	      double xa, double ya, // m    // or rho_min, theta_min
	      double hx, double hy, double hz, // m    // or h_rho (m), h_theta (rad) and hz
	      double length, // m, if -1 takes the default : Ez.size() * hz
	      double freq, // in Hz
	      double direction, // +1 = forward travelling, -1 = backward travelling
	      double P_map = 1.0, // W
	      double P_actual = 1.0 ); // W
  RF_FieldMap();
  virtual std::shared_ptr<Element> clone() const override { return std::make_shared<RF_FieldMap>(*this); }
  double get_length() const override { return (z1-z0)/1e3; } // return m
  double get_hx() const { return hx/1e3; } // return m
  double get_hy() const { return hy/1e3; } // return m
  double get_hz() const { return hz/1e3; } // return m
  double get_nx() const { return Nx; }
  double get_ny() const { return Ny; }
  double get_nz() const { return Nz; }
  double get_xa() const { return xa/1e3; } // return m
  double get_ya() const { return ya/1e3; } // return m
  double get_x0() const { return x0/1e3; } // return m
  double get_y0() const { return y0/1e3; } // return m
  double get_z0() const { return z0/1e3; } // return m
  double get_x1() const { return (x0 + width)/1e3; } // return m
  double get_y1() const { return (y0 + height)/1e3; } // return m
  double get_z1() const { return z1/1e3; } // return m
  bool is_cylindrical() const { return cylindrical; }
  void set_cylindrical(bool c );
  double get_t0() const { return t0; } // mm/c
  double get_phi() const { return phi; }
  double get_phid() const { return 180.0 * phi / M_PI; }
  double get_P_map() const { return P_map; }
  double get_P_actual() const { return P_actual; }
  double get_frequency() const { return (C_LIGHT*1e3) * omega / (2.0*M_PI ); } // Hz
  double get_direction() const { return direction; }
  const StaticVector<3> &get_static_Bfield() const { return static_Bfield; }
  void set_static_Bfield(double Bx, double By, double Bz ) { static_Bfield = StaticVector<3>(Bx, By, Bz); }
  void set_hx(double hx_ ) { hx = hx_*1e3; init_bounding_box(); } // accepts m
  void set_hy(double hy_ ) { hy = hy_*1e3; init_bounding_box(); } // accepts m
  void set_hz(double hz_ ) { hz = hz_*1e3; init_bounding_box(); } // accepts m
  void set_xa(double xa_ ) { xa = xa_*1e3; init_bounding_box(); } // accepts m
  void set_ya(double ya_ ) { ya = ya_*1e3; init_bounding_box(); } // accepts m
  void set_z0(double z0_ ) { z0 = z0_*1e3; } // accepts m
  void set_z1(double z1_ ) { z1 = z1_*1e3; } // accepts m
  void set_length(double length_ ); // accepts m
  void set_phi(double p ) { phi = p; scale_factor_and_phase = std::polar(sqrt(P_actual / P_map), phi); }
  void set_phid(double p ) { phi = M_PI * p / 180.0; scale_factor_and_phase = std::polar(sqrt(P_actual / P_map), phi); }
  void set_t0(double t ) { t0_is_set = true; t0 = t; }
  void unset_t0() { t0_is_set = false; t0 = 0.0; }
  void set_frequency(double f ) { omega = 2.0 * M_PI * (f / double(C_LIGHT*1e3)); }
  void set_direction(double d ) { direction = d == 0.0 ? 0 : (d > 0.0 ? +1 : -1); }
  void set_P_map(double P ) { P_map = P; scale_factor_and_phase = std::polar(sqrt(P_actual / P_map), phi); }
  void set_P_actual(double P ) { P_actual = P; scale_factor_and_phase = std::polar(sqrt(P_actual / P_map), phi); }
  std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> get_field_complex(double x, double y, double z, double t ); // x,y,z [mm] ; t [mm/c]
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t )  override; // x,y,z [mm] ; t [mm/c]

};

// template specialization
typedef RF_FieldMap<TMesh3d_LINT<StaticVector<3,fftwComplex>>> RF_FieldMap_LINT;
typedef RF_FieldMap<TMesh3d_CINT<StaticVector<3,fftwComplex>>> RF_FieldMap_CINT;

#endif /* rf_field_map_hh */
