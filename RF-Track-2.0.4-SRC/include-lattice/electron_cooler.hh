/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef electron_cooler_hh
#define electron_cooler_hh

#include <unordered_map>
#include <algorithm>

#include "plasma.hh"
#include "drift.hh"
#include "particle.hh"
#include "particle_key.hh"
#include "mesh2d.hh"

#include "cooling_force.hh"

class ElectronCooler : public Element, public Plasma {

  static std::map<std::pair<double,double>, CoolingForce> cooling_force_table;
  CoolingForce *cooling_force_ptr;
  
  std::vector<TMesh3d<StaticVector<3>>> dP_to_electrons_mesh_parallel; // [MeV/c / m^3] momentum density transferred to the grid by the bunch, it's an array for parallelization

public:

  ElectronCooler(double length = 0.0 /* m */, double rx = 0.0 /* m */, double ry = 0.0 /* m */, double ne = 0.0 /* #/m**3 */, double Vz = 0.0 /* c */ );
  virtual std::shared_ptr<Element> clone() const override { return std::make_shared<ElectronCooler>(*this); }
  void set_nsteps(size_t nsteps ) override { Element::set_nsteps(nsteps); Plasma::set_nsteps(nsteps); }
  double get_length() const override /* m */ { return Plasma::get_length(); }
  StaticVector<3> get_current(double x /* mm */, double y /* mm */, double z /* mm */ ) const /* A */ { return get_current_density(x,y,z) * get_area(); }
  void set_temperature(double kb_T_r /* eV */, double kb_T_l /* eV */ ) override; // set transverse and longitudinal temperatures, in the rest frame of the electrons
  void set_electron_mesh(size_t Nx, size_t Ny, size_t Nz, double density /* #/m^3 */, double Vx /* c */, double Vy /* c */, double Vz /* c */ ) { Element::set_nsteps(Nz); set_plasma_mesh(Nx, Ny, Nz, density, Vx, Vy, Vz); }
  void set_electron_mesh(size_t Nz, const MatrixNd &density /* #/m^3 */, const MatrixNd &Vx /* c */, const MatrixNd &Vy /* c */, const MatrixNd &Vz /* c */ ) { Element::set_nsteps(Nz); set_plasma_mesh(Nz, density, Vx, Vy, Vz); }
  void set_electron_mesh(const Mesh3d &density /* #/m^3 */, const Mesh3d &Vx /* c */, const Mesh3d &Vy /* c */, const Mesh3d &Vz /* c */ ) { Element::set_nsteps(density.size3()); set_plasma_mesh(density, Vx, Vy, Vz); }
  void compute_cooling_force();
  
  // tracking
  TransportTable track(Bunch6d &bunch ) override;

};

#endif /* electron_cooler_hh */
