/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef generic_field_hh
#define generic_field_hh

#include <complex>
#include <mutex>
#include <array>

#include "element.hh"
#include "parallel_ode_solver.hh"

class GenericField : public Element, public Parallel_ODE_Solver {
  // auxiliary variables for GSL ODE integration
  std::vector<gsl_odeiv2_system> sys;
  mutable bool gsl_error;
  struct Params {
    double mass;
    double charge;
    GenericField *field;
    bool purely_magnetic;
  };
  static int func(double t_mm, const double Y[], double dY[], void *params_ );
  static int jac(double t_mm, const double Y[], double *dfdy, double dfdt[], void *params_ );
protected:
  virtual void track0_initialize(Bunch6d &bunch ) override; // initialize
  virtual void track0_finalize(Bunch6d &bunch ) override; // finalize
  virtual void track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t number_of_steps, size_t thread ) const override;
public:
  GenericField() = default;
  virtual ~GenericField() = default;
  virtual std::shared_ptr<Element> clone() const override = 0;
  virtual double get_length() const override = 0;
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t ) override = 0;

};

#endif /* generic_field_hh */
