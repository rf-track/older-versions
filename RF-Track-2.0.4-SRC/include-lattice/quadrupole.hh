/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef quadrupole_hh
#define quadrupole_hh

#include "element.hh"

class Quadrupole : public Element {
  double length;   ///< quad length [m]
  double strength; ///< quad integrated strength [MeV/m] ; strength = Brho * K1L = P/Q * K1L = MeV/c/e * 1/m = MV/c/m
  void track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t number_of_steps, size_t thread  ) const override;
public:
  Quadrupole(double L = 0.0, double strength_ = 0.0 ) : length(L), strength(strength_) {}
  Quadrupole(double L, double P_over_Q /* MeV/c/e */, double k1 /* 1/m^2 */ ) : length(L), strength(k1 * P_over_Q * L) {}
  virtual std::shared_ptr<Element> clone() const override { return std::make_shared<Quadrupole>(*this); }
  double get_K1(double P_over_Q /* MeV/c/e */ ) const { return strength / P_over_Q / length; } // 1/m**2
  double get_K1L(double P_over_Q /* MeV/c/e */ ) const { return strength / P_over_Q; } // 1/m
  double get_gradient() const { return strength / length / C_LIGHT * 1e6 ; } // T/m
  double get_length() const  override { return length; } // m
  double get_strength() const { return strength; } // MV/c/m
  void set_K1(double P_over_Q /* MeV/c/e */, double k1 /* 1/m^2 */ ) { strength = P_over_Q * k1 * length; }
  void set_K1L(double P_over_Q /* MeV/c/e */, double K1L /* 1/m */ ) { strength = P_over_Q * K1L; }
  void set_length(double l ) { length = l; }
  void set_strength(double s ) { strength = s; }
  void set_gradient(double G /* T/m */ ) { strength = G * length * C_LIGHT / 1e6; }
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t )  override // x,y,z [mm] ; t [mm/c]
  {
    if (z<=0.0 || z>length*1e3)
      return std::pair<StaticVector<3>, StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0));
    const double gradient = strength / length * 1e3; // eV/m/mm
    double Bx = gradient * y / C_LIGHT; // T = V/m/c
    double By = gradient * x / C_LIGHT; // T = V/m/c
    return std::pair<StaticVector<3>, StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(Bx, By, 0.0)); 
  }

};

#endif /* quadrupole_hh */
