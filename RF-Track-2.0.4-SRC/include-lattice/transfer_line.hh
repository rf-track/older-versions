/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef transfer_line_hh
#define transfer_line_hh

#include "element.hh"

class TransferLine : public Element {
  MatrixNd twiss_matrix; // 5-column matrix  S, beta_x, alpha_x, mu_x, beta_y, alpha_y, mu_y
  double ref_Pc;  ///< reference momentum, MeV/c (default -1, use the beam's reference momentum)
  double DQx, DQy; /// < chromaticity in units of 2*pi
  double momentum_compaction; /// m
public:
  TransferLine(const char *twiss_file, double ref_Pc_ = -1.0 );
  TransferLine(const MatrixNd &twiss_matrix_, double DQx_, double DQy_, double momentum_compaction_, double ref_Pc_ = -1.0 );
  TransferLine(const MatrixNd &twiss_matrix_, double ref_Pc_ = -1.0 );
  TransferLine();
  virtual std::shared_ptr<Element> clone() const override { return std::make_shared<TransferLine>(*this); }
  double get_length() const override { return twiss_matrix[twiss_matrix.rows()-1][0]; }
  TransportTable track(Bunch6d &bunch ) override;

};

#endif /* transfer_line_hh */
