/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef tracking_options_hh
#define tracking_options_hh

#include <string>
#include <gsl/gsl_math.h>
#include "transport_table.hh"

struct TrackingOptions {
  std::string odeint_algorithm = "leapfrog"; // built for speed 
  double odeint_epsabs = 1.0; // applies only to GSL algorithms
  double odeint_epsrel = 0.0; // applies only to GSL algorithms
  bool open_boundaries = true; // if 'true' tracks the beam even outside of the volume's boundaries
  bool backtrack_at_entrance = false; // at start, backtrack the beam such that the foremost particle is exactly at S=S0
  int verbosity = 0; // verbosity level, 0 = silent
  double dt_mm = 1.0; // mm/c, integration step
  double t_max_mm = GSL_POSINF; // track until t<t_max_mm, if '0' tracks all the way to the end
  double tt_dt_mm = 0.0; // every tt_dt_mm tabulate the bunch parameters and add them to transport table
  double sc_dt_mm = 0.0; // every sc_dt_mm steps applies one space-charge kick
  double wp_dt_mm = 0.0; // every wp_dt_mm (watch point) saves the beam on a file
  std::string wp_basename = "watch_beam"; // saves the beam distributions with name watch_beam.0000n.txt
  bool wp_gzip = false; // gzips files
};

#endif /* tracking_options_hh */
