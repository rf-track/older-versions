/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef static_magnetic_field_map_hh
#define static_magnetic_field_map_hh

#include <complex>
#include <mutex>
#include <array>

#include "fftw_mesh3d.hh"
#include "generic_field.hh"

// Static_Magnetic_FieldMap in vacuum
class Static_Magnetic_FieldMap : public GenericField {
  TMesh3d_CINT<StaticVector<4>> mesh_A; // electromagnetic four-potential: PhiM, Ax, Ay, Az
  StaticVector<3> Bmean; // T
  // cartesian bounding box
  double x0, y0, z0, z1; // mm
  double hx, hy, hz; // mm
public:
  Static_Magnetic_FieldMap(const Mesh3d &Ax = DefaultMesh3d,
			   const Mesh3d &Ay = DefaultMesh3d,
			   const Mesh3d &Az = DefaultMesh3d,
			   const Mesh3d &PhiM = DefaultMesh3d,
			   double x0 = 0.0, double y0 = 0.0, // m
			   double hx = 1.0, double hy = 1.0, double hz = 1.0, // m
			   double length = -1.0); // if -1 takes the default : (Ez.size()-1) * hz
  Static_Magnetic_FieldMap(const Mesh3d &Bx,
			   const Mesh3d &By,
			   const Mesh3d &Bz,
			   double x0, double y0, // m
			   double hx, double hy, double hz, // m
			   double length = -1.0, // if -1 takes the default : (Ez.size()-1) * hz
			   double quality = 1.0 ); // quality factor for the FFT smoothing (0..1)
  ~Static_Magnetic_FieldMap() = default;
  virtual std::shared_ptr<Element> clone() const { return std::make_shared<Static_Magnetic_FieldMap>(*this); }
  double get_length() const { return (z1-z0)/1e3; } // return m
  double get_hx() const { return hx/1e3; } // return m
  double get_hy() const { return hy/1e3; } // return m
  double get_hz() const { return hz/1e3; } // return m
  double get_nx() const { return mesh_A.size1(); }
  double get_ny() const { return mesh_A.size2(); }
  double get_nz() const { return mesh_A.size3(); }
  double get_x0() const { return x0/1e3; } // return m
  double get_y0() const { return y0/1e3; } // return m
  double get_z0() const { return z0/1e3; } // return m
  double get_x1() const { return (x0 + (mesh_A.size1()-1)*hx)/1e3; } // return m
  double get_y1() const { return (y0 + (mesh_A.size2()-1)*hy)/1e3; } // return m
  double get_z1() const { return z1/1e3; } // return m
  void set_hx(double hx_ ) { hx = hx_*1e3; } // accepts m
  void set_hy(double hy_ ) { hy = hy_*1e3; } // accepts m
  void set_hz(double hz_ ) { hz = hz_*1e3; } // accepts m
  void set_x0(double x0_ ) { x0 = x0_*1e3; } // accepts m
  void set_y0(double y0_ ) { y0 = y0_*1e3; } // accepts m
  void set_z0(double z0_ ) { z0 = z0_*1e3; } // accepts m
  void set_z1(double z1_ ) { z1 = z1_*1e3; } // accepts m
  void set_length(double length ); // accepts m
  void set_Bx_By_Bz(Mesh3d Bx,
		    Mesh3d By,
		    Mesh3d Bz, double quality = 1.0 );
  Mesh3d get_Ax() const;
  Mesh3d get_Ay() const;
  Mesh3d get_Az() const;
  Mesh3d get_PhiM() const;
  double get_divB(double x, double y, double z ) const;
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t = 0.0 ); // x,y,z [mm] ; t [mm/c]
  std::pair<StaticMatrix<3,3>, StaticMatrix<3,3>> get_field_jacobian(double x, double y, double z, double t = 0.0 ); // x,y,z [mm] t [mm/c], returns V/m/mm, and T/mm

};

#endif /* static_magnetic_field_map_hh */
