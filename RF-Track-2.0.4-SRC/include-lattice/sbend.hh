/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef sbend_hh
#define sbend_hh

#include "element.hh"
#include "conversion.hh"

class SBend : public Element {
  double length;   ///< quad length [m]
  double angle; // rad
  double h; // 1/m, curvature of the reference system
  double k0; // 1/m, dipole strength; strength = Brho * K0 * L = P0/Q * angle = MeV/c/e = MV/c
  double k1; // 1/m^2, qudrupole focusing strength [MeV/m] ; strength = Brho * K1L = P0/Q * K1L = MeV/c/e * 1/m = MV/c/m
  double E1, E2; // radian, entrance and exit angles
  double P_over_Q; // MeV/c/e = MV/c reference rigidity
  double k1L; // 1/m, integrated qudrupole focusing strength
  void track0_initialize(Bunch6d &bunch ) override;
  void track0_finalize(Bunch6d &bunch ) override;
  void track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t number_of_steps, size_t thread  ) const override;
public:
  SBend(double L = 0.0 /* m */, double angle = 0.0 /* rad */, double ref_P_over_Q = 0.0 /* MV/c */, double E1_ = 0.0  /* rad */, double E2_ = 0.0  /* rad */ );
  virtual std::shared_ptr<Element> clone() const override { return std::make_shared<SBend>(*this); }
  double get_E1() const { return E1; } // rad
  double get_E2() const { return E2; } // rad
  double get_E1d() const { return rad2deg(E1); } // deg
  double get_E2d() const { return rad2deg(E2); } // deg
  double get_h() const { return h; } // 1/m
  double get_K0() const { return k0; } // 1/m
  double get_K1() const { return k1; } // 1/m**2
  double get_K1L() const { return k1L; } // 1/m
  double get_angle() const { return angle; } // radian
  double get_angled() const { return rad2deg(angle); } // deg
  double get_length() const override { return length; } // m
  double get_Brho() const { return P_over_Q / 299.792458; } // T*m
  double get_Bfield() const { return P_over_Q * k0 / 299.792458; } // T
  double get_P_over_Q() const { return P_over_Q; } // MeV/c/e = MV/c
  void set_E1(double a /* rad */ ) { E1 = a; }
  void set_E2(double a /* rad */ ) { E2 = a; }
  void set_E1d(double a /* deg */ ) { E1 = deg2rad(a); }
  void set_E2d(double a /* deg */ ) { E2 = deg2rad(a); }
  void set_h(double h_ /* 1/m */ ) { h = h_; }
  void set_K0(double K0 /* 1/m */ ) { k0 = K0; angle = k0 * length; }
  void set_K1(double K1 /* 1/m^2 */ ) { k1 = K1; k1L = k1 * length; }
  void set_K1L(double K1L /* 1/m */ ) { k1 = K1L / length; k1L = K1L; }
  void set_angle(double a /* radian */ ) { k0 = h = a / length; angle = a; }
  void set_angled(double a /* deg */ ) { k0 = h = deg2rad(a) / length; angle = a; }
  void set_length(double l ) { length = l; }
  void set_Brho(double Br /* T*m */ ) { P_over_Q = Br * 299.792458; } // T*m
  void set_Bfield(double B /* T */ ) { k0 = h = B / P_over_Q * 299.792458; angle = k0 * length; } // 1/m
  void set_P_over_Q(double P_Q /* MeV/c/e = MV/c */ ) { P_over_Q = P_Q; } // 1/m

};

#endif /* sbend_hh */
