/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef static_field_map_2d_hh
#define static_field_map_2d_hh

#include "generic_field.hh"
#include "mesh2d.hh"

template <typename MESH_2D>
class Static_Magnetic_FieldMap_2d : public GenericField {
  // rows are along Z, columns are along R
  MESH_2D static_Bfield_2d; // T
  double hr, hz; // mm
  double z0, z1; // mm
  StaticVector<3> static_Bfield; // [T] static magnetic field embedding the Drift (e.g. solenoid)
public:
  Static_Magnetic_FieldMap_2d(const Mesh2d &static_Br, // T
			      const Mesh2d &static_Bz, // T
			      double hr, // m
			      double hz, // m
			      double length = -1); // if -1 takes the default : Ez.size() * hz
  Static_Magnetic_FieldMap_2d() = default;
  virtual std::shared_ptr<Element> clone() const override { return std::make_shared<Static_Magnetic_FieldMap_2d>(*this); }
  double get_length() const override { return (z1-z0)/1e3; } // return m
  double get_hr() const { return hr/1e3; } // return m
  double get_hz() const { return hz/1e3; } // return m
  double get_nr() const { return static_Bfield_2d.size2(); }
  double get_nz() const { return static_Bfield_2d.size1(); }
  double get_z0() const { return z0/1e3; } // return m
  double get_z1() const { return z1/1e3; } // return m
  const StaticVector<3> &get_static_Bfield() const { return static_Bfield; }
  void set_static_Bfield(double Bx, double By, double Bz ) { static_Bfield = StaticVector<3>(Bx, By, Bz); }
  void set_hr(double hr_ ) { hr = hr_*1e3; } // accepts m
  void set_hz(double hz_ ) { hz = hz_*1e3; } // accepts m
  void set_z0(double z0_ ) { z0 = z0_*1e3; } // accepts m
  void set_z1(double z1_ ) { z1 = z1_*1e3; } // accepts m
  void set_length(double length_ ); // accepts m
  std::pair<StaticVector<3>, StaticVector<3>> get_field(double x, double y, double z, double t )  override; // x,y,z [mm] ; t [mm/c]

};

// template specialization
typedef Static_Magnetic_FieldMap_2d<TMesh2d_LINT<StaticVector<2>>> Static_Magnetic_FieldMap_2d_LINT;
typedef Static_Magnetic_FieldMap_2d<TMesh2d_CINT<StaticVector<2>>> Static_Magnetic_FieldMap_2d_CINT;

#endif /* static_field_map_2d_hh */
