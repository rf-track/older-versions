03-11-2020 Andrea Latina <andrea.latina@cern.ch>
* Added new element RF_FieldMap_2d() optimized for cylindrically symmetric
	fields
* Changed version number to 2.0.4

26-10-2020 Andrea Latina <andrea.latina@cern.ch>
* Added new element LaserBeam() to simulate Compton back scattering

15-04-2020 Andrea Latina <andrea.latina@cern.ch>
* Changed the way TrackingOptions.t_max_mm works, now indefinite tracking
	is achieved with t_max_mm = Inf, and no longer t_max_mm = 0

04-02-2020 Andrea Latina <andrea.latina@cern.ch>
* Added element Static_Magnetic_FieldMap_2d for cylindrically symmetric fields

10-06-2019 Andrea Latina <andrea.latina@cern.ch>
* Improved default field's Jacobian for Elements
* Added support for SDDS files (writing)
* Added element SBend for dS tracking
* Added external elements
* Added Python interface
* Reimplemented misalignments in Bunch6d tracking, using Frame()

24-04-2019 Andrea Latina <andrea.latina@cern.ch>
* Added element Volume
* Added element Coil

18-04-2019 Andrea Latina <andrea.latina@cern.ch>
* Version 2.0 started
* reorganisation of classes and files
* new FieldMap elements with extensions _LINT and _CINT for different 
  interpolation

31-07-2018 Andrea Latina <andrea.latina@cern.ch>
* First release published, v1.0.0

20-03-2018 Andrea Latina <andrea.latina@cern.ch>
* Added element RF_Field_1d with reconstruction of the 3d E and B fields from
  the on-axis electric field

21-12-2017 Andrea Latina <andrea.latina@cern.ch>
* Added magnetic element Adiabatic Matching Device

03-10-2017 Andrea Latina <andrea.latina@cern.ch>
* Bunch6d::get_phase_space and Bunch6dT::get_phase_space now use the first
  particle as reference particle
