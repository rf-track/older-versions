# RF-Track
RF-Track is a new tracking code developed at CERN for the optimization of particle accelerators, which offers outstanding flexibility and rapid simulation speed.

RF-Track can simulate beams of particles with arbitrary energy, mass, and charge, even mixed, solving fully relativistic equations of motion.  It can simulate the effects of space-charge forces, both in bunched and continuous-wave beams. It can transport the beams through common elements as well as through "special" ones: 1D, 2D, and 3D static or oscillating radio-frequency electromagnetic field maps (real and complex), flux concentrators, and electron coolers. It allows element overlap, and direct and indirect space-charge calculation using fast parallel algorithms.

RF-Track is written in optimized and parallel C++ and uses the scripting languages Octave and Python as user interfaces. General knowledge of Octave or Python is recommended to get the best out of RF-Track.

# How to compile and install RF-Track
In order to run RF-Track some binary packages and libraries are necessary:
* GSL - GNU Scientific Library
* FFTW Library
* Octave (optional)
* Python (optional)
## Installing the prerequisites
### On macOS
To compile RF-Track on macOS, one option is to use MacPorts in order to retrieve the necessary packages:
1. Install MacPorts, https://www.macports.org/install.php
2. If you have it already, be sure everything is up do date
```
$ sudo port selfupdate
$ sudo port upgrade outdated
```
3. Install the necessary packages
```
$ sudo port install fftw-3 gsl clang
```
If you intend to use RF-Track within Octave, you should also install this package:
```
$ sudo port install octave
```
If you intend to use RF-Track within Python, you should make sure Python is installed on yoursystem, together with its library NumPy:
```
$ sudo port install py-numpy
```
4. Select the right compiler by setting the environment variables, e.g.
```
$ export CC=clang-mp-9.0
$ export CXX=clang++-mp-9.0
```
### On Ubuntu
It is good practice to update all packages before you install RF-Track:
```
$ sudo apt-get update
$ sudo apt-get upgrade
```
In order to install the few necessary packages on Ubuntu, you must give the following command:
```
$ sudo apt install libgsl-dev fftw-dev git
```
If you intend to use RF-Track within Octave, you should also install this package:
```
$ sudo apt install liboctave-dev
```
If you intend to use RF-Track within Python, you should make sure Python is installed on your system, together with its library NumPy:
```
$ sudo apt install libpython-dev python-dev python-numpy
```

## Downloading RF-Track
The best way to get RF-Track is to clone the Git repository:
```
$ git clone https://gitlab.cern.ch/rf-track/rf-track-2.0.git
```
## Compiling RF-Track
In order to compile RF-Track you first need to configure it for your system. First thing, you need to enter its directory:
```
$ cd rf-track-2.0
```
If you want to use RF-Track within Octave, you can give the following command:
```
$ ./configure --prefix=$HOME --enable-octave
```
If you want to use RF-Track within Python, you can give the following command:
```
$ ./configure --prefix=$HOME --enable-python
```
You can use both `--enable-octave` and `--enable-python` simultaneously, if you intend to use RF-Track with both Python and Octave. Use the command `./configure --help` to get more options.
Then, you can compile it and install it:
```
$ make -j8
$ make install
```
When you give `make install`, the file `RF-Track.oct` will be copied to $HOME/share/octave, and the files `RF_Track.py` and `_RF_Track.so` will be copied to $HOME/share/python. In reality, the installation is not even necessary. All you have to do is make sure that your Octave and Python scripts point to the directory `rf-track-2.0` where RF-Track was compiled.

## Using RF-Track
For using RF-Track with Octave, you should start your Octave scripts with the command:
```
octave:1> addpath(’/Users/alatina/Codes/rf-track-2.0’);
```
(you can add this command to the file `.octaverc` in your home directory to run it automatically whenever Octave is launched).
In Python, you should give the commands:
```
>>> import os,sys
>>> BIN = os.path.expanduser("/Users/alatina/Codes/rf-track-2.0")
>>> sys.path.append(BIN)
```
