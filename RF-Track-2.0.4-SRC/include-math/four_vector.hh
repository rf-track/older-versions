/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef four_vector_hh
#define four_vector_hh

#include <iostream>

class FourVector {
protected:
  double v[4]; // +--- signature
public:
  FourVector(double a, double b, double c, double d ) : v{a,b,c,d} {}
  FourVector() = default;
  ~FourVector() = default;

  const double &operator[](int i ) const { return v[i]; }
  double &operator[](int i ) { return v[i]; }
  
  friend double sqr(const FourVector &p ) { return p*p; }
  
  friend double operator * (const FourVector &a, const FourVector &b ) { return a.v[0]*b.v[0] - a.v[1]*b.v[1] - a.v[2]*b.v[2] - a.v[3]*b.v[3]; }
  friend FourVector operator * (double a, const FourVector &b ) { return FourVector(a*b.v[0], a*b.v[1], a*b.v[2], a*b.v[3]); }
  friend FourVector operator * (const FourVector &a, double b ) { return FourVector(b*a.v[0], b*a.v[1], b*a.v[2], b*a.v[3]); }
  friend FourVector operator + (const FourVector &a, const FourVector &b ) { return FourVector(a.v[0]+b.v[0], a.v[1]+b.v[1], a.v[2]+b.v[2], a.v[3]+b.v[3]); }
  friend FourVector operator - (const FourVector &a, const FourVector &b ) { return FourVector(a.v[0]-b.v[0], a.v[1]-b.v[1], a.v[2]-b.v[2], a.v[3]-b.v[3]); }
  friend FourVector operator - (const FourVector &a ) { return FourVector(-a.v[0], -a.v[1], -a.v[2], -a.v[3]); }

  friend std::ostream &operator << (std::ostream &stream, const FourVector a ) { return stream << ' ' << a.v[0] << ' ' << a.v[1] << ' ' << a.v[2] << ' ' << a.v[3]; }

  friend bool operator != (const FourVector &a, const FourVector &b ) { return a.v[0]!=b.v[0] || a.v[1]!=b.v[1] || a.v[2]!=b.v[2] || a.v[3]!=b.v[3]; }
};

#endif /* four_vector_hh */
