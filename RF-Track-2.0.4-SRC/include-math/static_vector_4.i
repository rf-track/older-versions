/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* static_vector_4.i */

%{
#include "static_vector.hh"
%}

#if defined(SWIGOCTAVE)

// Octave Matrix -> C++ StaticVector<4>
%typemap(in) const StaticVector<4> & {
  const ColumnVector &matrix = $input.column_vector_value();
  $1 = new StaticVector<4>;
  for (int j=0; j<4; j++)
    (*$1)[j] = matrix(j);
 }

%typemap(freearg) const StaticVector<4> & {
  if ($1) delete $1;
 }

%typemap(typecheck) const StaticVector<4> & {
  octave_value obj = $input;
  if (obj.is_real_scalar() || obj.is_real_matrix()) {
    const ColumnVector &matrix = obj.column_vector_value();
    if (matrix.numel()==4) {
      $1 = 1;
    } else {
      $1 = 0;
    }
  } else {
    $1 = 0;
  }
 }

%typemap(out) const StaticVector<4> & {
  ColumnVector retval(4);
  retval(0) = (*$1)[0];
  retval(1) = (*$1)[1];
  retval(2) = (*$1)[2];
  retval(3) = (*$1)[3];
  $result = retval;
 }

%typemap(out) StaticVector<4> {
  ColumnVector retval(4);
  for (size_t i=0; i<4; i++)
    retval(i) = ($1)[i];
  $result = retval;
 }

// C++ StaticVector<4> -> Octave Matrix (as an output argument)
%typemap(in, numinputs=0) StaticVector<4> & (StaticVector<4> temp ) {
  $1 = &temp;
 }

%typemap(argout) StaticVector<4> & {
  ColumnVector ret(4);
  for (size_t i=0; i<4; i++)
    ret(i) = (*$1)[i];
  $result->append(ret);
 }

%typemap(freearg,noblock=1) StaticVector<4> & {
 }

%typemap(argout) const StaticVector<4> & {
 }
#endif

#if defined(SWIGPYTHON)
%{
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#define is_array(a) ((a) && PyArray_Check((PyArrayObject *)a))
%}

%init %{
  import_array();
  %}

// NumPy Matrix -> C++ StaticVector<4>
%typemap(in) const StaticVector<4> & {
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 1, 2);
    npy_intp strides[2];
    if (PyArray_NDIM(array) == 1) {
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = 0;
    } else {
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = PyArray_STRIDES(array)[1];
    }
    char *data = PyArray_BYTES(array);
    $1 = new StaticVector<4>;
    for (int i=0; i<4; i++)
      (*$1)[i] = *(double *)(data + i*strides[0]);
    Py_DECREF(array);
  }
 }

%typemap(freearg) const StaticVector<4> & {
  if ($1) delete $1;
 }

%typemap(typecheck) const StaticVector<4> & {
  if (is_array($input) || PyFloat_Check($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 1, 2);
    npy_intp rows, cols;
    if (PyArray_NDIM(array) == 1) {
      rows = PyArray_DIMS(array)[0];
      cols = 1;
    } else {
      rows = PyArray_DIMS(array)[0];
      cols = PyArray_DIMS(array)[1];
    }
    if (rows==4 && cols==1) {
      $1 = 1;
    } else {
      $1 = 0;
    }
  } else {
    $1 = 0;
  }
 }

// C++ StaticVector<4> -> NumPy Matrix (as a return value)
%typemap(out) StaticVector<4> {
  npy_intp dimensions[2] = { 4, 1 };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (int i=0; i<4; i++)
    *(double*)(data + i*strides[0]) = $1[i];
  $result = PyArray_Return(res);
 }

%typemap(out) const StaticVector<4> & {
  npy_intp dimensions[2] = { 4, 1 };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (int i=0; i<4; i++)
    *(double*)(data + i*strides[0]) = (*$1)[i];
  $result = PyArray_Return(res);
}

// C++ StaticVector<4> -> NumPy Matrix (as an output argument)
%typemap(in, numinputs=0) StaticVector<4> & (StaticVector<4> temp ) {
  $1 = &temp;
 }

%typemap(argout) StaticVector<4> & {
  npy_intp dimensions[2] = { 4, 1 };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (size_t i=0; i<4; i++)
    *(double*)(data + i*strides[0]) = (*$1)[i];
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res));
 }

%typemap(freearg,noblock=1) StaticVector<4> {
 }

%typemap(argout) const StaticVector<4> & {
 }
#endif
