/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* mesh1d.i */

#if defined(SWIGOCTAVE)

// Octave array -> C++ Mesh1d
%typemap(in) const ComplexMesh1d & {
  const ComplexColumnVector &array1d = $input.complex_array_value();
  const size_t W = array1d.numel();
  $1 = new ComplexMesh1d(W);
  for (size_t i=0; i<W; i++) {
    (*$1).elem(i) = array1d(i);
  }
 }

%typemap(freearg) const ComplexMesh1d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const ComplexMesh1d &  {
  octave_value obj = $input;
  $1 = false;
  if (obj.is_complex_matrix() || obj.is_real_matrix()) {
    const ComplexColumnVector array1d = obj.complex_array_value();
    dim_vector dims = array1d.dims();
    if (dims.length()==2) {
      size_t W = dims.elem(0);
      size_t H = dims.elem(1);
      if (W==1 || H==1) {
	$1 = true;
      }
    }
  }
}

// C++ Mesh1d -> Octave array
%typemap(out) const ComplexMesh1d & {
  ComplexColumnVector ret((*$1).size1());
  for (size_t i=0; i<(*$1).size1(); i++) {
    ret(i) = (*$1).elem(i);
  }
  $result = octave_value(ret);
}

// T_ijk as an output argument
/*
%typemap(in, numinputs=0) ComplexMesh1d & (ComplexMesh1d temp ) {
  $1 = &temp;
}

%typemap(argout) ComplexMesh1d & {
  ComplexColumnVector T((*$1).size1());
  for (size_t i=0; i<(*$1).size1(); i++) {
    T(i) = (*$1).elem(i);
  }
  $result->append(T);
}
*/

// Octave array -> C++ Mesh1d
%typemap(in) const Mesh1d & {
  const ColumnVector &array1d = $input.array_value();
  size_t W = array1d.numel();
  $1 = new Mesh1d(W);
  for (size_t i=0; i<W; i++) {
    (*$1).elem(i) = array1d(i);
  }
 }

%typemap(freearg) const Mesh1d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const Mesh1d &  {
  octave_value obj = $input;
  $1 = false;
  if (obj.is_real_matrix()) {
    const ColumnVector array1d = obj.array_value();
    dim_vector dims = array1d.dims();
    if (dims.length()==2) {
      size_t W = dims.elem(0);
      size_t H = dims.elem(1);
      if (W==1 || H==1) {
	$1 = true;
      }
    }
  }
}

// C++ Mesh1d -> Octave array
%typemap(out) Mesh1d {
  ColumnVector ret(($1).size1());
  for (size_t i=0; i<($1).size1(); i++) {
    ret(i) = ($1).elem(i);
  }
  $result = octave_value(ret);
}

#endif

#if defined(SWIGPYTHON)
%{
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#define is_array(a) ((a) && PyArray_Check((PyArrayObject *)a))
%}

%init %{
  import_array();
  %}

// Python array -> C++ Mesh1d
%typemap(in) const ComplexMesh1d & {
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_CDOUBLE, 1, 1);
    npy_intp strides = PyArray_STRIDES(array)[0];
    npy_intp W = PyArray_DIMS(array)[0];
    char *data = PyArray_BYTES(array);
    $1 = new ComplexMesh1d(W);
    for (int i=0; i<W; i++) {
      const auto &src = *(npy_cdouble *)(data + i*strides);
      auto &dest = (*$1).elem(i);
      dest.real = src.real;
      dest.imag = src.imag;
    }
    Py_DECREF(array);
  }
 }

%typemap(freearg) const ComplexMesh1d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const ComplexMesh1d &  {
  if (is_array($input) || PyComplex_Check($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_CDOUBLE, 1, 1);
    if (PyArray_NDIM(array) == 1) {
      $1 = 1;
    } else {
      $1 = 0;
    }
  } else {
    $1 = 0;
  }
 }

// C++ Mesh1d -> Python array
%typemap(out) const ComplexMesh1d & {
  npy_intp dimensions[2] = { (npy_intp)(*$1).size1(), 1 };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (size_t i=0; i<(*$1).size1(); i++) {
    (*(npy_cdouble*)(data + i*strides[0])).real = (*$1).elem(i).real;
    (*(npy_cdouble*)(data + i*strides[0])).imag = (*$1).elem(i).imag;
  }
  $result = PyArray_Return(res);
}

// Python array -> C++ Mesh1d
%typemap(in) const Mesh1d & {
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 1, 1);
    npy_intp strides = PyArray_STRIDES(array)[0];
    npy_intp W = PyArray_DIMS(array)[0];
    char *data = PyArray_BYTES(array);
    $1 = new Mesh1d(W);
    for (int i=0; i<W; i++) {
      (*$1).elem(i) = (*(double *)(data + i*strides));
    }
    Py_DECREF(array);
  }
 }

%typemap(freearg) const Mesh1d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const Mesh1d &  {
  if (is_array($input) || PyFloat_Check($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 1, 1);
    if (PyArray_NDIM(array) == 1) {
      $1 = 1;
    } else {
      $1 = 0;
    }
  } else {
    $1 = 0;
  }
 }

// C++ Mesh1d -> Python array
%typemap(out) const Mesh1d & {
  npy_intp dimensions[2] = { (npy_intp)((*$1).size1()), 1 };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (size_t i=0; i<(*$1).size1(); i++) {
    (*(double*)(data + i*strides[0])) = $1[i];
  }
  $result = PyArray_Return(res);
 }

#endif



