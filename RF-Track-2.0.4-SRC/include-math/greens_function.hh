/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef greens_function_hh
#define greens_function_hh

#include "RF_Track.hh"
#include "fftw_mesh3d.hh"

namespace GreensFunction {

  template <typename GREENS_FUNCTION>
  void compute_mesh(GREENS_FUNCTION greens_function, fftwMesh3d &mesh_G, double hx, double hy, double hz, double coasting = 0.0 )
  {
    // Computes the Green's function, with 8-fold mirroring 
    const size_t Nthreads = RFT::number_of_threads;
    const size_t Nx = mesh_G.size1();
    const size_t Ny = mesh_G.size2();
    const size_t Nz = mesh_G.size3();
    const size_t Nxx = Nx>>1;
    const size_t Nyy = Ny>>1;
    const size_t Nzz = Nz>>1;
    if (greens_function.is_even) {
      auto compute_greens_function_parallel = [&] (size_t thread, size_t start, size_t end ) -> void {
	for (size_t i=start; i<end; i++) {
	  const double x0 = i*hx;
	  for (size_t j=0; j<=Nyy; j++) {
	    const double y0 = j*hy;
	    for (size_t k=0; k<=Nzz; k++) {
	      const double z0 = k*hz;
	      const double elem_ijk = coasting==0.0 ? greens_function(x0,y0,z0,hx,hy,hz) : greens_function(coasting,x0,y0,z0,hx,hy,hz);
	      mesh_G.elem(i, j, k) = elem_ijk;
	      if (i!=Nxx) {
		mesh_G.elem(Nx-i, j, k) = elem_ijk;
		if (j!=Nyy) {
		  mesh_G.elem(Nx-i, Ny-j, k) = elem_ijk;
		  if (k!=Nzz) {
		    mesh_G.elem(Nx-i, Ny-j, Nz-k) = elem_ijk;
		  }
		}
		if (k!=Nzz) {
		  mesh_G.elem(Nx-i, j, Nz-k) = elem_ijk;
		}
	      }
	      if (j!=Nyy) {
		mesh_G.elem(i, Ny-j, k) = elem_ijk;
		if (k!=Nzz) {
		  mesh_G.elem(i, Ny-j, Nz-k) = elem_ijk;
		}
	      }
	      if (k!=Nzz) {
		mesh_G.elem(i, j, Nz-k) = elem_ijk;
	      }
	    }
	  }
	}
      };
      for_all(Nthreads, Nxx+1, compute_greens_function_parallel);
    } else {
      auto compute_greens_function_parallel = [&] (size_t thread, size_t start, size_t end ) -> void {
	for (size_t i=start; i<end; i++) {
	  const double x0 = i*hx;
	  const size_t mi = Nx-i-1;
	  for (size_t j=0; j<Nyy; j++) {
	    const double y0 = j*hy;
	    const size_t mj = Ny-j-1;
	    for (size_t k=0; k<Nzz; k++) {
	      const double z0 = k*hz;
	      const size_t mk = Nz-k-1;
	      if (coasting==0.0) {
		mesh_G.elem( i,  j,  k) = greens_function( x0,  y0,  z0, hx, hy, hz);
		mesh_G.elem(mi,  j,  k) = greens_function(-x0,  y0,  z0, hx, hy, hz);
		mesh_G.elem( i, mj,  k) = greens_function( x0, -y0,  z0, hx, hy, hz);
		mesh_G.elem(mi, mj,  k) = greens_function(-x0, -y0,  z0, hx, hy, hz);
		mesh_G.elem( i,  j, mk) = greens_function( x0,  y0, -z0, hx, hy, hz);
		mesh_G.elem(mi,  j, mk) = greens_function(-x0,  y0, -z0, hx, hy, hz);
		mesh_G.elem( i, mj, mk) = greens_function( x0, -y0, -z0, hx, hy, hz);
		mesh_G.elem(mi, mj, mk) = greens_function(-x0, -y0, -z0, hx, hy, hz);
	      } else {
		mesh_G.elem( i,  j,  k) = greens_function(coasting, x0,  y0,  z0, hx, hy, hz);
		mesh_G.elem(mi,  j,  k) = greens_function(coasting,-x0,  y0,  z0, hx, hy, hz);
		mesh_G.elem( i, mj,  k) = greens_function(coasting, x0, -y0,  z0, hx, hy, hz);
		mesh_G.elem(mi, mj,  k) = greens_function(coasting,-x0, -y0,  z0, hx, hy, hz);
		mesh_G.elem( i,  j, mk) = greens_function(coasting, x0,  y0, -z0, hx, hy, hz);
		mesh_G.elem(mi,  j, mk) = greens_function(coasting,-x0,  y0, -z0, hx, hy, hz);
		mesh_G.elem( i, mj, mk) = greens_function(coasting, x0, -y0, -z0, hx, hy, hz);
		mesh_G.elem(mi, mj, mk) = greens_function(coasting,-x0, -y0, -z0, hx, hy, hz);
	      }
	    }
	  }
	}
      };
      for_all(Nthreads, Nxx, compute_greens_function_parallel);
    }
  }
}

#endif /* greens_function_hh */
