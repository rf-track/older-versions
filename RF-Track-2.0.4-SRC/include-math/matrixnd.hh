/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef matrixnd_hh
#define matrixnd_hh

#include <cstring>
#include <cstdlib>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include "vectornd.hh"
#include "static_matrix.hh"

class MatrixNd {
	
  gsl_matrix *matrix;
		
public:

  MatrixNd(size_t _rows, size_t _cols )			{ matrix = (_rows==0 || _cols==0) ? nullptr : gsl_matrix_alloc(_rows, _cols); }
  MatrixNd()						{ matrix = nullptr; }

  MatrixNd(const gsl_matrix *src ) 			{ gsl_matrix_memcpy (matrix = gsl_matrix_alloc(src->size1, src->size2), src); }
  MatrixNd(const MatrixNd &src ) 			{ if (src.matrix) gsl_matrix_memcpy(matrix = gsl_matrix_alloc(src.matrix->size1, src.matrix->size2), src.matrix); else matrix = nullptr; }
  MatrixNd(size_t _rows, size_t _cols, double x )	{ if (_rows==0 || _cols==0) matrix = nullptr; else gsl_matrix_set_all(matrix = gsl_matrix_alloc(_rows, _cols), x); }
  MatrixNd(size_t _rows, size_t _cols, const double *data )
  {
    if (_rows==0 || _cols==0)
      matrix = nullptr;
    else {
      matrix = gsl_matrix_alloc(_rows, _cols);
      for (size_t i=0; i<_rows; i++) {
	for (size_t j=0; j<_cols; j++) {
	  gsl_matrix_set(matrix, i, j, data[_cols*i+j]);
	}
      }
    }
  }
  template <size_t M, size_t N>
  explicit MatrixNd(const StaticMatrix<M,N> &m ) : MatrixNd(M, N, m.c_ptr()) {}  

  ~MatrixNd() { if (matrix) gsl_matrix_free(matrix); }

  explicit operator gsl_matrix *() { return matrix; }
  explicit operator const gsl_matrix *() const { return matrix; }
  
  void set(double x )	{ if (matrix) gsl_matrix_set_all(matrix, x); }
  void clear() 		{ if (matrix) gsl_matrix_set_zero(matrix); }
  void identity()	{ if (matrix) gsl_matrix_set_identity(matrix); }
  void transpose()	{ if (matrix) gsl_matrix_transpose(matrix); }

  size_t rows() const 	 { return matrix ? matrix->size1 : 0u; }
  size_t columns() const { return matrix ? matrix->size2 : 0u; }

  size_t size1() const 	{ return rows(); }
  size_t size2() const 	{ return columns(); }

  void resize(size_t size_1, size_t size_2 )
  {
    if (matrix) {
      if (matrix->size1 != size_1 || matrix->size2 != size_2) {
	gsl_matrix_free(matrix);
	matrix = (size_1==0 || size_2==0) ? nullptr : gsl_matrix_alloc(size_1, size_2);
      } 
    } else {
      matrix = (size_1==0 || size_2==0) ? nullptr : gsl_matrix_alloc(size_1, size_2);
    }
  }

  void resize(size_t size_1, size_t size_2, double val )
  {
    resize(size_1, size_2);
    gsl_matrix_set_all(matrix, val);
  }
  
  const gsl_matrix *gsl_matrix_ptr() const { return matrix; }
  gsl_matrix *gsl_matrix_ptr() { return matrix; }

  MatrixNd get_submatrix(size_t k1, size_t k2, size_t n1, size_t n2 ) const
  {
    if (!matrix) 
      return MatrixNd();
    const gsl_matrix &m = gsl_matrix_const_submatrix(matrix, k1, k2, n1, n2).matrix;
    return MatrixNd(&m);
  }
  
  friend double trace(const MatrixNd &m );
  friend MatrixNd transpose(const MatrixNd &m );

  const MatrixNd &operator = (double x ) { if (matrix) gsl_matrix_set_all(matrix, x); return *this; }
  const MatrixNd &operator = (const MatrixNd &src )
  {
    if (this != &src) {
      if (size1() != src.size1() || size2() != src.size2()) {
	resize(src.size1(), src.size2());
      }
      if (matrix)
	gsl_matrix_memcpy(matrix, src.matrix);
    }
    return *this; 
  }
	
  double *operator [] (size_t i )  				{ return matrix ? ::gsl_matrix_ptr(matrix, i, 0) : nullptr; }
  const double *operator [] (size_t i ) const 			{ return matrix ? ::gsl_matrix_const_ptr(matrix, i, 0) : nullptr; }
	
  const MatrixNd &operator += (const MatrixNd &a )		{ if (matrix && a.matrix) gsl_matrix_add(matrix, a.matrix); return *this; }
  const MatrixNd &operator -= (const MatrixNd &a )		{ if (matrix && a.matrix) gsl_matrix_sub(matrix, a.matrix); return *this; }

  const MatrixNd &operator *= (double x )			{ if (matrix) gsl_matrix_scale(matrix, x); return *this; }
  const MatrixNd &operator /= (double x )			{ if (matrix) gsl_matrix_scale(matrix, 1/x); return *this; }

  // FIRENDS
	
  friend MatrixNd inverse(MatrixNd a )
  {
    if (!a.matrix)
      return MatrixNd();
    int s;
    MatrixNd inverse(a.matrix->size1, a.matrix->size2);
    gsl_permutation *p=gsl_permutation_alloc(a.matrix->size1);
    gsl_linalg_LU_decomp(a.matrix,p,&s);
    gsl_linalg_LU_invert(a.matrix,p,inverse.matrix);
    gsl_permutation_free(p);
    return inverse;	
  }

  friend double det(MatrixNd a )
  {
    if (!a.matrix)
      return 0.0;
    double retval;
    if (gsl_permutation *p = gsl_permutation_alloc(a.matrix->size1)) { 
      int s;
      gsl_linalg_LU_decomp(a.matrix,p,&s);
      retval = gsl_linalg_LU_det(a.matrix,s);
      gsl_permutation_free(p);
    } else {
      std::cerr << "error: cannot compute matrix determinant\n";
      retval = 0.0;
    }
    return retval;
  }
  
  friend MatrixNd operator + (MatrixNd a, const MatrixNd &b )	{ a+=b; return a; }
  friend MatrixNd operator - (MatrixNd a, const MatrixNd &b )	{ a-=b; return a; }
  friend MatrixNd operator - (MatrixNd a )
  {
    if (!a.matrix)
      return MatrixNd();
    for (size_t i=0;i<a.matrix->size1;i++) {
      double *ptr=::gsl_matrix_ptr(a.matrix,i,0);
      for (size_t j=0;j<a.matrix->size2;j++)	ptr[j]=-ptr[j];
    }
    return a;
  }

  friend MatrixNd operator / (const MatrixNd &a, const MatrixNd &b )	{ return a * inverse(b); }

  friend VectorNd operator * (const MatrixNd &a, const VectorNd &x )
  {
    if (!a.matrix)
      return VectorNd();
    VectorNd y(a.matrix->size1);
    gsl_blas_dgemv(CblasNoTrans, 1.0, a.matrix, x.gsl_vector_ptr(), 0.0, y.gsl_vector_ptr());
    return y;
  }

  friend MatrixNd operator * (const MatrixNd &a, const MatrixNd &b )
  {
    if (!a.matrix || !b.matrix)
      return MatrixNd();
    MatrixNd c(a.matrix->size1,b.matrix->size2);
    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, a.matrix, b.matrix, 0.0, c.matrix);
    return c;
  }
  friend MatrixNd operator * (double x, MatrixNd a )			{ if (a.matrix) gsl_matrix_scale(a.matrix, x); return a; }
  friend MatrixNd operator * (MatrixNd a, double x )			{ if (a.matrix) gsl_matrix_scale(a.matrix, x); return a; }

  friend std::ostream &operator << (std::ostream &stream, const MatrixNd &a );
  friend std::istream &operator >> (std::istream &stream, MatrixNd &a );

  friend OStream &operator << (OStream &stream, const MatrixNd &v );
  friend IStream &operator >> (IStream &stream, MatrixNd &v );

};

inline double trace(const MatrixNd &m )
{
  if (!m.matrix)
    return 0.0;
  double result=m[0][0];
  for (size_t i=1;i<m.matrix->size1;i++)	result+=m[i][i];
  return result;
}

inline MatrixNd transpose(const MatrixNd &m )
{
  if (!m.matrix)
    return MatrixNd();
  MatrixNd t(m.matrix->size2, m.matrix->size1);
  gsl_matrix_transpose_memcpy(t.gsl_matrix_ptr(), m.gsl_matrix_ptr());
  return t;
}

inline std::ostream &operator << (std::ostream &stream, const MatrixNd &m )
{
  stream << m.size1() << 'x' << m.size2() << ':' << std::endl;
  for (size_t i=0;i<m.size1();i++) {
    for (size_t j=0;j<m.size2();j++)
      stream << ' ' << m[i][j];	
    stream << std::endl;
  }
  return stream; 
}

inline std::istream &operator >> (std::istream &stream, MatrixNd &m )
{
  std::string _size_str;
  stream >> _size_str; // in the form "MxN:"
  char _size[_size_str.size()+1];
  strncpy(_size,_size_str.c_str(),_size_str.size());
  _size[_size_str.size()] = '\0';
  size_t size1=(size_t)strtol(strtok(_size," x"), (char **)nullptr, 10);
  size_t size2=(size_t)strtol(strtok(nullptr, " :"), (char **)nullptr, 10);
  m.resize(size1, size2);
  for (size_t i=0;i<m.size1();i++)
    for (size_t j=0;j<m.size2();j++)
      stream >> m[i][j];	
  return stream; 
}

inline OStream &operator << (OStream &stream, const MatrixNd &m )	
{ 
  stream << m.size1() << m.size2();
  for (size_t i=0; i<m.size1(); i++)
    for (size_t j=0; j<m.size2(); j++)
      stream << m[i][j];
  return stream;
}

inline IStream &operator >> (IStream &stream, MatrixNd &m )
{
  size_t size1, size2;
  stream >> size1 >> size2;
  m.resize(size1, size2);
  for (size_t i=0; i<size1; i++)
    for (size_t j=0; j<size2; j++)
      stream >> m[i][j];
  return stream;
}

static inline MatrixNd IdentityNd(size_t n )
{
  MatrixNd tmp(n,n);
  if (n>0)
    gsl_matrix_set_identity(tmp.gsl_matrix_ptr());
  return tmp;
}

static inline VectorNd solve(MatrixNd A, VectorNd b )
{
  VectorNd x(b.size());
  gsl_linalg_HH_solve(A.gsl_matrix_ptr(), b.gsl_vector_ptr(), x.gsl_vector_ptr());
  return x;
}

#endif /* matrixnd_hh */
