/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef greens_function_coulomb_horizontal_plates_hh
#define greens_function_coulomb_horizontal_plates_hh

#include "greens_functions/coulomb.hh"

#define N_mirror_charges 32

namespace GreensFunction {
  struct Coulomb_HorizontalPlates {
    enum { is_even = 1 };
    double h; // mm, half gap
    Coulomb_HorizontalPlates(double h_ = 1.0 ) : h(h_) {}
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = Coulomb()(x0,y0,z0,dx,dy,dz);
      for (size_t n=1; n<=N_mirror_charges; n++) {
	sum += (n&1 ? -1 : +1) * (Coulomb()(x0,n*2*h+y0,z0,dx,dy,dz) + Coulomb()(x0,n*2*h-y0,z0,dx,dy,dz));
      }
      return sum;
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = (*this)(x0,y0,z0,dx,dy,dz);
      for (size_t n=1; n<=N_coasting; n++) {
	const double zp = n*L_coasting-z0;
	const double zm = n*L_coasting+z0;
	const double value = (*this)(x0,y0,zm,dx,dy,dz) + (*this)(x0,y0,zp,dx,dy,dz);
	sum += value;
      }
      return sum;
    }
  };
  struct IntegratedCoulomb_HorizontalPlates {
    enum { is_even = 1 };
    double h; // mm, half gap
    IntegratedCoulomb_HorizontalPlates(double h_ = 1.0 ) : h(h_) {}
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (x0==0.0 && y0==0.0 && z0==0.0)
	return Coulomb_HorizontalPlates(h)(x0,y0,z0,dx,dy,dz);
      double sum = IntegratedCoulomb()(x0,y0,z0,dx,dy,dz);
      for (size_t n=1; n<=N_mirror_charges; n++) {
	sum += (n&1 ? -1 : +1) * (Coulomb()(x0,n*2*h+y0,z0,dx,dy,dz) + Coulomb()(x0,n*2*h-y0,z0,dx,dy,dz));
      }
      return sum;
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = (*this)(x0,y0,z0,dx,dy,dz);
      Coulomb_HorizontalPlates coulomb(h);
      for (size_t n=1; n<=N_coasting; n++) {
	const double zp = n*L_coasting-z0;
	const double zm = n*L_coasting+z0;
	const double value = coulomb(x0,y0,zm,dx,dy,dz) + coulomb(x0,y0,zp,dx,dy,dz);
	sum += value;
      }
      return sum;
    }
  };
}

#endif /* greens_functions_coulomb_horizontal_plates_hh */
