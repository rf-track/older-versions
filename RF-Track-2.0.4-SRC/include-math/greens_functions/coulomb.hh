/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef greens_functions_coulomb_hh
#define greens_functions_coulomb_hh

#include "greens_function.hh"
#include "greens_functions/coulomb_2d.hh"

#define N_coasting 8

namespace GreensFunction {
  struct Coulomb {
    enum { is_even = 1 };
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (x0==0.0 && y0==0.0 && z0==0.0) return 0.0;
      return 1.0/(4*M_PI*hypot(x0,y0,z0));
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = (*this)(x0,y0,z0,dx,dy,dz);
      for (size_t n=1; n<=N_coasting; n++) {
	const double zp = n*L_coasting+z0;
	const double zm = n*L_coasting-z0;
	const double value = (*this)(x0,y0,zm,dx,dy,dz) + (*this)(x0,y0,zp,dx,dy,dz);
	sum += value;
      }
      return sum;
    }
  };
  struct IntegratedCoulomb {
    enum { is_even = 1 };
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      // indefinite integral: integrate(integrate(integrate(1/sqrt(x^2+y^2+(g*z)^2),x),y),z)/(dx*dy*dz);
      auto F = [] (double x, double y, double z ) {
	if (x==0.0 && y==0.0) return 0.0;
	if (y==0.0 && z==0.0) return 0.0;
	if (z==0.0 && x==0.0) return 0.0;
	const double xy = x*y;
	const double zx = z*x;
	const double yz = y*z;
	const double x2 = x*x;
	const double y2 = y*y;
	const double z2 = z*z;
	if (x==0.0) return 0.5*yz*log(y2+z2);
	if (y==0.0) return 0.5*zx*log(z2+x2);
	if (z==0.0) return 0.5*xy*log(x2+y2);
	const double r = hypot(x, y, z);
	const double rx = r*x;
	const double ry = r*y;
	const double rz = r*z;
	return xy*log(z+r) + yz*log(x+r) + zx*log(y+r) - 0.5 * (x2*atan(yz/rx) + y2*atan(zx/ry) + z2*atan(xy/rz));
      };
      const double hdx = 0.5*dx;
      const double hdy = 0.5*dy;
      const double hdz = 0.5*dz;
      const double sum =
	+F(x0+hdx, y0+hdy, z0+hdz)
	+F(x0-hdx, y0-hdy, z0+hdz)
	+F(x0-hdx, y0+hdy, z0-hdz)
	+F(x0+hdx, y0-hdy, z0-hdz)
	-F(x0-hdx, y0+hdy, z0+hdz)
	-F(x0+hdx, y0-hdy, z0+hdz)
	-F(x0+hdx, y0+hdy, z0-hdz)
	-F(x0-hdx, y0-hdy, z0-hdz);
      return sum/(4*M_PI*dx*dy*dz);
    }
    inline double operator() (double L_coasting, double x0, double y0, double z0, double dx, double dy, double dz )
    {
      double sum = (*this)(x0,y0,z0,dx,dy,dz);
      Coulomb coulomb;
      for (size_t n=1; n<=N_coasting; n++) {
	const double zp = n*L_coasting+z0;
	const double zm = n*L_coasting-z0;
	const double value = coulomb(x0,y0,zm,dx,dy,dz) + coulomb(x0,y0,zp,dx,dy,dz);
	sum += value;
      }
      return sum;
    }
  };

  template <>
  void compute_mesh(IntegratedCoulomb greens_function, fftwMesh3d &mesh_G, double hx, double hy, double hz, double coasting );
}

#endif /* greens_functions_coulomb_hh */
