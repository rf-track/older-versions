/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef greens_functions_coulomb_2d_hh
#define greens_functions_coulomb_2d_hh

#include "hypot.hh"

namespace GreensFunction {
  struct Coulomb_2d {
    enum { is_even = 1 };
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (z0!=0.0 || (x0==0.0 && y0==0.0))
	return 0.0;
      auto sqr = [] (double x ) { return x*x; };
      return -log(sqr(x0)+sqr(y0))/(4*M_PI*dz);
    }
  };
  struct IntegratedCoulomb_2d {
    enum { is_even = 1 };
    inline double operator() (double x0, double y0, double z0, double dx, double dy, double dz )
    {
      if (z0!=0.0) return 0.0;
      /* infinitely-long line charge */
      // integrate(integrate(-log(y^2+x^2),x),y);
      auto f = [] (double x, double y ) {
	if (x==0.0 || y==0.0) return 0.0;
	const double x2=x*x;
	const double y2=y*y;
	return x*y*(3-log(y2+x2))-x2*atan(y/x)-atan(x/y)*y2;
      };
      const double hdx = 0.5*dx;
      const double hdy = 0.5*dy;
      const double sum =
	+f(x0+hdx,y0+hdy)
	+f(x0-hdx,y0-hdy)
	-f(x0+hdx,y0-hdy)
	-f(x0-hdx,y0+hdy);
      return sum/(4*M_PI*dx*dy*dz);
    }
  };
}

#endif /* greens_functions_coulomb_2d_hh */
