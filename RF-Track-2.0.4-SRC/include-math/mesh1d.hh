/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef mesh1d_hh
#define mesh1d_hh

#include <cmath>
#include <complex>
#include <assert.h>
#include <valarray>
#include <algorithm>

#include "numtools.hh"
#include "fftw_complex.hh"

template <class T, class Alloc = std::allocator<T>>
class TMesh1d {
protected:
  size_t nx; /// width, height
  std::vector<T, Alloc> data_;
  T dummy;
public:
  template <class Alloc1> TMesh1d(const TMesh1d<T,Alloc1> &m ) : nx(m.size1()), data_(m.size()) { for (size_t i=0; i<m.size(); i++) { data_[i] = m.data()[i]; } } 
  explicit TMesh1d(size_t Nx = 4 ) : nx(Nx), data_(Nx) {}
  TMesh1d(size_t Nx, T val ) : nx(Nx), data_(Nx,val) {}
  void resize(size_t Nx, T val ) { nx=Nx; data_.resize(Nx,val); }
  void resize(size_t Nx ) { nx=Nx; data_.resize(Nx); }
  size_t size() const { return data_.size(); }
  size_t size1() const { return nx; }
  T *data() { return data_.data(); }
  const T *data() const { return data_.data(); }
  template <class Alloc1> const TMesh1d &operator = (const TMesh1d<T,Alloc1> &m ) { resize(m.size1()); for (size_t i=0; i<m.size(); i++) { data_[i] = m.data()[i]; } return *this; }
  const TMesh1d &operator -= (const TMesh1d &m ) { for (size_t i=0; i<m.size(); i++) { data_[i] -= m.data()[i]; } return *this; }
  const TMesh1d &operator += (const TMesh1d &m ) { for (size_t i=0; i<m.size(); i++) { data_[i] += m.data()[i]; } return *this; }
  const TMesh1d &operator = (double a ) { for (T &x : data_) x = a; return *this; }
  const TMesh1d &operator *= (double a ) { for (T &x : data_) x *= a; return *this; }
  const TMesh1d &operator /= (double a ) { for (T &x : data_) x /= a; return *this; }
  friend TMesh1d operator / (const TMesh1d &m, double a ) { TMesh1d n = m; n/=a; return n; }
  inline const T &elem(size_t i ) const { return data_[i]; }
  inline T &elem(size_t i )
  {
    if (i>=nx) return dummy;
    return data_[i];
  }

  inline void add_value(double x, const T &value )
  {
    if (x<0.0) return;
    if (x>double(nx)-1.0) return;
    if (x==double(nx)-1.0) {
      elem(nx-1) += value;
      return;
    }
    double iinteger;
    const double ifrac = modf(x, &iinteger);
    const size_t i = size_t(iinteger);
    elem(i+1) += value * (ifrac);
    elem(i)   += value * (1.0 - ifrac);
  }
};

//

#include "mesh1d_lint.hh"
#include "mesh1d_cint.hh"

//

typedef TMesh1d     <double> Mesh1d;
typedef TMesh1d_LINT<double> Mesh1d_LINT;
typedef TMesh1d_CINT<double> Mesh1d_CINT;

typedef TMesh1d     <fftwComplex> ComplexMesh1d;
typedef TMesh1d_LINT<fftwComplex> ComplexMesh1d_LINT;
typedef TMesh1d_CINT<fftwComplex> ComplexMesh1d_CINT;

extern ComplexMesh1d DefaultComplexMesh1d;
extern Mesh1d DefaultMesh1d;

#endif /* mesh1d_hh */
