/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* matrixnd.i */

%{
#include "RF_Track.hh"
#include "matrixnd.hh"
%}

#if defined(SWIGOCTAVE)

// Octave Matrix -> C++ MatrixNd
%typemap(in) const MatrixNd & {
  const Matrix &matrix = $input.matrix_value();
  $1 = new MatrixNd(matrix.rows(), matrix.columns());
  auto matrix_copy_parallel = [&] (size_t thread, size_t start, size_t end ) {
    for (size_t i=start; i<end; i++) {
      for (size_t j=0; j<(size_t)matrix.columns(); j++)
	((*$1)[i])[j] = matrix(i,j);
    }
  };
  for_all(RFT::number_of_threads, matrix.rows(), matrix_copy_parallel);
 }

%typemap(freearg) const MatrixNd & {
  if ($1) delete $1;
 }

%typemap(typecheck,precedence=0) const MatrixNd & {
  octave_value obj = $input;
  $1 = obj.is_real_scalar() || obj.is_real_matrix() ? 1 : 0;
 }

// C++ MatrixNd -> Octave Matrix (as a return value)
%typemap(out) MatrixNd {
  Matrix ret($1.rows(), $1.columns());
  auto matrix_copy_parallel = [&] (size_t thread, size_t start, size_t end ) {
    for (size_t i=start; i<end; i++) {
      for (size_t j=0; j<$1.columns(); j++)
	ret(i,j) = ($1[i])[j];
    }
  };
  for_all(RFT::number_of_threads, $1.rows(), matrix_copy_parallel);
  $result = octave_value(ret);
 }

%typemap(out) const MatrixNd & {
  Matrix ret((*$1).rows(), (*$1).columns());
  auto matrix_copy_parallel = [&] (size_t thread, size_t start, size_t end ) {
    for (size_t i=start; i<end; i++)
      for (size_t j=0; j<(*$1).columns(); j++)
	ret(i,j) = ((*$1)[i])[j];
  };
  for_all(RFT::number_of_threads, (*$1).rows(), matrix_copy_parallel);
x  $result = octave_value(ret);
 }

// C++ MatrixNd -> Octave Matrix (as an output argument)
%typemap(in, numinputs=0) MatrixNd & (MatrixNd temp ) {
  $1 = &temp;
 }

%typemap(argout) MatrixNd & {
  Matrix ret((*$1).rows(), (*$1).columns());
  auto matrix_copy_parallel = [&] (size_t thread, size_t start, size_t end ) {
    for (size_t i=start; i<end; i++)
      for (size_t j=0; j<(*$1).columns(); j++)
	ret(i,j) = ((*$1)[i])[j];
  };
  for_all(RFT::number_of_threads, (*$1).rows(), matrix_copy_parallel);
  $result->append(ret);
 }

%typemap(freearg,noblock=1) MatrixNd & {
 }

#endif

#if defined(SWIGPYTHON)
%{
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#define is_array(a) ((a) && PyArray_Check((PyArrayObject *)a))
%}

%init %{
  import_array();
  %}

// NumPy Matrix -> C++ MatrixNd
%typemap(in) const MatrixNd & {
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 1, 2);
    npy_intp strides[2] = { 0, 0 };
    npy_intp rows, cols;
    if (PyArray_NDIM(array) == 1) {
      rows = 1;
      cols = PyArray_DIMS(array)[0];
      strides[0] = 0;
      strides[1] = PyArray_STRIDES(array)[0];
    } else {
      rows = PyArray_DIMS(array)[0];
      cols = PyArray_DIMS(array)[1];
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = PyArray_STRIDES(array)[1];
    }
    char *data = PyArray_BYTES(array);
    $1 = new MatrixNd(rows, cols);
    for (int i=0; i<rows; i++)
      for (int j=0; j<cols; j++)
        ((*$1)[i])[j] = *(double *)(data + i*strides[0] + j*strides[1]);
    Py_DECREF(array);
  } else {
    $1 = new MatrixNd(1, 1);
    (*$1)[0][0] = PyFloat_AsDouble($input);
    Py_DECREF($input);
  }
 }

%typemap(freearg) const MatrixNd & {
  if ($1) delete $1;
 }

%typemap(typecheck,precedence=0) const MatrixNd & {
  $1 = is_array($input) || PyFloat_Check($input) ? 1 : 0;
 }

%typemap(argout,noblock=1) const MatrixNd & {
 }

// C++ MatrixNd -> NumPy Matrix (as a return value)
%typemap(out) MatrixNd {
  npy_intp dimensions[2] = { (npy_intp)$1.rows(), (npy_intp)$1.columns() };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (int i=0; i<dimensions[0]; i++)
    for (int j=0; j<dimensions[1]; j++)
      *(double*)(data + i*strides[0] + j*strides[1]) = ($1[i])[j];
  $result = PyArray_Return(res);
 }

%typemap(out) const MatrixNd & {
  npy_intp dimensions[2] = { npy_intp((*$1).rows()), npy_intp((*$1).columns()) };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (int i=0; i<dimensions[0]; i++)
    for (int j=0; j<dimensions[1]; j++)
      *(double*)(data + i*strides[0] + j*strides[1]) = ((*$1)[i])[j];
  $result = PyArray_Return(res);
}

// C++ MatrixNd -> NumPy Matrix (as an output argument)
%typemap(in, numinputs=0) MatrixNd & (MatrixNd temp ) {
  $1 = &temp;
 }

%typemap(argout) MatrixNd & {
  npy_intp dimensions[2] = { npy_intp($1->rows()), npy_intp($1->columns()) };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (int i=0; i<dimensions[0]; i++)
    for (int j=0; j<dimensions[1]; j++)
      *(double*)(data + i*strides[0] + j*strides[1]) = ((*$1)[i])[j];
  $result = SWIG_Python_AppendOutput($result, PyArray_Return(res));
 }

%typemap(freearg,noblock=1) MatrixNd & {
 }

#endif
