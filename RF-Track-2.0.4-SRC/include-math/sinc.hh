/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef sinc_hh
#define sinc_hh

#include <limits>
#include <cmath>

namespace {
  // based on http://www.boost.org/doc/libs/1_66_0/boost/math/special_functions/sinc.hpp
  inline double sinc(double x ) // sinc(x) = sin(pi*x) / (pi*x)
  {
    const double taylor_0_bound = sqrt(std::numeric_limits<double>::epsilon());
    const double taylor_2_bound = sqrt(taylor_0_bound);
    const double taylor_4_bound = sqrt(taylor_2_bound);
    x *= M_PI;
    if (fabs(x) >= taylor_4_bound)
      return sin(x)/x;
    // approximation by taylor series in x at 0 up to order 0
    double result = 1.0;
    if (fabs(x) >= taylor_0_bound) {
      double x2 = x*x;
      // approximation by taylor series in x at 0 up to order 2
      result -= x2/6.0;
      if (fabs(x) >= taylor_2_bound) {
	// approximation by taylor series in x at 0 up to order 4
	result += (x2*x2)/120.0;
      }
    } 
    return result;
  }
}

#endif /* sinc_hh */
