/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef fftw_mesh3d_hh
#define fftw_mesh3d_hh

#include "mesh3d_lint.hh"
#include "mesh3d_cint.hh"
#include "fftw_allocator.hh"

typedef TMesh3d     <double, fftwAllocator<double>> fftwMesh3d;
typedef TMesh3d_LINT<double, fftwAllocator<double>> fftwMesh3d_LINT;
typedef TMesh3d_CINT<double, fftwAllocator<double>> fftwMesh3d_CINT;

typedef TMesh3d     <fftwComplex, fftwAllocator<fftwComplex>> fftwComplexMesh3d;
typedef TMesh3d_LINT<fftwComplex, fftwAllocator<fftwComplex>> fftwComplexMesh3d_LINT;
typedef TMesh3d_CINT<fftwComplex, fftwAllocator<fftwComplex>> fftwComplexMesh3d_CINT;

#endif /* fftw_mesh3d_hh */
