/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef mesh3d_cint_hh
#define mesh3d_cint_hh

#include "mesh3d.hh"

template <class T, class Alloc = std::allocator<T>>
class TMesh3d_CINT : public TMesh3d<T,Alloc> {
protected:
  using TMesh3d<T,Alloc>::nx;
  using TMesh3d<T,Alloc>::ny;
  using TMesh3d<T,Alloc>::nz;
public:
  using TMesh3d<T,Alloc>::TMesh3d;
  using TMesh3d<T,Alloc>::elem;
  using TMesh3d<T,Alloc>::operator=;
  
  template <class Alloc1> TMesh3d_CINT(const TMesh3d<T,Alloc1> &m ) : TMesh3d<T,Alloc>(m) {}

  inline T operator()(double x, double y, double z ) const // tricubic interpolation
  {
    if (x<0.0) return T{0.0};
    if (y<0.0) return T{0.0};
    if (z<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    if (y>double(ny)-1.0) return T{0.0};
    if (z>double(nz)-1.0) return T{0.0};
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k>0 && k+2<nz) {
	T p0 = elem(i,j,k-1);
	T p1 = elem(i,j,k);
	T p2 = elem(i,j,k+1);
	T p3 = elem(i,j,k+2);
	return CINT1(p0,p1,p2,p3,kfrac);
      }
      if (k>1 && k+1<nz) {
	T p0 = elem(i,j,k-2);
	T p1 = elem(i,j,k-1);
	T p2 = elem(i,j,k);
	T p3 = elem(i,j,k+1);
	return CINT2(p0,p1,p2,p3,kfrac);
      }
      if (k==0) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	T p2 = elem(i,j,k+2);
	T p3 = elem(i,j,k+3);
	return CINT0(p0,p1,p2,p3,kfrac);
      }
      T p0 = elem(i,j,k);
      return p0;
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j>0 && j+2<ny) {
	T p0 = z_interpolate(i,j-1);
	T p1 = z_interpolate(i,j);
	T p2 = z_interpolate(i,j+1);
	T p3 = z_interpolate(i,j+2);
	return CINT1(p0,p1,p2,p3,jfrac);
      }
      if (j>1 && j+1<ny) {
	T p0 = z_interpolate(i,j-2);
	T p1 = z_interpolate(i,j-1);
	T p2 = z_interpolate(i,j);
	T p3 = z_interpolate(i,j+1);
	return CINT2(p0,p1,p2,p3,jfrac);
      }
      if (j==0) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	T p2 = z_interpolate(i,j+2);
	T p3 = z_interpolate(i,j+3);
	return CINT0(p0,p1,p2,p3,jfrac);
      }
      T p0 = z_interpolate(i,j);
      return p0;
    };
    auto xyz_interpolate = [&] () {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i>0 && i+2<nx) {
	T p0 = yz_interpolate(i-1);
	T p1 = yz_interpolate(i);
	T p2 = yz_interpolate(i+1);
	T p3 = yz_interpolate(i+2);
	return CINT1(p0,p1,p2,p3,ifrac);
      }
      if (i>1 && i+1<nx) {
	T p0 = yz_interpolate(i-2);
	T p1 = yz_interpolate(i-1);
	T p2 = yz_interpolate(i);
	T p3 = yz_interpolate(i+1);
	return CINT2(p0,p1,p2,p3,ifrac);
      }
      if (i==0) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	T p2 = yz_interpolate(i+2);
	T p3 = yz_interpolate(i+3);
	return CINT0(p0,p1,p2,p3,ifrac);
      }
      T p0 = yz_interpolate(i);
      return p0;
    };
    return xyz_interpolate();
  }

  inline T deriv_x(double x, double y, double z ) const // tricubic interpolation
  {
    if (x<0.0) return T{0.0};
    if (y<0.0) return T{0.0};
    if (z<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    if (y>double(ny)-1.0) return T{0.0};
    if (z>double(nz)-1.0) return T{0.0};
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k>0 && k+2<nz) {
	T p0 = elem(i,j,k-1);
	T p1 = elem(i,j,k);
	T p2 = elem(i,j,k+1);
	T p3 = elem(i,j,k+2);
	return CINT1(p0,p1,p2,p3,kfrac);
      }
      if (k>1 && k+1<nz) {
	T p0 = elem(i,j,k-2);
	T p1 = elem(i,j,k-1);
	T p2 = elem(i,j,k);
	T p3 = elem(i,j,k+1);
	return CINT2(p0,p1,p2,p3,kfrac);
      }
      if (k==0) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	T p2 = elem(i,j,k+2);
	T p3 = elem(i,j,k+3);
	return CINT0(p0,p1,p2,p3,kfrac);
      }
      T p0 = elem(i,j,k);
      return p0;
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j>0 && j+2<ny) {
	T p0 = z_interpolate(i,j-1);
	T p1 = z_interpolate(i,j);
	T p2 = z_interpolate(i,j+1);
	T p3 = z_interpolate(i,j+2);
	return CINT1(p0,p1,p2,p3,jfrac);
      }
      if (j>1 && j+1<ny) {
	T p0 = z_interpolate(i,j-2);
	T p1 = z_interpolate(i,j-1);
	T p2 = z_interpolate(i,j);
	T p3 = z_interpolate(i,j+1);
	return CINT2(p0,p1,p2,p3,jfrac);
      }
      if (j==0) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	T p2 = z_interpolate(i,j+2);
	T p3 = z_interpolate(i,j+3);
	return CINT0(p0,p1,p2,p3,jfrac);
      }
      T p0 = z_interpolate(i,j);
      return p0;
    };
    auto xyz_interpolate = [&]() {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i>0 && i+2<nx) {
	T p0 = yz_interpolate(i-1);
	T p1 = yz_interpolate(i);
	T p2 = yz_interpolate(i+1);
	T p3 = yz_interpolate(i+2);
	return CINT1_deriv(p0,p1,p2,p3,ifrac);
      }
      if (i>1 && i+1<nx) {
	T p0 = yz_interpolate(i-2);
	T p1 = yz_interpolate(i-1);
	T p2 = yz_interpolate(i);
	T p3 = yz_interpolate(i+1);
	return CINT2_deriv(p0,p1,p2,p3,ifrac);
      }
      if (i==0) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	T p2 = yz_interpolate(i+2);
	T p3 = yz_interpolate(i+3);
	return CINT0_deriv(p0,p1,p2,p3,ifrac);
      }
      T p0 = yz_interpolate(i-3);
      T p1 = yz_interpolate(i-2);
      T p2 = yz_interpolate(i-1);
      T p3 = yz_interpolate(i);
      return CINT2_deriv(p0,p1,p2,p3,1.0); // backward differentiation
    };
    return xyz_interpolate();
  }

  inline T deriv_y(double x, double y, double z ) const // tricubic interpolation
  {
    if (x<0.0) return T{0.0};
    if (y<0.0) return T{0.0};
    if (z<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    if (y>double(ny)-1.0) return T{0.0};
    if (z>double(nz)-1.0) return T{0.0};
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k>0 && k+2<nz) {
	T p0 = elem(i,j,k-1);
	T p1 = elem(i,j,k);
	T p2 = elem(i,j,k+1);
	T p3 = elem(i,j,k+2);
	return CINT1(p0,p1,p2,p3,kfrac);
      }
      if (k>1 && k+1<nz) {
	T p0 = elem(i,j,k-2);
	T p1 = elem(i,j,k-1);
	T p2 = elem(i,j,k);
	T p3 = elem(i,j,k+1);
	return CINT2(p0,p1,p2,p3,kfrac);
      }
      if (k==0) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	T p2 = elem(i,j,k+2);
	T p3 = elem(i,j,k+3);
	return CINT0(p0,p1,p2,p3,kfrac);
      }
      T p0 = elem(i,j,k);
      return p0;
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j>0 && j+2<ny) {
	T p0 = z_interpolate(i,j-1);
	T p1 = z_interpolate(i,j);
	T p2 = z_interpolate(i,j+1);
	T p3 = z_interpolate(i,j+2);
	return CINT1_deriv(p0,p1,p2,p3,jfrac);
      }
      if (j>1 && j+1<ny) {
	T p0 = z_interpolate(i,j-2);
	T p1 = z_interpolate(i,j-1);
	T p2 = z_interpolate(i,j);
	T p3 = z_interpolate(i,j+1);
	return CINT2_deriv(p0,p1,p2,p3,jfrac);
      }
      if (j==0) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	T p2 = z_interpolate(i,j+2);
	T p3 = z_interpolate(i,j+3);
	return CINT0_deriv(p0,p1,p2,p3,jfrac);
      }
      T p0 = z_interpolate(i,j-3);
      T p1 = z_interpolate(i,j-2);
      T p2 = z_interpolate(i,j-1);
      T p3 = z_interpolate(i,j);
      return CINT2_deriv(p0,p1,p2,p3,1.0); // backward differentiation
    };
    auto xyz_interpolate = [&]() {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i>0 && i+2<nx) {
	T p0 = yz_interpolate(i-1);
	T p1 = yz_interpolate(i);
	T p2 = yz_interpolate(i+1);
	T p3 = yz_interpolate(i+2);
	return CINT1(p0,p1,p2,p3,ifrac);
      }
      if (i>1 && i+1<nx) {
	T p0 = yz_interpolate(i-2);
	T p1 = yz_interpolate(i-1);
	T p2 = yz_interpolate(i);
	T p3 = yz_interpolate(i+1);
	return CINT2(p0,p1,p2,p3,ifrac);
      }
      if (i==0) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	T p2 = yz_interpolate(i+2);
	T p3 = yz_interpolate(i+3);
	return CINT0(p0,p1,p2,p3,ifrac);
      }
      T p0 = yz_interpolate(i);
      return p0;
    };
    return xyz_interpolate();
  }
  
  inline T deriv_z(double x, double y, double z ) const // tricubic interpolation
  {
    if (x<0.0) return T{0.0};
    if (y<0.0) return T{0.0};
    if (z<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    if (y>double(ny)-1.0) return T{0.0};
    if (z>double(nz)-1.0) return T{0.0};
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k>0 && k+2<nz) {
	T p0 = elem(i,j,k-1);
	T p1 = elem(i,j,k);
	T p2 = elem(i,j,k+1);
	T p3 = elem(i,j,k+2);
	return CINT1_deriv(p0,p1,p2,p3,kfrac);
      }
      if (k>1 && k+1<nz) {
	T p0 = elem(i,j,k-2);
	T p1 = elem(i,j,k-1);
	T p2 = elem(i,j,k);
	T p3 = elem(i,j,k+1);
	return CINT2_deriv(p0,p1,p2,p3,kfrac);
      }
      if (k==0) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	T p2 = elem(i,j,k+2);
	T p3 = elem(i,j,k+3);
	return CINT0_deriv(p0,p1,p2,p3,kfrac);
      }
      T p0 = elem(i,j,k-3);
      T p1 = elem(i,j,k-2);
      T p2 = elem(i,j,k-1);
      T p3 = elem(i,j,k);
      return CINT2_deriv(p0,p1,p2,p3,1.0); // backward differentiation
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j>0 && j+2<ny) {
	T p0 = z_interpolate(i,j-1);
	T p1 = z_interpolate(i,j);
	T p2 = z_interpolate(i,j+1);
	T p3 = z_interpolate(i,j+2);
	return CINT1(p0,p1,p2,p3,jfrac);
      }
      if (j>1 && j+1<ny) {
	T p0 = z_interpolate(i,j-2);
	T p1 = z_interpolate(i,j-1);
	T p2 = z_interpolate(i,j);
	T p3 = z_interpolate(i,j+1);
	return CINT2(p0,p1,p2,p3,jfrac);
      }
      if (j==0) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	T p2 = z_interpolate(i,j+2);
	T p3 = z_interpolate(i,j+3);
	return CINT0(p0,p1,p2,p3,jfrac);
      }
      T p0 = z_interpolate(i,j);
      return p0;
    };
    auto xyz_interpolate = [&]() {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i>0 && i+2<nx) {
	T p0 = yz_interpolate(i-1);
	T p1 = yz_interpolate(i);
	T p2 = yz_interpolate(i+1);
	T p3 = yz_interpolate(i+2);
	return CINT1(p0,p1,p2,p3,ifrac);
      }
      if (i>1 && i+1<nx) {
	T p0 = yz_interpolate(i-2);
	T p1 = yz_interpolate(i-1);
	T p2 = yz_interpolate(i);
	T p3 = yz_interpolate(i+1);
	return CINT2(p0,p1,p2,p3,ifrac);
      }
      if (i==0) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	T p2 = yz_interpolate(i+2);
	T p3 = yz_interpolate(i+3);
	return CINT0(p0,p1,p2,p3,ifrac);
      }
      T p0 = yz_interpolate(i);
      return p0;
    };
    return xyz_interpolate();
  }

  inline T deriv2_x2(double x, double y, double z ) const // tricubic interpolation
  {
    if (x<0.0) return T{0.0};
    if (y<0.0) return T{0.0};
    if (z<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    if (y>double(ny)-1.0) return T{0.0};
    if (z>double(nz)-1.0) return T{0.0};
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k>0 && k+2<nz) {
	T p0 = elem(i,j,k-1);
	T p1 = elem(i,j,k);
	T p2 = elem(i,j,k+1);
	T p3 = elem(i,j,k+2);
	return CINT1(p0,p1,p2,p3,kfrac);
      }
      if (k>1 && k+1<nz) {
	T p0 = elem(i,j,k-2);
	T p1 = elem(i,j,k-1);
	T p2 = elem(i,j,k);
	T p3 = elem(i,j,k+1);
	return CINT2(p0,p1,p2,p3,kfrac);
      }
      if (k==0) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	T p2 = elem(i,j,k+2);
	T p3 = elem(i,j,k+3);
	return CINT0(p0,p1,p2,p3,kfrac);
      }
      T p0 = elem(i,j,k);
      return p0;
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j>0 && j+2<ny) {
	T p0 = z_interpolate(i,j-1);
	T p1 = z_interpolate(i,j);
	T p2 = z_interpolate(i,j+1);
	T p3 = z_interpolate(i,j+2);
	return CINT1(p0,p1,p2,p3,jfrac);
      }
      if (j>1 && j+1<ny) {
	T p0 = z_interpolate(i,j-2);
	T p1 = z_interpolate(i,j-1);
	T p2 = z_interpolate(i,j);
	T p3 = z_interpolate(i,j+1);
	return CINT2(p0,p1,p2,p3,jfrac);
      }
      if (j==0) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	T p2 = z_interpolate(i,j+2);
	T p3 = z_interpolate(i,j+3);
	return CINT0(p0,p1,p2,p3,jfrac);
      }
      T p0 = z_interpolate(i,j);
      return p0;
    };
    auto xyz_interpolate = [&]() {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i>0 && i+2<nx) {
	T p0 = yz_interpolate(i-1);
	T p1 = yz_interpolate(i);
	T p2 = yz_interpolate(i+1);
	T p3 = yz_interpolate(i+2);
	return CINT1_deriv2(p0,p1,p2,p3,ifrac);
      }
      if (i>1 && i+1<nx) {
	T p0 = yz_interpolate(i-2);
	T p1 = yz_interpolate(i-1);
	T p2 = yz_interpolate(i);
	T p3 = yz_interpolate(i+1);
	return CINT2_deriv2(p0,p1,p2,p3,ifrac);
      }
      if (i==0) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	T p2 = yz_interpolate(i+2);
	T p3 = yz_interpolate(i+3);
	return CINT0_deriv2(p0,p1,p2,p3,ifrac);
      }
      T p0 = yz_interpolate(i-3);
      T p1 = yz_interpolate(i-2);
      T p2 = yz_interpolate(i-1);
      T p3 = yz_interpolate(i);
      return CINT2_deriv2(p0,p1,p2,p3,1.0); // backward differentiation
    };
    return xyz_interpolate();
  }

  inline T deriv2_y2(double x, double y, double z ) const // tricubic interpolation
  {
    if (x<0.0) return T{0.0};
    if (y<0.0) return T{0.0};
    if (z<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    if (y>double(ny)-1.0) return T{0.0};
    if (z>double(nz)-1.0) return T{0.0};
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k>0 && k+2<nz) {
	T p0 = elem(i,j,k-1);
	T p1 = elem(i,j,k);
	T p2 = elem(i,j,k+1);
	T p3 = elem(i,j,k+2);
	return CINT1(p0,p1,p2,p3,kfrac);
      }
      if (k>1 && k+1<nz) {
	T p0 = elem(i,j,k-2);
	T p1 = elem(i,j,k-1);
	T p2 = elem(i,j,k);
	T p3 = elem(i,j,k+1);
	return CINT2(p0,p1,p2,p3,kfrac);
      }
      if (k==0) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	T p2 = elem(i,j,k+2);
	T p3 = elem(i,j,k+3);
	return CINT0(p0,p1,p2,p3,kfrac);
      }
      T p0 = elem(i,j,k);
      return p0;
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j>0 && j+2<ny) {
	T p0 = z_interpolate(i,j-1);
	T p1 = z_interpolate(i,j);
	T p2 = z_interpolate(i,j+1);
	T p3 = z_interpolate(i,j+2);
	return CINT1_deriv2(p0,p1,p2,p3,jfrac);
      }
      if (j>1 && j+1<ny) {
	T p0 = z_interpolate(i,j-2);
	T p1 = z_interpolate(i,j-1);
	T p2 = z_interpolate(i,j);
	T p3 = z_interpolate(i,j+1);
	return CINT2_deriv2(p0,p1,p2,p3,jfrac);
      }
      if (j==0) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	T p2 = z_interpolate(i,j+2);
	T p3 = z_interpolate(i,j+3);
	return CINT0_deriv2(p0,p1,p2,p3,jfrac);
      }
      T p0 = z_interpolate(i,j-3);
      T p1 = z_interpolate(i,j-2);
      T p2 = z_interpolate(i,j-1);
      T p3 = z_interpolate(i,j);
      return CINT2_deriv2(p0,p1,p2,p3,1.0); // backward differentiation
    };
    auto xyz_interpolate = [&]() {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i>0 && i+2<nx) {
	T p0 = yz_interpolate(i-1);
	T p1 = yz_interpolate(i);
	T p2 = yz_interpolate(i+1);
	T p3 = yz_interpolate(i+2);
	return CINT1(p0,p1,p2,p3,ifrac);
      }
      if (i>1 && i+1<nx) {
	T p0 = yz_interpolate(i-2);
	T p1 = yz_interpolate(i-1);
	T p2 = yz_interpolate(i);
	T p3 = yz_interpolate(i+1);
	return CINT2(p0,p1,p2,p3,ifrac);
      }
      if (i==0) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	T p2 = yz_interpolate(i+2);
	T p3 = yz_interpolate(i+3);
	return CINT0(p0,p1,p2,p3,ifrac);
      }
      T p0 = yz_interpolate(i);
      return p0;
    };
    return xyz_interpolate();
  }
  
  inline T deriv2_z2(double x, double y, double z ) const // tricubic interpolation
  {
    if (x<0.0) return T{0.0};
    if (y<0.0) return T{0.0};
    if (z<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    if (y>double(ny)-1.0) return T{0.0};
    if (z>double(nz)-1.0) return T{0.0};
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k>0 && k+2<nz) {
	T p0 = elem(i,j,k-1);
	T p1 = elem(i,j,k);
	T p2 = elem(i,j,k+1);
	T p3 = elem(i,j,k+2);
	return CINT1_deriv2(p0,p1,p2,p3,kfrac);
      }
      if (k>1 && k+1<nz) {
	T p0 = elem(i,j,k-2);
	T p1 = elem(i,j,k-1);
	T p2 = elem(i,j,k);
	T p3 = elem(i,j,k+1);
	return CINT2_deriv2(p0,p1,p2,p3,kfrac);
      }
      if (k==0) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	T p2 = elem(i,j,k+2);
	T p3 = elem(i,j,k+3);
	return CINT0_deriv2(p0,p1,p2,p3,kfrac);
      }
      T p0 = elem(i,j,k-3);
      T p1 = elem(i,j,k-2);
      T p2 = elem(i,j,k-1);
      T p3 = elem(i,j,k);
      return CINT2_deriv2(p0,p1,p2,p3,1.0); // backward differentiation
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j>0 && j+2<ny) {
	T p0 = z_interpolate(i,j-1);
	T p1 = z_interpolate(i,j);
	T p2 = z_interpolate(i,j+1);
	T p3 = z_interpolate(i,j+2);
	return CINT1(p0,p1,p2,p3,jfrac);
      }
      if (j>1 && j+1<ny) {
	T p0 = z_interpolate(i,j-2);
	T p1 = z_interpolate(i,j-1);
	T p2 = z_interpolate(i,j);
	T p3 = z_interpolate(i,j+1);
	return CINT2(p0,p1,p2,p3,jfrac);
      }
      if (j==0) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	T p2 = z_interpolate(i,j+2);
	T p3 = z_interpolate(i,j+3);
	return CINT0(p0,p1,p2,p3,jfrac);
      }
      T p0 = z_interpolate(i,j);
      return p0;
    };
    auto xyz_interpolate = [&]() {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i>0 && i+2<nx) {
	T p0 = yz_interpolate(i-1);
	T p1 = yz_interpolate(i);
	T p2 = yz_interpolate(i+1);
	T p3 = yz_interpolate(i+2);
	return CINT1(p0,p1,p2,p3,ifrac);
      }
      if (i>1 && i+1<nx) {
	T p0 = yz_interpolate(i-2);
	T p1 = yz_interpolate(i-1);
	T p2 = yz_interpolate(i);
	T p3 = yz_interpolate(i+1);
	return CINT2(p0,p1,p2,p3,ifrac);
      }
      if (i==0) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	T p2 = yz_interpolate(i+2);
	T p3 = yz_interpolate(i+3);
	return CINT0(p0,p1,p2,p3,ifrac);
      }
      T p0 = yz_interpolate(i);
      return p0;
    };
    return xyz_interpolate();
  }

  inline T deriv2_xy(double x, double y, double z ) const // tricubic interpolation
  {
    if (x<0.0) return T{0.0};
    if (y<0.0) return T{0.0};
    if (z<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    if (y>double(ny)-1.0) return T{0.0};
    if (z>double(nz)-1.0) return T{0.0};
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k>0 && k+2<nz) {
	T p0 = elem(i,j,k-1);
	T p1 = elem(i,j,k);
	T p2 = elem(i,j,k+1);
	T p3 = elem(i,j,k+2);
	return CINT1(p0,p1,p2,p3,kfrac);
      }
      if (k>1 && k+1<nz) {
	T p0 = elem(i,j,k-2);
	T p1 = elem(i,j,k-1);
	T p2 = elem(i,j,k);
	T p3 = elem(i,j,k+1);
	return CINT2(p0,p1,p2,p3,kfrac);
      }
      if (k==0) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	T p2 = elem(i,j,k+2);
	T p3 = elem(i,j,k+3);
	return CINT0(p0,p1,p2,p3,kfrac);
      }
      T p0 = elem(i,j,k);
      return p0;
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j>0 && j+2<ny) {
	T p0 = z_interpolate(i,j-1);
	T p1 = z_interpolate(i,j);
	T p2 = z_interpolate(i,j+1);
	T p3 = z_interpolate(i,j+2);
	return CINT1_deriv(p0,p1,p2,p3,jfrac);
      }
      if (j>1 && j+1<ny) {
	T p0 = z_interpolate(i,j-2);
	T p1 = z_interpolate(i,j-1);
	T p2 = z_interpolate(i,j);
	T p3 = z_interpolate(i,j+1);
	return CINT2_deriv(p0,p1,p2,p3,jfrac);
      }
      if (j==0) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	T p2 = z_interpolate(i,j+2);
	T p3 = z_interpolate(i,j+3);
	return CINT0_deriv(p0,p1,p2,p3,jfrac);
      }
      T p0 = z_interpolate(i,j-3);
      T p1 = z_interpolate(i,j-2);
      T p2 = z_interpolate(i,j-1);
      T p3 = z_interpolate(i,j);
      return CINT2_deriv(p0,p1,p2,p3,1.0); // backward differentiation
    };
    auto xyz_interpolate = [&]() {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i>0 && i+2<nx) {
	T p0 = yz_interpolate(i-1);
	T p1 = yz_interpolate(i);
	T p2 = yz_interpolate(i+1);
	T p3 = yz_interpolate(i+2);
	return CINT1_deriv(p0,p1,p2,p3,ifrac);
      }
      if (i>1 && i+1<nx) {
	T p0 = yz_interpolate(i-2);
	T p1 = yz_interpolate(i-1);
	T p2 = yz_interpolate(i);
	T p3 = yz_interpolate(i+1);
	return CINT2_deriv(p0,p1,p2,p3,ifrac);
      }
      if (i==0) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	T p2 = yz_interpolate(i+2);
	T p3 = yz_interpolate(i+3);
	return CINT0_deriv(p0,p1,p2,p3,ifrac);
      }
      T p0 = yz_interpolate(i-3);
      T p1 = yz_interpolate(i-2);
      T p2 = yz_interpolate(i-1);
      T p3 = yz_interpolate(i);
      return CINT2_deriv(p0,p1,p2,p3,1.0); // backward differentiation
    };
    return xyz_interpolate();
  }

  inline T deriv2_yz(double x, double y, double z ) const // tricubic interpolation
  {
    if (x<0.0) return T{0.0};
    if (y<0.0) return T{0.0};
    if (z<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    if (y>double(ny)-1.0) return T{0.0};
    if (z>double(nz)-1.0) return T{0.0};
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k>0 && k+2<nz) {
	T p0 = elem(i,j,k-1);
	T p1 = elem(i,j,k);
	T p2 = elem(i,j,k+1);
	T p3 = elem(i,j,k+2);
	return CINT1_deriv(p0,p1,p2,p3,kfrac);
      }
      if (k>1 && k+1<nz) {
	T p0 = elem(i,j,k-2);
	T p1 = elem(i,j,k-1);
	T p2 = elem(i,j,k);
	T p3 = elem(i,j,k+1);
	return CINT2_deriv(p0,p1,p2,p3,kfrac);
      }
      if (k==0) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	T p2 = elem(i,j,k+2);
	T p3 = elem(i,j,k+3);
	return CINT0_deriv(p0,p1,p2,p3,kfrac);
      }
      T p0 = elem(i,j,k-3);
      T p1 = elem(i,j,k-2);
      T p2 = elem(i,j,k-1);
      T p3 = elem(i,j,k);
      return CINT2_deriv(p0,p1,p2,p3,1.0); // backward differentiation
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j>0 && j+2<ny) {
	T p0 = z_interpolate(i,j-1);
	T p1 = z_interpolate(i,j);
	T p2 = z_interpolate(i,j+1);
	T p3 = z_interpolate(i,j+2);
	return CINT1_deriv(p0,p1,p2,p3,jfrac);
      }
      if (j>1 && j+1<ny) {
	T p0 = z_interpolate(i,j-2);
	T p1 = z_interpolate(i,j-1);
	T p2 = z_interpolate(i,j);
	T p3 = z_interpolate(i,j+1);
	return CINT2_deriv(p0,p1,p2,p3,jfrac);
      }
      if (j==0) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	T p2 = z_interpolate(i,j+2);
	T p3 = z_interpolate(i,j+3);
	return CINT0_deriv(p0,p1,p2,p3,jfrac);
      }
      T p0 = z_interpolate(i,j-3);
      T p1 = z_interpolate(i,j-2);
      T p2 = z_interpolate(i,j-1);
      T p3 = z_interpolate(i,j);
      return CINT2_deriv(p0,p1,p2,p3,1.0); // backward differentiation
    };
    auto xyz_interpolate = [&]() {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i>0 && i+2<nx) {
	T p0 = yz_interpolate(i-1);
	T p1 = yz_interpolate(i);
	T p2 = yz_interpolate(i+1);
	T p3 = yz_interpolate(i+2);
	return CINT1(p0,p1,p2,p3,ifrac);
      }
      if (i>1 && i+1<nx) {
	T p0 = yz_interpolate(i-2);
	T p1 = yz_interpolate(i-1);
	T p2 = yz_interpolate(i);
	T p3 = yz_interpolate(i+1);
	return CINT2(p0,p1,p2,p3,ifrac);
      }
      if (i==0) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	T p2 = yz_interpolate(i+2);
	T p3 = yz_interpolate(i+3);
	return CINT0(p0,p1,p2,p3,ifrac);
      }
      T p0 = yz_interpolate(i);
      return p0;
    };
    return xyz_interpolate();
  }
  
  inline T deriv2_xz(double x, double y, double z ) const // tricubic interpolation
  {
    if (x<0.0) return T{0.0};
    if (y<0.0) return T{0.0};
    if (z<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    if (y>double(ny)-1.0) return T{0.0};
    if (z>double(nz)-1.0) return T{0.0};
    auto z_interpolate = [&] (size_t i, size_t j ) {
      double kinteger, kfrac = modf(z, &kinteger);
      const size_t k = size_t(kinteger);
      if (k>0 && k+2<nz) {
	T p0 = elem(i,j,k-1);
	T p1 = elem(i,j,k);
	T p2 = elem(i,j,k+1);
	T p3 = elem(i,j,k+2);
	return CINT1_deriv(p0,p1,p2,p3,kfrac);
      }
      if (k>1 && k+1<nz) {
	T p0 = elem(i,j,k-2);
	T p1 = elem(i,j,k-1);
	T p2 = elem(i,j,k);
	T p3 = elem(i,j,k+1);
	return CINT2_deriv(p0,p1,p2,p3,kfrac);
      }
      if (k==0) {
	T p0 = elem(i,j,k);
	T p1 = elem(i,j,k+1);
	T p2 = elem(i,j,k+2);
	T p3 = elem(i,j,k+3);
	return CINT0_deriv(p0,p1,p2,p3,kfrac);
      }
      T p0 = elem(i,j,k-3);
      T p1 = elem(i,j,k-2);
      T p2 = elem(i,j,k-1);
      T p3 = elem(i,j,k);
      return CINT2_deriv(p0,p1,p2,p3,1.0); // backward differentiation
    };
    auto yz_interpolate = [&] (size_t i ) {
      double jinteger, jfrac = modf(y, &jinteger);
      const size_t j = size_t(jinteger);
      if (j>0 && j+2<ny) {
	T p0 = z_interpolate(i,j-1);
	T p1 = z_interpolate(i,j);
	T p2 = z_interpolate(i,j+1);
	T p3 = z_interpolate(i,j+2);
	return CINT1(p0,p1,p2,p3,jfrac);
      }
      if (j>1 && j+1<ny) {
	T p0 = z_interpolate(i,j-2);
	T p1 = z_interpolate(i,j-1);
	T p2 = z_interpolate(i,j);
	T p3 = z_interpolate(i,j+1);
	return CINT2(p0,p1,p2,p3,jfrac);
      }
      if (j==0) {
	T p0 = z_interpolate(i,j);
	T p1 = z_interpolate(i,j+1);
	T p2 = z_interpolate(i,j+2);
	T p3 = z_interpolate(i,j+3);
	return CINT0(p0,p1,p2,p3,jfrac);
      }
      T p0 = z_interpolate(i,j);
      return p0;
    };
    auto xyz_interpolate = [&]() {
      double iinteger, ifrac = modf(x, &iinteger);
      const size_t i = size_t(iinteger);
      if (i>0 && i+2<nx) {
	T p0 = yz_interpolate(i-1);
	T p1 = yz_interpolate(i);
	T p2 = yz_interpolate(i+1);
	T p3 = yz_interpolate(i+2);
	return CINT1_deriv(p0,p1,p2,p3,ifrac);
      }
      if (i>1 && i+1<nx) {
	T p0 = yz_interpolate(i-2);
	T p1 = yz_interpolate(i-1);
	T p2 = yz_interpolate(i);
	T p3 = yz_interpolate(i+1);
	return CINT2_deriv(p0,p1,p2,p3,ifrac);
      }
      if (i==0) {
	T p0 = yz_interpolate(i);
	T p1 = yz_interpolate(i+1);
	T p2 = yz_interpolate(i+2);
	T p3 = yz_interpolate(i+3);
	return CINT0_deriv(p0,p1,p2,p3,ifrac);
      }
      T p0 = yz_interpolate(i-3);
      T p1 = yz_interpolate(i-2);
      T p2 = yz_interpolate(i-1);
      T p3 = yz_interpolate(i);
      return CINT2_deriv(p0,p1,p2,p3,1.0); // backward differentiation
    };
    return xyz_interpolate();
  }

};

//

typedef TMesh3d_CINT<double> Mesh3d_CINT;
typedef TMesh3d_CINT<fftwComplex> ComplexMesh3d_CINT;

#endif /* mesh3d_cint_hh */
