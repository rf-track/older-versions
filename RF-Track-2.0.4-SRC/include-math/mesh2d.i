/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* mesh2d.i */

#if defined(SWIGOCTAVE)

// Octave array -> C++ Mesh2d
%typemap(in) const ComplexMesh2d & {
  const ComplexNDArray &array2d = $input.complex_array_value();
  dim_vector dims = array2d.dims();
  size_t W = dims.elem(0);
  size_t H = dims.elem(1);
  $1 = new ComplexMesh2d(W, H);
  for (size_t i=0; i<W; i++) {
    for (size_t j=0; j<H; j++) {
      (*$1).elem(i,j) = array2d(i,j);
    }
  }
 }

%typemap(freearg) const ComplexMesh2d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const ComplexMesh2d &  {
  octave_value obj = $input;
  $1 = false;
  if (obj.is_complex_matrix() || obj.is_real_matrix()) {
    const ComplexNDArray array2d = obj.complex_array_value();
    if (array2d.dims().length() == 2) {
      $1 = true;
    }
  }
 }

// C++ Mesh2d -> Octave array
%typemap(out) const ComplexMesh2d & {
  dim_vector dv2;
  dv2.resize(2);
  dv2.elem(0)=(*$1).size1();
  dv2.elem(1)=(*$1).size2();
  ComplexNDArray ret(dv2);
  for (int i=0; i<dv2.elem(0); i++) {
    for (int j=0; j<dv2.elem(1); j++) {
      ret(i,j) = (*$1).elem(i,j);
    }
  }
  $result = octave_value(ret);
}

// T_ijk as an output argument
/*
%typemap(in, numinputs=0) ComplexMesh2d & (ComplexMesh2d temp ) {
  $1 = &temp;
}

%typemap(argout) ComplexMesh2d & {
  dim_vector dv2;
  dv2.resize(2);
  dv2.elem(0)=(*$1).size1();
  dv2.elem(1)=(*$1).size2();
  ComplexNDArray T(dv2);
  for (int i=0; i<dv2.elem(0); i++) {
    for (int j=0; j<dv2.elem(1); j++) {
      T(i,j) = (*$1).elem(i,j);
    }
  }
  $result->append(T);
}
*/

// Octave array -> C++ Mesh2d
%typemap(in) const Mesh2d & {
  const NDArray &array2d = $input.array_value();
  dim_vector dims = array2d.dims();
  size_t W = dims.elem(0);
  size_t H = dims.elem(1);
  $1 = new Mesh2d(W, H);
  for (size_t i=0; i<W; i++) {
    for (size_t j=0; j<H; j++) {
      (*$1).elem(i,j) = array2d(i,j);
    }
  }
 }

%typemap(freearg) const Mesh2d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const Mesh2d &  {
  octave_value obj = $input;
  $1 = false;
  if (obj.is_real_matrix()) {
    const NDArray array2d = obj.array_value();
    if (array2d.dims().length() == 2) {
      $1 = true;
    }
  }
}

// C++ Mesh2d -> Octave array
%typemap(out) Mesh2d {
  dim_vector dv2;
  dv2.resize(2);
  dv2.elem(0)=($1).size1();
  dv2.elem(1)=($1).size2();
  NDArray ret(dv2);
  for (size_t i=0; i<($1).size1(); i++) {
    for (size_t j=0; j<($1).size2(); j++) {
      ret(i,j) = ($1).elem(i,j);
    }
  }
  $result = octave_value(ret);
}

#endif

#if defined(SWIGPYTHON)
%{
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#define is_array(a) ((a) && PyArray_Check((PyArrayObject *)a))
%}

%init %{
  import_array();
  %}

// Python array -> C++ Mesh2d
%typemap(in) const ComplexMesh2d & {
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_CDOUBLE, 2, 2);
    if (PyArray_NDIM(array) == 2) {
      auto W = PyArray_DIMS(array)[0];
      auto H = PyArray_DIMS(array)[1];
      npy_intp strides[2];
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = PyArray_STRIDES(array)[1];
      char *data = PyArray_BYTES(array);
      $1 = new ComplexMesh2d(W, H);
      for (int i=0; i<W; i++) {
	for (int j=0; j<H; j++) {
	  const auto &src = (*(npy_cdouble *)(data + i*strides[0] + j*strides[1]));
	  auto &dest = (*$1).elem(i,j);
	  dest.real = src.real;
	  dest.imag = src.imag;
	}
      }
    }
    Py_DECREF(array);
  }
 }

%typemap(freearg) const ComplexMesh2d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const ComplexMesh2d &  {
  $1 = false;
  if (is_array($input) && PyComplex_Check($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_CDOUBLE, 2, 2);
    if (PyArray_NDIM(array) == 2) {
      $1 = true;
    }
  }
 }

// C++ Mesh2d -> Python array
%typemap(out) const ComplexMesh2d & {
  npy_intp dimensions[2] = { (npy_intp)($1->size1()), (npy_intp)($1->size2())) };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_CDOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (int i=0; i<dimensions[0]; i++)
    for (int j=0; j<dimensions[1]; j++) {
      const auto &src = (*$1).elem(i,j);
      auto &dest = *(npy_cdouble *)(data + i*strides[0] + j*strides[1]);
      dest.real = src.real;
      dest.imag = src.imag;
    }
  $result = PyArray_Return(res);
 }

// Python array -> C++ Mesh2d
%typemap(in) const Mesh2d & {
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 2, 2);
    if (PyArray_NDIM(array) == 2) {
      auto W = PyArray_DIMS(array)[0];
      auto H = PyArray_DIMS(array)[1];
      npy_intp strides[2];
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = PyArray_STRIDES(array)[1];
      char *data = PyArray_BYTES(array);
      $1 = new Mesh2d(W, H);
      for (int i=0; i<W; i++) {
	for (int j=0; j<H; j++) {
	    const auto &src = (*(npy_double *)(data + i*strides[0] + j*strides[1]));
	    auto &dest = (*$1).elem(i,j);
	    dest = src;
	}
      }
    }
    Py_DECREF(array);
  }
 }

%typemap(freearg) const Mesh2d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const Mesh2d &  {
  $1 = false;
  if (is_array($input) && PyFloat_Check($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 2, 2);
    if (PyArray_NDIM(array) == 2) {
      $1 = true;
    }
  }
 }

// C++ Mesh2d -> Python array
%typemap(out) Mesh2d {
  npy_intp dimensions[2] = { (npy_intp)($1.size1()), (npy_intp)($1.size2()) };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(2, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (int i=0; i<dimensions[0]; i++)
    for (int j=0; j<dimensions[1]; j++) {
      const auto &src = ($1).elem(i,j);
      auto &dest = *(npy_double *)(data + i*strides[0] + j*strides[1]);
      dest = src;
    }
  $result = PyArray_Return(res);
 }

#endif
