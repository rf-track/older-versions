/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef mesh2d_lint_hh
#define mesh2d_lint_hh

#include "mesh2d.hh"

template <class T, class Alloc = std::allocator<T>>
class TMesh2d_LINT : public TMesh2d<T,Alloc> {
protected:
  using TMesh2d<T,Alloc>::nx;
  using TMesh2d<T,Alloc>::ny;
public:
  using TMesh2d<T,Alloc>::TMesh2d;
  using TMesh2d<T,Alloc>::elem;

  template <class Alloc1> TMesh2d_LINT(const TMesh2d<T,Alloc1> &m ) : TMesh2d<T,Alloc>(m) {}

  inline T operator()(double x, double y ) const // bilinear interpolation
  {
    if (x<0.0) return T{0.0};
    if (y<0.0) return T{0.0};
    if (x>double(nx)-1.0) return T{0.0};
    if (y>double(ny)-1.0) return T{0.0};
    double iinteger, jinteger;
    double ifrac = modf(x, &iinteger);
    double jfrac = modf(y, &jinteger);
    size_t i = size_t(iinteger);
    size_t j = size_t(jinteger);
    auto i0j0 = elem(i,j);
    if (i+1<nx) { // X OK
      auto i1j0 = elem(i+1,j);
      auto ifj0 = LINT(i0j0, i1j0, ifrac);
      if (j+1<ny) { // X & Y OK
        auto i0j1 = elem(i,j+1);
        auto i1j1 = elem(i+1,j+1);
        auto ifj1 = LINT(i0j1, i1j1, ifrac);
        auto ifjf = LINT(ifj0, ifj1, jfrac);
	return ifjf;
      } else {
	return ifj0;
      }
    } else {
      if (j+1<ny) {
        auto i0j1 = elem(i,j+1);
        auto i0jf = LINT(i0j0, i0j1, jfrac);
	return i0jf;
      } else {
	return i0j0;
      }
    }
  }

};

#endif /* mesh2d_lint_hh */
