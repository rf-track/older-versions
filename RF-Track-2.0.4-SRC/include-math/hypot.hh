/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef hypot_hh
#define hypot_hh

#include <cmath>
#include <algorithm>

namespace {
  using std::hypot;
  inline double hypot(double x, double y, double z )
  {
    x = std::abs(x);
    y = std::abs(y);
    z = std::abs(z);
    const double a = std::max(x, std::max(y, z));
    if (a==0.0) return 0.0;
    x/=a; y/=a; z/=a;
    return a * std::sqrt(x*x + y*y + z*z);
  }
  inline double hypot(double x, double y, double z, double w )
  {
    x = std::abs(x);
    y = std::abs(y);
    z = std::abs(z);
    w = std::abs(w);
    const double a = std::max(x, std::max(y, std::max(z, w)));
    if (a==0.0) return 0.0;
    x/=a; y/=a; z/=a; w/=a;
    return a * std::sqrt(x*x + y*y + z*z + w*w);
  }
}

#endif /* hypot_hh */
