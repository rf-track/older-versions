/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

/* mesh3d.i */

#if defined(SWIGOCTAVE)

// Octave array -> C++ Mesh3d
%typemap(in) const ComplexMesh3d & {
  const ComplexNDArray &array3d = $input.complex_array_value();
  dim_vector dims = array3d.dims();
  size_t W = dims.elem(0);
  size_t H = dims.elem(1);
  size_t D = dims.elem(2);
  $1 = new ComplexMesh3d(W, H, D);
  for (size_t i=0; i<W; i++) {
    for (size_t j=0; j<H; j++) {
      for (size_t k=0; k<D; k++) {
	(*$1).elem(i,j,k) = array3d(i,j,k);
      }
    }
  }
 }

%typemap(freearg) const ComplexMesh3d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const ComplexMesh3d &  {
  octave_value obj = $input;
  $1 = false;
  if (obj.is_complex_matrix() || obj.is_real_matrix()) {
    const ComplexNDArray array3d = obj.complex_array_value();
    if (array3d.dims().length() == 3) {
      $1 = true;
    }
  }
}

// C++ Mesh3d -> Octave array
%typemap(out) const ComplexMesh3d & {
  dim_vector dv3;
  dv3.resize(3);
  dv3.elem(0)=(*$1).size1();
  dv3.elem(1)=(*$1).size2();
  dv3.elem(2)=(*$1).size3();
  ComplexNDArray ret(dv3);
  for (int i=0; i<dv3.elem(0); i++) {
    for (int j=0; j<dv3.elem(1); j++) {
      for (int k=0; k<dv3.elem(2); k++) {
        ret(i,j,k) = (*$1).elem(i,j,k);
      }
    }
  }
  $result = octave_value(ret);
}

// T_ijk as an output argument
/*
%typemap(in, numinputs=0) ComplexMesh3d & (ComplexMesh3d temp ) {
  $1 = &temp;
}

%typemap(argout) ComplexMesh3d & {
  dim_vector dv3;
  dv3.resize(3);
  dv3.elem(0)=(*$1).size1();
  dv3.elem(1)=(*$1).size2();
  dv3.elem(2)=(*$1).size3();
  ComplexNDArray T(dv3);
  for (int i=0; i<dv3.elem(0); i++) {
    for (int j=0; j<dv3.elem(1); j++) {
      for (int k=0; k<dv3.elem(2); k++) {
        T(i,j,k) = (*$1).elem(i,j,k);
      }
    }
  }
  $result->append(T);
}
*/

// Octave array -> C++ Mesh3d
%typemap(in) const Mesh3d & {
  const NDArray &array3d = $input.array_value();
  dim_vector dims = array3d.dims();
  size_t W = dims.elem(0);
  size_t H = dims.elem(1);
  size_t D = dims.elem(2);
  $1 = new Mesh3d(W, H, D);
  for (size_t i=0; i<W; i++) {
    for (size_t j=0; j<H; j++) {
      for (size_t k=0; k<D; k++) {
	(*$1).elem(i,j,k) = array3d(i,j,k);
      }
    }
  }
 }

%typemap(freearg) const Mesh3d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const Mesh3d &  {
  octave_value obj = $input;
  $1 = false;
  if (obj.is_real_matrix()) {
    const NDArray array3d = obj.array_value();
    if (array3d.dims().length() == 3) {
      $1 = true;
    }
  }
}

// C++ Mesh3d -> Octave array
%typemap(out) Mesh3d {
  dim_vector dv3;
  dv3.resize(3);
  dv3.elem(0)=($1).size1();
  dv3.elem(1)=($1).size2();
  dv3.elem(2)=($1).size3();
  NDArray ret(dv3);
  for (size_t i=0; i<($1).size1(); i++) {
    for (size_t j=0; j<($1).size2(); j++) {
      for (size_t k=0; k<($1).size3(); k++) {
        ret(i,j,k) = ($1).elem(i,j,k);
      }
    }
  }
  $result = octave_value(ret);
}

#endif

#if defined(SWIGPYTHON)
%{
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <Python.h>
#include <numpy/arrayobject.h>
#define is_array(a) ((a) && PyArray_Check((PyArrayObject *)a))
%}

%init %{
  import_array();
  %}

// Python array -> C++ Mesh3d
%typemap(in) const ComplexMesh3d & {
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_CDOUBLE, 3, 3);
    if (PyArray_NDIM(array) == 3) {
      auto W = PyArray_DIMS(array)[0];
      auto H = PyArray_DIMS(array)[1];
      auto D = PyArray_DIMS(array)[2];
      npy_intp strides[3];
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = PyArray_STRIDES(array)[1];
      strides[2] = PyArray_STRIDES(array)[2];
      char *data = PyArray_BYTES(array);
      $1 = new ComplexMesh3d(W, H, D);
      for (int i=0; i<W; i++) {
	for (int j=0; j<H; j++) {
	  for (int k=0; k<D; k++) {
	    const auto &src = (*(npy_cdouble *)(data + i*strides[0] + j*strides[1] + k*strides[2]));
	    auto &dest = (*$1).elem(i,j,k);
	    dest.real = src.real;
	    dest.imag = src.imag;
	  }
	}
      }
    }
    Py_DECREF(array);
  }
 }

%typemap(freearg) const ComplexMesh3d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const ComplexMesh3d &  {
  $1 = false;
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_CDOUBLE, 3, 3);
    if (PyArray_NDIM(array) == 3) {
      $1 = true;
    }
  }
 }

// C++ Mesh3d -> Python array
%typemap(out) const ComplexMesh3d & {
  npy_intp dimensions[3] = { (npy_intp)($1->size1()), (npy_intp)($1->size2()), (npy_intp)($1->size3()) };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(3, dimensions, NPY_CDOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (int i=0; i<dimensions[0]; i++)
    for (int j=0; j<dimensions[1]; j++)
      for (int k=0; k<dimensions[2]; k++) {
	const auto &src = (*$1).elem(i,j,k);
	auto &dest = *(npy_cdouble *)(data + i*strides[0] + j*strides[1] + k*strides[2]);
	dest.real = src.real;
	dest.imag = src.imag;
      }
  $result = PyArray_Return(res);
 }

// Python array -> C++ Mesh3d
%typemap(in) const Mesh3d & {
  if (is_array($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 3, 3);
    if (PyArray_NDIM(array) == 3) {
      auto W = PyArray_DIMS(array)[0];
      auto H = PyArray_DIMS(array)[1];
      auto D = PyArray_DIMS(array)[2];
      npy_intp strides[3];
      strides[0] = PyArray_STRIDES(array)[0];
      strides[1] = PyArray_STRIDES(array)[1];
      strides[2] = PyArray_STRIDES(array)[2];
      char *data = PyArray_BYTES(array);
      $1 = new Mesh3d(W, H, D);
      for (int i=0; i<W; i++) {
	for (int j=0; j<H; j++) {
	  for (int k=0; k<D; k++) {
	    const auto &src = (*(npy_double *)(data + i*strides[0] + j*strides[1] + k*strides[2]));
	    auto &dest = (*$1).elem(i,j,k);
	    dest = src;
	  }
	}
      }
    }
    Py_DECREF(array);
  }
 }

%typemap(freearg) const Mesh3d & {
  if ($1) delete $1;
 }

%typemap(typecheck) const Mesh3d &  {
  $1 = false;
  if (is_array($input) && PyFloat_Check($input)) {
    PyArrayObject *array = (PyArrayObject *)PyArray_ContiguousFromObject($input, NPY_DOUBLE, 3, 3);
    if (PyArray_NDIM(array) == 3) {
      $1 = true;
    }
  }
 }

// C++ Mesh3d -> Python array
%typemap(out) Mesh3d {
  npy_intp dimensions[3] = { (npy_intp)($1.size1()), (npy_intp)($1.size2()), (npy_intp)($1.size3()) };
  PyArrayObject *res = (PyArrayObject *)PyArray_SimpleNew(3, dimensions, NPY_DOUBLE);
  npy_intp *strides = PyArray_STRIDES(res);
  char *data = PyArray_BYTES(res);
  for (int i=0; i<dimensions[0]; i++)
    for (int j=0; j<dimensions[1]; j++)
      for (int k=0; k<dimensions[2]; k++) {
	const auto &src = ($1).elem(i,j,k);
	auto &dest = *(npy_double *)(data + i*strides[0] + j*strides[1] + k*strides[2]);
	dest = src;
      }
  $result = PyArray_Return(res);
 }

#endif
