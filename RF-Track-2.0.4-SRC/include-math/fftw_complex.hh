/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef fftw_complex_hh
#define fftw_complex_hh

#include <iostream>
#include <complex>
#include "static_vector.hh"

struct fftwComplex {
  double real;
  double imag;
  fftwComplex() = default;
  fftwComplex(double re, double im = 0.0) : real(re), imag(im) {}
  fftwComplex(const std::complex<double> &c ) : real(c.real()), imag(c.imag()) {}
  inline operator std::complex<double> () const { return std::complex<double>(real, imag); }
  inline const fftwComplex &operator *= (const fftwComplex &b ) { double real_ = real * b.real - imag * b.imag; imag = imag * b.real + real * b.imag; real = real_; return *this; }
  inline const fftwComplex &operator *= (const double &b ) { real *= b; imag *= b; return *this; }
  inline const fftwComplex &operator /= (const double &b ) { real /= b; imag /= b; return *this; }
  inline const fftwComplex &operator += (const fftwComplex &a ) { real += a.real; imag += a.imag; return *this; }
  inline const fftwComplex &operator -= (const fftwComplex &a ) { real -= a.real; imag -= a.imag; return *this; }
  inline fftwComplex operator - () const { return fftwComplex(-real, -imag); }
  inline fftwComplex conj() const { return fftwComplex(real,-imag); }
  friend double abs(const fftwComplex &a ) { return hypot(a.real, a.imag); }
  friend fftwComplex conj(const fftwComplex &a ) { return fftwComplex(a.real,-a.imag); }
  friend const double &real(const fftwComplex &a ) { return a.real; }
  friend const double &imag(const fftwComplex &a ) { return a.imag; }
  friend inline fftwComplex operator + (const fftwComplex &a, const fftwComplex &b ) { return fftwComplex(a.real+b.real, a.imag+b.imag); }
  friend inline fftwComplex operator - (const fftwComplex &a, const fftwComplex &b ) { return fftwComplex(a.real-b.real, a.imag-b.imag); }
  friend inline fftwComplex operator * (const fftwComplex &a, const fftwComplex &b ) { return fftwComplex(a.real*b.real-a.imag*b.imag,
													  a.imag*b.real+a.real*b.imag); }
  friend inline fftwComplex operator * (const fftwComplex &a, const double &b ) { return fftwComplex(a.real*b, a.imag*b); }
  friend inline fftwComplex operator * (const double &b, const fftwComplex &a ) { return fftwComplex(a.real*b, a.imag*b); }
  friend inline fftwComplex operator / (const fftwComplex &a, const double &b ) { return fftwComplex(a.real/b, a.imag/b); }
  friend inline fftwComplex operator / (const fftwComplex &a, const fftwComplex &b ) { return fftwComplex(a.real*b.real+a.imag*b.imag, a.imag*b.real-a.real*b.imag)/(b.real*b.real+b.imag*b.imag); }
  friend std::ostream &operator << (std::ostream &stream, const fftwComplex &c ) { return stream << c.real << ' ' << c.imag; }
};

namespace {
  inline StaticVector<3,fftwComplex> cross(const StaticVector<3> &a, const StaticVector<3,fftwComplex> &b )
  {
    StaticVector<3,fftwComplex> c;
    c[0] = a[1]*b[2]-a[2]*b[1];
    c[1] = a[2]*b[0]-a[0]*b[2];
    c[2] = a[0]*b[1]-a[1]*b[0];
    return c;
  }
  inline StaticVector<3,fftwComplex> cross(const StaticVector<3,fftwComplex> &a, const StaticVector<3,fftwComplex> &b )
  {
    StaticVector<3,fftwComplex> c;
    c[0] = a[1]*b[2]-a[2]*b[1];
    c[1] = a[2]*b[0]-a[0]*b[2];
    c[2] = a[0]*b[1]-a[1]*b[0];
    return c;
  }
  inline fftwComplex dot(const StaticVector<3> &a, const StaticVector<3,fftwComplex> &b )
  {
    fftwComplex c = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
    return c;
  }
  template <size_t N>
  inline fftwComplex dot(const StaticVector<N,fftwComplex> &a, const StaticVector<N,fftwComplex> &b )
  {
    fftwComplex result = 0.0;
    for (size_t i = 0; i<N; i++) result += conj(a[i]) * b[i];
    return result;
  }
  template <size_t N>
  inline StaticVector<N,fftwComplex> operator * (const fftwComplex &a, const StaticVector<N> &b )
  {
    StaticVector<3,fftwComplex> result;
    for (size_t i = 0; i<N; i++) result[i] = a * b[i];
    return result;
  }
  template <size_t N>
  inline StaticVector<N,fftwComplex> operator * (const StaticVector<N> &b, const fftwComplex &a )
  {
    return a*b;
  }
  template <size_t N>
  inline StaticVector<N,fftwComplex> operator * (const fftwComplex &a, const StaticVector<N,fftwComplex> &b )
  {
    StaticVector<3,fftwComplex> result;
    for (size_t i = 0; i<N; i++) result[i] = a * b[i];
    return result;
  }
  template <size_t N>
  inline StaticVector<N,fftwComplex> operator * (const StaticVector<N,fftwComplex> &b, const fftwComplex &a )
  {
    return a*b;
  }
}

#endif /* fftw_complex_hh */
