/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef quaternion_hh
#define quaternion_hh

#include <iostream>
#include <cmath>

#include "static_vector.hh"

struct Quaternion {

  double s;
  
  StaticVector<3> v;
  
  Quaternion(double _s, double _x, double _y, double _z ) : s(_s), v(_x, _y, _z) {}
  Quaternion(double _s, const StaticVector<3> &_v ) : s(_s), v(_v) {}
  Quaternion() = default;
  
  double real() const { return s; }
  const StaticVector<3> &imag() const { return v; }
  
  friend double norm(const Quaternion &q ) { return sqrt(q.s * q.s + dot(q.v, q.v)); }  
  friend Quaternion conj(const Quaternion &q ) { return Quaternion(q.s, -q.v); }
  
  friend Quaternion normalize(const Quaternion &q );
  friend Quaternion inverse(const Quaternion &q ) { return conj(q) / (q.s * q.s + dot(q.v, q.v)); }
  friend Quaternion exp(const Quaternion &q ) { double qv = norm(q.v); return exp(q.s) * Quaternion(cos(qv), sin(qv) * q.v / qv); }

  const Quaternion &operator += (const Quaternion &q ) { s += q.s; v += q.v; return *this; }
  const Quaternion &operator -= (const Quaternion &q ) { s -= q.s; v -= q.v; return *this; }
  const Quaternion &operator *= (const Quaternion &q ) { return *this = *this * q; }
  const Quaternion &operator *= (const StaticVector<3> &v ) { return *this = *this * Quaternion(0.0, v); }
  const Quaternion &operator *= (double t ) { s *= t; v *= t; return *this; }
  const Quaternion &operator /= (double t ) { s /= t; v /= t; return *this; }
  
  friend Quaternion operator * (const Quaternion &a, const Quaternion &b ) { return Quaternion(a.s * b.s - dot(a.v, b.v), a.s * b.v + b.s * a.v + cross(a.v, b.v)); }
  friend Quaternion operator * (const StaticVector<3> &a, const Quaternion &b ) { return Quaternion(-dot(a, b.v), b.s * a + cross(a, b.v)); }
  friend Quaternion operator * (const Quaternion &a, const StaticVector<3> &b ) { return a * Quaternion(0.0, b); }
  
  friend Quaternion operator * (const Quaternion &a, double b )	{ return Quaternion(a.s * b, a.v * b); }
  friend Quaternion operator * (double a, const Quaternion &b )	{ return Quaternion(b.s * a, b.v * a); }
  
  friend Quaternion operator / (const Quaternion &a, const Quaternion &b ) { return a * inverse(b); }
  friend Quaternion operator / (const Quaternion &a, double b )	{ return Quaternion(a.s / b, a.v / b); }
  
  friend Quaternion operator + (const Quaternion &a, const Quaternion &b ) { return Quaternion(a.s + b.s, a.v + b.v); }
  friend Quaternion operator - (const Quaternion &a, const Quaternion &b ) { return Quaternion(a.s - b.s, a.v - b.v); }
  friend Quaternion operator - (const Quaternion &a ) { return Quaternion(-a.s, -a.v); }

  friend std::ostream &operator << (std::ostream &stream, const Quaternion &a )	{ return stream << a.s << ' ' << a.v; }

};

#endif /* quaternion_hh */
