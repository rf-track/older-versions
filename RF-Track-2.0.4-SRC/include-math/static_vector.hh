/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef vector_hh
#define vector_hh

#include <iostream>
#include <cstddef>
#include <cstdarg>
#include <limits>
#include <cmath>
#include <array>

#include "hypot.hh"
#include "stream.hh"
#include "numtools.hh"

namespace {
  inline double norm(const double &x ) { return fabs(x); }
}

template <size_t N, typename T=double> class StaticVector {
  T data[N];
public:
  StaticVector() = default;
  explicit StaticVector(T arg ) { for (size_t i=0; i<N; i++) data[i] = arg; }
  explicit StaticVector(const T *v ) { for (size_t i=0; i<N; i++) data[i] = v[i]; }
  StaticVector(const std::array<T,N> &v ) { for (size_t i=0; i<N; i++) data[i] = v[i]; }
  // Initializers for N=2 and N=3, are given explicitely, as they are the most frenquently used 
  StaticVector(T v0, T v1 )
  {
    data[0] = v0;	
    if (N >= 2) {
      data[1] = v1;
      if (N >= 3) {
	for (size_t i=2; i<N; i++)
	  data[i] = 0.0;
      }
    }
  }

  StaticVector(T v0, T v1, T v2 ) 
  {
    data[0] = v0;	
    if (N >= 2) {
      data[1] = v1;
      if (N >= 3) {
	data[2] = v2;
	if (N >= 4) {
	  for (size_t i=3; i<N; i++)
	    data[i] = 0.0;
	}
      }
    }
  }
  
  StaticVector(T v0, T v1, T v2, T v3, ... ) 
  {
    data[0] = v0;	
    if (N >= 2) {
      data[1] = v1;
      if (N >= 3) {
	data[2] = v2;
	if (N >= 4) {
	  data[3] = v3;
	  if (N >= 5) {
	    va_list ap;
	    va_start(ap, v3);
	    for (size_t i=4; i < N; i++)
	      data[i] = va_arg(ap, T);
	    va_end(ap);
	  }
	}
      }
    }
  }
  
  inline void zero() { for (size_t i=0; i<N; i++) data[i]=0; }
  
  inline const T &operator [] (size_t i ) const { return data[i]; }
  inline T &operator [] (size_t i ) { return data[i]; }
  
  friend T norm_sqr(const StaticVector &v )
  {
    return dot(v,v);
  }
  
  friend T norm(const StaticVector &v )
  {
    if (N==3) return hypot(v[0], v[1], v[2]);
    if (N==2) return hypot(v[0], v[1]);
    if (N==1) return norm(v[0]);
    T max_ = 0.0;
    for (size_t i=0; i<N; i++) {
      const T &v_ = norm(v[i]);
      if (v_>max_)
	max_ = v_;
    }
    if (max_ == 0.0)
      return T{0.0};
    T sum = 0;
    for (size_t i=0, f=0; i<N; i++) {
      const T &v_ = norm(v[i]);
      if (v_!=max_ || f==1) {
	const T &x_ = v_ / max_;
	sum += x_ * x_;
      } else {
	f = 1;
      }
    }
    return sum<2.0*std::numeric_limits<double>::epsilon() ? max_ : max_ * sqrt(1.0 + sum);
  }

  friend StaticVector normalize(const StaticVector &v )
  {
    const T v_norm = norm(v);
    if (v_norm==0.0)
      return StaticVector(0.0);
    StaticVector retval;
    for (size_t i=0; i<N; i++) {
      retval[i] = v[i] / v_norm;
    }
    return retval;
  }

  friend T dot(const StaticVector &a, const StaticVector &b ) { T result = a.data[0] * b.data[0]; for (size_t i = 1; i < N; i++) result += a.data[i] * b.data[i]; return result; }

  bool operator != (const StaticVector &v ) const { for (size_t i=0; i<N; i++) if (data[i]!=v.data[i]) return true; return false; } 
  bool operator == (const StaticVector &v ) const { for (size_t i=0; i<N; i++) if (data[i]!=v.data[i]) return false; return true; } 
  
  const StaticVector &operator += (const StaticVector &v ) { for (size_t i = 0; i < N; i++) data[i] += v.data[i]; return *this; } 
  const StaticVector &operator -= (const StaticVector &v ) { for (size_t i = 0; i < N; i++) data[i] -= v.data[i]; return *this; } 
  const StaticVector &operator *= (T x ) { for (size_t i = 0; i < N; i++) data[i] *= x; return *this; } 
  const StaticVector &operator /= (T x ) { for (size_t i = 0; i < N; i++) data[i] /= x; return *this; } 
  
  // clang forced me to add these....
  StaticVector &operator = (const StaticVector &v ) { for (size_t i=0; i<N; i++) data[i]=v.data[i]; return *this; } 
  StaticVector &operator = (const volatile StaticVector &v ) { for (size_t i=0; i<N; i++) data[i]=v.data[i]; return *this; } 
  friend StaticVector operator + (const StaticVector &a, const volatile StaticVector &b ) { StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] + b.data[i]; return result; }
  friend StaticVector operator - (const volatile StaticVector &a, const StaticVector &b ) { StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] - b.data[i]; return result; }
  friend StaticVector operator - (const StaticVector &a, const volatile StaticVector &b ) { StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] - b.data[i]; return result; }
  // end
  
  friend StaticVector operator + (const StaticVector &a, const StaticVector &b )	{ StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] + b.data[i]; return result; }
  friend StaticVector operator - (const StaticVector &a, const StaticVector &b )	{ StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] - b.data[i]; return result; }
  friend StaticVector operator - (const StaticVector &v )				{ StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = -v.data[i]; return result; }
  friend const StaticVector &operator + (const StaticVector &v ) { return v; }
  
  // friend T operator * (const StaticVector &a, const StaticVector &b ) { T result = a.data[0] * b.data[0]; for (size_t i = 1; i < N; i++) result += a.data[i] * b.data[i]; return result; }
  friend StaticVector operator * (const T &a, const StaticVector &b ) { StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a * b.data[i]; return result; }
  friend StaticVector operator * (const StaticVector &a, const T &b ) { StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] * b; return result; }
  
  friend StaticVector operator / (const StaticVector &a, const T &b ) { StaticVector result; for (size_t i = 0; i < N; i++) result.data[i] = a.data[i] / b; return result; }

  friend std::ostream &operator << (std::ostream &stream, const StaticVector &v ) { for (size_t i = 0; i < N; i++) stream << ' ' << v[i]; return stream; }
  
  friend OStream &operator << (OStream &stream, const StaticVector &v )	{ if (stream) stream.write(v.data, N); return stream; }
  friend IStream &operator >> (IStream &stream, StaticVector &v ) { if (stream) stream.read(v.data, N); return stream; }

};

namespace {
  template <typename T>
  inline StaticVector<3,T> cross(const StaticVector<3,T> &a, const StaticVector<3,T> &b )
  {
    return StaticVector<3,T>(a[1] * b[2] - a[2] * b[1],
			     a[2] * b[0] - a[0] * b[2],
			     a[0] * b[1] - a[1] * b[0]);
  }
}

static inline double hypot(double m, StaticVector<3> P )
{
  m    = fabs(m);
  P[0] = fabs(P[0]);
  P[1] = fabs(P[1]);
  P[2] = fabs(P[2]);
  const double _max = std::max(std::max(std::max(m, P[0]), P[1]), P[2]);
  if (m == _max) {
    P /= _max;
    return _max*sqrt(1.0 + dot(P,P));
  }
  if (P[0] == _max) {
    m /= _max;
    P[1] /= _max;
    P[2] /= _max;
    return _max*sqrt(1.0 + m*m + P[1]*P[1] + P[2]*P[2]);
  }
  if (P[1] == _max) {
    m /= _max;
    P[0] /= _max;
    P[2] /= _max;
    return _max*sqrt(1.0 + m*m + P[0]*P[0] + P[2]*P[2]);
  }
  m /= _max;
  P[0] /= _max;
  P[1] /= _max;
  return _max*sqrt(1.0 + m*m + P[0]*P[0] + P[1]*P[1]);
}

typedef StaticVector<2,double> Vector2d;
typedef StaticVector<3,double> Vector3d;
typedef StaticVector<4,double> Vector4d;

static Vector3d polar(double rho, double theta, double phi ) 
{  
  double s_t = sin(theta);
  double c_t = cos(theta);
  double s_p = sin(phi);
  double c_p = cos(phi);
  return Vector3d(rho * s_t * c_p, rho * s_t * s_p, rho * c_t); 
}

#endif /* static_vector_hh */
