/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef rbf_stencil_hh
#define rbf_stencil_hh

struct RBF_Stencil {

  size_t Nx, Ny, Nz, Nd;

  double a;

  MatrixNd A;
  VectorNd c;
  
  size_t get_stencil_size() const { return Nd; }
  
  StaticMatrix<3,3> func(double x, double y, double z )
  {
    const double a2 = a*a, x2 = x*x, y2 = y*y, z2 = z*z, r2 = x2 + y2 + z2;
    const double exp_a_r2 = exp(-a*r2);
    StaticMatrix<3,3> retval;
    retval[0][0] = -exp_a_r2*(4*a2*z2+4*a2*y2-4*a);
    retval[1][1] = -exp_a_r2*(4*a2*z2+4*a2*x2-4*a);
    retval[2][2] = -exp_a_r2*(4*a2*y2+4*a2*x2-4*a);
    retval[0][1] = retval[1][0] = 4*a2*exp_a_r2*x*y;
    retval[0][2] = retval[2][0] = 4*a2*exp_a_r2*x*z;
    retval[1][2] = retval[2][1] = 4*a2*exp_a_r2*y*z;
    return retval;
  }

  RBF_Stencil(size_t _Nx=3, size_t _Ny=3, size_t _Nz=3, double hx=1.0, double hy=1.0, double hz=1.0, double _a=-1.0 ) :
    Nx(_Nx), Ny(_Ny), Nz(_Nz), Nd(Nx*Ny*Nz), a(_a), A(3*Nd, 3*Nd), c(3*Nd)
  {
    if (a==-1.0) {
      auto sqr = [] (double x ) { return x*x; };
      a = 18.0 / (sqr(Nx*hx) + sqr(Ny*hy) + sqr(Nz*hz)); // such that exp(-a*r^2) corresponds to a \sigma = 1/6 of the stencil's diagonal
    }

    std::vector<int> indxs_i(Nd), indxs_j(Nd), indxs_k(Nd);
    {
      size_t l = 0;
      for (size_t i = 0; i<Nx; i++) {
	for (size_t j = 0; j<Ny; j++) {
	  for (size_t k = 0; k<Nz; k++) {
	    indxs_i[l] = i;
	    indxs_j[l] = j;
	    indxs_k[l] = k;
	    l++;
	  }
	}
      }
    }

    for (size_t i=0; i<Nd; i++) {
      for (size_t j=0; j<Nd; j++) {
	if (i<=j) {
	  int di = indxs_i[i] - indxs_i[j];
	  int dj = indxs_j[i] - indxs_j[j];
	  int dk = indxs_k[i] - indxs_k[j];
	  const auto Phi = func(di*hx, dj*hy, dk*hz);
	  A[i     ][j     ] = Phi[0][0];
	  A[i     ][j+  Nd] = Phi[0][1];
	  A[i     ][j+2*Nd] = Phi[0][2];
	  A[i+  Nd][j     ] = Phi[1][0];
	  A[i+  Nd][j+  Nd] = Phi[1][1];
	  A[i+  Nd][j+2*Nd] = Phi[1][2];
	  A[i+2*Nd][j     ] = Phi[2][0];
	  A[i+2*Nd][j+  Nd] = Phi[2][1];
	  A[i+2*Nd][j+2*Nd] = Phi[2][2];
	} else {
	  A[i     ][j     ] = A[j     ][i     ];
	  A[i     ][j+  Nd] = A[j     ][i+  Nd];
	  A[i     ][j+2*Nd] = A[j     ][i+2*Nd];
	  A[i+  Nd][j     ] = A[j+  Nd][i     ];
	  A[i+  Nd][j+  Nd] = A[j+  Nd][i+  Nd];
	  A[i+  Nd][j+2*Nd] = A[j+  Nd][i+2*Nd];
	  A[i+2*Nd][j     ] = A[j+2*Nd][i     ];
	  A[i+2*Nd][j+  Nd] = A[j+2*Nd][i+  Nd];
	  A[i+2*Nd][j+2*Nd] = A[j+2*Nd][i+2*Nd];
	}
      }
    }

    if (gsl_linalg_cholesky_decomp1(A) != GSL_SUCCESS) {
      std::cerr << "error: could not compute the Cholesky decomposition of matrix A\n";
      exit(0);
    }
  }

  double *prepare(double *Bx, double *By, double *Bz )
  {
    for (size_t i=0; i<Nd; i++) {
      c[i     ] = Bx[i];
      c[i+  Nd] = By[i];
      c[i+2*Nd] = Bz[i];
    }
    gsl_linalg_cholesky_svx(A, c);
    return c.c_ptr();
  }

};

#endif /* rbf_stencil_ hh */

