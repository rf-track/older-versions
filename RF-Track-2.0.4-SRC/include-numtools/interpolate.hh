/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef interpolate_hh
#define interpolate_hh

namespace {

  // linear interpolation
  template <typename T>
  inline T LINT(const T &y1, const T &y2, const double &t ) // linear interpolation
  { return y1*(1.-t)+y2*t; }
  
  template <typename T>
  inline T LINT_deriv(const T &y1, const T & y2 ) // linear interpolation, first derivative
  { return y2-y1; }

  // cubic interpolation
  template <typename T> // for t=0 gives y0
  inline T CINT0(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double tt=t*t, ttt=tt*t; return (ttt*y2+(6*t-2*ttt)*y1+(ttt-6*t+6)*y0)/6; }

  template <typename T> // for t=0 gives y1
  inline T CINT1(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double tt=t*t, ttt=tt*t; return (ttt*y3+(-3*ttt+3*tt+3*t+1)*y2+(3*ttt-6*tt+4)*y1+(-ttt+3*tt-3*t+1)*y0)/6; }
  
  template <typename T> // for t=0 gives y2
  inline T CINT2(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double tt=t*t, ttt=tt*t; return -((ttt-3*tt-3*t-1)*y3+(-2*ttt+6*tt-4)*y2+(ttt-3*tt+3*t-1)*y1)/6; }
  
  // cubic interpolation, derivative
  template <typename T> // for t=0 gives y0
  inline T CINT0_deriv(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double tt=t*t; return (tt*y2+(2-2*tt)*y1+(tt-2)*y0)/2; }
  
  template <typename T> // for t=0 gives y1
  inline T CINT1_deriv(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double tt=t*t; return (tt*y3+(-3*tt+2*t+1)*y2+(3*tt-4*t)*y1+(-tt+2*t-1)*y0)/2; }
  
  template <typename T> // for t=0 gives y2
  inline T CINT2_deriv(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { double tt=t*t; return -((tt-2*t-1)*y3+(4*t-2*tt)*y2+(tt-2*t+1)*y1)/2; }
  
  // cubic interpolation, 2nd derivative
  template <typename T> // for t=0 gives y0
  inline T CINT0_deriv2(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return t*y2-2*t*y1+t*y0; }

  template <typename T> // for t=0 gives y1
  inline T CINT1_deriv2(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return t*y3+(1-3*t)*y2+(3*t-2)*y1+(1-t)*y0; }
  
  template <typename T> // for t=0 gives y2
  inline T CINT2_deriv2(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return (1-t)*y3+(2*t-2)*y2+(1-t)*y1; }

  template <typename T>
  inline T CINT0_deriv3(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return y2-2*y1+y0; }
  
  template <typename T>
  inline T CINT1_deriv3(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return y3-3*y2+3*y1-y0; }
  
  template <typename T>
  inline T CINT2_deriv3(const T &y0, const T &y1, const T &y2, const T &y3, const double &t )
  { return -y3+2*y2-y1; }

}

#endif /* interpolate_hh */
