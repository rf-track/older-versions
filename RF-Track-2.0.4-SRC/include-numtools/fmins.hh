/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef fmins_hh
#define fmins_hh

#include "vectornd.hh"
#include <gsl/gsl_multimin.h>

struct Fmins {
  double tol;
  const gsl_multimin_fminimizer_type *minimizer;
  int niter;
  bool verbose;
  Fmins(double t = 1e-2, const gsl_multimin_fminimizer_type *m = gsl_multimin_fminimizer_nmsimplex2, int n = 100, bool v = true ) : tol(t), minimizer(m), niter(n), verbose(v) {}
  template <typename Func> static double my_func(const gsl_vector *X, void *func_ptr ) { Func &func = *reinterpret_cast<Func*>(func_ptr); return func(X); }
  template <typename Func> VectorNd operator()(Func func, const VectorNd &X, const VectorNd &step_size )
  {
    VectorNd result;
    if (const gsl_multimin_fminimizer_type *T = minimizer) {
	if (gsl_multimin_fminimizer *s = gsl_multimin_fminimizer_alloc(T, X.size())) {
	  /* Initialize method and iterate */
	  typedef double (*_my_func) (const gsl_vector * x, void * params);
	  gsl_multimin_function minex_func;
	  minex_func.n = X.size();
	  minex_func.f = (_my_func)(&my_func<Func>);
	  minex_func.params = &func;
	  /* Set initial step sizes to 1 */
	  gsl_multimin_fminimizer_set(s, &minex_func, static_cast<const gsl_vector*>(X), static_cast<const gsl_vector*>(step_size));
	  /* Minimization loop */
	  auto old_flags = std::cout.setf(std::ios::fixed);
	  auto old_precision = std::cout.precision(5);
	  for (int iter = 0, status = GSL_CONTINUE; status == GSL_CONTINUE && iter < niter; ++iter) {
	    if (gsl_multimin_fminimizer_iterate(s))
	      break;
	    double size = gsl_multimin_fminimizer_size(s);
	    status = gsl_multimin_test_size(size, tol);
	    if (verbose) {
	      if (status == GSL_SUCCESS)
		std::cout << "converged to minimum at" << std::endl;
	      std::cout << iter << VectorNd(s->x) << "\t f() = " << s->fval << '\t' << "size = " << size << std::endl;
	    }
	  }
	  std::cout.precision(old_precision);
	  std::cout.setf(old_flags);
	  result = VectorNd(s->x);
	  gsl_multimin_fminimizer_free(s);
	}
    }
    return result;
  }
  template <typename Func> VectorNd operator()(Func func, const VectorNd &X, const double step_size = 0.001 )
  {
    VectorNd result;
    if (gsl_vector *ss = gsl_vector_alloc(X.size())) {
      gsl_vector_set_all(ss, step_size);
      result = operator()(func, X, ss);
      gsl_vector_free(ss);
    }
    return result;
  }

};

#endif /* fmins_hh */
