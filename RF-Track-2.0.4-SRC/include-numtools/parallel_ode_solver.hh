/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef parallel_ode_solver_hh
#define parallel_ode_solver_hh

#include <vector>
#include <gsl/gsl_odeiv2.h>

class Parallel_ODE_Solver {
  
  enum {
    analytic,
    leapfrog,
    gsl_rk2, // gsl algorithms start from this one
    gsl_rk4,
    gsl_rkf45,
    gsl_rkck,
    gsl_rk8pd,
    gsl_rk1imp,
    gsl_rk2imp,
    gsl_rk4imp,
    gsl_bsimp,
    gsl_msadams,
    gsl_msbdf
  } algorithm;

protected:

  std::vector<gsl_odeiv2_driver*> gsl_drivers;
  double gsl_epsabs;
  double gsl_epsrel;
  
public:
  
  Parallel_ODE_Solver() : algorithm(leapfrog), gsl_epsabs(1.0), gsl_epsrel(0.0) {} // default analytic 
  ~Parallel_ODE_Solver() { free_gsl_drivers(); }
  
  void set_odeint_epsabs(double epsabs ) { gsl_epsabs = epsabs; }
  void set_odeint_epsrel(double epsrel ) { gsl_epsrel = epsrel; }
  double get_odeint_epsabs() { return gsl_epsabs; }
  double get_odeint_epsrel() { return gsl_epsrel; }
  const char *get_odeint_algorithm() const;
  bool        set_odeint_algorithm(const char *name );
  
  gsl_odeiv2_driver *get_gsl_driver(size_t i ) { return gsl_drivers[i]; }
  void init_gsl_drivers(const std::vector<gsl_odeiv2_system> &sys, double epsabs, double epsrel );
  void init_gsl_drivers(const std::vector<gsl_odeiv2_system> &sys );
  void free_gsl_drivers();

  bool use_analytic() const { return algorithm == analytic; }
  bool use_leapfrog() const { return algorithm == leapfrog; }
  bool use_gsl() const { return algorithm >= gsl_rk2; }

};

#endif /* parallel_ode_solver_hh */
