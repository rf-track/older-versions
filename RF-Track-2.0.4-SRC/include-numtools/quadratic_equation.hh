/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef quadratic_equation_hh
#define quadratic_equation_hh

#include <array>
#include <cmath>

namespace {
  inline std::array<double,2> quadratic_equation(double a, double b, double c )
  {
    // numerically stable solutions to a*x^2 + b*x + c = 0
    double r1, r2;
    if (a==0.0) {
      r1 = r2 = -c/b;
    } else if (b==0.0) {
      r1 = r2 = sqrt(-c/a);
    } else if (c==0.0) {
      r1 = -b/a;
      r2 = 0.0;
    } else {
      const double sqrt_D = sqrt(b*b - 4*a*c);
      if (b>0) {
	r1 = (2*c) / (-b-sqrt_D);
	r2 = (-b-sqrt_D) / (2*a);
      } else {
	r1 = (-b+sqrt_D) / (2*a);
	r2 = (2*c) / (-b+sqrt_D);
      }
    }
    return std::array<double,2>{r1, r2};
  }
}

#endif /* quadratic_equation_hh */

