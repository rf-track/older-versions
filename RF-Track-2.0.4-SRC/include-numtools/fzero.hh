/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef fzero_hh
#define fzero_hh

#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>

class Fzero {

  int max_iter;

  gsl_root_fsolver *s0;

public:

  typedef double (*f_ptr)(double X, void *params );

  Fzero(int max_iter = 1000 ) : max_iter(max_iter) { s0 = gsl_root_fsolver_alloc (gsl_root_fsolver_brent); }
  ~Fzero() { if (s0) gsl_root_fsolver_free (s0); }
  
  double operator()(f_ptr f, double X_min, double X_max, void *params, double epsabs = 0.0, double epsrel = 1e-6 )
  {
    gsl_function F;
    F.function = f;
    F.params = params;
    gsl_root_fsolver_set (s0, &F, X_min, X_max);
    int status = 0, iter = 0;
    double X;
    do {
      iter++;
      status = gsl_root_fsolver_iterate (s0);
      X = gsl_root_fsolver_root (s0);
      X_min = gsl_root_fsolver_x_lower (s0);
      X_max = gsl_root_fsolver_x_upper (s0);
      status = gsl_root_test_interval (X_min, X_max, epsabs, epsrel);
    } while (status == GSL_CONTINUE && iter < max_iter);
    return X;
  }

};

#endif /* fzero_hh */
