/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef numtools_hh
#define numtools_hh

#include <valarray>

#include "interpolate.hh"
#include "fmins.hh"

namespace {
  std::valarray<double> gaussian_pdf(const std::valarray<double> &x, double mu, double sigma )
  {
    double x_length = x.max() - x.min();
    return x_length*exp(-(x*x-2*mu*x+mu*mu)/(2*sigma*sigma))/(sigma*sqrt(2*M_PI)*(x.size()-1));
  }
  
  std::valarray<double> linspace(double xmin, double xmax, size_t N )
  {
    std::valarray<double> x(N);
    for(size_t n=0; n<N; n++)
      x[n] = xmin + ((xmax - xmin) * n) / (N-1);
    return x;
  }

  void linspace(double xmin, double xmax, size_t N, double *dest )
  {
    for(size_t n=0; n<N; n++)
      dest[n] = xmin + ((xmax - xmin) * n) / (N-1);
  }
  
  double kahan_sum(const std::valarray<double> &array )
  {
    double sum = 0.0;
    double c = 0.0;
    for (auto x : array) {
      const volatile double y = x - c;
      const volatile double t = sum + y;
      c = (t - sum) - y;
      sum = t;
    }
    return sum;
  }
}

template <class T>
class CumulativeKahanSum {
  T sum_;
  T c_;
public:
  CumulativeKahanSum(T init = T{0.0}) : sum_(init), c_(0.0) {}
  void operator += (const T &x )
  {
    const volatile T y = x - c_;
    const volatile T t = sum_ + y;
    c_ = (t - sum_) - y;
    sum_ = t;
  }
  void operator  = (const T &x ) { sum_ = x; c_ = T{0.0}; }
  void operator -= (const T &x ) { operator += (-x); }
  operator const T &() const { return sum_; }
  const T &sum() const { return sum_; }
};

#endif /* numtools_hh */
