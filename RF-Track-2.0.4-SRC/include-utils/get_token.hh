/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef get_token_hh
#define get_token_hh

#include <assert.h>

namespace {
  bool get_token(const char **str, const char *tok)
  {
    assert(str && *str && tok); // sanity check
    size_t len = strlen(tok);
    if (strncmp(*str, tok, len)==0) {
      *str += len;
      return true;
    }
    return false;
  }
  bool get_token(const char **str, const char tok)
  {
    assert(str && *str); // sanity check
    if (**str == tok) {
      *str += 1;
      return true;
    }
    return false;
  }
}

#endif /* get_token_hh */
