/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef pair_hh
#define pair_hh

#include <iostream>

template <typename T> struct Pair {
  T x;
  T y;
  Pair() = default;
  Pair(T u, T v ) : x(u), y(v) {} 
  friend T average(const Pair &t ) { return (t.x + t.y) * 0.5; }
  friend std::ostream &operator<<(std::ostream &stream, const Pair &p ) { return stream << "\n x = " << p.x << "\n y = " << p.y << std::endl; }
};

#endif /* pair_hh */
