/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef sub_process_hh
#define sub_process_hh

#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <cstring>

class SubProcess {
protected:

  pid_t PID;
  int send_fd;   // to write to
  int recv_fd;   // to read from

  SubProcess() = default;

public:
  
  SubProcess(const char *command );
  ~SubProcess();
  
  int get_stdin() const  { return send_fd; }
  int get_stdout() const { return recv_fd; }

  const pid_t &pid() const { return PID; }
  int wait() { int status; waitpid(pid(), &status, 0); return status; }
  int kill(int sig ) { return ::kill(PID, sig); }

  ssize_t send(const void *buf, size_t nbytes ) { return write(send_fd, buf, nbytes); }
  ssize_t send(const char *str ) { return write(send_fd, str, strlen(str)); }

  ssize_t recv(void *buf, size_t nbytes ) { return read(recv_fd, buf, nbytes); }

};

#endif /* sub_process_hh */
