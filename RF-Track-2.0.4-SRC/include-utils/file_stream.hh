/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef file_stream_hh
#define file_stream_hh

#include <cstdio>
#include <string>
#include <vector>
#include <list>
#include <fcntl.h>
#include <unistd.h>
#include "stream.hh"

class File_OStream : public OStream {
  int _fd;
  size_t length;
  bool locked; // do not close
protected:

  inline bool writable() const { return _fd != -1; }

public:

  explicit File_OStream(const char *path, int oflag = O_CREAT | O_WRONLY, int mode = 0644 ) : length(0), locked(false) { _fd = ::open(path, oflag, mode); }
  explicit File_OStream(int __fd ) : _fd(__fd), length(0), locked(true) {}
  File_OStream() : _fd(-1), length(0), locked(false) {}
	
  ~File_OStream() { if (_fd!=-1 && !locked) ::close(_fd); }

  int fd() { return _fd; }

  bool open(const char *path, int oflag = O_CREAT | O_WRONLY, int mode = 0644 )
  {
    if (_fd!=-1 && !locked) ::close(_fd);
    _fd = ::open(path, oflag, mode);
    locked = false;
    length = 0;
    return _fd != -1;
  }

  bool open(int __fd )
  {
    if (_fd!=-1 && !locked) ::close(_fd);
    _fd = __fd;
    locked = true;
    length = 0;
    return _fd != -1;
  }
  
  void close()
  {
    if (_fd!=-1 && !locked) ::close(_fd);
    _fd=-1;
  }

  // output streams
  size_t get_length() const { return length; }
  void flush() {}
	
  // input streams
  bool eof() const { return _fd == -1; }
  void rewind() {}
	
  // write methods
  inline size_t write(const char *ptr, size_t n )
  {
    size_t i = 0;
    while (i<n) {
      ssize_t t = ::write(_fd,ptr+i,n-i);
      if (t == -1)
	break;
      i += t;
    }
    length += i;
    return i;
  }
  
  inline size_t write(const char &ref )
  {
    ssize_t i;
    do {
      i = ::write(_fd,&ref,1);
      if (i == -1)
	return 0;
    } while (i != 1);
    length += 1;
    return 1;
  }
        
#define WRITE(TYPE)							\
  inline size_t write(const TYPE *ptr, size_t n ) { return write((const char*)ptr, n * sizeof *ptr) / sizeof *ptr; } \
  inline size_t write(const TYPE &ref )           { return write((const char*)&ref, sizeof ref) / sizeof ref; }
  WRITE(bool);
  WRITE(float);
  WRITE(double);
  WRITE(long double);
  WRITE(signed short);
  WRITE(signed int);
  WRITE(signed long int);
  WRITE(unsigned short);
  WRITE(unsigned int);
  WRITE(unsigned long int);
#undef WRITE
  
};

class File_IStream : public IStream {
  int _fd;
  bool locked; // do not close
protected:

  inline bool readable() const { return _fd != -1; }

public:

  explicit File_IStream(const char *path, int oflag = O_RDONLY ) : locked(false) { _fd = ::open(path, oflag); }
  explicit File_IStream(int __fd ) : _fd(__fd), locked(true) {}
  File_IStream() : _fd(-1), locked(false) {}
	
  ~File_IStream() { if (_fd!=-1 && !locked) ::close(_fd); }

  int fd() { return _fd; }

  bool open(const char *path, int oflag = O_RDONLY )
  {
    if (_fd!=-1 && !locked) ::close(_fd);
    _fd = ::open(path, oflag);
    locked = false;
    return _fd != -1;
  }

  bool open(int __fd )
  {
    if (_fd!=-1 && !locked) ::close(_fd);
    _fd = __fd;
    locked = true;
    return _fd != -1;
  }
	
  void close()
  {
    if (_fd!=-1 && !locked) ::close(_fd);
    _fd=-1;
  }

  // output streams
  void flush() {}
	
  // input streams
  bool eof() const { return _fd == -1; }
  void rewind() {}
	
  // read methods
  inline size_t read(char *ptr, size_t n )
  {
    size_t i = 0;
    while (i<n) {
      ssize_t t = ::read(_fd,ptr+i,n-i);
      if (t == 0 || t == -1)
	break;
      i += t;
    }
    return i;
  }
  
  inline size_t read(char &c )
  {
    ssize_t i = ::read(_fd,&c,1);
    if (i == 0 || i == -1)
      return 0;
    return 1;
  }
  
#define READ(TYPE)							\
  inline size_t read(TYPE *ptr, size_t n ) { return read((char*)ptr, n * sizeof *ptr) / sizeof *ptr; } \
  inline size_t read(TYPE &ref )           { return read((char*)&ref, sizeof ref) / sizeof ref; }
  READ(bool);
  READ(float);
  READ(double);
  READ(long double);
  READ(signed short);
  READ(signed int);
  READ(signed long int);
  READ(unsigned short);
  READ(unsigned int);
  READ(unsigned long int);
#undef READ
  
};

class File_IOStream : public IOStream {
  int _fd;
  size_t length;
  bool locked; // do not close
protected:

  inline bool writable() const { return _fd != -1; }
  inline bool readable() const { return _fd != -1; }

public:

  explicit File_IOStream(const char *path, int oflag = O_RDWR ) : length(0), locked(false) { _fd = ::open(path, oflag); }
  explicit File_IOStream(int __fd ) : _fd(__fd), length(0), locked(true) {}
  File_IOStream() : _fd(-1), length(0), locked(false) {}
	
  ~File_IOStream() { if (_fd!=-1 && !locked) ::close(_fd); }

  int fd() { return _fd; }

  bool open(const char *path, int oflag = O_RDWR )
  {
    if (_fd!=-1 && !locked) ::close(_fd);
    _fd = ::open(path, oflag);
    locked = false;
    length = 0;
    return _fd != -1;
  }

  bool open(int __fd )
  {
    if (_fd!=-1 && !locked) ::close(_fd);
    _fd = __fd;
    locked = true;
    length = 0;
    return _fd != -1;
  }

  void close()
  {
    if (_fd!=-1 && !locked) ::close(_fd);
    _fd=-1;
  }

  // output streams
  size_t get_length() const { return length; }
  void flush() {}
	
  // input streams
  bool eof() const { return _fd == -1; }
  void rewind() {}
	
  // write methods
  inline size_t write(const char *ptr, size_t n )
  {
    size_t i = 0;
    while (i<n) {
      ssize_t t = ::write(_fd,ptr+i,n-i);
      if (t == -1)
	break;
      i += t;
    }
    length += i;
    return i;
  }
  
  inline size_t write(const char &ref )
  {
    ssize_t i;
    do {
      i = ::write(_fd,&ref,1);
      if (i == -1)
	return 0;
    } while (i != 1);
    length += 1;
    return 1;
  }
        
#define WRITE(TYPE)							\
  inline size_t write(const TYPE *ptr, size_t n ) { return write((const char*)ptr, n * sizeof *ptr) / sizeof *ptr; } \
  inline size_t write(const TYPE &ref )           { return write((const char*)&ref, sizeof ref) / sizeof ref; }
  WRITE(bool);
  WRITE(float);
  WRITE(double);
  WRITE(long double);
  WRITE(signed short);
  WRITE(signed int);
  WRITE(signed long int);
  WRITE(unsigned short);
  WRITE(unsigned int);
  WRITE(unsigned long int);
#undef WRITE

  // read methods
  inline size_t read(char *ptr, size_t n )
  {
    size_t i = 0;
    while (i<n) {
      ssize_t t = ::read(_fd,ptr+i,n-i);
      if (t == 0 || t == -1)
	break;
      i += t;
    }
    return i;
  }
  
  inline size_t read(char &c )
  {
    ssize_t i = ::read(_fd,&c,1);
    if (i == 0 || i == -1)
      return 0;
    return 1;
  }
  
#define READ(TYPE)							\
  inline size_t read(TYPE *ptr, size_t n ) { return read((char*)ptr, n * sizeof *ptr) / sizeof *ptr; } \
  inline size_t read(TYPE &ref )           { return read((char*)&ref, sizeof ref) / sizeof ref; }
  READ(bool);
  READ(float);
  READ(double);
  READ(long double);
  READ(signed short);
  READ(signed int);
  READ(signed long int);
  READ(unsigned short);
  READ(unsigned int);
  READ(unsigned long int);
#undef READ
  
};

#endif /* file_stream_hh */
