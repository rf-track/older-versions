/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#ifndef for_all_hh
#define for_all_hh

#include <algorithm>
#include <cstddef>
#include <vector>
#include <thread>

namespace {
  template <typename Function>
  size_t for_all(size_t Nthreads, size_t size, Function func )
  {
    // func must be in the form: func(thread, start, end)
    if (size<Nthreads)
      Nthreads = size;
    if (Nthreads) {
      std::vector<std::thread> threads(Nthreads-1);
      for (size_t thread=1; thread<Nthreads; thread++) {
	size_t start = thread * size / Nthreads;
	size_t end = (thread+1) * size / Nthreads;
	threads[thread-1] = std::thread(func, thread, start, end);
      }
      func(0, 0, size / Nthreads);
      for (auto &t: threads)
	t.join();
    }
    return Nthreads; // returns the effective number of threads used  
  }
}

#endif /* for_all_hh */
