/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <array>
#include <cmath>

#include "RF_Track.hh"
#include "static_magnetic_field_map_2d.hh"

template <typename MESH_2D>
Static_Magnetic_FieldMap_2d<MESH_2D>::Static_Magnetic_FieldMap_2d(const Mesh2d &static_Br, // T
								  const Mesh2d &static_Bz, // T
								  double hr_, // m
								  double hz_, // m
								  double length_) : hr(hr_*1e3), // accepts m, stores mm
										    hz(hz_*1e3), // accepts m, stores mm
										    z0(0.0), // mm
										    static_Bfield(0.0)
{
  size_t Nz = static_Br.size1();
  size_t Nr = static_Br.size2();
  static_Bfield_2d.resize(Nz,Nr);
  for (size_t i=0; i<Nz; i++) {
    for (size_t j=0; j<Nr; j++) {
      static_Bfield_2d.elem(i,j) = StaticVector<2>(static_Bz.elem(i,j), static_Br.elem(i,j));
    }
  }
  set_nsteps(Nz-1);
  set_length(length_);
}

template <typename MESH_2D>
void Static_Magnetic_FieldMap_2d<MESH_2D>::set_length(double length_ )  // accepts m
{
  const double z1_max = (get_nz()-1)*hz;
  if (length_<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length_*1e3;
    // if (z1>z1_max) z1 = z1_max;
  }
}

template <typename MESH_2D>
std::pair<StaticVector<3>, StaticVector<3>> Static_Magnetic_FieldMap_2d<MESH_2D>::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t  /* mm/c */ )
{
  // returned fields are in V/m and T
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticVector<3>, StaticVector<3>>(0.0, 0.0);
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticVector<3>, StaticVector<3>>(0.0, 0.0);
  z /= hz;
  if (x==0.0 && y==0.0) {
    const double Bz0 = static_Bfield_2d(z, 0.0)[0]; // T
    return std::pair<StaticVector<3>, StaticVector<3>>({ 0.0, 0.0, 0.0 }, { static_Bfield[0], static_Bfield[1], Bz0 + static_Bfield[2] });
  }
  const double r = hypot(x, y); // mm
  const double r_ = r / hr; //
  const auto B_2d = static_Bfield_2d(z, r_); // T
  const double Bz = B_2d[0]; // T
  const double Br_r = B_2d[1] / r; // T/mm
  const double Bx = Br_r * x; // T
  const double By = Br_r * y; // T
  const auto E = StaticVector<3>(gsl_isnan(Bz) ? GSL_NAN : 0);
  const auto B = StaticVector<3>(Bx + static_Bfield[0], By + static_Bfield[1], Bz + static_Bfield[2]);
  return std::pair<StaticVector<3>, StaticVector<3>>(E,B);
}

// template specializations
template class Static_Magnetic_FieldMap_2d<TMesh2d_LINT<StaticVector<2>>>;
template class Static_Magnetic_FieldMap_2d<TMesh2d_CINT<StaticVector<2>>>;
