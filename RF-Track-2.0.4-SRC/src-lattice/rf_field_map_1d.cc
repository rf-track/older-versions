/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>
#include <gsl/gsl_sys.h>

#include <fftw3.h>

#include "RF_Track.hh"
#include "rf_field_map_1d.hh"
#include "constants.hh"
#include "move_particle.hh"

template <typename COMPLEX_MESH_1D>
RF_FieldMap_1d<COMPLEX_MESH_1D>::RF_FieldMap_1d(const ComplexMesh1d &Ez,
						double hz_,
						double length_,
						double frequency_,
						double direction_,
						double P_map_,
						double P_actual_ ) : Efield_z_1d_reference(Ez), // V/m
								     hz(hz_*1e3), // accepts m, stores mm
								     z0(0.0), // mm
								     omega(2.0 * M_PI * (frequency_ / (C_LIGHT*1e3))), // frequency in Hz, omega in radian/(mm/c)
								     direction(direction_ == 0.0 ? 0 : (direction_ > 0.0 ? +1 : -1)),
								     P_map(P_map_), // W
								     P_actual(P_actual_), // W
								     scale_factor_and_phase(sqrt(P_actual / P_map)),
								     phi0(0.0),
								     t0_is_set(false),
								     t0(0.0),
								     static_Bfield(0.0)
{
  set_smooth(smooth_radius);
  set_nsteps(Efield_z_1d.size1()-1);
  set_length(length_);
}

template <typename COMPLEX_MESH_1D>
void RF_FieldMap_1d<COMPLEX_MESH_1D>::set_length(double length_ )  // accepts m
{
  const double z1_max = (get_nz()-1)*hz; // mm
  if (length_<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length_*1e3;
    // if (z1>z1_max) z1 = z1_max;
  }
}

template <typename COMPLEX_MESH_1D>
void RF_FieldMap_1d<COMPLEX_MESH_1D>::set_smooth(double radius )
{
  smooth_radius = radius;
  if (smooth_radius != 0.0) {
    size_t N = Efield_z_1d_reference.size();
    std::vector<fftwComplex> in(2*N);
    std::vector<fftwComplex> Fin(2*N);
    std::vector<fftwComplex> filter(2*N);
    std::vector<fftwComplex> Ffilter(2*N);
    fftw_plan_with_nthreads(RFT::number_of_threads);
    fftw_plan p1;
    if ((p1 = fftw_plan_dft_1d(2*N, (fftw_complex*)filter.data(), (fftw_complex*)Ffilter.data(), FFTW_FORWARD, FFTW_ESTIMATE))) {
      fftw_plan p2;
      if ((p2 = fftw_plan_dft_1d(2*N, (fftw_complex*)in.data(), (fftw_complex*)Fin.data(), FFTW_FORWARD, FFTW_ESTIMATE))) {
	fftw_plan p3;
	if ((p3 = fftw_plan_dft_1d(2*N, (fftw_complex*)Fin.data(), (fftw_complex*)in.data(), FFTW_BACKWARD, FFTW_ESTIMATE))) {
	  const auto sqr = [] (double x ) { return x*x; };
	  filter[0] = 1.0;
	  for (size_t i = 1; i<=N; i++) {
	    const double weight = exp(-(sqr(double(i)/smooth_radius)));
	    filter[i] = filter[2*N-i] = weight;
	  }
	  double sum = 0.0;
	  for (size_t i = 0; i<2*N; i++) sum += real(filter[i]);
	  for (size_t i = 0; i<2*N; i++) filter[i] /= sum;
	  fftw_execute(p1);
	  for (size_t i = 0; i<N; i++) {
	    in[i] = in[2*N-(i+1)] = Efield_z_1d_reference.elem(i);
	  }
	  fftw_execute(p2);
	  for (size_t i = 0; i<2*N; i++) {
	    Fin[i] *= Ffilter[i];
	  }
	  fftw_execute(p3);
	  Efield_z_1d.resize(N);
	  for (size_t i = 0; i<N; i++) {
	    Efield_z_1d.elem(i) = in[i] / (2*N);
	  }
	  fftw_destroy_plan(p3);
	}
	fftw_destroy_plan(p2);
      }
      fftw_destroy_plan(p1);
    }
  } else {
    Efield_z_1d = Efield_z_1d_reference;
  }
}

template <typename COMPLEX_MESH_1D>
std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> RF_FieldMap_1d<COMPLEX_MESH_1D>::get_field_complex(double x /* mm */, double y /* mm */, double z /* mm */, double t  /* mm/c */ )
{
  // returned fields are in V/m and T
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>(0.0, 0.0);
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>(0.0, 0.0);
  z /= hz;

  // phase computation
  static std::mutex mutex;
  if (!t0_is_set) {
    if (mutex.try_lock()) {
      t0_is_set = true;
      t0 = t;
    } else {
      mutex.lock();
    }
    mutex.unlock();
  }
  const fftwComplex phase = [&] () {
    const double ph = direction * omega * (t - t0); // radian
    const double c = cos(ph);
    const double s = sin(ph);
    return scale_factor_and_phase * fftwComplex(c,s); // complex
  } ();
  
  if (x==0.0 && y==0.0) {
    const auto Ez0 = Efield_z_1d(z) * phase; // V/m
    return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>({ 0.0, 0.0, Ez0 }, { static_Bfield[0], static_Bfield[1], static_Bfield[2] });
  }
  
  const auto func0       = Efield_z_1d(z); // V/m
  const auto dfunc0_dz   = Efield_z_1d.deriv (z) / hz; // V/m/mm
  const auto d2func0_dz2 = Efield_z_1d.deriv2(z) / hz / hz; // V/m/mm**2
  const auto d3func0_dz3 = Efield_z_1d.deriv3(z) / hz / hz / hz; // V/m/mm**3

  const double r2 = x*x + y*y; // mm**2
  const double omega2 = omega*omega; // (radian/(mm/c))**2
  const auto func = func0 - r2 * (d2func0_dz2 + omega2 * func0) / 4.0; // V/m
  const auto dphase_dt = fftwComplex(0.0, direction*omega) * phase; // radian/(mm/c)

  // electric field, Efield_z_1d
  const fftwComplex Ez = func * phase; // V/m
  const fftwComplex Er_r = (-dfunc0_dz / 2.0 + r2 * (d3func0_dz3 + omega2 * dfunc0_dz) / 16.0) * phase; // V/m/mm
  const fftwComplex Ex = Er_r * x; // V/m
  const fftwComplex Ey = Er_r * y; // V/m

  // magnetic field due to the oscillating electric field
  const fftwComplex Bt_r = (func0 / 2.0 - r2 * (d2func0_dz2 + omega2 * func0) / 16.0) * dphase_dt / C_LIGHT; // T/mm, Mohsen
  fftwComplex Bx = -Bt_r * y; // T
  fftwComplex By =  Bt_r * x; // T

  return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>({ Ex, Ey, Ez }, { Bx + static_Bfield[0], By + static_Bfield[1], static_Bfield[2] });
}

template <typename COMPLEX_MESH_1D>
std::pair<StaticVector<3>,StaticVector<3>> RF_FieldMap_1d<COMPLEX_MESH_1D>::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  const auto &field = get_field_complex(x, y, z, t);
  return std::pair<StaticVector<3>, StaticVector<3>>(StaticVector<3>(real(field.first[0]),
								     real(field.first[1]),
								     real(field.first[2])),
						     StaticVector<3>(real(field.second[0]),
								     real(field.second[1]),
								     real(field.second[2])));
}

// template specializations
template class RF_FieldMap_1d<ComplexMesh1d_LINT>;
template class RF_FieldMap_1d<ComplexMesh1d_CINT>;
