/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>
#include <gsl/gsl_sys.h>

#include "static_electric_field_map.hh"
#include "RF_Track.hh"
#include "numtools.hh"
#include "constants.hh"
#include "move_particle.hh"

Static_Electric_FieldMap::Static_Electric_FieldMap(const Mesh3d &Phi,
					       double x0_, double y0_, /*, double z0_, */ // m
					       double hx_, double hy_, double hz_, // m
					       double length ) : mesh_Phi(Phi),
								 x0(x0_*1e3), // accepts m, stores mm
								 y0(y0_*1e3), // accepts m, stores mm
								 z0(0.0), // mm
								 hx(hx_*1e3), // accepts m, stores mm
								 hy(hy_*1e3), // accepts m, stores mm
								 hz(hz_*1e3)  // accepts m, stores mm
{
  set_nsteps(mesh_Phi.size3()-1);
  set_length(length);
}

Static_Electric_FieldMap::Static_Electric_FieldMap(const Mesh3d &Ex,
					       const Mesh3d &Ey,
					       const Mesh3d &Ez,
					       double x0_, double y0_, /*, double z0_, */ // m
					       double hx_, double hy_, double hz_, // m
					       double length,
					       double quality ) : x0(x0_*1e3), // accepts m, stores mm
								  y0(y0_*1e3), // accepts m, stores mm
								  z0(0.0), // mm
								  hx(hx_*1e3), // accepts m, stores mm
								  hy(hy_*1e3), // accepts m, stores mm
								  hz(hz_*1e3)  // accepts m, stores mm
{
  set_Ex_Ey_Ez(Ex,Ey,Ez, quality);
  set_nsteps(Ex.size3()-1);
  set_length(length);
}

void Static_Electric_FieldMap::set_Ex_Ey_Ez(const Mesh3d &Ex, const Mesh3d &Ey, const Mesh3d &Ez, double quality )
{
  const int
    Nx = Ex.size1(),
    Ny = Ex.size2(),
    Nz = Ex.size3();

  mesh_Phi.resize(Nx,Ny,Nz);

  const size_t Nthreads = RFT::number_of_threads;

  E0 = StaticVector<3>(0.0);
  
  if (true) {

    E0 = [&] () {
      StaticVector<3> average;
      CumulativeKahanSum<double> sum;
      sum = 0.0; for (size_t i=0; i<Ex.size(); i++) { sum += Ex.data()[i]; } average[0] = sum / Ex.size();
      sum = 0.0; for (size_t i=0; i<Ey.size(); i++) { sum += Ey.data()[i]; } average[1] = sum / Ey.size();
      sum = 0.0; for (size_t i=0; i<Ez.size(); i++) { sum += Ez.data()[i]; } average[2] = sum / Ez.size();
      return average;
    } ();

    fftwComplexMesh3d    mesh_Ez_hat(Nx,Ny,Nz/2+1); // Fourier transform of E component
    TMesh3d<fftwComplex> mesh_Ex_hat(Nx,Ny,Nz/2+1);
    TMesh3d<fftwComplex> mesh_Ey_hat(Nx,Ny,Nz/2+1);
  
    // create fftw plans
    fftw_plan_with_nthreads(Nthreads);
    fftw_plan p1, p2;
    if ((p1 = fftw_plan_dft_r2c_3d(Nx, Ny, Nz, mesh_Phi.data(), (fftw_complex *)mesh_Ez_hat.data(), FFTW_ESTIMATE))) {
      if ((p2 = fftw_plan_dft_c2r_3d(Nx, Ny, Nz, (fftw_complex *)mesh_Ez_hat.data(), mesh_Phi.data(), FFTW_ESTIMATE))) {

	// Fourier transform of Ex, Ey, and Ez
	mesh_Phi = Ex; fftw_execute(p1); mesh_Ex_hat = mesh_Ez_hat;
	mesh_Phi = Ey; fftw_execute(p1); mesh_Ey_hat = mesh_Ez_hat;
	mesh_Phi = Ez; fftw_execute(p1);

	// imposes div(E) == 0, stores the result in mesh_Ez_hat
	{
	  const double fx_cut = quality * 2.0*M_PI * 0.5 / hx; // quality * Nyquist frequency
	  const double fy_cut = quality * 2.0*M_PI * 0.5 / hy; // quality * Nyquist frequency
	  const double fz_cut = quality * 2.0*M_PI * 0.5 / hz; // quality * Nyquist frequency
	  for (int i=0; i<Nx; i++) {
	    StaticVector<3> k_vec;
	    k_vec[0] = 2.0*M_PI * double((2*i<=Nx) ? i : (i-Nx)) / (Nx*hx); // 1/mm
	    for (int j=0; j<Ny; j++) {
	      k_vec[1] = 2.0*M_PI * double((2*j<=Ny) ? j : (j-Ny)) / (Ny*hy); // 1/mm
	      for (int k=0; k<Nz/2+1; k++) {
		k_vec[2] = 2.0*M_PI * double((2*k<=Nz) ? k : (k-Nz)) / (Nz*hz); // 1/mm
		if (i!=0 || j!=0 || k!=0) {
		  if (fabs(k_vec[0]) <= fx_cut &&
		      fabs(k_vec[1]) <= fy_cut &&
		      fabs(k_vec[2]) <= fz_cut) {
		    StaticVector<3,fftwComplex> E_hat;
		    E_hat[0] = mesh_Ex_hat.elem(i,j,k); // V/m/mm
		    E_hat[1] = mesh_Ey_hat.elem(i,j,k); // V/m/mm
		    E_hat[2] = mesh_Ez_hat.elem(i,j,k); // V/m/mm
		    mesh_Ez_hat.elem(i,j,k) = fftwComplex(0.0,1.0) * dot(k_vec,E_hat) / dot(k_vec,k_vec); // V/m/mm*mm^2 = V/m*mm
		  } else {
		    mesh_Ez_hat.elem(i,j,k) = 0.0;
		  }
		} else {
		  mesh_Ez_hat.elem(i,j,k) = 0.0;
		}
	      }
	    }
	  }
	}
      
	// anti-transform Ez_hat (Phi_hat) to find mesh_Phi
	fftw_execute(p2);
	mesh_Phi /= Nx*Ny*Nz; // V/m*mm
      
	fftw_destroy_plan(p2);
      }
      fftw_destroy_plan(p1);
    }

  } else {
    
    auto compute_Phi_parallel = [&] (size_t thread, size_t start, size_t end ) {

      const auto G = [] (const StaticVector<3> &dr ) {
	const double norm_dr = norm(dr);
	if (norm_dr == 0.0) return 0.0;
	return -1.0 / (4 * M_PI * norm_dr); // 1/mm
      };

      for (size_t i=start; i<end; i++) {
	for (int j=0; j<Ny; j++) {
	  for (int k=0; k<Nz; k++) {
	    double Phi = 0.0;
	    for (int jj=0; jj<Ny; jj+=2) {
	      for (int kk=0; kk<Nz; kk+=2) {
		StaticVector<3> dr0, n0(-1,0,0);
		dr0[0] = (i -  0) * hx;
		dr0[1] = (j - jj) * hy;
		dr0[2] = (k - kk) * hz;
		StaticVector<3> E0;
		E0[0] = Ex.elem(0,jj,kk);
		E0[1] = Ey.elem(0,jj,kk);
		E0[2] = Ez.elem(0,jj,kk);
		const int ii = Nx-1;
		StaticVector<3> dr1, n1(+1,0,0);
		dr1[0] = (i - ii) * hx;
		dr1[1] = (j - jj) * hy;
		dr1[2] = (k - kk) * hz;
		StaticVector<3> E1;
		E1[0] = Ex.elem(ii,jj,kk);
		E1[1] = Ey.elem(ii,jj,kk);
		E1[2] = Ez.elem(ii,jj,kk);
		Phi += dot(n0, E0) * G(dr0) * 4 * hy * hz;
		Phi += dot(n1, E1) * G(dr1) * 4 * hy * hz;
		// Phi += dot(E0, Gn(dr0, n0)) * 4 * hy * hz;
		// Phi -= dot(E1, Gn(dr1, n1)) * 4 * hy * hz;
	      }
	    }
	    for (int ii=0; ii<Nx; ii+=2) {
	      for (int kk=0; kk<Nz; kk+=2) {
		StaticVector<3> dr0, n0(0,-1,0);
		dr0[0] = (i - ii) * hx;
		dr0[1] = (j -  0) * hy;
		dr0[2] = (k - kk) * hz;
		StaticVector<3> E0;
		E0[0] = Ex.elem(ii,0,kk);
		E0[1] = Ey.elem(ii,0,kk);
		E0[2] = Ez.elem(ii,0,kk);
		const int jj = Ny-1;
		StaticVector<3> dr1, n1(0,+1,0);
		dr1[0] = (i - ii) * hx;
		dr1[1] = (j - jj) * hy;
		dr1[2] = (k - kk) * hz;
		StaticVector<3> E1;
		E1[0] = Ex.elem(ii,jj,kk);
		E1[1] = Ey.elem(ii,jj,kk);
		E1[2] = Ez.elem(ii,jj,kk);
		Phi += dot(n0, E0) * G(dr0) * 4 * hz * hx;
		Phi += dot(n1, E1) * G(dr1) * 4 * hz * hx;
		// Phi += dot(E0, Gn(dr0, n0)) * 4 * hz * hx;
		// Phi -= dot(E1, Gn(dr1, n1)) * 4 * hz * hx;
	      }
	    }
	    for (int ii=0; ii<Nx; ii+=2) {
	      for (int jj=0; jj<Ny; jj+=2) {
		StaticVector<3> dr0, n0(0,0,-1);
		dr0[0] = (i - ii) * hx;
		dr0[1] = (j - jj) * hy;
		dr0[2] = (k -  0) * hz;
		StaticVector<3> E0;
		E0[0] = Ex.elem(ii,jj,0);
		E0[1] = Ey.elem(ii,jj,0);
		E0[2] = Ez.elem(ii,jj,0);
		const int kk = Nz-1;
		StaticVector<3> dr1, n1(0,0,+1);
		dr1[0] = (i - ii) * hx;
		dr1[1] = (j - jj) * hy;
		dr1[2] = (k - kk) * hz;
		StaticVector<3> E1;
		E1[0] = Ex.elem(ii,jj,kk);
		E1[1] = Ey.elem(ii,jj,kk);
		E1[2] = Ez.elem(ii,jj,kk);
		Phi += dot(n0, E0) * G(dr0) * 4 * hx * hy;
		Phi += dot(n1, E1) * G(dr1) * 4 * hx * hy;
		// Phi += dot(E0, Gn(dr0, n0)) * 4 * hx * hy;
		// Phi -= dot(E1, Gn(dr1, n1)) * 4 * hx * hy;
	      }
	    }
	    mesh_Phi.elem(i,j,k) = Phi;
	  }
	}
      }
    };
    for_all(Nthreads, Nx, compute_Phi_parallel);
  }
}

void Static_Electric_FieldMap::set_length(double length )  // accepts m
{
  const double z1_max = (mesh_Phi.size3()-1)*hz;
  if (length<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length*1e3;
    // if (z1>z1_max) z1 = z1_max;
  }
}

std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>> Static_Electric_FieldMap::get_field_jacobian(double x, double y, double z, double /* t */ ) // x,y,z [mm], returns V/m/mm, and T/mm
{
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(0.0, 0.0);
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(0.0, 0.0);
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Phi.size1()-1;
  int height_ = mesh_Phi.size2()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_)
    return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(0.0, 0.0);
  const double inv_hxhx = 1.0/(hx*hx);
  const double inv_hxhy = 1.0/(hx*hy);
  const double inv_hxhz = 1.0/(hx*hz);
  const double inv_hyhy = 1.0/(hy*hy);
  const double inv_hyhz = 1.0/(hy*hz);
  const double inv_hzhz = 1.0/(hz*hz);
  const auto E = StaticMatrix<3,3>(-mesh_Phi.deriv2_x2(x,y,z)*inv_hxhx, -mesh_Phi.deriv2_xy(x,y,z)*inv_hxhy, -mesh_Phi.deriv2_xz(x,y,z)*inv_hxhz,
				   -mesh_Phi.deriv2_xy(x,y,z)*inv_hxhy, -mesh_Phi.deriv2_y2(x,y,z)*inv_hyhy, -mesh_Phi.deriv2_yz(x,y,z)*inv_hyhz,
				   -mesh_Phi.deriv2_xz(x,y,z)*inv_hxhz, -mesh_Phi.deriv2_yz(x,y,z)*inv_hyhz, -mesh_Phi.deriv2_z2(x,y,z)*inv_hzhz); // V/m/mm
  const auto B = StaticMatrix<3,3>(0.0);
  return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(E, B);
}

std::pair<StaticVector<3>,StaticVector<3>> Static_Electric_FieldMap::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticVector<3>,StaticVector<3>>(0.0, 0.0); 
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticVector<3>,StaticVector<3>>(0.0, 0.0); 
  // Relic of a test: Idealization of a constant field, with the field gradient observed in the ELENA field map
  // x /= hx;
  // y /= hy;
  // double dBz_dy =  1.0 / 1e6 / 40.0;
  // double dBy_dy = -1.0 / 1e6 / 40.0;
  // return std::pair<StaticVector<3>,StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0, 0.05 / 1e4 + dBy_dy * y, -100 / 1e4 + dBz_dy * y)); 
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Phi.size1()-1;
  int height_ = mesh_Phi.size2()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_)
    return std::pair<StaticVector<3>,StaticVector<3>>(0.0, 0.0); 
  const auto E = [&] () {
    auto retval = E0;
    const auto dPhi_dx = mesh_Phi.deriv_x(x,y,z)/hx; // V/m*mm/mm
    const auto dPhi_dy = mesh_Phi.deriv_y(x,y,z)/hy; // V/m*mm/mm
    const auto dPhi_dz = mesh_Phi.deriv_z(x,y,z)/hz; // V/m*mm/mm
    retval -= StaticVector<3>(dPhi_dx, dPhi_dy, dPhi_dz); // V/m
    return retval;
  } ();
  const auto B = StaticVector<3>(0.0);
  return std::pair<StaticVector<3>,StaticVector<3>>(E, B);
}
