/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <array>
#include <cmath>

#include "RF_Track.hh"
#include "static_magnetic_field_map_1d.hh"

template <typename MESH_1D>
Static_Magnetic_FieldMap_1d<MESH_1D>::Static_Magnetic_FieldMap_1d(const Mesh1d &static_Bz,
								  double hz_,
								  double length_ ) : static_Bfield_z_1d(static_Bz),
										     hz(hz_*1e3), // accepts m, stores mm
										     z0(0.0), // mm
										     static_Bfield(0.0)
{
  set_nsteps(static_Bfield_z_1d.size1()-1);
  set_length(length_);
}

template <typename MESH_1D>
void Static_Magnetic_FieldMap_1d<MESH_1D>::set_length(double length_ )  // accepts m
{
  const double z1_max = (get_nz()-1)*hz;
  if (length_<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length_*1e3;
    // if (z1>z1_max) z1 = z1_max;
  }
}

template <typename MESH_1D>
std::pair<StaticVector<3>, StaticVector<3>> Static_Magnetic_FieldMap_1d<MESH_1D>::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t  /* mm/c */ )
{
  // returned fields are in V/m and T
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticVector<3>, StaticVector<3>>(0.0, 0.0);
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticVector<3>, StaticVector<3>>(0.0, 0.0);
  z /= hz;
  const auto Bz0 = static_Bfield_z_1d(z); // T
  if (x==0.0 && y==0.0) {
    return std::pair<StaticVector<3>, StaticVector<3>>({ 0.0, 0.0, 0.0 }, { static_Bfield[0], static_Bfield[1], Bz0 + static_Bfield[2] });
  }
  const double r2 = x*x + y*y; // mm**2
  // static magnetic field, Bfield_z_1d
  const double dBz0_dz   = static_Bfield_z_1d.deriv (z) / hz; // T/mm
  const double d2Bz0_dz2 = static_Bfield_z_1d.deriv2(z) / hz / hz; // T/mm**2
  const double d3Bz0_dz3 = static_Bfield_z_1d.deriv3(z) / hz / hz / hz; // T/mm**3
  const double Br_r = -dBz0_dz / 2.0 + r2 * d3Bz0_dz3 / 16.0; // T/mm
  const double Bz = Bz0 - r2 * d2Bz0_dz2 / 4.0; // T
  const double Bx = Br_r * x; // T
  const double By = Br_r * y; // T
  const auto E = StaticVector<3>(gsl_isnan(Bz) ? GSL_NAN : 0);
  const auto B = StaticVector<3>(Bx + static_Bfield[0], By + static_Bfield[1], Bz + static_Bfield[2]);
  return std::pair<StaticVector<3>, StaticVector<3>>(E,B);
}

// template specializations
template class Static_Magnetic_FieldMap_1d<Mesh1d_LINT>;
template class Static_Magnetic_FieldMap_1d<Mesh1d_CINT>;
