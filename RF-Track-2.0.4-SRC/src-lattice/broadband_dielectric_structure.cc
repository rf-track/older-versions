/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "broadband_dielectric_structure.hh"
#include <algorithm>
#include <gsl/gsl_interp.h>

BroadbandDielectricStructure::BroadbandDielectricStructure(double x0_, double y0_, // m
							   double hx_, double hy_, double hz_, // m
							   double length_, // m
							   double t0_, double t1_, // mm/c
							   size_t N, // number of time slices
							   double P_map_, // W
							   double P_actual_ ) : x0(x0_*1e3), // accepts m, stores mm
										y0(y0_*1e3), // accepts m, stores mm
										hx(hx_*1e3), // accepts m, stores mm
										hy(hy_*1e3), // accepts m, stores mm
										hz(hz_*1e3), // accepts m, stores mm
										z0(0.0), // m
										z1(length_*1e3), // accepts m, stores mm
										t0(t0_), // mm/c
										t1(t1_), // mm/c
										P_map(P_map_), // W
										P_actual(P_actual_), // W
										scale_factor(sqrt(P_actual / P_map)),
										static_Efield{{{ 0.0, 0.0, 0.0 }}},
										static_Bfield{{{ 0.0, 0.0, 0.0 }}}
{
  resize(N);
}

void BroadbandDielectricStructure::resize(size_t N )  // set the number of timesteps (use 0 to free memory)
{
  field_slices.resize(N);
  ta.resize(N);
  for (size_t i=0; i<N; i++) { ta[i] = t0 + (t1 - t0) * double(i) / double(N-1); }
  set_nsteps(N-1);
  dt = (t1 - t0) / double(N-1);
}

std::pair<StaticVector<3>,StaticVector<3>> BroadbandDielectricStructure::get_field(double x /* mm */,
										   double y /* mm */,
										   double z /* mm */,
										   double t /* mm/c */ )
{
  if (field_slices.size() == 0)
    return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, static_Bfield);

  if (t<ta[0] || t>ta[ta.size()-1])
    return std::pair<StaticVector<3>, StaticVector<3>>(GSL_NAN, GSL_NAN);

  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticVector<3>, StaticVector<3>>(0.0, 0.0);
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticVector<3>, StaticVector<3>>(0.0, 0.0);
  
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width  = field_slices[0].Ex.size1()-1;
  int height = field_slices[0].Ex.size2()-1;
  
  if (x<0.0 || y<0.0 || x>width || y>height)
    return std::pair<StaticVector<3>,StaticVector<3>>(0.0, 0.0);

  size_t indx = gsl_interp_bsearch(ta.data(), t, 0, ta.size()-1);
  
  const StaticVector<3> E1 = StaticVector<3>(field_slices[indx].Ex(x, y, z), field_slices[indx].Ey(x, y, z), field_slices[indx].Ez(x, y, z));
  const StaticVector<3> B1 = StaticVector<3>(field_slices[indx].Bx(x, y, z), field_slices[indx].By(x, y, z), field_slices[indx].Bz(x, y, z));
  const StaticVector<3> E2 = StaticVector<3>(field_slices[indx+1].Ex(x, y, z), field_slices[indx+1].Ey(x, y, z), field_slices[indx+1].Ez(x, y, z));
  const StaticVector<3> B2 = StaticVector<3>(field_slices[indx+1].Bx(x, y, z), field_slices[indx+1].By(x, y, z), field_slices[indx+1].Bz(x, y, z));
  
  // linear interpolation: E = E1 + (t - t1) / (t2 - t1) * (E2 - E1);
  const StaticVector<3> E = E1 + (t - ta[indx]) / dt * (E2 - E1);
  const StaticVector<3> B = B1 + (t - ta[indx]) / dt * (B2 - B1);
  
  return std::pair<StaticVector<3>, StaticVector<3>>(scale_factor * E + static_Efield,
						     scale_factor * B + static_Bfield);
}
