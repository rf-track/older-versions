/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <utility>
#include "RF_Track.hh"
#include "element.hh"
#include "for_all.hh"

void Aperture::set_aperture(double ax /* m */, double ay /* m */, const char *shape )
{
  rx = ax*1e3; // mm
  ry = ay == -1.0 ? rx : ay*1e3; // mm
  set_aperture_shape(shape);
}

void Aperture::set_aperture_shape(const char *shape )
{
  if (!strcmp(shape, "circular"))
    Aperture::shape = Aperture::CIRCULAR;
  else if (!strcmp(shape, "rectangular"))
    Aperture::shape = Aperture::RECTANGULAR;
  else if (!strcmp(shape, "none"))
    Aperture::shape = Aperture::NONE;
  else
    std::cerr << "error: unknown aperture shape '"<< shape << "'. Valid options are: 'none', 'rectangular', and 'circular'.\n";
}

const char *Aperture::get_aperture_shape() const
{
  if (Aperture::shape == Aperture::CIRCULAR)
    return "circular";
  if (Aperture::shape == Aperture::RECTANGULAR)
    return "rectangular";
  return "none";
}

void Element::set_offsets(double x0, double y0, double z0, // m
			  double roll, double pitch, double yaw, // rad
			  const std::string &reference )
{
  // Tait-Bryan angles to accelerator convention
  const double rotX = pitch; // rad
  const double rotY = yaw; // rad
  const double rotZ = roll; // rad
  const double L_mm = 1e3*get_length(); // mm
  if (reference == "entrance") {
    frame = Frame(1e3*Vector3d(x0, y0, z0), Rotation(rotZ, rotY, rotX)); // mm, rad
  } else if (reference == "center") {
    frame = Frame(1e3*Vector3d(x0, y0, z0) + Vector3d(0.0, 0.0, +0.5*L_mm), Rotation(rotZ, rotY, rotX)); // mm, rad
    frame *= Frame(Vector3d(0.0, 0.0, -0.5*L_mm));
  } else if (reference == "exit") {
    frame = Frame(1e3*Vector3d(x0, y0, z0) + Vector3d(0.0, 0.0, +L_mm), Rotation(rotZ, rotY, rotX)); // mm, rad
    frame *= Frame(Vector3d(0.0, 0.0, -L_mm));
  } else if (reference != "entrance") {
    std::cerr << "error: argument 'reference' must be either 'entrance', 'center', or 'exit'\n";
  }
}

void Element::set_offsets(double x0, double y0, double z0, // m
			  const std::string &reference )
{
  const double L_mm = 1e3*get_length(); // mm
  if (reference == "entrance") {
    frame.set_origin(1e3*Vector3d(x0, y0, z0)); // mm
  } else if (reference == "center") {
    frame.set_origin(1e3*Vector3d(x0, y0, z0) + Vector3d(0.0, 0.0, +0.5*L_mm)); // mm
    frame *= Frame(Vector3d(0.0, 0.0, -0.5*L_mm));
  } else if (reference == "exit") {
    frame.set_origin(1e3*Vector3d(x0, y0, z0) + Vector3d(0.0, 0.0, +L_mm)); // mm
    frame *= Frame(Vector3d(0.0, 0.0, -L_mm));
  } else if (reference != "entrance") {
    std::cerr << "error: argument 'reference' must be either 'entrance', 'center', or 'exit'\n";
  }
}

void Element::set_angles(double roll, double pitch, double yaw, // rad
			 const std::string &reference )
{
  // Tait-Bryan angles to accelerator convention
  const double rotX = pitch; // rad
  const double rotY = yaw; // rad
  const double rotZ = roll; // rad
  const double L_mm = 1e3*get_length(); // mm
  Rotation rotation = Rotation(rotZ, rotY, rotX); // rad
  if (reference == "entrance") {
    frame.set_axes(rotation);
  } else if (reference == "center") {
    frame = Frame(frame.get_origin() + Vector3d(0.0, 0.0, +0.5*L_mm), Rotation(rotZ, rotY, rotX)); // mm, rad
    frame *= Frame(Vector3d(0.0, 0.0, -0.5*L_mm));
  } else if (reference == "exit") {
    frame = Frame(frame.get_origin() + Vector3d(0.0, 0.0, +L_mm), Rotation(rotZ, rotY, rotX)); // mm, rad
    frame *= Frame(Vector3d(0.0, 0.0, -L_mm));
  } else if (reference != "entrance") {
    std::cerr << "error: argument 'reference' must be either 'entrance', 'center', or 'exit'\n";
  }
}

TransportTable Element::track(Bunch6d &bunch )
{
  const size_t Nthreads = RFT::number_of_threads;

  TransportTable transport_table;
  
  auto kick_particle = [] (Particle &particle, const double *force, double dS ) -> void {
    const double Fx = force[0]; // MeV/m
    const double Fy = force[1]; // MeV/m
    const double Fz = force[2]; // MeV/m
    auto P = particle.get_Px_Py_Pz(); // MeV/c
    auto V = P / particle.get_total_energy(); // c
    const double dt = dS / V[2]; // m/c
    P[0] += Fx * dt; // MeV/c
    P[1] += Fy * dt; // MeV/c
    P[2] += Fz * dt; // MeV/c
    particle.xp = P[0] * 1e3 / P[2]; // mrad
    particle.yp = P[1] * 1e3 / P[2]; // mrad
    particle.Pc = norm(P); // MeV/c
  };

  // move to the element's frame
  for_all(Nthreads, bunch.size(), [&] (size_t thread, size_t start, size_t end) -> void {
    for (size_t i=start; i<end; i++) {
      Particle &particle = bunch[i];
      if (particle) {
	const auto X = Vector3d(particle.x, particle.y, 0.0); // mm
	const auto P = particle.get_Px_Py_Pz(); // MeV/c
	const auto X_ = X / frame; // mm, in the element's reference frame
	const auto P_ = P / frame.get_axes(); // MeV/c, momentum in the element's reference frame
	particle.x = X_[0];
	particle.y = X_[1];
	particle.xp = P_[0] * 1e3 / P_[2];
	particle.yp = P_[1] * 1e3 / P_[2];
	if (X_[2] != 0.0) {
	  const auto V_ = P_ / particle.get_total_energy(); // c, velocity in the element's reference frame
	  const double dt_mm = X_[2] / V_[2]; // mm/c
	  particle.x -= V_[0] * dt_mm;
	  particle.y -= V_[1] * dt_mm;
	  particle.t -= dt_mm;
	}
      }
    }
  });

  track0_initialize(bunch);

  // parallel tracking through the element in sc_nsteps space-charge steps...
  size_t number_of_steps = nsteps;

  if (sc_nsteps > number_of_steps)
    number_of_steps = sc_nsteps;

  if (sc_nsteps == 0 && tt_nsteps > number_of_steps)
    number_of_steps = tt_nsteps;

  MatrixNd sc_force;

  const double S_element = bunch.S;
  size_t sc_step = 0, tt_step = 0;
  while(1) {
    size_t step_start;
    size_t step_end;
    if (sc_nsteps>0) {
      step_start = sc_step * number_of_steps / sc_nsteps;
      step_end = (sc_step+1) * number_of_steps / sc_nsteps;
      if (sc_step==0) {
	double dS = (step_end - step_start) * get_length() / number_of_steps;
	if (bunch.size()>1)
	  std::cout << "\33[2K\rinfo: applying space-charge 0" << '/' << sc_nsteps << std::flush;

	RFT::SC_engine->compute_force(sc_force, bunch);
	for (size_t i=0; i<bunch.size(); i++) {
	  auto &particle = bunch[i];
	  if (particle) {
	    kick_particle(particle, sc_force[i], dS * 0.5);
	  }
	}
      }
    } else if (tt_nsteps>0) {
      step_start = tt_step * number_of_steps / tt_nsteps;
      step_end = (tt_step+1) * number_of_steps / tt_nsteps;
    } else {
      step_start = 0;
      step_end = number_of_steps;
    }

    // parallel tracking
    for_all(Nthreads, bunch.size(), [&] (size_t thread, size_t start, size_t end) -> void {
      for (size_t i=start; i<end; i++) {
	Particle &particle = bunch[i];
	if (particle) {
	  track0(particle, S_element, step_start, step_end, number_of_steps, thread);
	}
      }
    });
    
    const double dS = (step_end - step_start) * get_length() / number_of_steps; // m
    bunch.S += dS;

    if (step_end == number_of_steps) { // last step
      // apply last half space-charge
      if (sc_nsteps>0) {
	if (bunch.size()>1)
	  std::cout << "\33[2K\rinfo: applying space-charge " << (sc_step+1) << '/' << sc_nsteps << std::flush;
	RFT::SC_engine->compute_force(sc_force, bunch);
	for (size_t i=0; i<bunch.size(); i++) {
	  auto &particle = bunch[i];
	  if (particle) {
	    kick_particle(particle, sc_force[i], dS * 0.5);
	  }
	}
      }
      transport_table.append_bunch_info(bunch);
      break;
    }

    transport_table.append_bunch_info(bunch);

    // apply space-charge
    if (sc_nsteps>0) {
      if (bunch.size()>1)
	std::cout << "\33[2K\rinfo: applying space-charge " << (sc_step+1) << '/' << sc_nsteps << std::flush;
      RFT::SC_engine->compute_force(sc_force, bunch);
      for (size_t i=0; i<bunch.size(); i++) {
	auto &particle = bunch[i];
	if (particle) {
	  kick_particle(particle, sc_force[i], dS);
	}
      }
      sc_step++;
    } else if (tt_nsteps>0) {
      tt_step++;
    }
  }
  
  if (sc_nsteps>0 && bunch.size()>1) {
    std::cout << std::endl;
  }
  
  track0_finalize(bunch);

  for (size_t i=0; i<bunch.size(); i++) {
    auto &particle = bunch[i];
    if (particle) {
      if (!is_particle_inside_aperture(particle)) {
	particle.lost_at(bunch.S);
      }
    }
  }

  // move to the labs's frame
  for_all(Nthreads, bunch.size(),  [&](size_t thread, size_t start, size_t end) -> void {
    const double L_mm = get_length()*1e3; // mm
    for (size_t i=start; i<end; i++) {
      Particle &particle = bunch[i];
      if (particle) {
	const auto X_ = Vector3d(particle.x, particle.y, L_mm); // mm
	const auto P_ = particle.get_Px_Py_Pz(); // MeV/c
	const auto X = frame * X_; // mm, in the lab's reference frame
	const auto P = frame.get_axes() * P_; // MeV/c, momentum in the lab's reference frame
	particle.x = X[0];
	particle.y = X[1];
	particle.xp = P[0] * 1e3 / P[2];
	particle.yp = P[1] * 1e3 / P[2];
	if (X[2] != L_mm) {
	  const auto V = P / particle.get_total_energy(); // c, velocity in the lab's reference frame
	  const double dt_mm = (X[2] - L_mm) / V[2]; // mm/c
	  particle.x -= V[0] * dt_mm;
	  particle.y -= V[1] * dt_mm;
	  particle.t -= dt_mm;
	}
      }
    }
  });

  return transport_table;
}

std::pair<StaticMatrix<3,3>, StaticMatrix<3,3>> Element::get_field_jacobian(double x, double y, double z, double t ) // x,y,z [mm] t [mm/c], returns V/m/mm, and T/mm
{
  const double h = 1, hh = 2*h; // mm
  std::pair<StaticVector<3>, StaticVector<3>> EB[8];
  enum { MMM, MMP, MPM, MPP, PMM, PMP, PPM, PPP };
  EB[MMM] = get_field(x-h, y-h, z-h, t);
  EB[MMP] = get_field(x-h, y-h, z+h, t);
  EB[MPM] = get_field(x-h, y+h, z-h, t);
  EB[MPP] = get_field(x-h, y+h, z+h, t);
  EB[PMM] = get_field(x+h, y-h, z-h, t);
  EB[PMP] = get_field(x+h, y-h, z+h, t);
  EB[PPM] = get_field(x+h, y+h, z-h, t);
  EB[PPP] = get_field(x+h, y+h, z+h, t);
  StaticVector<3> E[6], B[6];
  enum { OOM, OOP, OMO, OPO, MOO, POO };
  E[OOM] = 0.25 * (EB[MMM].first + EB[MPM].first + EB[PMM].first + EB[PPM].first);
  E[OOP] = 0.25 * (EB[MMP].first + EB[MPP].first + EB[PMP].first + EB[PPP].first);
  E[OMO] = 0.25 * (EB[MMM].first + EB[MMP].first + EB[PMM].first + EB[PMP].first);
  E[OPO] = 0.25 * (EB[MPM].first + EB[MPP].first + EB[PPM].first + EB[PPP].first);
  E[MOO] = 0.25 * (EB[MMM].first + EB[MMP].first + EB[MPM].first + EB[MPP].first);
  E[POO] = 0.25 * (EB[PMM].first + EB[PMP].first + EB[PPM].first + EB[PPP].first);
  B[OOM] = 0.25 * (EB[MMM].second + EB[MPM].second + EB[PMM].second + EB[PPM].second);
  B[OOP] = 0.25 * (EB[MMP].second + EB[MPP].second + EB[PMP].second + EB[PPP].second);
  B[OMO] = 0.25 * (EB[MMM].second + EB[MMP].second + EB[PMM].second + EB[PMP].second);
  B[OPO] = 0.25 * (EB[MPM].second + EB[MPP].second + EB[PPM].second + EB[PPP].second);
  B[MOO] = 0.25 * (EB[MMM].second + EB[MMP].second + EB[MPM].second + EB[MPP].second);
  B[POO] = 0.25 * (EB[PMM].second + EB[PMP].second + EB[PPM].second + EB[PPP].second);
  StaticMatrix<3,3> E_jac, B_jac;
  E_jac[0][0] = (E[POO][0] - E[MOO][0]) / hh;
  E_jac[0][1] = (E[OPO][0] - E[OMO][0]) / hh;
  E_jac[0][2] = (E[OOP][0] - E[OOM][0]) / hh;
  E_jac[1][0] = (E[POO][1] - E[MOO][1]) / hh;
  E_jac[1][1] = (E[OPO][1] - E[OMO][1]) / hh;
  E_jac[1][2] = (E[OOP][1] - E[OOM][1]) / hh;
  E_jac[2][0] = (E[POO][2] - E[MOO][2]) / hh;
  E_jac[2][1] = (E[OPO][2] - E[OMO][2]) / hh;
  E_jac[2][2] = (E[OOP][2] - E[OOM][2]) / hh;
  B_jac[0][0] = (B[POO][0] - B[MOO][0]) / hh;
  B_jac[0][1] = (B[OPO][0] - B[OMO][0]) / hh;
  B_jac[0][2] = (B[OOP][0] - B[OOM][0]) / hh;
  B_jac[1][0] = (B[POO][1] - B[MOO][1]) / hh;
  B_jac[1][1] = (B[OPO][1] - B[OMO][1]) / hh;
  B_jac[1][2] = (B[OOP][1] - B[OOM][1]) / hh;
  B_jac[2][0] = (B[POO][2] - B[MOO][2]) / hh;
  B_jac[2][1] = (B[OPO][2] - B[OMO][2]) / hh;
  B_jac[2][2] = (B[OOP][2] - B[OOM][2]) / hh;
  return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(E_jac, B_jac);
}
