/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "drift.hh"

std::pair<StaticVector<3>,StaticVector<3>> Drift::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  if (z<0 || z>1e3*length)
    return std::pair<StaticVector<3>, StaticVector<3>>(0.0, 0.0);
  return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, static_Bfield);
}
