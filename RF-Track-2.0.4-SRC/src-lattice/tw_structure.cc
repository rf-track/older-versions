/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <array>
#include <cmath>
#include <thread>
#include <gsl/gsl_sf_bessel.h>

#include "RF_Track.hh"
#include "tw_structure.hh"

TW_Structure::TW_Structure(const std::vector<double> &coeffs_, int first_suffix, double frequency, double ph_adv, double n_of_cells ) :
  z0(0.0), phase_advance(ph_adv), start_coefficient(first_suffix), coeffs(coeffs_), number_of_cells(n_of_cells),
  phi0(0.0),
  t0_is_set(false),
  t0(0.0),
  static_Bfield(0.0, 0.0, 0.0)
{
  omega = 2.0*M_PI * frequency / (C_LIGHT*1e3); // radian/(mm/c), or radian/mm
  Lcell = phase_advance / omega; // mm
  kn.resize(coeffs.size());
  qn.resize(coeffs.size());
  coeffs1.resize(coeffs.size());
  coeffs2.resize(coeffs.size());
  ordinary_bessel.resize(coeffs.size());
  const auto sqr = [] (double x ) { return x*x; };
  const double k0 = phase_advance / Lcell; // 1/mm
  for (size_t i=0; i<qn.size(); i++) {
    const int n = int(i) + start_coefficient;
    kn[i] = k0 + 2.0*n*M_PI/Lcell; // 1/mm
    qn[i] = sqrt(fabs(sqr(omega) - sqr(kn[i]))); // 1/mm
    coeffs1[i] = coeffs[i] * kn[i]; // V/m
    coeffs2[i] = coeffs[i] * omega / C_LIGHT; // T/mm
    ordinary_bessel[i] = fabs(omega / kn[i]) >= 1.0; // if true, uses ordinary bessel functions
  }
  set_length(-1.0);
}

void TW_Structure::set_length(double length_ )  // accepts m
{
  const double z1_max = get_cell_length() * fabs(number_of_cells) * 1e3; // mm
  if (length_<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length_*1e3;
    // if (z1>z1_max) z1 = z1_max;
  }
}

std::pair<StaticVector<3>,StaticVector<3>> TW_Structure::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  // returned fields are in V/m and T
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticVector<3>, StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0));
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticVector<3>, StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0));
  static std::mutex mutex;
  if (!t0_is_set) {
    if (mutex.try_lock()) {
      t0_is_set = true;
      t0 = t;
    } else {
      mutex.lock();
    }
    mutex.unlock();
  }
  if (number_of_cells<0) z += Lcell * 0.5; // starts from the center of the cell
  const double r = hypot(x,y); // mm
  double Ez = 0.0; // V/m
  double Er_r = 0.0; // V/m
  double Bt_r = 0.0; // T
  const double omega_t_phi0 = omega * (t - t0) + phi0;
  const double taylor_0_bound = sqrt(8*std::numeric_limits<double>::epsilon());
  for (size_t i=0; i<qn.size(); i++) {
    const double arg_omega_t_kn_z = omega_t_phi0 - kn[i]*z;
    const double sin_omega_t_kn_z = sin(arg_omega_t_kn_z);
    const double cos_omega_t_kn_z = cos(arg_omega_t_kn_z);
    double bessel_0, bessel_1_qnr;
    if (fabs(qn[i]*r) > taylor_0_bound) {
      double bessel_1;
      if (ordinary_bessel[i]) {
	bessel_0 = gsl_sf_bessel_J0 (qn[i]*r);
	bessel_1 = gsl_sf_bessel_J1 (qn[i]*r);
      } else {
	bessel_0 = gsl_sf_bessel_I0 (qn[i]*r);
	bessel_1 = gsl_sf_bessel_I1 (qn[i]*r);
      }
      bessel_1_qnr = bessel_1 / (qn[i]*r);
    } else {
      bessel_0 = 1.0;
      bessel_1_qnr = 0.5;
    }
    Ez   += coeffs [i] * sin_omega_t_kn_z * bessel_0;    // V/m
    Er_r += coeffs1[i] * cos_omega_t_kn_z * bessel_1_qnr; // V/m/mm
    Bt_r += coeffs2[i] * cos_omega_t_kn_z * bessel_1_qnr; // T/mm
  }
  const double Ex =  Er_r * x; // V/m
  const double Ey =  Er_r * y; // V/m
  const double Bx = -Bt_r * y; // T
  const double By =  Bt_r * x; // T
  return std::pair<StaticVector<3>, StaticVector<3>>({ Ex, Ey, Ez }, { Bx + static_Bfield[0], By + static_Bfield[1], static_Bfield[2] });
}
