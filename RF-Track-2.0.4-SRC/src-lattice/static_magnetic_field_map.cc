/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>
#include <gsl/gsl_sys.h>

#include "static_magnetic_field_map.hh"
#include "RF_Track.hh"
#include "numtools.hh"
#include "constants.hh"
#include "move_particle.hh"

Static_Magnetic_FieldMap::Static_Magnetic_FieldMap(const Mesh3d &Ax,
						   const Mesh3d &Ay,
						   const Mesh3d &Az,
						   const Mesh3d &PhiM,
						   double x0_, double y0_, /*, double z0_, */ // m
						   double hx_, double hy_, double hz_, // m
						   double length ) : x0(x0_*1e3), // accepts m, stores mm
								     y0(y0_*1e3), // accepts m, stores mm
								     z0(0.0), // mm
								     hx(hx_*1e3), // accepts m, stores mm
								     hy(hy_*1e3), // accepts m, stores mm
								     hz(hz_*1e3)  // accepts m, stores mm
{
  mesh_A.resize(Ax.dims());
  for (size_t i=0; i<Ax.size1(); i++) {
    for (size_t j=0; j<Ay.size2(); j++) {
      for (size_t k=0; k<Az.size3(); k++) {
	auto &A = mesh_A.elem(i,j,k);
	A[0] = PhiM.elem(i,j,k);
	A[1] = Ax.elem(i,j,k);
	A[2] = Ay.elem(i,j,k);
	A[3] = Az.elem(i,j,k);
      }
    }
  }
  set_nsteps(mesh_A.size3()-1);
  set_length(length);
}

Static_Magnetic_FieldMap::Static_Magnetic_FieldMap(const Mesh3d &Bx,
						   const Mesh3d &By,
						   const Mesh3d &Bz,
						   double x0_, double y0_, /*, double z0_, */ // m
						   double hx_, double hy_, double hz_, // m
						   double length,
						   double quality ) : x0(x0_*1e3), // accepts m, stores mm
								      y0(y0_*1e3), // accepts m, stores mm
								      z0(0.0), // mm
								      hx(hx_*1e3), // accepts m, stores mm
								      hy(hy_*1e3), // accepts m, stores mm
								      hz(hz_*1e3)  // accepts m, stores mm
{
  set_Bx_By_Bz(Bx,By,Bz, quality);
  set_nsteps(Bx.size3()-1);
  set_length(length);
}

void Static_Magnetic_FieldMap::set_Bx_By_Bz(Mesh3d Bx, Mesh3d By, Mesh3d Bz, double quality )
{
  const size_t Nthreads = RFT::number_of_threads;

  const int
    Nx = Bx.size1(),
    Ny = Bx.size2(),
    Nz = Bx.size3();
  
  std::vector<bool> isnan(Nx*Ny*Nz);
  for_all(Nthreads, Nx, [&] (int thread, int start, int end ) {
      for (int i=start; i<end; i++) {
	for (int j=0; j<Ny; j++) {
	  for (int k=0; k<Nz; k++) {
	    if ((isnan[k + Nz * (j + Ny * i)] = gsl_isnan(Bx.elem(i,j,k)) || gsl_isnan(By.elem(i,j,k)) || gsl_isnan(Bz.elem(i,j,k)))) {
	      Bx.elem(i,j,k) = 0.0;
	      By.elem(i,j,k) = 0.0;
	      Bz.elem(i,j,k) = 0.0;
	    }
	  }
	}
      }
    });

  mesh_A.resize(Nx,Ny,Nz);

  Bmean = StaticVector<3>(0.0);
  
  if (quality>0.0) {

    Bmean = [&] () {
      StaticVector<3> average;
      CumulativeKahanSum<double> sum;
      sum = 0.0; for (size_t i=0; i<Bx.size(); i++) { sum += Bx.data()[i]; } average[0] = sum / Bx.size();
      sum = 0.0; for (size_t i=0; i<By.size(); i++) { sum += By.data()[i]; } average[1] = sum / By.size();
      sum = 0.0; for (size_t i=0; i<Bz.size(); i++) { sum += Bz.data()[i]; } average[2] = sum / Bz.size();
      return average;
    } ();
  
    Mesh3d mesh_Bx_mirror(2*Nx,2*Ny,2*Nz); // Complex array for Bx
    Mesh3d mesh_By_mirror(2*Nx,2*Ny,2*Nz); // Complex array for By
    Mesh3d mesh_Bz_mirror(2*Nx,2*Ny,2*Nz); // Complex array for Bz
    Mesh3d mesh_PhiM_mirror(2*Nx,2*Ny,2*Nz); // Complex array for PhiM

    fftwComplexMesh3d mesh_Bx_hat(2*Nx,2*Ny,Nz+1); // Fourier transform of Ax component
    fftwComplexMesh3d mesh_By_hat(2*Nx,2*Ny,Nz+1); // Fourier transform of Ay component
    fftwComplexMesh3d mesh_Bz_hat(2*Nx,2*Ny,Nz+1); // Fourier transform of Az component
    fftwComplexMesh3d mesh_PhiM_hat(2*Nx,2*Ny,Nz+1); // Fourier transform of PhiM component
   
    // create fftw plans
    fftw_plan_with_nthreads(Nthreads);
    fftw_plan p1, p2, p3, p4, p5, p6, p7;
    bool success = false;
    if ((p1 = fftw_plan_dft_r2c_3d(2*Nx, 2*Ny, 2*Nz, mesh_Bx_mirror.data(), (fftw_complex *)mesh_Bx_hat.data(), FFTW_ESTIMATE))) {
      if ((p2 = fftw_plan_dft_r2c_3d(2*Nx, 2*Ny, 2*Nz, mesh_By_mirror.data(), (fftw_complex *)mesh_By_hat.data(), FFTW_ESTIMATE))) {
	if ((p3 = fftw_plan_dft_r2c_3d(2*Nx, 2*Ny, 2*Nz, mesh_Bz_mirror.data(), (fftw_complex *)mesh_Bz_hat.data(), FFTW_ESTIMATE))) {
	  if ((p4 = fftw_plan_dft_c2r_3d(2*Nx, 2*Ny, 2*Nz, (fftw_complex *)mesh_Bx_hat.data(), mesh_Bx_mirror.data(), FFTW_ESTIMATE))) {
	    if ((p5 = fftw_plan_dft_c2r_3d(2*Nx, 2*Ny, 2*Nz, (fftw_complex *)mesh_By_hat.data(), mesh_By_mirror.data(), FFTW_ESTIMATE))) {
	      if ((p6 = fftw_plan_dft_c2r_3d(2*Nx, 2*Ny, 2*Nz, (fftw_complex *)mesh_Bz_hat.data(), mesh_Bz_mirror.data(), FFTW_ESTIMATE))) {
		if ((p7 = fftw_plan_dft_c2r_3d(2*Nx, 2*Ny, 2*Nz, (fftw_complex *)mesh_PhiM_hat.data(), mesh_PhiM_mirror.data(), FFTW_ESTIMATE))) {
		  success = true;
		  
		  // Fourier transform Bx, By, and Bz
		  auto copy_and_mirror = [&Nx, &Ny, &Nz] (const Mesh3d &from, Mesh3d &to ) {
		    for (int i=0; i<Nx; i++) {
		      for (int j=0; j<Ny; j++) {
			for (int k=0; k<Nz; k++) {
			  const auto elem_ijk = from.elem(i,j,k);
			  to.elem(i,j,k) = elem_ijk;
			  to.elem(i,j,2*Nz-1-k) = elem_ijk;
			  to.elem(i,2*Ny-1-j,k) = elem_ijk;
			  to.elem(i,2*Ny-1-j,2*Nz-1-k) = elem_ijk;
			  to.elem(2*Nx-1-i,j,k) = elem_ijk;
			  to.elem(2*Nx-1-i,j,2*Nz-1-k) = elem_ijk;
			  to.elem(2*Nx-1-i,2*Ny-1-j,k) = elem_ijk;
			  to.elem(2*Nx-1-i,2*Ny-1-j,2*Nz-1-k) = elem_ijk;
			}
		      }
		    }
		  };

		  copy_and_mirror(Bx, mesh_Bx_mirror);
		  copy_and_mirror(By, mesh_By_mirror);
		  copy_and_mirror(Bz, mesh_Bz_mirror);
		  
		  fftw_execute(p1);
		  fftw_execute(p2);
		  fftw_execute(p3);
		 
		  // imposes div(B) == 0
		  {
		    const double fx_cut = (2*M_PI) * quality * 0.5 / hx; // quality * Nyquist frequency
		    const double fy_cut = (2*M_PI) * quality * 0.5 / hy; // quality * Nyquist frequency
		    const double fz_cut = (2*M_PI) * quality * 0.5 / hz; // quality * Nyquist frequency
		    for_all(Nthreads, 2*Nx, [&] (size_t thread, int start, int end ) {
			for (int i=start; i<end; i++) {
			  StaticVector<3> k_vec;
			  k_vec[0] = (2*M_PI) * double((i<Nx) ? i : (i-2*Nx)) / (2*Nx*hx); // 1/mm
			  for (int j=0; j<2*Ny; j++) {
			    k_vec[1] = (2*M_PI) * double((j<Ny) ? j : (j-2*Ny)) / (2*Ny*hy); // 1/mm
			    for (int k=0; k<Nz+1; k++) {
			      k_vec[2] = (2*M_PI) * double((k<Nz) ? k : (k-2*Nz)) / (2*Nz*hz); // 1/mm
			      if (fabs(k_vec[0]) <= fx_cut &&
				  fabs(k_vec[1]) <= fy_cut &&
				  fabs(k_vec[2]) <= fz_cut) {
				const double k_vec_norm_sqr = dot(k_vec,k_vec);
				if (k_vec_norm_sqr!=0.0) {
				  const double inv_k_vec_norm_sqr = 1.0/k_vec_norm_sqr; // mm^2
				  StaticVector<3,fftwComplex> B_hat;
				  B_hat[0] = mesh_Bx_hat.elem(i,j,k); // T
				  B_hat[1] = mesh_By_hat.elem(i,j,k); // T
				  B_hat[2] = mesh_Bz_hat.elem(i,j,k); // T
				  StaticVector<3,fftwComplex> An_hat = fftwComplex(0.0,1.0) * cross(k_vec,B_hat) * inv_k_vec_norm_sqr; // T*mm;
				  mesh_Bx_hat.elem(i,j,k) = An_hat[0]; // T*mm
				  mesh_By_hat.elem(i,j,k) = An_hat[1]; // T*mm
				  mesh_Bz_hat.elem(i,j,k) = An_hat[2]; // T*mm
				  mesh_PhiM_hat.elem(i,j,k) = fftwComplex(0.0,1.0) * dot(k_vec,B_hat) * inv_k_vec_norm_sqr; // T*mm
				} else {
				  mesh_Bx_hat.elem(i,j,k) = 0.0;
				  mesh_By_hat.elem(i,j,k) = 0.0;
				  mesh_Bz_hat.elem(i,j,k) = 0.0;
				  mesh_PhiM_hat.elem(i,j,k) = 0.0;
				}
			      } else {
				mesh_Bx_hat.elem(i,j,k) = 0.0;
				mesh_By_hat.elem(i,j,k) = 0.0;
				mesh_Bz_hat.elem(i,j,k) = 0.0;
				mesh_PhiM_hat.elem(i,j,k) = 0.0;
			      }
			    }
			  }
			}
		      });
		  }
		  
		  // anti-transform Bxyz_hat (mesh_Axyz)
		  fftw_execute(p4);
		  fftw_execute(p5);
		  fftw_execute(p6);
		  fftw_execute(p7);
		  {
		    const double scale = 8.0*Nx*Ny*Nz;
		    mesh_Bx_mirror /= scale; // T*mm
		    mesh_By_mirror /= scale; // T*mm
		    mesh_Bz_mirror /= scale; // T*mm
		    mesh_PhiM_mirror /= scale; // T*mm
		  }
		  
		  for_all(Nthreads, Nx, [&] (size_t thread, int start, int end ) {
		      for (int i=start; i<end; i++) {
			for (int j=0; j<Ny; j++) {
			  for (int k=0; k<Nz; k++) {
			    auto &A = mesh_A.elem(i,j,k);
			    A[0] = mesh_PhiM_mirror.elem(i,j,k);
			    A[1] = mesh_Bx_mirror.elem(i,j,k);
			    A[2] = mesh_By_mirror.elem(i,j,k);
			    A[3] = mesh_Bz_mirror.elem(i,j,k);
			  }
			}
		      }
		    });
		  
		  fftw_destroy_plan(p7);
		}
		fftw_destroy_plan(p6);
	      }
	      fftw_destroy_plan(p5);
	    }
	    fftw_destroy_plan(p4);
	  }
	  fftw_destroy_plan(p3);
	}
	fftw_destroy_plan(p2);
      }
      fftw_destroy_plan(p1);
    }

    if (!success) {
      std::cerr << "error: something when wrong while initializing fftw...\n";
    }
    
  } else {

    const auto G = [] (const StaticVector<3> &dr ) {
      const double norm_dr = norm(dr);
      if (norm_dr == 0.0) return 0.0;
      return -1.0 / (4 * M_PI * norm_dr); // 1/mm
    };
    
    const auto Gn = [] (const StaticVector<3> &dr, const StaticVector<3> &m ) {
      const double norm_dr = norm(dr);
      if (norm_dr == 0.0) return StaticVector<3>(0.0);
      const double tmp = norm_dr - dot(m, dr); // mm
      if (tmp == 0.0) return StaticVector<3>(0.0);
      return cross(m, dr) / (4 * M_PI * tmp * norm_dr); // 1/mm
    };
    
    for_all(Nthreads, Nx, [&] (size_t thread, int start, int end ) {
	for (int i=start; i<end; i++) {
	  for (int j=0; j<Ny; j++) {
	    for (int k=0; k<Nz; k++) {
	      CumulativeKahanSum<StaticVector<3>>
		At(StaticVector<3>(0.0)),
		An(StaticVector<3>(0.0));
	      for (int jj=0; jj<Ny; jj++) {
		for (int kk=0; kk<Nz; kk++) {
		  StaticVector<3> dr0, n0(-1,0,0);
		  dr0[0] = (i -  0) * hx;
		  dr0[1] = (j - jj) * hy;
		  dr0[2] = (k - kk) * hz;
		  StaticVector<3> B0;
		  B0[0] = Bx.elem(0,jj,kk);
		  B0[1] = By.elem(0,jj,kk);
		  B0[2] = Bz.elem(0,jj,kk);
		  const int ii = Nx-1;
		  StaticVector<3> dr1, n1(+1,0,0);
		  dr1[0] = (i - ii) * hx;
		  dr1[1] = (j - jj) * hy;
		  dr1[2] = (k - kk) * hz;
		  StaticVector<3> B1;
		  B1[0] = Bx.elem(ii,jj,kk);
		  B1[1] = By.elem(ii,jj,kk);
		  B1[2] = Bz.elem(ii,jj,kk);
		  An += dot(n0, B0) * Gn(dr0, n0) * hy * hz;
		  An += dot(n1, B1) * Gn(dr1, n1) * hy * hz;
		  At += cross(n0, B0) * G(dr0) * hy * hz;
		  At += cross(n1, B1) * G(dr1) * hy * hz;
		}
	      }
	      for (int ii=0; ii<Nx; ii++) {
		for (int kk=0; kk<Nz; kk++) {
		  StaticVector<3> dr0, n0(0,-1,0);
		  dr0[0] = (i - ii) * hx;
		  dr0[1] = (j -  0) * hy;
		  dr0[2] = (k - kk) * hz;
		  StaticVector<3> B0;
		  B0[0] = Bx.elem(ii,0,kk);
		  B0[1] = By.elem(ii,0,kk);
		  B0[2] = Bz.elem(ii,0,kk);
		  const int jj = Ny-1;
		  StaticVector<3> dr1, n1(0,+1,0);
		  dr1[0] = (i - ii) * hx;
		  dr1[1] = (j - jj) * hy;
		  dr1[2] = (k - kk) * hz;
		  StaticVector<3> B1;
		  B1[0] = Bx.elem(ii,jj,kk);
		  B1[1] = By.elem(ii,jj,kk);
		  B1[2] = Bz.elem(ii,jj,kk);
		  An += dot(n0, B0) * Gn(dr0, n0) * hz * hx;
		  An += dot(n1, B1) * Gn(dr1, n1) * hz * hx;
		  At += cross(n0, B0) * G(dr0) * hz * hx;
		  At += cross(n1, B1) * G(dr1) * hz * hx;
		}
	      }
	      for (int ii=0; ii<Nx; ii++) {
		for (int jj=0; jj<Ny; jj++) {
		  StaticVector<3> dr0, n0(0,0,-1);
		  dr0[0] = (i - ii) * hx;
		  dr0[1] = (j - jj) * hy;
		  dr0[2] = (k -  0) * hz;
		  StaticVector<3> B0;
		  B0[0] = Bx.elem(ii,jj,0);
		  B0[1] = By.elem(ii,jj,0);
		  B0[2] = Bz.elem(ii,jj,0);
		  const int kk = Nz-1;
		  StaticVector<3> dr1, n1(0,0,+1);
		  dr1[0] = (i - ii) * hx;
		  dr1[1] = (j - jj) * hy;
		  dr1[2] = (k - kk) * hz;
		  StaticVector<3> B1;
		  B1[0] = Bx.elem(ii,jj,kk);
		  B1[1] = By.elem(ii,jj,kk);
		  B1[2] = Bz.elem(ii,jj,kk);
		  An += dot(n0, B0) * Gn(dr0, n0) * hx * hy;
		  An += dot(n1, B1) * Gn(dr1, n1) * hx * hy;
		  At += cross(n0, B0) * G(dr0) * hx * hy;
		  At += cross(n1, B1) * G(dr1) * hx * hy;
		}
	      }
	      const StaticVector<3> &An_ = An;
	      const StaticVector<3> &At_ = At;
	      mesh_A.elem(i,j,k)[0] = 0;
	      mesh_A.elem(i,j,k)[1] = An_[0] + At_[0];
	      mesh_A.elem(i,j,k)[2] = An_[1] + At_[1];
	      mesh_A.elem(i,j,k)[3] = An_[2] + At_[2];
	    }
	  }
	}
      });
  }

  for (int i=0; i<Nx; i++) {
    for (int j=0; j<Ny; j++) {
      for (int k=0; k<Nz; k++) {
	if (isnan[k + Nz * (j + Ny * i)]) {
	  mesh_A.elem(i,j,k) = StaticVector<4>(GSL_NAN);
	}
      }
    }
  }
}

void Static_Magnetic_FieldMap::set_length(double length )  // accepts m
{
  const double z1_max = (mesh_A.size3()-1)*hz;
  if (length<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length*1e3;
    // if (z1>z1_max) z1 = z1_max;
  }
}

std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>> Static_Magnetic_FieldMap::get_field_jacobian(double x, double y, double z, double /* t */ ) // x,y,z [mm], returns V/m/mm, and T/mm
{
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(0.0, 0.0);
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(0.0, 0.0);
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_A.size1()-1;
  int height_ = mesh_A.size2()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_)
    return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(0.0, 0.0);
  const double inv_hxhx = 1.0/(hx*hx);
  const double inv_hxhy = 1.0/(hx*hy);
  const double inv_hxhz = 1.0/(hx*hz);
  const double inv_hyhy = 1.0/(hy*hy);
  const double inv_hyhz = 1.0/(hy*hz);
  const double inv_hzhz = 1.0/(hz*hz);
  const auto B = [&] () {
    const auto d2A_dxy = mesh_A.deriv2_xy(x,y,z)*inv_hxhy; // T/mm
    const auto d2A_dyz = mesh_A.deriv2_yz(x,y,z)*inv_hyhz; // T/mm
    const auto d2A_dxz = mesh_A.deriv2_xz(x,y,z)*inv_hxhz; // T/mm
    const auto d2A_dx2 = mesh_A.deriv2_x2(x,y,z)*inv_hxhx; // T/mm
    const auto d2A_dy2 = mesh_A.deriv2_y2(x,y,z)*inv_hyhy; // T/mm
    const auto d2A_dz2 = mesh_A.deriv2_z2(x,y,z)*inv_hzhz; // T/mm
    auto retval = StaticMatrix<3,3>(-d2A_dx2[0], -d2A_dxy[0], -d2A_dxz[0],
				    -d2A_dxy[0], -d2A_dy2[0], -d2A_dyz[0],
				    -d2A_dxz[0], -d2A_dyz[0], -d2A_dz2[0]); // T/mm
    retval += StaticMatrix<3,3>(d2A_dxy[3]-d2A_dxz[2], d2A_dy2[3]-d2A_dyz[2], d2A_dyz[3]-d2A_dz2[2],
				d2A_dxz[1]-d2A_dx2[3], d2A_dyz[1]-d2A_dxy[3], d2A_dz2[1]-d2A_dxz[3],
				d2A_dx2[2]-d2A_dxy[1], d2A_dxy[2]-d2A_dy2[1], d2A_dxz[2]-d2A_dyz[1]); // T/mm
    return retval;
  } ();
  return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(0, B);
}

std::pair<StaticVector<3>,StaticVector<3>> Static_Magnetic_FieldMap::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticVector<3>,StaticVector<3>>(0.0, 0.0);
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticVector<3>,StaticVector<3>>(0.0, 0.0);
  // Relic of a test: Idealization of a constant field, with the field gradient observed in the ELENA field map
  // x /= hx;
  // y /= hy;
  // double dBz_dy =  1.0 / 1e6 / 40.0;
  // double dBy_dy = -1.0 / 1e6 / 40.0;
  // return std::pair<StaticVector<3>,StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0, 0.05 / 1e4 + dBy_dy * y, -100 / 1e4 + dBz_dy * y)); 
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_A.size1()-1;
  int height_ = mesh_A.size2()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_)
    return std::pair<StaticVector<3>,StaticVector<3>>(0.0, 0.0); 
  const auto B = [&] () {
    auto retval = Bmean;
    const auto dA_dx = mesh_A.deriv_x(x,y,z)/hx; // T
    const auto dA_dy = mesh_A.deriv_y(x,y,z)/hy; // T
    const auto dA_dz = mesh_A.deriv_z(x,y,z)/hz; // T
    const auto dPhiM_dx = dA_dx[0]; // T
    const auto dPhiM_dy = dA_dy[0]; // T
    const auto dPhiM_dz = dA_dz[0]; // T
    retval -= StaticVector<3>(dPhiM_dx, dPhiM_dy, dPhiM_dz); // T
    const auto dAx_dy = dA_dy[1]; // T
    const auto dAx_dz = dA_dz[1]; // T
    const auto dAy_dx = dA_dx[2]; // T
    const auto dAy_dz = dA_dz[2]; // T
    const auto dAz_dx = dA_dx[3]; // T
    const auto dAz_dy = dA_dy[3]; // T
    retval += StaticVector<3>(dAz_dy-dAy_dz, dAx_dz-dAz_dx, dAy_dx-dAx_dy); // T
    return retval;
  } ();
  const auto E = StaticVector<3>(gsl_isnan(B[0]) ? GSL_NAN : 0);
  return std::pair<StaticVector<3>,StaticVector<3>>(E, B);
}

double Static_Magnetic_FieldMap::get_divB(double x /* mm */, double y /* mm */, double z /* mm */ ) const
{
  if (z<0.0 || z>(z1-z0)) // outside element
    return 0.0;
  z += z0;
  if (z<0.0 || z>z1)
    return 0.0;
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_A.size1()-1;
  int height_ = mesh_A.size2()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_)
    return 0.0;
  const double inv_hxhx = 1.0/(hx*hx);
  const double inv_hyhy = 1.0/(hy*hy);
  const double inv_hzhz = 1.0/(hz*hz);
  const auto d2PhiM_dx2 = mesh_A.deriv2_x2(x,y,z)[0]*inv_hxhx; // T/mm
  const auto d2PhiM_dy2 = mesh_A.deriv2_y2(x,y,z)[0]*inv_hyhy; // T/mm
  const auto d2PhiM_dz2 = mesh_A.deriv2_z2(x,y,z)[0]*inv_hzhz; // T/mm
  return - d2PhiM_dx2 - d2PhiM_dy2 - d2PhiM_dz2; // T/mm
}

Mesh3d Static_Magnetic_FieldMap::get_PhiM() const {
  Mesh3d mesh_PhiM(mesh_A.dims());
  for (size_t i=0; i<mesh_A.size1(); i++) {
    for (size_t j=0; j<mesh_A.size2(); j++) {
      for (size_t k=0; k<mesh_A.size3(); k++) {
	mesh_PhiM.elem(i,j,k) = mesh_A.elem(i,j,k)[0];
      }
    }
  }
  return mesh_PhiM;
}

Mesh3d Static_Magnetic_FieldMap::get_Ax() const {
  Mesh3d mesh_Ax(mesh_A.dims());
  for (size_t i=0; i<mesh_A.size1(); i++) {
    for (size_t j=0; j<mesh_A.size2(); j++) {
      for (size_t k=0; k<mesh_A.size3(); k++) {
	mesh_Ax.elem(i,j,k) = mesh_A.elem(i,j,k)[1];
      }
    }
  }
  return mesh_Ax;
}

Mesh3d Static_Magnetic_FieldMap::get_Ay() const {
  Mesh3d mesh_Ay(mesh_A.dims());
  for (size_t i=0; i<mesh_A.size1(); i++) {
    for (size_t j=0; j<mesh_A.size2(); j++) {
      for (size_t k=0; k<mesh_A.size3(); k++) {
	mesh_Ay.elem(i,j,k) = mesh_A.elem(i,j,k)[2];
      }
    }
  }
  return mesh_Ay;
}

Mesh3d Static_Magnetic_FieldMap::get_Az() const {
  Mesh3d mesh_Az(mesh_A.dims());
  for (size_t i=0; i<mesh_A.size1(); i++) {
    for (size_t j=0; j<mesh_A.size2(); j++) {
      for (size_t k=0; k<mesh_A.size3(); k++) {
	mesh_Az.elem(i,j,k) = mesh_A.elem(i,j,k)[3];
      }
    }
  }
  return mesh_Az;
}
