/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <fcntl.h>
#include <mutex>
#include "RF_Track.hh"
#include "external_field.hh"

ExternalField::ExternalField(double length_, /* m */
			     std::string external_cmd, /* string */
			     size_t n_of_threads ) : external_command(external_cmd),
						     length(length_),
						     index(0),
						     static_Efield{{{ 0.0, 0.0, 0.0 }}},
						     static_Bfield{{{ 0.0, 0.0, 0.0 }}}
{
  size_t n = n_of_threads>0 ? n_of_threads : RFT::number_of_threads;
  while (processes.size()<n) processes.push_back(_SubProcess(external_command.c_str()));
}

void ExternalField::set_number_of_threads(size_t n_of_threads )
{
  size_t n = n_of_threads>0 ? n_of_threads : RFT::number_of_threads;
  while (processes.size()>n) processes.pop_back();
  while (processes.size()<n) processes.push_back(_SubProcess(external_command.c_str()));
}

std::pair<StaticVector<3>,StaticVector<3>> ExternalField::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  static std::mutex mtx; // mutex for critical section
  mtx.lock();
  const size_t process_idx = index++;
  if (index==processes.size()) index = 0;
  mtx.unlock();
  auto &s = processes[process_idx];
  sprintf(s.sbuffer, "%.17f %.17f %.17f %.17f\n", x, y, z, t);
  s.process->send(s.sbuffer);
  s.process->recv(s.rbuffer, sizeof(s.rbuffer));
  double Ex, Ey, Ez, Bx, By, Bz;
  sscanf(s.rbuffer, "%lf %lf %lf %lf %lf %lf", &Ex, &Ey, &Ez, &Bx, &By, &Bz);
  return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield + StaticVector<3>(Ex, Ey, Ez), static_Bfield + StaticVector<3>(Bx, By, Bz));
}
