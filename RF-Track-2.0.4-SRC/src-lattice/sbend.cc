/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <complex>
#include "RF_Track.hh"
#include "sbend.hh"

SBend::SBend(double L, double a, double ref_P_over_Q, double E1_, double E2_ ) : length(L), angle(a), E1(E1_), E2(E2_), P_over_Q(ref_P_over_Q)
{
  k0 = h = length!=0.0 ? angle/length : 0.0; // 1/m
  k1 = 0.0;
  k1L = 0.0;
}

void SBend::track0_initialize(Bunch6d &bunch )
{
  const double t1 = tan(E1) * k0; // 1/m
  for (size_t i=0; i<bunch.size(); i++) {
    auto &particle = bunch[i];
    if (particle) {
      const double V = particle.get_beta(); // c
      particle.xp +=  t1 * particle.x; // mrad
      particle.yp -=  t1 * particle.y; // mrad
      particle.t  += -t1 * particle.x * particle.x / 1e3 / V; // mm/c, path length difference due to entrance angle
    }
  }
}

void SBend::track0_finalize(Bunch6d &bunch )
{
  const double t2 = tan(E2) * k0; // 1/m
  for (size_t i=0; i<bunch.size(); i++) {
    auto &particle = bunch[i];
    if (particle) {
      const double V = particle.get_beta(); // c
      particle.xp +=  t2 * particle.x; // mrad
      particle.yp -=  t2 * particle.y; // mrad
      particle.t  += -t2 * particle.x * particle.x / 1e3 / V; // mm/c, path length difference due to entrance angle
    }
  }
}

void SBend::track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t number_of_steps, size_t thread ) const
{
  if (length==0.0) {
    if (!is_particle_inside_aperture(particle)) {
      particle.lost_at(S);
      return;
    }
    const double P_Q = particle.Pc / particle.Q; // MV/c
    const double delta_plus_1 = P_Q / P_over_Q;
    const double fraction = double(int(end_step) - int(start_step)) / number_of_steps;
    const double angle_ = fraction * angle / delta_plus_1; // rad 
    const double k1L_ = fraction * k1L / delta_plus_1; // 1/m
    particle.xp += -k1L_ * particle.x + angle_; // mrad
    particle.yp +=  k1L_ * particle.y; // mrad
    return;
  }
  const double dS = length / number_of_steps; // m
  const double P_Q = particle.Pc / particle.Q; // MV/c
  const double delta_plus_1 = P_Q / P_over_Q;
  const double k0_ = k0 / delta_plus_1; // 1/m^2
  const double k1_ = k1 / delta_plus_1; // 1/m^2
  const double Kx = k0_*h + k1_; // 1/m^2
  const double Ky =       - k1_; // 1/m^2
  std::complex<double> sqrt_Kx, sqrt_Ky; // 1/m
  double Sx, Cx, Sy, Cy;
  if (Kx!=0.0) {
    sqrt_Kx = sqrt(std::complex<double>(Kx)); // 1/m
    Sx = real(sin(sqrt_Kx*dS) / sqrt_Kx); // m
    Cx = real(cos(sqrt_Kx*dS)); // 1
  } else {
    sqrt_Kx = 0.0; // 1/m
    Sx = dS; // m
    Cx = 1.0;
  }
  if (Ky!=0.0) {
    sqrt_Ky = sqrt(std::complex<double>(Ky)); // 1/m
    Sy = real(sin(sqrt_Ky*dS) / sqrt_Ky); // m
    Cy = real(cos(sqrt_Ky*dS)); // 1
  } else {
    sqrt_Ky = 0.0; // 1/m
    Sy = dS; // m
    Cy = 1.0; // 1
  }
  const double V = particle.get_beta(); // c
  auto sqr = [] (double x ) { return x*x; };
  for (size_t step=start_step; step<end_step; step++) {
    if (!is_particle_inside_aperture(particle)) {
      particle.lost_at(S + (double(step) + 0.5) * dS);
      break;
    }
    const double
      x  = particle.x / 1e3, // m
      y  = particle.y / 1e3, // m
      xp = particle.xp / 1e3, // rad
      yp = particle.yp / 1e3; // rad
    
    // useful constants
    const double A = -Kx*x-k0_+h; // 1/m
    const double B = xp;
    const double C = -Ky*y; // 1/m
    const double D = yp;
    
    // transverse map
    double x_ = x*Cx + xp*Sx + (h-k0_) * (Kx!=0.0 ? ((1.0-Cx)/Kx) : (0.5*sqr(dS)));
    double y_ = y*Cy + yp*Sy;
    double xp_ = A*Sx + B*Cx;
    double yp_ = C*Sy + D*Cy;
    
    // longitudinal map
    double dS_ = dS; // m, will be the total path length difference traveled by the particle
    if (Kx!=0.0) {
      dS_ += -(h*((Cx-1)*xp+Sx*A+dS*(k0_-h)))/Kx;
      dS_ += 0.5*(sqr(A)*(dS-Cx*Sx)/(2*Kx)+sqr(B)*(dS+Cx*Sx)/2+A*B*(1-sqr(Cx))/Kx);
    } else {
      dS_ += h*dS*(3*dS*xp+6*x-(k0_-h)*sqr(dS))/6;
      dS_ += 0.5*(sqr(B)+sqr(A*dS)/3+A*B*dS)*dS;
    }
    if (Ky!=0.0) {
      dS_ += 0.5*(sqr(C)*(dS-Cy*Sy)/(2*Ky)+sqr(D)*(dS+Cy*Sy)/2+C*D*(1-sqr(Cy))/Ky);
    } else {
      dS_ += 0.5*sqr(D)*dS;
    }
    particle.x  = x_ * 1e3; // mm
    particle.y  = y_ * 1e3; // mm
    particle.xp = xp_ * 1e3; // mrad
    particle.yp = yp_ * 1e3; // mrad
    particle.t += dS_ * 1e3 / V; // mm/c
  }
}
