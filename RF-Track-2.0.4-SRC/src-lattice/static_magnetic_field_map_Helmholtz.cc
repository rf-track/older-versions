/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>
#include <gsl/gsl_sys.h>

#include "static_magnetic_field_map.hh"
#include "RF_Track.hh"
#include "numtools.hh"
#include "constants.hh"
#include "move_particle.hh"

StaticMagneticField::StaticMagneticField(const Mesh3d &Phi,
			 const Mesh3d &Ax,
			 const Mesh3d &Ay,
			 const Mesh3d &Az,
			 double x0_, double y0_, /*, double z0_, */ // m
			 double hx_, double hy_, double hz_, // m
			 double length ) : mesh_Phi(Phi),
					   mesh_Ax(Ax),
					   mesh_Ay(Ay),
					   mesh_Az(Az),
					   mesh_PhiM(Ax.size1(), Ax.size2(), Ax.size3(), 0.0),
					   E0(0.0, 0.0, 0.0),
					   B0(0.0, 0.0, 0.0),
					   x0(x0_*1e3), // accepts m, stores mm
					   y0(y0_*1e3), // accepts m, stores mm
					   z0(0.0), // mm
					   hx(hx_*1e3), // accepts m, stores mm
					   hy(hy_*1e3), // accepts m, stores mm
					   hz(hz_*1e3), // accepts m, stores mm
					   use_Phi(true),
					   use_A(true),
					   use_PhiM(true)
{
  set_nsteps(mesh_Phi.size3()-1);
  set_length(length);
}

StaticMagneticField::StaticMagneticField(const Mesh3d &Ex,
			 const Mesh3d &Ey,
			 const Mesh3d &Ez,
			 const Mesh3d &Bx,
			 const Mesh3d &By,
			 const Mesh3d &Bz,
			 double x0_, double y0_, /*, double z0_, */ // m
			 double hx_, double hy_, double hz_, // m
			 double length,
			 double quality ) : x0(x0_*1e3), // accepts m, stores mm
					    y0(y0_*1e3), // accepts m, stores mm
					    z0(0.0), // mm
					    hx(hx_*1e3), // accepts m, stores mm
					    hy(hy_*1e3), // accepts m, stores mm
					    hz(hz_*1e3), // accepts m, stores mm
					    use_Phi(true),
					    use_A(true),
					    use_PhiM(true)
{
  set_Ex_Ey_Ez(Ex,Ey,Ez, quality);
  set_Bx_By_Bz(Bx,By,Bz, quality);
  set_nsteps(Ex.size3()-1);
  set_length(length);
}

void StaticMagneticField::set_Ex_Ey_Ez(const Mesh3d &Ex, const Mesh3d &Ey, const Mesh3d &Ez, double quality )
{
  enable_Phi();
  
  const int
    Nx = Ex.size1(),
    Ny = Ex.size2(),
    Nz = Ex.size3();

  mesh_Phi.resize(Nx,Ny,Nz);

  fftwComplexMesh3d    mesh_Ez_hat(Nx,Ny,Nz/2+1); // Fourier transform of E component
  TMesh3d<fftwComplex> mesh_Ex_hat(Nx,Ny,Nz/2+1);
  TMesh3d<fftwComplex> mesh_Ey_hat(Nx,Ny,Nz/2+1);
  
  // initialize the average field
  E0 = [&] () {
    StaticVector<3> average;
    CumulativeKahanSum<double> sum;
    sum = 0.0; for (size_t i=0; i<Ex.size(); i++) { sum += Ex.data()[i]; } average[0] = sum / Ex.size();
    sum = 0.0; for (size_t i=0; i<Ey.size(); i++) { sum += Ey.data()[i]; } average[1] = sum / Ex.size();
    sum = 0.0; for (size_t i=0; i<Ez.size(); i++) { sum += Ez.data()[i]; } average[2] = sum / Ex.size();
    return average;
  } ();
  
  // create fftw plans
  const size_t Nthreads = RFT::number_of_threads;
  fftw_plan_with_nthreads(Nthreads);
  fftw_plan p1, p2;
  if ((p1 = fftw_plan_dft_r2c_3d(Nx, Ny, Nz, mesh_Phi.data(), (fftw_complex *)mesh_Ez_hat.data(), FFTW_ESTIMATE))) {
    if ((p2 = fftw_plan_dft_c2r_3d(Nx, Ny, Nz, (fftw_complex *)mesh_Ez_hat.data(), mesh_Phi.data(), FFTW_ESTIMATE))) {

      // Fourier transform of Ex, Ey, and Ez
      mesh_Phi = Ex; fftw_execute(p1); mesh_Ex_hat = mesh_Ez_hat;
      mesh_Phi = Ey; fftw_execute(p1); mesh_Ey_hat = mesh_Ez_hat;
      mesh_Phi = Ez; fftw_execute(p1);

      // imposes div(E) == 0, stores the result in mesh_Ez_hat
      {
	const double fx_cut = quality * 2.0*M_PI * 0.5 / hx; // quality * Nyquist frequency
	const double fy_cut = quality * 2.0*M_PI * 0.5 / hy; // quality * Nyquist frequency
	const double fz_cut = quality * 2.0*M_PI * 0.5 / hz; // quality * Nyquist frequency
	for (int i=0; i<Nx; i++) {
	  StaticVector<3> k_vec;
	  k_vec[0] = 2.0*M_PI * double((2*i<=Nx) ? i : (i-Nx)) / (Nx*hx); // 1/mm
	  for (int j=0; j<Ny; j++) {
	    k_vec[1] = 2.0*M_PI * double((2*j<=Ny) ? j : (j-Ny)) / (Ny*hy); // 1/mm
	    for (int k=0; k<Nz/2+1; k++) {
	      k_vec[2] = 2.0*M_PI * double((2*k<=Nz) ? k : (k-Nz)) / (Nz*hz); // 1/mm
	      if (i!=0 || j!=0 || k!=0) {
		if (fabs(k_vec[0]) <= fx_cut &&
		    fabs(k_vec[1]) <= fy_cut &&
		    fabs(k_vec[2]) <= fz_cut) {
		  StaticVector<3,fftwComplex> E_hat;
		  E_hat[0] = mesh_Ex_hat.elem(i,j,k); // V/m/mm
		  E_hat[1] = mesh_Ey_hat.elem(i,j,k); // V/m/mm
		  E_hat[2] = mesh_Ez_hat.elem(i,j,k); // V/m/mm
		  mesh_Ez_hat.elem(i,j,k) = fftwComplex(0.0,1.0) * dot(k_vec,E_hat) / dot(k_vec,k_vec); // V/m/mm*mm^2 = V/m*mm
		} else {
		  mesh_Ez_hat.elem(i,j,k) = 0.0;
		}
	      } else {
		mesh_Ez_hat.elem(i,j,k) = 0.0;
	      }
	    }
	  }
	}
      }
      
      // anti-transform Ez_hat (Phi_hat) to find mesh_Phi
      fftw_execute(p2);
      mesh_Phi /= Nx*Ny*Nz; // V/m*mm
      
      fftw_destroy_plan(p2);
    }
    fftw_destroy_plan(p1);
  }
}

void StaticMagneticField::set_Bx_By_Bz(const Mesh3d &Bx, const Mesh3d &By, const Mesh3d &Bz, double quality )
{
  enable_A();
  enable_PhiM();

  const int
    Nx = Bx.size1(),
    Ny = Bx.size2(),
    Nz = Bx.size3();
   
  mesh_Ax.resize(Nx,Ny,Nz);
  mesh_Ay.resize(Nx,Ny,Nz);
  mesh_Az.resize(Nx,Ny,Nz);
  mesh_PhiM.resize(Nx,Ny,Nz);
   
  fftwComplexMesh3d mesh_Bx_hat(Nx,Ny,Nz/2+1); // Fourier transform of Ax component
  fftwComplexMesh3d mesh_By_hat(Nx,Ny,Nz/2+1); // Fourier transform of Ay component
  fftwComplexMesh3d mesh_Bz_hat(Nx,Ny,Nz/2+1); // Fourier transform of Az component
  fftwComplexMesh3d mesh_PhiM_hat(Nx,Ny,Nz/2+1); // Fourier transform of PhiM component
   
  // initialize the average field
  B0 = [&] () {
    StaticVector<3> average;
    CumulativeKahanSum<double> sum;
    sum = 0.0; for (size_t i=0; i<Bx.size(); i++) { sum += Bx.data()[i]; } average[0] = sum / Bx.size();
    sum = 0.0; for (size_t i=0; i<By.size(); i++) { sum += By.data()[i]; } average[1] = sum / By.size();
    sum = 0.0; for (size_t i=0; i<Bz.size(); i++) { sum += Bz.data()[i]; } average[2] = sum / Bz.size();
    return average;
  } (); 
  
  // create fftw plans
  const size_t Nthreads = RFT::number_of_threads;
  fftw_plan_with_nthreads(Nthreads);
  fftw_plan p1, p2, p3, p4, p5, p6, p7;
  if ((p1 = fftw_plan_dft_r2c_3d(Nx, Ny, Nz, mesh_Ax.data(), (fftw_complex *)mesh_Bx_hat.data(), FFTW_ESTIMATE))) {
    if ((p2 = fftw_plan_dft_r2c_3d(Nx, Ny, Nz, mesh_Ay.data(), (fftw_complex *)mesh_By_hat.data(), FFTW_ESTIMATE))) {
      if ((p3 = fftw_plan_dft_r2c_3d(Nx, Ny, Nz, mesh_Az.data(), (fftw_complex *)mesh_Bz_hat.data(), FFTW_ESTIMATE))) {
	if ((p4 = fftw_plan_dft_c2r_3d(Nx, Ny, Nz, (fftw_complex *)mesh_Bx_hat.data(), mesh_Ax.data(), FFTW_ESTIMATE))) {
	  if ((p5 = fftw_plan_dft_c2r_3d(Nx, Ny, Nz, (fftw_complex *)mesh_By_hat.data(), mesh_Ay.data(), FFTW_ESTIMATE))) {
	    if ((p6 = fftw_plan_dft_c2r_3d(Nx, Ny, Nz, (fftw_complex *)mesh_Bz_hat.data(), mesh_Az.data(), FFTW_ESTIMATE))) {
	      if ((p7 = fftw_plan_dft_c2r_3d(Nx, Ny, Nz, (fftw_complex *)mesh_PhiM_hat.data(), mesh_PhiM.data(), FFTW_ESTIMATE))) {
		 
		// Fourier transform Bx, By, and Bz
		mesh_Ax = Bx;
		mesh_Ay = By;
		mesh_Az = Bz;
		 
		fftw_execute(p1);
		fftw_execute(p2);
		fftw_execute(p3);
		 
		// imposes div(B) == 0
		{
		  const double fx_cut = quality * 2.0*M_PI * 0.5 / hx; // quality * Nyquist frequency
		  const double fy_cut = quality * 2.0*M_PI * 0.5 / hy; // quality * Nyquist frequency
		  const double fz_cut = quality * 2.0*M_PI * 0.5 / hz; // quality * Nyquist frequency
		  for (int i=0; i<Nx; i++) {
		    StaticVector<3> k_vec;
		    k_vec[0] = 2.0*M_PI * double((2*i<=Nx) ? i : (i-Nx)) / (Nx*hx); // 1/mm
		    for (int j=0; j<Ny; j++) {
		      k_vec[1] = 2.0*M_PI * double((2*j<=Ny) ? j : (j-Ny)) / (Ny*hy); // 1/mm
		      for (int k=0; k<Nz/2+1; k++) {
			k_vec[2] = 2.0*M_PI * double((2*k<=Nz) ? k : (k-Nz)) / (Nz*hz); // 1/mm
			if (i!=0 || j!=0 || k!=0) {
			  if (fabs(k_vec[0]) <= fx_cut &&
			      fabs(k_vec[1]) <= fy_cut &&
			      fabs(k_vec[2]) <= fz_cut) {
			    const double inv_k_vec_norm_sqr = 1.0/dot(k_vec,k_vec); // mm^2
			    StaticVector<3,fftwComplex> B_hat;
			    B_hat[0] = mesh_Bx_hat.elem(i,j,k); // T
			    B_hat[1] = mesh_By_hat.elem(i,j,k); // T
			    B_hat[2] = mesh_Bz_hat.elem(i,j,k); // T
			    StaticVector<3,fftwComplex> A_hat = fftwComplex(0.0,1.0) * cross(k_vec,B_hat) * inv_k_vec_norm_sqr; // T*mm;
			    A_hat -= k_vec * dot(k_vec,A_hat) * inv_k_vec_norm_sqr; // T*mm, removes any radial component (i.e. imposes div(A) == 0)
			    mesh_Bx_hat.elem(i,j,k) = A_hat[0]; // T*mm
			    mesh_By_hat.elem(i,j,k) = A_hat[1]; // T*mm
			    mesh_Bz_hat.elem(i,j,k) = A_hat[2]; // T*mm
			    mesh_PhiM_hat.elem(i,j,k) = fftwComplex(0.0,1.0) * dot(k_vec,B_hat) * inv_k_vec_norm_sqr; // T*mm
			  } else {
			    mesh_Bx_hat.elem(i,j,k) = 0.0;
			    mesh_By_hat.elem(i,j,k) = 0.0;
			    mesh_Bz_hat.elem(i,j,k) = 0.0;
			    mesh_PhiM_hat.elem(i,j,k) = 0.0;
			  }
			} else {
			  mesh_Bx_hat.elem(i,j,k) = 0.0;
			  mesh_By_hat.elem(i,j,k) = 0.0;
			  mesh_Bz_hat.elem(i,j,k) = 0.0;
			  mesh_PhiM_hat.elem(i,j,k) = 0.0;
			}
		      }
		    }
		  }
		}
		 
		// anti-transform Bxyz_hat (mesh_Axyz)
		fftw_execute(p4);
		fftw_execute(p5);
		fftw_execute(p6);
		fftw_execute(p7);
		{
		  const double scale = Nx*Ny*Nz;
		  mesh_Ax /= scale; // T*mm
		  mesh_Ay /= scale; // T*mm
		  mesh_Az /= scale; // T*mm
		  mesh_PhiM /= scale; // T*mm
		}
		
		fftw_destroy_plan(p7);
	      }
	      fftw_destroy_plan(p6);
	    }
	    fftw_destroy_plan(p5);
	  }
	  fftw_destroy_plan(p4);
	}
	fftw_destroy_plan(p3);
      }
      fftw_destroy_plan(p2);
    }
    fftw_destroy_plan(p1);
  }
}

void StaticMagneticField::set_length(double length )  // accepts m
{
  const double z1_max = (mesh_Phi.size3()-1)*hz;
  if (length<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length*1e3;
    // if (z1>z1_max) z1 = z1_max;
  }
}

std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>> StaticMagneticField::get_field_jacobian(double x, double y, double z, double /* t */ ) // x,y,z [mm], returns V/m/mm, and T/mm
{
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(StaticMatrix<3,3>(0.0), StaticMatrix<3,3>(0.0));
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(StaticMatrix<3,3>(0.0), StaticMatrix<3,3>(0.0));
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Phi.size1()-1;
  int height_ = mesh_Phi.size2()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_)
    return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(StaticMatrix<3,3>(0.0), StaticMatrix<3,3>(0.0));
  const double inv_hxhx = 1.0/(hx*hx);
  const double inv_hxhy = 1.0/(hx*hy);
  const double inv_hxhz = 1.0/(hx*hz);
  const double inv_hyhy = 1.0/(hy*hy);
  const double inv_hyhz = 1.0/(hy*hz);
  const double inv_hzhz = 1.0/(hz*hz);
  const auto E = use_Phi ? StaticMatrix<3,3>(-mesh_Phi.deriv2_x2(x,y,z)*inv_hxhx, -mesh_Phi.deriv2_xy(x,y,z)*inv_hxhy, -mesh_Phi.deriv2_xz(x,y,z)*inv_hxhz,
					     -mesh_Phi.deriv2_xy(x,y,z)*inv_hxhy, -mesh_Phi.deriv2_y2(x,y,z)*inv_hyhy, -mesh_Phi.deriv2_yz(x,y,z)*inv_hyhz,
					     -mesh_Phi.deriv2_xz(x,y,z)*inv_hxhz, -mesh_Phi.deriv2_yz(x,y,z)*inv_hyhz, -mesh_Phi.deriv2_z2(x,y,z)*inv_hzhz) : StaticMatrix<3,3>(0.0); // V/m/mm
  const auto B = [&] () {
    auto retval = use_PhiM ? StaticMatrix<3,3>(-mesh_PhiM.deriv2_x2(x,y,z)*inv_hxhx, -mesh_PhiM.deriv2_xy(x,y,z)*inv_hxhy, -mesh_PhiM.deriv2_xz(x,y,z)*inv_hxhz,
					       -mesh_PhiM.deriv2_xy(x,y,z)*inv_hxhy, -mesh_PhiM.deriv2_y2(x,y,z)*inv_hyhy, -mesh_PhiM.deriv2_yz(x,y,z)*inv_hyhz,
					       -mesh_PhiM.deriv2_xz(x,y,z)*inv_hxhz, -mesh_PhiM.deriv2_yz(x,y,z)*inv_hyhz, -mesh_PhiM.deriv2_z2(x,y,z)*inv_hzhz) : StaticMatrix<3,3>(0.0); // T/mm
    if (use_A) {
      const auto d2Ax_dxy = mesh_Ax.deriv2_xy(x,y,z)*inv_hxhy; // T/mm
      const auto d2Ax_dyz = mesh_Ax.deriv2_yz(x,y,z)*inv_hyhz; // T/mm
      const auto d2Ax_dxz = mesh_Ax.deriv2_xz(x,y,z)*inv_hxhz; // T/mm
      const auto d2Ax_dy2 = mesh_Ax.deriv2_y2(x,y,z)*inv_hyhy; // T/mm
      const auto d2Ax_dz2 = mesh_Ax.deriv2_z2(x,y,z)*inv_hzhz; // T/mm
      const auto d2Ay_dxy = mesh_Ay.deriv2_xy(x,y,z)*inv_hxhy; // T/mm
      const auto d2Ay_dxz = mesh_Ay.deriv2_xz(x,y,z)*inv_hxhz; // T/mm
      const auto d2Ay_dyz = mesh_Ay.deriv2_yz(x,y,z)*inv_hyhz; // T/mm
      const auto d2Ay_dx2 = mesh_Ay.deriv2_x2(x,y,z)*inv_hxhx; // T/mm
      const auto d2Ay_dz2 = mesh_Ay.deriv2_z2(x,y,z)*inv_hzhz; // T/mm
      const auto d2Az_dxy = mesh_Az.deriv2_xy(x,y,z)*inv_hxhy; // T/mm
      const auto d2Az_dxz = mesh_Az.deriv2_xz(x,y,z)*inv_hxhz; // T/mm
      const auto d2Az_dyz = mesh_Az.deriv2_yz(x,y,z)*inv_hyhz; // T/mm
      const auto d2Az_dx2 = mesh_Az.deriv2_x2(x,y,z)*inv_hxhx; // T/mm
      const auto d2Az_dy2 = mesh_Az.deriv2_y2(x,y,z)*inv_hyhy; // T/mm
      retval += StaticMatrix<3,3>(d2Az_dxy-d2Ay_dxz, d2Az_dy2-d2Ay_dyz, d2Az_dyz-d2Ay_dz2,
				  d2Ax_dxz-d2Az_dx2, d2Ax_dyz-d2Az_dxy, d2Ax_dz2-d2Az_dxz,
				  d2Ay_dx2-d2Ax_dxy, d2Ay_dxy-d2Ax_dy2, d2Ay_dxz-d2Ax_dyz); // T/mm
    }
    return retval;
  } ();
  return std::pair<StaticMatrix<3,3>,StaticMatrix<3,3>>(E, B);
}

std::pair<StaticVector<3>,StaticVector<3>> StaticMagneticField::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticVector<3>,StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0)); 
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticVector<3>,StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0)); 
  // Relic of a test: Idealization of a constant field, with the field gradient observed in the ELENA field map
  // x /= hx;
  // y /= hy;
  // double dBz_dy =  1.0 / 1e6 / 40.0;
  // double dBy_dy = -1.0 / 1e6 / 40.0;
  // return std::pair<StaticVector<3>,StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0, 0.05 / 1e4 + dBy_dy * y, -100 / 1e4 + dBz_dy * y)); 
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Phi.size1()-1;
  int height_ = mesh_Phi.size2()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_)
    return std::pair<StaticVector<3>,StaticVector<3>>(StaticVector<3>(0.0), StaticVector<3>(0.0)); 
  const auto E = [&] () {
    auto retval = E0;
    if (use_Phi) {
      const auto dPhi_dx = mesh_Phi.deriv_x(x,y,z)/hx; // V/m*mm/mm
      const auto dPhi_dy = mesh_Phi.deriv_y(x,y,z)/hy; // V/m*mm/mm
      const auto dPhi_dz = mesh_Phi.deriv_z(x,y,z)/hz; // V/m*mm/mm
      retval -= StaticVector<3>(dPhi_dx, dPhi_dy, dPhi_dz); // V/m
    }
    return retval;
  } ();
  const auto B = [&] () {
    auto retval = B0;
    if (use_PhiM) {
      const auto dPhiM_dx = mesh_PhiM.deriv_x(x,y,z)/hx; // T
      const auto dPhiM_dy = mesh_PhiM.deriv_y(x,y,z)/hy; // T
      const auto dPhiM_dz = mesh_PhiM.deriv_z(x,y,z)/hz; // T
      retval -= StaticVector<3>(dPhiM_dx, dPhiM_dy, dPhiM_dz); // T
    }
    if (use_A) {
      const auto dAx_dy = mesh_Ax.deriv_y(x,y,z)/hy; // T
      const auto dAx_dz = mesh_Ax.deriv_z(x,y,z)/hz; // T
      const auto dAy_dx = mesh_Ay.deriv_x(x,y,z)/hx; // T
      const auto dAy_dz = mesh_Ay.deriv_z(x,y,z)/hz; // T
      const auto dAz_dx = mesh_Az.deriv_x(x,y,z)/hx; // T
      const auto dAz_dy = mesh_Az.deriv_y(x,y,z)/hy; // T
      retval += StaticVector<3>(dAz_dy-dAy_dz, dAx_dz-dAz_dx, dAy_dx-dAx_dy); // T
    }
    return retval;
  } ();
  return std::pair<StaticVector<3>,StaticVector<3>>(E, B);
}

struct Params {
  double mass;
  double charge;
  StaticMagneticField *rf_field;
};

static int func(double S /* m */, const double Y[], double dY[], void *params_ )
{
  // init parameters
  const Params *params = (const Params *)(params_);
  StaticMagneticField *this_ = params->rf_field;

  // check aperture
  if (!this_->is_point_inside_aperture(Y[0], Y[1]))
    return GSL_EBADFUNC;

  // compute force
  const auto field = this_->get_field(Y[0], Y[1], S*1e3, Y[2]);
  if (gsl_isnan(field.first[0])) // NaNs indicate walls
    return GSL_EBADFUNC;

  // init variables
  const double mass = params->mass;
  const double charge = params->charge;
  const auto &P = StaticVector<3>(Y[3], Y[4], Y[5]); // MeV/c
  const auto &v = P / hypot(mass,P); // c

  const auto &Efield = field.first; // V/m
  const auto &Bfield = field.second * C_LIGHT; // T*c = V/m
  const auto &F = charge * (Efield + cross(v,Bfield)) / 1e6; // MeV/m
  
  // update system of equations for GSL
  const double inv_Pz = 1e3 / P[2]; // 1e3/(MeV/c)
  const double inv_Vz = 1.0 / v[2]; // 1/c
  dY[0] = P[0] * inv_Pz; // mrad
  dY[1] = P[1] * inv_Pz; // mrad
  dY[2] = 1e3  * inv_Vz; // 1e3/c
  dY[3] = F[0] * inv_Vz; // MeV/m/c
  dY[4] = F[1] * inv_Vz; // MeV/m/c
  dY[5] = F[2] * inv_Vz; // MeV/m/c
  return GSL_SUCCESS;
}

static int jac(double S /* m */, const double Y[], double *dfdy, 
	       double dfdt[], void *params_ )
{
  // init parameters
  const Params *params = (const Params *)(params_);
  StaticMagneticField *this_ = params->rf_field;

  if (!this_->is_point_inside_aperture(Y[0], Y[1]))
    return GSL_EBADFUNC;

  const double mass = params->mass;
  const double charge = params->charge;

  // init variables
  const auto &P = StaticVector<3>(Y[3], Y[4], Y[5]); // MeV/c
  const auto &E = hypot(mass,P); // MeV
  const auto &v = P / E; // c

  // compute force
  const auto &field = this_->get_field(Y[0], Y[1], S*1e3, Y[2]);
  if (gsl_isnan(field.first[0])) // in a consistent map if one component of E is Nan, then all other components of E and of B are Nan too
    return GSL_EBADFUNC;
  const auto &Ef = field.first / 1e6; // V/m -> MV/m
  const auto &Bf = field.second * C_LIGHT / 1e6; // T*c = V/m -> MV/m
  const auto &F = charge * (Ef + cross(v, Bf)); // MeV/m
  const auto &a = (F - dot(v,F)*v) / E; // c^2/m

  // dfdt
  const double inv_Vz = 1.0 / v[2];
  const double inv_Pz = 1.0 / P[2];
  const double xp = P[0] * inv_Pz; // rad
  const double yp = P[1] * inv_Pz; // rad
  dfdt[0] = 1e3 * (a[0] - xp * a[2]) * inv_Vz * inv_Vz; // mrad
  dfdt[1] = 1e3 * (a[1] - yp * a[2]) * inv_Vz * inv_Vz; // mrad
  dfdt[2] = -a[2] * inv_Vz * inv_Vz * inv_Vz; // 1/c
  dfdt[3] = F[0] * dfdt[2]; // MeV/m/c
  dfdt[4] = F[1] * dfdt[2]; // MeV/m/c
  dfdt[5] = F[2] * dfdt[2]; // MeV/m/c
  dfdt[2] *= 1e3; // 1000/c
  
  // jacobian of force with Y
  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, 6, 6);
  gsl_matrix *m = &dfdy_mat.matrix;
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      gsl_matrix_set (m, i, j, 0.0); // dVi / dj
    }
  }
  auto &&jacobian = this_->get_field_jacobian(Y[0], Y[1], S*1e3, Y[2]);
  jacobian.first /= 1e6; // V/m/mm -> MV/m/mm
  jacobian.second *= C_LIGHT / 1e6; // T/mm*c = V/m/mm -> MV/m/mm
  const auto &Ef_jac = jacobian.first; // V/m/mm -> MV/m/mm
  const auto &Bf_jac = jacobian.second; // T/mm*c = V/m/mm -> MV/m/mm
  gsl_matrix_set (m, 3, 0, charge * (Ef_jac[0][0] * inv_Vz + yp * Bf_jac[2][0]        - Bf_jac[1][0])); // MeV/m/c/mm
  gsl_matrix_set (m, 3, 1, charge * (Ef_jac[0][1] * inv_Vz + yp * Bf_jac[2][1]        - Bf_jac[1][1])); // MeV/m/c/mm
  gsl_matrix_set (m, 3, 2, charge * (Ef_jac[0][2] +        v[1] * Bf_jac[2][2] - v[2] * Bf_jac[1][2])); // MeV/m/mm
  gsl_matrix_set (m, 4, 0, charge * (Ef_jac[1][0] * inv_Vz + Bf_jac[0][0] -   xp * Bf_jac[2][0])); // MeV/m/c/mm
  gsl_matrix_set (m, 4, 1, charge * (Ef_jac[1][1] * inv_Vz + Bf_jac[0][1] -   xp * Bf_jac[2][1])); // MeV/m/c/mm
  gsl_matrix_set (m, 4, 2, charge * (Ef_jac[1][2] +   v[2] * Bf_jac[0][2] - v[0] * Bf_jac[2][2])); // MeV/m/mm
  gsl_matrix_set (m, 5, 0, charge * (Ef_jac[2][0] * inv_Vz + xp * Bf_jac[1][0] -   yp * Bf_jac[0][0])); // MeV/m/c/mm
  gsl_matrix_set (m, 5, 1, charge * (Ef_jac[2][1] * inv_Vz + xp * Bf_jac[1][1] -   yp * Bf_jac[0][1])); // MeV/m/c/mm
  gsl_matrix_set (m, 5, 2, charge * (Ef_jac[2][2] +        v[0] * Bf_jac[1][2] - v[1] * Bf_jac[0][2])); // MeV/m/mm
  
  const double inv_E = 1.0 / E;
  gsl_matrix_set (m, 0, 3, inv_Pz); // dxp / dj
  gsl_matrix_set (m, 0, 4, 0.0); // dxp / dj
  gsl_matrix_set (m, 0, 5, -xp * inv_Pz); // dxp / dj
  gsl_matrix_set (m, 1, 3, 0.0); // dyp / dj
  gsl_matrix_set (m, 1, 4, inv_Pz); // dyp / dj
  gsl_matrix_set (m, 1, 5, -yp * inv_Pz); // dyp / dj
  gsl_matrix_set (m, 2, 3, xp * inv_E); // dyp / dj
  gsl_matrix_set (m, 2, 4, yp * inv_E); // dyp / dj
  gsl_matrix_set (m, 2, 5, (inv_E - inv_Pz * inv_Vz)); // dyp / dj
  gsl_matrix_set (m, 3, 3, charge * (Ef[0] * gsl_matrix_get(m, 2, 3) + Bf[2] * gsl_matrix_get(m, 1, 3)));
  gsl_matrix_set (m, 3, 4, charge * (Ef[0] * gsl_matrix_get(m, 2, 4) + Bf[2] * gsl_matrix_get(m, 1, 4)));
  gsl_matrix_set (m, 3, 5, charge * (Ef[0] * gsl_matrix_get(m, 2, 5) + Bf[2] * gsl_matrix_get(m, 1, 5)));
  gsl_matrix_set (m, 4, 3, charge * (Ef[1] * gsl_matrix_get(m, 2, 3) - Bf[2] * gsl_matrix_get(m, 0, 3)));
  gsl_matrix_set (m, 4, 4, charge * (Ef[1] * gsl_matrix_get(m, 2, 4) - Bf[2] * gsl_matrix_get(m, 0, 4)));
  gsl_matrix_set (m, 4, 5, charge * (Ef[1] * gsl_matrix_get(m, 2, 5) - Bf[2] * gsl_matrix_get(m, 0, 5)));
  gsl_matrix_set (m, 5, 3, charge * (Ef[2] * gsl_matrix_get(m, 2, 3) + Bf[1] * gsl_matrix_get(m, 0, 3) - Bf[0] * gsl_matrix_get(m, 1, 3)));
  gsl_matrix_set (m, 5, 4, charge * (Ef[2] * gsl_matrix_get(m, 2, 4) + Bf[1] * gsl_matrix_get(m, 0, 4) - Bf[0] * gsl_matrix_get(m, 1, 4)));
  gsl_matrix_set (m, 5, 5, charge * (Ef[2] * gsl_matrix_get(m, 2, 5) + Bf[1] * gsl_matrix_get(m, 0, 5) - Bf[0] * gsl_matrix_get(m, 1, 5)));
  gsl_matrix_set (m, 0, 3, 1e3 * gsl_matrix_get (m, 0, 3));
  gsl_matrix_set (m, 0, 5, 1e3 * gsl_matrix_get (m, 0, 5));
  gsl_matrix_set (m, 1, 4, 1e3 * gsl_matrix_get (m, 1, 4));
  gsl_matrix_set (m, 1, 5, 1e3 * gsl_matrix_get (m, 1, 5));
  gsl_matrix_set (m, 2, 3, 1e3 * gsl_matrix_get (m, 2, 3));
  gsl_matrix_set (m, 2, 4, 1e3 * gsl_matrix_get (m, 2, 4));
  gsl_matrix_set (m, 2, 5, 1e3 * gsl_matrix_get (m, 2, 5));
  return GSL_SUCCESS;
}

void StaticMagneticField::track0_initialize(Bunch6d &bunch )
{
  // init GSL ODE
  sys.resize(RFT::number_of_threads);
  for (size_t i=0; i<RFT::number_of_threads; i++) {
    sys[i].function = func;
    sys[i].jacobian = jac;
    sys[i].dimension = 6;
    sys[i].params = new Params({ 0.0, 0.0, this });
  }
  if (use_gsl())
    init_gsl_drivers(sys);
  gsl_error = false;
}

void StaticMagneticField::track0_finalize()
{
  if (gsl_error)
    std::cerr << "warning: an error occurred integrating the equations of motion, consider increasing 'nsteps'\n";

  for (auto s: sys) {
    delete (Params*)(s.params);
  }

  if (use_gsl())
    free_gsl_drivers();
}

void StaticMagneticField::track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t thread ) const
{
  Params *params = (Params *)(sys[thread].params);
  params->mass = particle.mass;
  params->charge = particle.Q;
  const double dS = get_length() / nsteps; // m
  double S_initial; // m
  int status = GSL_SUCCESS;
  if (use_analytic()) { // use analytic
    for (size_t step = start_step; step<end_step; step++) {
      S_initial = step * dS; // m
      if (!is_particle_inside_aperture(particle)) {
	status = GSL_EBADFUNC;
	break;
      }
      const auto &field = const_cast<StaticMagneticField*>(this)->get_field(particle.x, particle.y, S_initial*1e3, particle.t);
      if (gsl_isnan(field.first[0])) { // in a consistent map if one component is Nan then all other components are Nan too
	status = GSL_EBADFUNC;
	break;
      }
      const auto &Ef = field.first; // V/m
      const auto &Bf = field.second; // T
      move_particle_through_EBfield(particle, Ef, Bf, dS*1e3);
    }    
  } else if (use_leapfrog()) { // use leapfrog
    double dY[6], Y[6];
    {
      const auto P = particle.get_Px_Py_Pz(); // momentum
      Y[0] = particle.x; // mm
      Y[1] = particle.y; // mm
      Y[2] = particle.t; // mm/c
      Y[3] = P[0]; // MeV/c
      Y[4] = P[1]; // MeV/c
      Y[5] = P[2]; // MeV/c
    }
    for (size_t step = start_step; step<end_step; step++) {
      S_initial = step * dS;
      if ((status = func(S_initial, Y, dY, sys[thread].params)) == GSL_EBADFUNC) {
	break;
      }
      const double Vz = 1.0 / dY[2]; // c/1e3
      const auto F = StaticVector<3>(dY[3], dY[4], dY[5]) * Vz; // MeV/mm
      const auto v = StaticVector<3>(dY[0] * Vz, dY[1] * Vz, Vz * 1e3); // c
      const auto a = (F - dot(v,F)*v) / particle.get_total_energy(); // c^2/mm
      double dt_mm = dS * dY[2]; // mm/c
      if (a[2]>2*std::numeric_limits<double>::epsilon()*v[2]*dt_mm)
	dt_mm = (sqrt(v[2]*v[2] + 2e3*a[2]*dS) - v[2]) / a[2]; // mm/c
      Y[0] += (v[0] + 0.5 * a[0] * dt_mm) * dt_mm; // X
      Y[1] += (v[1] + 0.5 * a[1] * dt_mm) * dt_mm; // Y
      Y[2] += dt_mm; // S
      Y[3] += dY[3] * dS; // Px
      Y[4] += dY[4] * dS; // Py
      Y[5] += dY[5] * dS; // Pz
    }
    // update particle
    double inv_Pz = 1e3 / Y[5]; // 1e3/(MeV/c)
    particle.x  = Y[0]; // mm
    particle.y  = Y[1]; // mm
    particle.t  = Y[2]; // mm/c
    particle.xp = Y[3] * inv_Pz; // mrad
    particle.yp = Y[4] * inv_Pz; // mrad
    particle.Pc = hypot(Y[3], Y[4], Y[5]); // MeV/c
  } else /* if (use_gsl()) */ { // use GSL
    double Y[6];
    {
      const auto P = particle.get_Px_Py_Pz(); // momentum
      Y[0] = particle.x; // mm
      Y[1] = particle.y; // mm
      Y[2] = particle.t; // mm/c
      Y[3] = P[0]; // MeV/c
      Y[4] = P[1]; // MeV/c
      Y[5] = P[2]; // MeV/c
    }
    S_initial = start_step * dS; // m
    gsl_odeiv2_driver *driver = drivers[thread];
    gsl_odeiv2_driver_reset_hstart(driver, dS);
    for (size_t step = start_step+1; step<=end_step; step++) {
      if ((status = gsl_odeiv2_driver_apply(driver, &S_initial, step * dS, Y)) != GSL_SUCCESS)
	break;
    }
    // update particle
    double inv_Pz = 1e3 / Y[5]; // 1e3/(MeV/c)
    particle.x  = Y[0]; // mm
    particle.y  = Y[1]; // mm
    particle.t  = Y[2]; // mm/c
    particle.xp = Y[3] * inv_Pz; // mrad
    particle.yp = Y[4] * inv_Pz; // mrad
    particle.Pc = hypot(Y[3], Y[4], Y[5]); // MeV/c
  }
  if (status == GSL_EBADFUNC) particle.lost_at(S + S_initial);
  else if (status != GSL_SUCCESS) gsl_error = true;
}

double StaticMagneticField::get_divB(double x /* mm */, double y /* mm */, double z /* mm */ ) const
{
  if (z<0.0 || z>(z1-z0)) // outside element
    return 0.0;
  z += z0;
  if (z<0.0 || z>z1)
    return 0.0;
  x -= x0;
  y -= y0;
  x /= hx;
  y /= hy;
  z /= hz;
  int width_  = mesh_Phi.size1()-1;
  int height_ = mesh_Phi.size2()-1;
  if (x<0.0 || y<0.0 || x>width_ || y>height_)
    return 0.0;
  double retval = 0.0;
  const double inv_hxhx = 1.0/(hx*hx);
  const double inv_hyhy = 1.0/(hy*hy);
  const double inv_hzhz = 1.0/(hz*hz);
  if (use_PhiM) {
    const auto dPhiM_dx = mesh_PhiM.deriv2_x2(x,y,z)*inv_hxhx; // T/mm
    const auto dPhiM_dy = mesh_PhiM.deriv2_y2(x,y,z)*inv_hyhy; // T/mm
    const auto dPhiM_dz = mesh_PhiM.deriv2_z2(x,y,z)*inv_hzhz; // T/mm
    retval -= dPhiM_dx + dPhiM_dy + dPhiM_dz; // T/mm
  }
  return retval;
}
