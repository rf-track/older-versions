/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "coil.hh"
#include <gsl/gsl_sf_ellint.h>

std::pair<StaticVector<3>,StaticVector<3>> Coil::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  if (R==0.0) return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, static_Bfield);

  // See https://tiggerntatie.github.io/emagnet/offaxis/iloopoffaxis.htm
  const double a = R*1e3; // mm
  const double r = hypot(x,y); // mm

  if (r==0.0) {
    const double Bz = [&] (double z_ /* mm */ ) {
      const double z2 = z_*z_;
      const double a2 = a*a;
      const double a3 = a2*a;
      return B0 * a3 / ((z2+a2)*hypot(z_,a));
    } (z - 0.5*length*1e3);
    return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, static_Bfield + StaticVector<3>(0.0, 0.0, Bz));
  }

  const auto B = [&] (double z_ /* mm */, double r_ /* mm */ ) {
    const double alpha = r_/a; // #
    const double beta = z_/a; // #
    const double gamma = z_/r_; // #
    const double sqrt_Q = hypot(1 + alpha, beta); // #
    const double Q = (1 + alpha)*(1 + alpha) + beta*beta; // #
    const double k = 2.0 * sqrt(alpha) / sqrt_Q;
    const double K = gsl_sf_ellint_Kcomp(k, GSL_PREC_DOUBLE);
    const double E = gsl_sf_ellint_Ecomp(k, GSL_PREC_DOUBLE);
    const double B0_ = B0 / (M_PI*sqrt_Q); // T
    const double Bz = B0_*      (E * (1 - alpha*alpha - beta*beta) / (Q - 4*alpha) + K); // T
    const double Br = B0_*gamma*(E * (1 + alpha*alpha + beta*beta) / (Q - 4*alpha) - K); // T
    return std::pair<double,double>(Br, Bz);
  } (z - 0.5*length*1e3, r);
  double Br = B.first; // T
  double Bz = B.second; // T
  return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, static_Bfield + StaticVector<3>(Br*x/r, Br*y/r, Bz));
}
