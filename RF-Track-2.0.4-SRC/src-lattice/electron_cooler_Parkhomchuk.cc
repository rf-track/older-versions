/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <array>
#include <limits>
#include <numeric>
#include <utility>

#include "for_all.hh"
#include "bunch6d.hh"
#include "bunch6dt.hh"
#include "numtools.hh"
#include "rotation.hh"
#include "lorentz_boost.hh"
#include "electron_cooler.hh"
#include "move_particle.hh"
#include "stats.hh"

// #define DEBUG
// #define DEBUG_STOPPING_FORCE
#define Nsigma1 10
#define Nsigma2 5

ElectronCooler::ElectronCooler(double length /* m */, double rx /* m */, double ry /* m */, double N_ele /* #/m^3 */, double Vz /* c */ ) : Plasma(length, rx, ry, N_ele, Vz)
{
  // set_aperture(rx, ry, "circular"); // the element aperture does not coincide with the electron beam size
  // const double Kbolt_T_ele_r = 0.04; // eV, see CAS CERN-92-01, pag 157-158
  // const double Kbolt_T_ele_l = 1.7e-4; // eV, see CAS CERN-92-01, pag 157-158
  set_temperature(0.04, 1.7e-4);
}

void ElectronCooler::set_temperature(double kb_T_r /* eV */, double kb_T_l /* eV */ ) // set transverse and longitudinal temperatures, in the rest frame of the electrons
{
  Plasma::set_temperature(kb_T_r, kb_T_l);
  const double D_ele_r = get_D_ele_r(); // c
  const double D_ele_l = get_D_ele_l(); // c
  const size_t Nr = 64, Nl = 128;
  const auto Vr_axis = linspace(0.0, Nsigma1*D_ele_r, Nr);
  const auto Vl_axis = linspace(-Nsigma2*D_ele_r, Nsigma2*D_ele_r, Nl);
  Vr_min = Vr_axis[0];
  Vl_min = Vl_axis[0];
  Vr_step = Vr_axis[1] - Vr_axis[0];
  Vl_step = Vl_axis[1] - Vl_axis[0];
  func_Vr.resize(Nr,Nl);
  func_Vl.resize(Nr,Nl);
  std::cerr << "info: pre-computing the cooling force... ";
  auto stopping_force = [] (double &ret_x, double &ret_y, double &ret_z, double Vx, double Vy, double Vz ) {
    if (Vx==0.0 && Vy==0.0 && Vz==0.0) {
      ret_x = 0.0;
      ret_y = 0.0;
      ret_z = 0.0;
      return;
    }
    const double den = hypot(Vx,Vy,Vz)*(Vx*Vx+Vy*Vy+Vz*Vz);
    ret_x = Vx / den;
    ret_y = Vy / den;
    ret_z = Vz / den;
  };
  auto compute_stopping_force_parallel = [&] (size_t thread, size_t start, size_t end ) {
    gsl_rng *rng = gsl_rng_clone(RFT::rng);
    for (size_t i=start; i<end; i++) {
      for (size_t j=0; j<Nl; j++) {
	const double Vr = Vr_axis[i];
	const double Vl = Vl_axis[j];
	CumulativeKahanSum<double> sum[2];
	const size_t N = 100000; // monte carlo approach, I should use a 2d convolution between stoppng force and electron distribution
	size_t Ncount = 0;
	const double threshold = 1.0/(D_ele_l*D_ele_l);
	for (size_t k=0; k<N; k++) {
	  double ret_x, ret_y, ret_z;
	  stopping_force(ret_x, ret_y, ret_z,
			 Vr  - gsl_ran_gaussian(rng,D_ele_r),
			 0.0 - gsl_ran_gaussian(rng,D_ele_r),
			 Vl  - gsl_ran_gaussian(rng,D_ele_l));
	  if (fabs(ret_x)<threshold && fabs(ret_z)<threshold) {
	    sum[0] += ret_x;
	    sum[1] += ret_z;
	    Ncount++;
	  }
	}
	const double f_r = sum[0] / Ncount;
	const double f_l = sum[1] / Ncount;
	func_Vr.elem(i,j) = f_r;
	func_Vl.elem(i,j) = f_l;
      }
    }
    gsl_rng_free(rng);
  };
  for_all(RFT::number_of_threads, Nr, compute_stopping_force_parallel);
  std::cerr << "done!\n";
#ifdef DEBUG_STOPPING_FORCE
  for (double x : Vr_axis) {
    for (double y : Vl_axis) {
      const double f1 = (x - Vr_min) / Vr_step;
      const double f2 = (y - Vl_min) / Vl_step;
      std::cout << f1 << ' ' << f2 << ' ' << func_Vr(f1,f2) << ' ' << func_Vl(f1,f2) << std::endl;
    }
  }
#endif  
}

// tracking
TransportTable ElectronCooler::track(Bunch6d &bunch )
{
  TransportTable transport_table;

#ifdef DEBUGGs
  std::cout << "ElectronCooler::enter tracking " << bunch.get_ngood() << std::endl;
#endif

  const size_t Nthreads = RFT::number_of_threads;
  const size_t Nparticles = bunch.size();

  // resize meshes
  size_t
    Nx = size1(),
    Ny = size2(),
    Nz = nsteps;

  // init mesh external forces [MeV/mm]
  dP_to_electrons_mesh_parallel.resize(Nthreads);
  for (auto &dP_to_electrons_mesh: dP_to_electrons_mesh_parallel)
    dP_to_electrons_mesh.resize(Nx, Ny, Nz);

  const double l_max_mm = sqrt(get_area())*1e3; // mm, transverse dimension of electron beam (approximate)

  // convert the field from tesla to MV/mm
  const auto Bf = get_static_Bfield() * C_LIGHT / 1e9; // MV/mm
  const auto norm_Bf_sqr = dot(Bf,Bf); // (MV/mm)^2

  // start integration loop
  const double dS = get_length() / nsteps;
  const double dS_mm = dS*1e3; // mm
  double S_mm = 0.0; // mm, coordinate along the element
  for (size_t step = 0; step<nsteps; step++) {
    // initialization of the thread's local variables
    std::vector<double> dt_mm_average(Nthreads); // mm/c, average time spent by the beam in one step (used to advance the electron mesh)
    const double l_debye_mm = [&] () { // mm, Debye length at half step
      const double half_dS_mm = 0.5 * dS_mm;
      auto P0 = bunch.get_average_particle();
      move_particle_through_Bfield(P0, get_static_Bfield(), half_dS_mm);
      const auto r_ion = StaticVector<3>(P0.x, P0.y, S_mm + half_dS_mm); // mm
      const auto &state = plasma_get_state_bnd(r_ion);
      return get_debye_length(state) * 1e3; // mm
    } ();
    const double l_scr_mm = std::min(l_max_mm, l_debye_mm); // mm, screening length in the lab frame
    auto track_particle_parallel = [&] (size_t thread, size_t start, size_t end ) {
      auto &dP_to_electrons_mesh = dP_to_electrons_mesh_parallel[thread]; // MeV/mm
      auto &dt_mm_avg = dt_mm_average[thread]; // mm/c
      for (size_t i=0; i<Nx*Ny*Nz; i++)
	dP_to_electrons_mesh.data()[i] = StaticVector<3>(0.0);
      dt_mm_avg = 0.0;
      size_t count = 0; // count of good particles (necessary to dt_mm_average);
      for (size_t n=start; n<end; n++) {
	auto &ion = bunch.particles[n];
	if (ion) {
	  // ion position halfway through the cell
	  const auto &r_ion = [&] () {
	    auto _ion = ion;
	    const double half_dS_mm = 0.5 * dS_mm;
	    move_particle_through_Bfield(_ion, get_static_Bfield(), half_dS_mm);
	    return StaticVector<3>(_ion.x, _ion.y, S_mm + half_dS_mm); // mm
	  }();
	  
	  if (!is_point_inside_aperture(r_ion[0], r_ion[1])) {
	    ion.lost_at(bunch.S + 0.5*dS);
	    continue;
	  }
	  
	  if (!is_point_inside_area(r_ion[0], r_ion[1])) { // if the ion is outside plasma just drift the ion
	    move_particle_through_Bfield(ion, get_static_Bfield(), dS_mm);
	    continue;
	  }

	  // consider the electrons
	  const auto &state = plasma_get_state(r_ion);
	  const auto N_ele = plasma_get_number_density(state); // #/m^3
	  if (N_ele == 0.0) { // if the ion is in a zero-density region, just drift the ion
	    move_particle_through_Bfield(ion, get_static_Bfield(), dS_mm);
	    continue;
	  }

	  // other auxiliary variables
	  const auto &V_ele = plasma_get_velocity(state); // c
	  const double inv_gamma_ele = sqrt(1.0 - dot(V_ele,V_ele));
	  const double gamma_ele = 1.0 / inv_gamma_ele;
	  const double E_ele = get_mass() * gamma_ele; // MeV, total energy of the electron

	  // radial and longitudinal electron temperatures (times the Boltzmann constant)
	  const double D_ele_r = get_D_ele_r(); // c
	  const double D_ele_l = get_D_ele_l(); // c
	  const double D_ele = hypot(D_ele_r, D_ele_l); // c

	  // prepare for physics
	  const double Q_ion = ion.Q;
	  const double Q_ele = get_Q();

	  //////////
	  // You have: e*e/(4*pi*epsilon0)
	  // You want: MeV*mm
	  // e*e/(4*pi*epsilon0) = 1.439964485044515e-12 MeV*mm
	  // e*e/(4*pi*epsilon0) = (1 / 694461572063.7623) MeV*mm
	  const double K = (Q_ion * Q_ele) / 694461572063.7623; // (e*e/4/pi/epsilon0) MeV*mm
	  const double reduced_mass = ion.mass * get_mass() / (ion.mass + get_mass()); // MeV/c^2
	  
#ifdef DEBUG
	  std::cout << "# " << D_ele_r << ' ' << D_ele_l << std::endl;
#endif
	  
	  // other variables
	  const auto &V_ion = ion.get_Vx_Vy_Vz(); // c
	  const auto &dt_mm = dS_mm / V_ion[2]; // mm/c, time spent by the ion in the cell
	  dt_mm_avg += (dt_mm - dt_mm_avg) / ++count; // mm/c, running average of dt_mm (used to advance the electron mesh)
	  const double gamma_ion = ion.get_gamma();

	  const StaticVector<3> F0 = [&](){ // MeV/m, stopping force for fast collisions
	    if (norm_Bf_sqr==0.0) { // unmagnetized plasma
	      const auto U0 = relativistic_velocity_addition(-V_ele, V_ion); // relativistic velocity addition, the ion velocity in the electron frame
	      const auto &nz = normalize(V_ele); // unit vector running parallel to the electron in the lab's frame
	      auto nr = normalize(U0 - nz * dot(U0, nz)); // radial ion velocity, orthogonal to V_ele
	      double U0_l = dot(U0, nz); // axial component of the velocity
	      double U0_r = dot(U0, nr); // radial velocity
	      if (U0_r<0.0) {
		U0_r = -U0_r;
		nr = -nr;
	      }
	      // sqrt(e*e/(MeV/c/c)/epsilon0/m^3) = 1.345181303415426e-10 * 1/(mm/c)
	      // sqrt(e*e/(MeV/c/c)/epsilon0/m^3) = (1 / 7433942156.800666) * 1/(mm/c)
	      const double omega_ele_p = sqrt(N_ele * Q_ele*Q_ele / E_ele) / 7433942156.800666; // rad / (mm/c), electron plasma frequency
	      const double N_ele_star = N_ele * inv_gamma_ele; // #/m^3, electron number density in the reference frame of the electrons
	      const double norm_U0 = hypot(std::max(fabs(U0_l), D_ele_l), std::max(fabs(U0_r), D_ele_r));
	      const double norm_U0_sqr = norm_U0*norm_U0; // c^2
	      const double KE = 0.5 * reduced_mass * norm_U0_sqr; // MeV, kinetic energy
	      const double l_min_mm = fabs(K) / (2*KE); // mm, minimum impact distance
	      const double r_max_mm = std::min(l_scr_mm, std::min(norm_U0 * dt_mm, norm_U0 / norm(omega_ele_p))); // mm
	      const double L0 = std::max(log(r_max_mm / l_min_mm), 8.0); // Coulomb logarithm
	      const auto F_ = -N_ele_star * 2.0*M_PI * (K*K) / (0.5*reduced_mass) / 1e6; // MeV * mm^2 / m^3 / 1e6 = MeV/m
	      StaticVector<3> F = [&] () {
		const double normalized_U0_r = U0_r / (Nsigma1*D_ele_r);
		const double normalized_U0_l = U0_l / (Nsigma2*D_ele_r);
		if (fabs(normalized_U0_r)>1.0 || fabs(normalized_U0_l)>1.0)
		  return F_ * L0 * U0 / pow(norm_U0,3); // (4.1) p235.pdf //// CAS CERN-92-01, pag 166 (5.4a)
		const double f1 = (U0_r - Vr_min) / Vr_step;
		const double f2 = (U0_l - Vl_min) / Vl_step;
		double F_r = F_ * L0 * func_Vr(f1,f2); //
		double F_l = F_ * L0 * func_Vl(f1,f2); //
		return nz*F_l + nr*F_r; // MeV/m
	      } ();
	      // moves to the lab frame
	      const auto F_4d = StaticVector<4>(dot(F,U0), F[0], F[1], F[2]) / sqrt(1.0 - dot(U0,U0)); // 4-force in the electrons frame
	      const auto F_lab_4d = lorentz_boost(-V_ele, F_4d); // 4-force in the Lab frame
	      return StaticVector<3>(F_lab_4d[1], F_lab_4d[2], F_lab_4d[3]) / gamma_ion; // stopping force in the Lab frame
	    }
	    return StaticVector<3>(0.0, 0.0, 0.0);
	  } ();

	  const StaticVector<3> FA = [&](){ // MeV/m, stopping force for adiabatic collisions
	    if (norm_Bf_sqr>=0.0) {
	      const auto &nz = normalize(Bf); // unit vector running parallel Bf
	      const auto V_ele_l = nz * dot(V_ele, nz); // component of the electron velocity parallel to Bf
	      const auto UA = relativistic_velocity_addition(-V_ele_l, V_ion); // relativistic velocity addition, the ion velocity in the electron frame
	      auto nr = normalize(UA - nz * dot(UA, nz)); // radial ion velocity, orthogonal to UA
	      const auto omega_ele = Q_ele * Bf / E_ele; // rad / (mm/c), angular velocity
	      const double N_ele_star = N_ele * sqrt(1.0 - dot(V_ele_l,V_ele_l)); // #/m^3, electron number density in the reference frame of the electrons
	      const double V_ele_r = dot(V_ele, nr); // radial component of the electron velocity
	      double UA_l = dot(UA, nz); // longitudinal component of the velocity
	      double UA_r = dot(UA, nr); // radial component of the velocity
	      if (UA_r<0.0) {
		UA_r = -UA_r;
		nr = -nr;
	      }
	      const double norm_UA = norm(UA); // c
	      const double norm_UA_sqr = norm_UA*norm_UA; // c^2
	      const double r_ion_mm = norm_UA / norm(omega_ele); // mm
	      const double r_ele_mm = hypot(V_ele_r, D_ele_r) / norm(omega_ele); // mm, Larmor radius of the electron
	      const double r_max_mm = std::min(l_scr_mm, std::min(norm_UA * dt_mm, r_ion_mm)); // mm		
	      const auto F_ = -N_ele_star * 2.0*M_PI * (K*K) / (0.5*reduced_mass) / 1e6; // MeV * mm^2 / m^3 / 1e6 = MeV/m
	      // std::cout << "l_debye_mm = " << l_debye_mm << std::endl;
	      // std::cout << "l_min_mm = " << l_min_mm << std::endl;
	      // std::cout << "l_scr_mm = " << l_scr_mm << std::endl;
	      // std::cout << "r_max_mm = " << r_max_mm << std::endl;
	      // std::cout << "r_ele_mm = " << r_ele_mm << std::endl;
	      // std::cout << "r_ion_mm = " << r_ion_mm << std::endl << std::endl;

	      // fast collisions
	      StaticVector<3> F_fast = [&] () {
		const double KE = 0.5 * reduced_mass * norm_UA_sqr; // MeV, kinetic energy
		const double l_min_mm = fabs(K) / (2*KE); // mm, minimum impact distance
		const double LA = std::max(log(r_ion_mm / l_min_mm), 8.0); // Coulomb logarithm
		const double normalized_UA_r = UA_r / (Nsigma1*D_ele_r);
		const double normalized_UA_l = UA_l / (Nsigma2*D_ele_r);
		if (fabs(normalized_UA_r)>1.0 || fabs(normalized_UA_l)>1.0)
		  return F_ * LA * UA / pow(norm_UA,3); // (4.1) p235.pdf //// CAS CERN-92-01, pag 166 (5.4a)
		const double f1 = (UA_r - Vr_min) / Vr_step;
		const double f2 = (UA_l - Vl_min) / Vl_step;
		double F_r = F_ * LA * func_Vr(f1,f2); //
		double F_l = F_ * LA * func_Vl(f1,f2); //
		return nz*F_l + nr*F_r; // MeV/m
	      } ();

	      // repeated collisions
	      StaticVector<3> F_repeated(0.0, 0.0, 0.0);
	      if (r_ele_mm>r_ion_mm) {
		if (norm_UA>D_ele) {
		  F_repeated = F_ / M_PI  * log(r_ele_mm / r_ion_mm) / D_ele / norm_UA_sqr * UA;
		}
	      }

	      // strong magnetization
	      StaticVector<3> F_strong(0.0, 0.0, 0.0);
	      if (r_max_mm>r_ele_mm) {
		if (norm_UA>D_ele_l) {
		  F_strong = 0.5 * F_ * log(r_max_mm / r_ele_mm)*UA_r*UA_r*UA/pow(norm_UA,5);
		} else {
		  // e*e * (1/(m**2)) / epsilon0 = 1.809512739058424e-14 MeV/m
		  // e*e * (1/(m**2)) / epsilon0 = (1 / 55263495990658.12) MeV/m
		  F_strong = Q_ion * Q_ele * pow(N_ele_star, 2.0/3.0) / 55263495990658.12 * UA / D_ele_l;
		  if (Q_ion * Q_ele>0)
		    F_strong += -M_PI * Q_ion * Q_ele * pow(N_ele_star, 2.0/3.0) * nz / 55263495990658.12; // Parkhomchuk and Skrinsky (25)
		}
	      }

	      const auto F = F_fast + F_repeated + F_strong;

	      const auto F_4d = StaticVector<4>(dot(F,UA), F[0], F[1], F[2]) / sqrt(1.0 - dot(UA,UA));; // 4-force in the electrons frame
	      const auto F_lab_4d = lorentz_boost(-V_ele_l, F_4d); // 4-force in the Lab frame
	      return StaticVector<3>(F_lab_4d[1], F_lab_4d[2], F_lab_4d[3]) / gamma_ion; // stopping force in the Lab frame
	    }
	    return StaticVector<3>(0.0, 0.0, 0.0);
	  } ();

	  const auto F = F0 + FA; // MeV/m

	  // particle update
	  if (gsl_isnan(F[0]) || gsl_isnan(F[1]) || gsl_isnan(F[2])) {
	    std::cerr << "error: Nan in Electron Coooling computation (ions) @ S = " << S_mm << " mm\n";
	    exit(1);
	  }

	  // apply the force on the ion, considering the effect of the magnetic field
	  apply_force_through_Bfield(ion, F, get_static_Bfield(), dS_mm);

	  // final momentum
	  if (fabs(ion.Pc) < std::numeric_limits<double>::epsilon()) {
	    ion.Pc = 0.0; // MeV/c
	    ion.lost_at(bunch.S + 0.5*dS);
	    continue;
	  }
	}
      }
    };
    /* const auto effective_Nthreads = */ for_all(Nthreads, Nparticles, track_particle_parallel);

    // merge the forces from different threads and store the result in dP_to_electrons_mesh[0]
    // compute also the average time spent by the beam during the current step, store the result is dt_mm_average[0]
    /*
    for (size_t thread=1; thread<effective_Nthreads; thread++) {
      const auto &dP_to_electrons_mesh = dP_to_electrons_mesh_parallel[thread];
      for (size_t i=0; i<Nx*Ny*Nz; i++) {
	dP_to_electrons_mesh_parallel[0].data()[i] += dP_to_electrons_mesh.data()[i];
      }
      dt_mm_average[0] += dt_mm_average[thread]; // mm/c
    }
    dt_mm_average[0] /= effective_Nthreads;
    */
    // advances the plasma (TO DO!)
    /////////apply_momentum_through_dt(dt_mm_average[0], dP_to_electrons_mesh_parallel[0]);

    S_mm += dS_mm;

    bunch.S += dS;
    transport_table.append_bunch_info(bunch);
  }

  return transport_table;
}
