/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <cstdio>
#include <string>

#include "lattice.hh"
#include "RF_Track.hh"
#include "quadrupole.hh"
#include "drift.hh"

Lattice::Lattice(const Lattice &lattice ) {
  for (auto &element_ptr : lattice.elements) {
    elements.push_back(element_ptr->clone());
  }
}

Lattice &Lattice::operator=(const Lattice &lattice )
{
  elements.clear();
  for (auto &element_ptr : lattice.elements) {
    elements.push_back(element_ptr->clone());
  }
  return *this;
}

double Lattice::get_length() const
{
  double length = 0.0;
  for (auto &element_ptr : elements) {
    length += element_ptr->get_length();
  }
  return length;
}

MatrixNd Lattice::get_bpm_readings() const
{
  MatrixNd ret;
  if (!bpm_readings.empty()) {
    size_t rows = bpm_readings.size();
    size_t columns = 3;
    ret.resize(rows, columns);
    // TO DO
  } else {
    std::cerr << "error: no bpm readings in memory!\n";
  }
  return ret;
}

/*
std::vector<RF_FieldMap_LINT*> Lattice::get_rf_fields()
{
  std::vector<RF_FieldMap_LINT*> ret_val;
  for (auto &e_ptr : elements) {
    if (RF_FieldMap_LINT *s_ptr=dynamic_cast<RF_FieldMap_LINT*>(e_ptr.get())) {
      ret_val.push_back(s_ptr);
    }
  }
  return ret_val;
}

std::vector<Drift*> Lattice::get_drifts()
{
  std::vector<Drift*> ret_val;
  for (auto &e_ptr : elements) {
    if (Drift *d_ptr=dynamic_cast<Drift*>(e_ptr.get())) {
      ret_val.push_back(d_ptr);
    }
  }
  return ret_val;
}

std::vector<Quadrupole*> Lattice::get_quadrupoles()
{
  std::vector<Quadrupole*> ret_val;
  for (auto &e_ptr : elements) {
    if (Quadrupole *q_ptr=dynamic_cast<Quadrupole*>(e_ptr.get())) {
      ret_val.push_back(q_ptr);
    }
  }
  return ret_val;
}
*/

Bunch6d Lattice::track(Bunch6d bunch )
{
  bpm_readings.clear();
  transport_table.clear();
  transport_table.append_bunch_info(bunch);
  for (auto &element_ptr : elements) {
    const auto &&transport_table_elem = element_ptr->track(bunch);
    transport_table.append(transport_table_elem);
  }
  transport_table.append_bunch_info(bunch);
  return bunch;
}
