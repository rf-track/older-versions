/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iterator>
#include <sstream>
#include <fstream>
#include <vector>
#include <list>
#include <algorithm>

#include "RF_Track.hh"
#include "transfer_line.hh"

TransferLine::TransferLine(const MatrixNd &twiss_matrix_, double ref_Pc_ ) : ref_Pc(ref_Pc_), DQx(0.0), DQy(0.0), momentum_compaction(0.0)
{
  if (twiss_matrix_.columns() < 7 || twiss_matrix_.columns() > 11) {
    std::cerr << "error: Bunch6d::TransferLine() requires a 7-to-11-column matrix as an input\n";
  } else {
    twiss_matrix.resize(twiss_matrix_.rows(), 11, 0.0);
    for (size_t i=0; i<twiss_matrix_.rows(); i++) {
      for (size_t j=0; j<twiss_matrix_.columns(); j++) {
	twiss_matrix[i][j] = twiss_matrix_[i][j];
      }
    }
  }
}

TransferLine::TransferLine(const MatrixNd &twiss_matrix_, double DQx_, double DQy_, double momentum_compaction_, double ref_Pc_ ) : ref_Pc(ref_Pc_), DQx(DQx_), DQy(DQy_), momentum_compaction(momentum_compaction_)
{
  if (twiss_matrix_.columns() < 7 || twiss_matrix_.columns() > 11) {
    std::cerr << "error: Bunch6d::TransferLine() requires a 7-to-11-column matrix as an input\n";
  } else {
    twiss_matrix.resize(twiss_matrix_.rows(), 11, 0.0);
    for (size_t i=0; i<twiss_matrix_.rows(); i++) {
      for (size_t j=0; j<twiss_matrix_.columns(); j++) {
	twiss_matrix[i][j] = twiss_matrix_[i][j];
      }
    }
  }
}

TransferLine::TransferLine(const char *twiss_file, double ref_Pc_ ) : ref_Pc(ref_Pc_), DQx(0.0), DQy(0.0), momentum_compaction(0.0)
{
  std::ifstream file(twiss_file);
  if (file) {
    int
      L_index = -1,
      BETX_index = -1,
      BETY_index = -1,
      ALFX_index = -1,
      ALFY_index = -1,
      MUX_index = -1,
      MUY_index = -1,
      DX_index = -1,
      DY_index = -1,
      DPX_index = -1,
      DPY_index = -1;
    struct Row {
      double S, BETX, ALFX, MUX, BETY, ALFY, MUY, DX, DPX, DY, DPY;
    };
    double S = 0.0; // m
    std::list<Row> temporary;
    for (std::string str; std::getline(file, str); ) {
      if (str[0] == '@') {
	std::istringstream line(str);
	std::string A,B,C,D; // e.g. @ ALFA             %le      0.12404
	if      (B == "ALFA") momentum_compaction = atof(D.c_str());
	else if (B == "DQ1")  DQx = atof(D.c_str());
	else if (B == "DQ2")  DQy = atof(D.c_str());
	else if (B == "PC")   { if (ref_Pc==-1.0) ref_Pc = 1e3*atof(D.c_str()); }
	line >> A >> B >> C >> D;
	std::cout << str << std::endl;
      } else if (str[0] == '*') {
	std::istringstream line(str);
	std::string item;
	for (int index = 0; line >> item; index++) {
	  if      (item == "L")    L_index = index-1;
	  else if (item == "BETX") BETX_index = index-1;
	  else if (item == "BETY") BETY_index = index-1;
	  else if (item == "ALFX") ALFX_index = index-1;
	  else if (item == "ALFY") ALFY_index = index-1;
	  else if (item == "MUX")  MUX_index = index-1;
	  else if (item == "MUY")  MUY_index = index-1;
	  else if (item == "DX")   DX_index = index-1;
	  else if (item == "DY")   DY_index = index-1;
	  else if (item == "DPX")  DPX_index = index-1;
	  else if (item == "DPY")  DPY_index = index-1;
	}
	if (L_index==-1 || BETX_index == -1 || BETY_index == -1 || ALFX_index == -1 || ALFY_index == -1 || MUX_index == -1 || MUY_index == -1) {
	  std::cerr << "error: something went wrong while parsing the MAD-X file\n";
	  return;
	}
      } else if (str[0] != '$') {
	std::istringstream line(str);
	std::vector<std::string> tokens;
	std::copy(std::istream_iterator<std::string>(line),
		  std::istream_iterator<std::string>(),
		  std::back_inserter(tokens));
	Row row = {
	  S += atof(tokens[L_index].c_str()),
	  atof(tokens[BETX_index].c_str()),
	  atof(tokens[BETY_index].c_str()),
	  atof(tokens[ALFX_index].c_str()),
	  atof(tokens[ALFY_index].c_str()),
	  atof(tokens[MUX_index].c_str()),
	  atof(tokens[MUY_index].c_str()),
	  DX_index != -1 ? atof(tokens[DX_index].c_str()) : 0.0,
	  DY_index != -1 ? atof(tokens[DY_index].c_str()) : 0.0,
	  DPX_index != -1 ? atof(tokens[DPX_index].c_str()) : 0.0,
	  DPY_index != -1 ? atof(tokens[DPY_index].c_str()) : 0.0
	};
	temporary.push_back(row);
      }
    }
    twiss_matrix.resize(temporary.size(), 11);
    size_t i = 0;
    for (const auto &row: temporary) {
      twiss_matrix[i][0] = row.S;
      twiss_matrix[i][1] = row.BETX;
      twiss_matrix[i][2] = row.BETY;
      twiss_matrix[i][3] = row.ALFX;
      twiss_matrix[i][4] = row.ALFY;
      twiss_matrix[i][5] = row.MUX;
      twiss_matrix[i][6] = row.MUY;
      twiss_matrix[i][7] = row.DX;
      twiss_matrix[i][8] = row.DY;
      twiss_matrix[i][9] = row.DPX;
      twiss_matrix[i][10] = row.DPY;
      std::cout << twiss_matrix[i] << std::endl;
      i++;
    }
  }
}

TransferLine::TransferLine() : twiss_matrix(1, 11, 0.0)
{
  DQx = DQy = momentum_compaction = 0.0;
}

TransportTable TransferLine::track(Bunch6d &bunch )
{
  const size_t Nthreads = RFT::number_of_threads;
  
  TransportTable transport_table;

  const double reference_momentum = ref_Pc != -1 ? ref_Pc : bunch.get_reference_momentum();
  const double length = get_length(); // m
  MatrixNd sc_force;

  if (twiss_matrix.rows()==1) {
    if (get_sc_nsteps()>0) {
      if (bunch.size()>1)
	std::cout << "\33[2K\rinfo: applying space-charge 0/1" << std::flush;
      const double s0 = 0.0; // m
      const double s1 = twiss_matrix[0][0]; // m
      const double dL0 = s1 - s0; // m
      RFT::SC_engine->compute_force(sc_force, bunch);
      bunch.kick(sc_force, 0.5*dL0*1e3);
    }
    const auto &bunch_info = bunch.get_info();
    const double s0       = 0.0;
    const double beta0_x  = bunch_info.beta_x;
    const double alpha0_x = bunch_info.alpha_x;
    const double mu0_x    = 0.0;
    const double beta0_y  = bunch_info.beta_y;
    const double alpha0_y = bunch_info.alpha_y;
    const double mu0_y    = 0.0;
    const double s1       = twiss_matrix[0][0];
    const double beta1_x  = twiss_matrix[0][1];
    const double alpha1_x = twiss_matrix[0][2];
    const double mu1_x    = twiss_matrix[0][3];
    const double beta1_y  = twiss_matrix[0][4];
    const double alpha1_y = twiss_matrix[0][5];
    const double mu1_y    = twiss_matrix[0][6];
    const double disp1_x   = twiss_matrix[0][7];
    const double disp1_px  = twiss_matrix[0][8];
    const double disp1_y   = twiss_matrix[0][9];
    const double disp1_py  = twiss_matrix[0][10];
    const double dL0     = s1 - s0; // m
    const double dmu0_x  = 2 * M_PI * (mu1_x-mu0_x); // rad
    const double dmu0_y  = 2 * M_PI * (mu1_y-mu0_y); // rad
    const double dmu0_x_ = 2 * M_PI * DQx * dL0 / length; // rad
    const double dmu0_y_ = 2 * M_PI * DQy * dL0 / length; // rad
    const double sqrt_beta0_beta1_x = sqrt(beta0_x*beta1_x);
    const double sqrt_beta0_beta1_y = sqrt(beta0_y*beta1_y);
    const double sqrt_beta1_div_beta0_x = sqrt(beta1_x/beta0_x);
    const double sqrt_beta1_div_beta0_y = sqrt(beta1_y/beta0_y);
    const double sqrt_beta0_div_beta1_x = 1.0/sqrt_beta1_div_beta0_x;
    const double sqrt_beta0_div_beta1_y = 1.0/sqrt_beta1_div_beta0_y;
    const double cos_dmu0_x = cos(dmu0_x);
    const double sin_dmu0_x = sin(dmu0_x);
    const double cos_dmu0_y = cos(dmu0_y);
    const double sin_dmu0_y = sin(dmu0_y);
    for_all(Nthreads, bunch.size(), [&] (size_t thread, size_t start, size_t end ) -> void {
	for (size_t n=start; n<end; n++) {
	  auto &particle = bunch[n];
	  const double d = (particle.Pc - reference_momentum) / reference_momentum;
	  // transverse plane
	  const double dmu_x = dmu0_x_ * d; // rad
	  const double dmu_y = dmu0_y_ * d; // rad
	  const double cos_mu_x =  (((sin_dmu0_x*dmu_x-3*cos_dmu0_x)*dmu_x-6*sin_dmu0_x)*dmu_x+6*cos_dmu0_x)/6;
	  const double sin_mu_x = -(((cos_dmu0_x*dmu_x+3*sin_dmu0_x)*dmu_x-6*cos_dmu0_x)*dmu_x-6*sin_dmu0_x)/6;
	  const double cos_mu_y =  (((sin_dmu0_y*dmu_y-3*cos_dmu0_y)*dmu_y-6*sin_dmu0_y)*dmu_y+6*cos_dmu0_y)/6;
	  const double sin_mu_y = -(((cos_dmu0_y*dmu_y+3*sin_dmu0_y)*dmu_y-6*cos_dmu0_y)*dmu_y-6*sin_dmu0_y)/6;
	  const double _x  = // mm
	    particle.x  * sqrt_beta1_div_beta0_x * (cos_mu_x + alpha0_x * sin_mu_x) +
	    particle.xp * sqrt_beta0_beta1_x * sin_mu_x +
	    disp1_x * d * 1e3;
	  const double _xp = // mrad
	    particle.x  * ((alpha0_x - alpha1_x) * cos_mu_x - (1 + alpha0_x * alpha1_x) * sin_mu_x) / sqrt_beta0_beta1_x +
	    particle.xp * sqrt_beta0_div_beta1_x * (cos_mu_x - alpha1_x * sin_mu_x) +
	    disp1_px * d * 1e3;
	  const double _y  = // mm
	    particle.y  * sqrt_beta1_div_beta0_y * (cos_mu_y + alpha0_y * sin_mu_y) +
	    particle.yp * sqrt_beta0_beta1_y * sin_mu_y +
	    disp1_y * d * 1e3;
	  const double _yp = // mrad
	    particle.y  * ((alpha0_y - alpha1_y) * cos_mu_y - (1 + alpha0_y * alpha1_y) * sin_mu_y) / sqrt_beta0_beta1_y +
	    particle.yp * sqrt_beta0_div_beta1_y * (cos_mu_y - alpha1_y * sin_mu_y) +
	    disp1_py * d * 1e3;
	  particle.x  = _x;  // mm
	  particle.xp = _xp; // mrad
	  particle.y  = _y;  // mm
	  particle.yp = _yp; // mrad
	  // longitudinal plane
	  const double dL = dL0 * (1.0 + momentum_compaction * d); // m
	  const double Vz = particle.get_Vx_Vy_Vz()[2];
	  particle.t += 1e3 * dL / Vz; // mm/c
	}
      });
      
    bunch.S += dL0;
    
    transport_table.append_bunch_info(bunch);

    // apply final space-charge
    if (get_sc_nsteps()>0) {
      if (bunch.size()>1)
	std::cout << "\33[2K\rinfo: applying space-charge 1/1" << std::endl;
      RFT::SC_engine->compute_force(sc_force, bunch);
      bunch.kick(sc_force, 0.5*dL0*1e3);
    }
    
  } else {
    for (size_t i=0; i<twiss_matrix.rows()-1; i++) {
      const double s0       = twiss_matrix[i][0];
      const double beta0_x  = twiss_matrix[i][1];
      const double alpha0_x = twiss_matrix[i][2];
      const double mu0_x    = twiss_matrix[i][3];
      const double beta0_y  = twiss_matrix[i][4];
      const double alpha0_y = twiss_matrix[i][5];
      const double mu0_y    = twiss_matrix[i][6];
      // const double disp0_x   = twiss_matrix[i][7];
      // const double disp0_px  = twiss_matrix[i][8];
      // const double disp0_y   = twiss_matrix[i][9];
      // const double disp0_py  = twiss_matrix[i][10];
      const double s1       = twiss_matrix[i+1][0];
      const double beta1_x  = twiss_matrix[i+1][1];
      const double alpha1_x = twiss_matrix[i+1][2];
      const double mu1_x    = twiss_matrix[i+1][3];
      const double beta1_y  = twiss_matrix[i+1][4];
      const double alpha1_y = twiss_matrix[i+1][5];
      const double mu1_y    = twiss_matrix[i+1][6];
      const double disp1_x   = twiss_matrix[i+1][7];
      const double disp1_px  = twiss_matrix[i+1][8];
      const double disp1_y   = twiss_matrix[i+1][9];
      const double disp1_py  = twiss_matrix[i+1][10];
      const double dL0     = s1 - s0; // m
      const double dmu0_x  = 2 * M_PI * (mu1_x-mu0_x); // rad
      const double dmu0_y  = 2 * M_PI * (mu1_y-mu0_y); // rad
      const double dmu0_x_ = 2 * M_PI * DQx * dL0 / length; // rad
      const double dmu0_y_ = 2 * M_PI * DQy * dL0 / length; // rad
      const double sqrt_beta0_beta1_x = sqrt(beta0_x*beta1_x);
      const double sqrt_beta0_beta1_y = sqrt(beta0_y*beta1_y);
      const double sqrt_beta1_div_beta0_x = sqrt(beta1_x/beta0_x);
      const double sqrt_beta1_div_beta0_y = sqrt(beta1_y/beta0_y);
      const double sqrt_beta0_div_beta1_x = 1.0/sqrt_beta1_div_beta0_x;
      const double sqrt_beta0_div_beta1_y = 1.0/sqrt_beta1_div_beta0_y;
      const double cos_dmu0_x = cos(dmu0_x);
      const double sin_dmu0_x = sin(dmu0_x);
      const double cos_dmu0_y = cos(dmu0_y);
      const double sin_dmu0_y = sin(dmu0_y);
      if (get_sc_nsteps()>0) {
	if (i==0) {
	  if (bunch.size()>1)
	    std::cout << "\33[2K\rinfo: applying space-charge 0" << '/' << twiss_matrix.rows()-1 << std::flush;
	  RFT::SC_engine->compute_force(sc_force, bunch);
	}
	bunch.kick(sc_force, 0.5*dL0*1e3);
      }
      for_all(Nthreads, bunch.size(), [&] (size_t thread, size_t start, size_t end ) -> void {
	  for (size_t n=start; n<end; n++) {
	    auto &particle = bunch[n];
	    const double d = (particle.Pc - reference_momentum) / reference_momentum;
	    // transverse plane
	    const double dmu_x = dmu0_x_ * d; // rad
	    const double dmu_y = dmu0_y_ * d; // rad
	    const double cos_mu_x =  (((sin_dmu0_x*dmu_x-3*cos_dmu0_x)*dmu_x-6*sin_dmu0_x)*dmu_x+6*cos_dmu0_x)/6;
	    const double sin_mu_x = -(((cos_dmu0_x*dmu_x+3*sin_dmu0_x)*dmu_x-6*cos_dmu0_x)*dmu_x-6*sin_dmu0_x)/6;
	    const double cos_mu_y =  (((sin_dmu0_y*dmu_y-3*cos_dmu0_y)*dmu_y-6*sin_dmu0_y)*dmu_y+6*cos_dmu0_y)/6;
	    const double sin_mu_y = -(((cos_dmu0_y*dmu_y+3*sin_dmu0_y)*dmu_y-6*cos_dmu0_y)*dmu_y-6*sin_dmu0_y)/6;
	    const double _x  = // mm
	      particle.x  * sqrt_beta1_div_beta0_x * (cos_mu_x + alpha0_x * sin_mu_x) +
	      particle.xp * sqrt_beta0_beta1_x * sin_mu_x +
	      disp1_x * d * 1e3;
	    const double _xp = // mrad
	      particle.x  * ((alpha0_x - alpha1_x) * cos_mu_x - (1 + alpha0_x * alpha1_x) * sin_mu_x) / sqrt_beta0_beta1_x +
	      particle.xp * sqrt_beta0_div_beta1_x * (cos_mu_x - alpha1_x * sin_mu_x) +
	      disp1_px * d * 1e3;
	    const double _y  = // mm
	      particle.y  * sqrt_beta1_div_beta0_y * (cos_mu_y + alpha0_y * sin_mu_y) +
	      particle.yp * sqrt_beta0_beta1_y * sin_mu_y +
	      disp1_y * d * 1e3;
	    const double _yp = // mrad
	      particle.y  * ((alpha0_y - alpha1_y) * cos_mu_y - (1 + alpha0_y * alpha1_y) * sin_mu_y) / sqrt_beta0_beta1_y +
	      particle.yp * sqrt_beta0_div_beta1_y * (cos_mu_y - alpha1_y * sin_mu_y) +
	      disp1_py * d * 1e3;	particle.x  = _x;  // mm
	    particle.xp = _xp; // mrad
	    particle.y  = _y;  // mm
	    particle.yp = _yp; // mrad
	    // longitudinal plane
	    const double dL = dL0 * (1.0 + momentum_compaction * d); // m
	    const double Vz = particle.get_Vx_Vy_Vz()[2];
	    particle.t += 1e3 * dL / Vz; // mm/c
	  }
        });

      bunch.S += dL0;
    
      transport_table.append_bunch_info(bunch);

      // apply space-charge
      if (get_sc_nsteps()>0) {
	if (bunch.size()>1)
	  std::cout << "\33[2K\rinfo: applying space-charge " << i+1 << '/' << twiss_matrix.rows()-1 << std::flush;
	RFT::SC_engine->compute_force(sc_force, bunch);
	bunch.kick(sc_force, 0.5*dL0*1e3);
      }
    }
    if (get_sc_nsteps()>0 && bunch.size()>1) {
      std::cout << std::endl;
    }
  }
  
  return transport_table;
}
