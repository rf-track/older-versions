/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>
#include <gsl/gsl_sys.h>

#include "RF_Track.hh"
#include "rf_field_map_2d.hh"
#include "constants.hh"
#include "move_particle.hh"

template <typename COMPLEX_MESH_2D>
RF_FieldMap_2d<COMPLEX_MESH_2D>::RF_FieldMap_2d(const ComplexMesh2d &Er,
						const ComplexMesh2d &Ez,
						const ComplexMesh2d &Br,
						const ComplexMesh2d &Bz,
						double hr_, double hz_, // m
						double length_,
						double frequency_,
						double direction_,
						double P_map_,
						double P_actual_ ) : disable_Efield(false),
								     disable_Bfield(false),
								     hr(hr_*1e3), // accepts m, stores mm
								     hz(hz_*1e3), // accepts m, stores mm
								     z0(0.0), // mm
								     omega(2.0 * M_PI * (frequency_ / (C_LIGHT*1e3))), // frequency in Hz
								     direction(direction_ == 0.0 ? 0 : (direction_ > 0.0 ? +1 : -1)),
								     P_map(P_map_), // W
								     P_actual(P_actual_), // W
								     scale_factor_and_phase(sqrt(P_actual / P_map)),
								     phi(0.0),
								     t0_is_set(false),
								     t0(0.0),
								     static_Bfield(0.0, 0.0, 0.0)
{
  Efield.resize(Nz,Nr);
  Bfield.resize(Nz,Nr);
  for (size_t i=0; i<Nz; i++) {
    for (size_t j=0; j<Nr; j++) {
      Efield.elem(i,j) = StaticVector<2,fftwComplex>(Ez.elem(i,j), Er.elem(i,j));
      Bfield.elem(i,j) = StaticVector<2,fftwComplex>(Bz.elem(i,j), Br.elem(i,j));
    }
  }
  set_nsteps(Nz-1);
  set_length(length_);
}

template <typename COMPLEX_MESH_2D>
RF_FieldMap_2d<COMPLEX_MESH_2D>::RF_FieldMap_2d(double Er, double Ez,
						const ComplexMesh2d &Br,
						const ComplexMesh2d &Bz,
						double hr_, double hz_, // m
						double length_,
						double frequency_,
						double direction_,
						double P_map_,
						double P_actual_ ) :	Nz(Br.size1()),
									Nr(Br.size2()),
									disable_Efield(true),
									disable_Bfield(false),
									hr(hr_*1e3), // accepts m, stores mm
									hz(hz_*1e3), // accepts m, stores mm
									z0(0.0), // mm
									omega(2.0 * M_PI * (frequency_ / (C_LIGHT*1e3))), // frequency in Hz
									direction(direction_ == 0.0 ? 0 : (direction_ > 0.0 ? +1 : -1)),
									P_map(P_map_), // W
									P_actual(P_actual_), // W
									scale_factor_and_phase(sqrt(P_actual / P_map)),
									phi(0.0),
									t0_is_set(false),
									t0(0.0),
									static_Bfield(0.0, 0.0, 0.0)
{
  Bfield.resize(Nz,Nr);
  for (size_t i=0; i<Nz; i++) {
    for (size_t j=0; j<Nr; j++) {
      Bfield.elem(i,j) = StaticVector<2,fftwComplex>(Bz.elem(i,j), Br.elem(i,j));
    }
  }
  set_nsteps(Nz-1);
  set_length(length_);
}

template <typename COMPLEX_MESH_2D>
RF_FieldMap_2d<COMPLEX_MESH_2D>::RF_FieldMap_2d(const ComplexMesh2d &Er,
						const ComplexMesh2d &Ez,
						double Br, double Bz,
						double hr_, double hz_, // m
						double length_,
						double frequency_,
						double direction_,
						double P_map_,
						double P_actual_ ) :	Nz(Er.size1()),
									Nr(Er.size2()),
									disable_Efield(false),
									disable_Bfield(true),
									hr(hr_*1e3), // accepts m, stores mm
									hz(hz_*1e3), // accepts m, stores mm
									z0(0.0), // mm
									omega(2.0 * M_PI * (frequency_ / (C_LIGHT*1e3))), // frequency in Hz
									direction(direction_ == 0.0 ? 0 : (direction_ > 0.0 ? +1 : -1)),
									P_map(P_map_), // W
									P_actual(P_actual_), // W
									scale_factor_and_phase(sqrt(P_actual / P_map)),
									phi(0.0),
									t0_is_set(false),
									t0(0.0),
									static_Bfield(0.0, 0.0, 0.0)
{
  Efield.resize(Nz,Nr);
  for (size_t i=0; i<Nz; i++) {
    for (size_t j=0; j<Nr; j++) {
      Efield.elem(i,j) = StaticVector<2,fftwComplex>(Ez.elem(i,j), Er.elem(i,j));
    }
  }
  set_nsteps(Nz-1);
  set_length(length_);
}

template <typename COMPLEX_MESH_2D>
RF_FieldMap_2d<COMPLEX_MESH_2D>::RF_FieldMap_2d() : Nz(1), Nr(1),
						    disable_Efield(true),
						    disable_Bfield(true),
						    hr(1e3), // accepts m, stores mm
						    hz(1e3), // accepts m, stores mm
						    z0(0.0), // mm
						    omega(0.0), // rad/(mm/c)
						    direction(0.0),
						    P_map(1.0), // W
						    P_actual(1.0), // W
						    scale_factor_and_phase(1.0),
						    phi(0.0),
						    t0_is_set(false),
						    t0(0.0),
						    static_Bfield(0.0, 0.0, 0.0)
{
  set_nsteps(0);
  set_length(0.0);
}

template <typename COMPLEX_MESH_2D>
void RF_FieldMap_2d<COMPLEX_MESH_2D>::set_length(double length_ )  // accepts m
{
  const double z1_max = (Nz-1)*hz;
  if (length_<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length_*1e3;
    // if (z1>z1_max) z1 = z1_max;
  }
}

template <typename COMPLEX_MESH_2D>
std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> RF_FieldMap_2d<COMPLEX_MESH_2D>::get_field_complex(double x /* mm */, double y /* mm */, double z /* mm */, double t  /* mm/c */ )
{
  // returned fields are in V/m and T
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>(0.0, 0.0);
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>(0.0, 0.0);
  z /= hz;
  // phase computation
  static std::mutex mutex;
  if (!t0_is_set) {
    if (mutex.try_lock()) {
      t0_is_set = true;
      t0 = t;
    } else {
      mutex.lock();
    }
    mutex.unlock();
  }
  const fftwComplex &f = [&] () {
    const double phase = direction * omega * (t - t0);
    const double c = cos(phase);
    const double s = sin(phase);
    return scale_factor_and_phase * fftwComplex(c,s); // complex
  } ();
  std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> field;
  if (x==0.0 && y==0.0) {
    if (disable_Efield) {
      field.first = StaticVector<3,fftwComplex>(0.0);
    } else {
      const fftwComplex Ez0 = Efield(z, 0.0)[0] * f; // V/m
      field.first = StaticVector<3,fftwComplex>(0.0, 0.0, Ez0);
    }
    if (disable_Bfield) {
      field.second = StaticVector<3,fftwComplex>(0.0);
    } else {
      const fftwComplex Bz0 = Bfield(z, 0.0)[0] * f; // T
      field.second = StaticVector<3,fftwComplex>(0.0, 0.0, Bz0);
    }
  } else {
    const double r = hypot(x, y); // mm
    const double r_ = r / hr; //
    if (disable_Efield) {
      field.first = StaticVector<3,fftwComplex>(0.0);
    } else {
      const auto E_2d = Efield(z, r_); // V/m
      const fftwComplex Ez = E_2d[0]; // V/m
      const fftwComplex Er_r = E_2d[1] / r; // V/m/mm
      const fftwComplex Ex = Er_r * x; // V/m
      const fftwComplex Ey = Er_r * y; // V/m
      field.first = StaticVector<3,fftwComplex>(Ex, Ey, Ez); // V/m
    }
    if (disable_Bfield) {
      field.second = StaticVector<3,fftwComplex>(0.0);
    } else {
      const auto B_2d = Bfield(z, r_); // T
      const fftwComplex Bz = B_2d[0]; // T
      const fftwComplex Br_r = B_2d[1] / r; // T/mm
      const fftwComplex Bx = Br_r * x; // T
      const fftwComplex By = Br_r * y; // T
      field.second = StaticVector<3,fftwComplex>(Bx, By, Bz); // T
    }
  }
  for(size_t i=0; i<3; i++) {
    if (static_Bfield[i]!=0.0) {
      field.second[i] += static_Bfield[i];
    }
  }
  if (disable_Efield && gsl_isnan(field.second[0].real)) {
    field.first = StaticVector<3,fftwComplex>(GSL_NAN);
  }
  return field;
}

template <typename COMPLEX_MESH_2D>
std::pair<StaticVector<3>,StaticVector<3>> RF_FieldMap_2d<COMPLEX_MESH_2D>::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  const auto &field = get_field_complex(x, y, z, t);
  return std::pair<StaticVector<3>, StaticVector<3>>(StaticVector<3>(real(field.first[0]),
								     real(field.first[1]),
								     real(field.first[2])),
						     StaticVector<3>(real(field.second[0]),
								     real(field.second[1]),
								     real(field.second[2])));
}

// template specializations
template class RF_FieldMap_2d<TMesh2d_LINT<StaticVector<2,fftwComplex>>>;
template class RF_FieldMap_2d<TMesh2d_CINT<StaticVector<2,fftwComplex>>>;
