/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "transport_table.hh"
#include "get_token.hh"

MatrixNd TransportTable::get_transport_table(const char *fmt ) const
{
  MatrixNd ret;
  if (!t_table.empty()) {
    size_t rows = t_table.size();
    size_t cols = 0; // will be initialized later
    size_t i = 0;
    bool error_msg = false;
    for (auto const &info : t_table) {
      std::vector<double> values;
      for(const char *ptr0 = fmt, *ptr = strchr(ptr0, '%'); ptr; ptr0 = ptr, ptr = strchr(ptr0, '%')) {
	/// print any constants in 'fmt', if present
	{
	  char *endptr;
	  std::string nptr(ptr0, size_t(ptr-ptr0));
	  double value = strtod(nptr.c_str(), &endptr);
	  if (value!=0.0 || endptr!=nptr.c_str()) {
	    values.push_back(value);
	  }
	}
	ptr++;
	double value = 0.0;
	if      (get_token(&ptr, "sigma_xp")) value = info.sigma_xp;
	else if (get_token(&ptr, "sigma_yp")) value = info.sigma_yp;
	else if (get_token(&ptr, "emitt_4d")) value = info.emitt_4d;
	else if (get_token(&ptr, "sigma_x")) value = info.sigma_x;
	else if (get_token(&ptr, "sigma_y")) value = info.sigma_y;
	else if (get_token(&ptr, "sigma_z")) value = info.sigma_z;
	else if (get_token(&ptr, "sigma_t")) value = info.sigma_t;
	else if (get_token(&ptr, "sigma_E")) value = info.sigma_E;
	else if (get_token(&ptr, "sigma_P")) value = info.sigma_P;
	else if (get_token(&ptr, "emitt_x")) value = info.emitt_x;
	else if (get_token(&ptr, "emitt_y")) value = info.emitt_y;
	else if (get_token(&ptr, "emitt_z")) value = info.emitt_z;
	else if (get_token(&ptr, "alpha_x")) value = info.alpha_x;
	else if (get_token(&ptr, "alpha_y")) value = info.alpha_y;
	else if (get_token(&ptr, "alpha_z")) value = info.alpha_z;
	else if (get_token(&ptr, "mean_xp")) value = info.mean_xp;
	else if (get_token(&ptr, "mean_yp")) value = info.mean_yp;
	else if (get_token(&ptr, "mean_x")) value = info.mean_x;
	else if (get_token(&ptr, "mean_y")) value = info.mean_y;
	else if (get_token(&ptr, "mean_t")) value = info.mean_t;
	else if (get_token(&ptr, "mean_P")) value = info.mean_P;
	else if (get_token(&ptr, "mean_K")) value = info.mean_K;
	else if (get_token(&ptr, "mean_E")) value = info.mean_E;
	else if (get_token(&ptr, "beta_x")) value = info.beta_x;
	else if (get_token(&ptr, "beta_y")) value = info.beta_y;
	else if (get_token(&ptr, "beta_z")) value = info.beta_z;
	else if (get_token(&ptr, "disp_xp")) value = info.disp_xp;
	else if (get_token(&ptr, "disp_yp")) value = info.disp_yp;
	else if (get_token(&ptr, "disp_x")) value = info.disp_x;
	else if (get_token(&ptr, "disp_y")) value = info.disp_y;
	else if (get_token(&ptr, "rmax")) value = info.rmax;
	else if (get_token(&ptr, 'N')) value = info.transmission;
	else if (get_token(&ptr, 'S')) value = info.S;
	else {
	  if (!error_msg) {
	    const char *next = strpbrk(ptr, "% \n\t");
	    std::string id = next ? std::string(ptr, next-ptr) : ptr;
	    std::cerr << "warning: unknown identifier '%" << id << "'\n";
	    error_msg = true;
	  }
	  continue;
	}
	values.push_back(value);
      }
      if (cols == 0) {
	cols = values.size();
	ret.resize(rows, cols);
      }
      for (size_t j = 0; j<cols; j++) {
	ret[i][j] = values[j];
      }
      i++;
    }
  } else if (!t_tableT.empty()) {
    size_t rows = t_tableT.size();
    size_t cols = 0; // will be initialized later
    size_t i = 0;
    bool error_msg = false;
    for (auto const &info : t_tableT) {
      std::vector<double> values;
      for(const char *ptr0 = fmt, *ptr = strchr(ptr0, '%'); ptr; ptr0 = ptr, ptr = strchr(ptr0, '%')) {
	/// print any constants in 'fmt', if present
	{
	  char *endptr;
	  std::string nptr(ptr0, size_t(ptr-ptr0));
	  double value = strtod(nptr.c_str(), &endptr);
	  if (value!=0.0 || endptr!=nptr.c_str()) {
	    values.push_back(value);
	  }
	}
	ptr++;
	double value = 0.0;
	if      (get_token(&ptr, "sigma_Px")) value = info.sigma_Px;
	else if (get_token(&ptr, "sigma_Py")) value = info.sigma_Py;
	else if (get_token(&ptr, "sigma_Pz")) value = info.sigma_Pz;
	else if (get_token(&ptr, "emitt_4d")) value = info.emitt_4d;
	else if (get_token(&ptr, "sigma_X")) value = info.sigma_X;
	else if (get_token(&ptr, "sigma_Y")) value = info.sigma_Y;
	else if (get_token(&ptr, "sigma_Z")) value = info.sigma_Z;
	else if (get_token(&ptr, "sigma_E")) value = info.sigma_E;
	else if (get_token(&ptr, "emitt_x")) value = info.emitt_x;
	else if (get_token(&ptr, "emitt_y")) value = info.emitt_y;
	else if (get_token(&ptr, "emitt_z")) value = info.emitt_z;
	else if (get_token(&ptr, "alpha_x")) value = info.alpha_x;
	else if (get_token(&ptr, "alpha_y")) value = info.alpha_y;
	else if (get_token(&ptr, "alpha_z")) value = info.alpha_z;
	else if (get_token(&ptr, "mean_Px")) value = info.mean_Px;
	else if (get_token(&ptr, "mean_Py")) value = info.mean_Py;
	else if (get_token(&ptr, "mean_Pz")) value = info.mean_Pz;
	else if (get_token(&ptr, "mean_X")) value = info.mean_X;
	else if (get_token(&ptr, "mean_Y")) value = info.mean_Y;
	else if (get_token(&ptr, "mean_S")) value = info.mean_S;
	else if (get_token(&ptr, "mean_K")) value = info.mean_K;
	else if (get_token(&ptr, "mean_E")) value = info.mean_E;
	else if (get_token(&ptr, "beta_x")) value = info.beta_x;
	else if (get_token(&ptr, "beta_y")) value = info.beta_y;
	else if (get_token(&ptr, "beta_z")) value = info.beta_z;
	else if (get_token(&ptr, "disp_xp")) value = info.disp_xp;
	else if (get_token(&ptr, "disp_yp")) value = info.disp_yp;
	else if (get_token(&ptr, "disp_x")) value = info.disp_x;
	else if (get_token(&ptr, "disp_y")) value = info.disp_y;
	else if (get_token(&ptr, "rmax")) value = info.rmax;
	else if (get_token(&ptr, 'N')) value = info.transmission;
	else if (get_token(&ptr, 't')) value = info.t;
	else {
	  if (!error_msg) {
	    const char *next = strpbrk(ptr, "% \n\t");
	    std::string id = next ? std::string(ptr, next-ptr) : ptr;
	    std::cerr << "warning: unknown identifier '%" << id << "'\n";
	    error_msg = true;
	  }
	  continue;
	}
	values.push_back(value);
      }
      if (cols == 0) {
	cols = values.size();
	ret.resize(rows, cols);
      }
      for (size_t j = 0; j<cols; j++) {
	ret[i][j] = values[j];
      }
      i++;
    }
  } else {
    std::cerr << "error: no transport table in memory!\n";
  }
  return ret;
}
