/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include "adiabatic_matching_device.hh"

std::pair<StaticVector<3>,StaticVector<3>> AdiabaticMatchingDevice::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  if (z<0 || z>1e3*length)
    return std::pair<StaticVector<3>, StaticVector<3>>(0.0, 0.0);
  const double r = hypot(x,y); // mm
  if (r1 >= 0.0 && r2 >= 0.0) {
    const double ra = r1 + (r2 - r1) * z / (1e3*length);
    if (r > ra)
      return std::pair<StaticVector<3>, StaticVector<3>>(GSL_NAN, GSL_NAN);
  }
  if (Bmax==0.0)
    return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, static_Bfield);
  // See CLIC note 465 https://cds.cern.ch/record/492189?ln=en
  const double Z = z/1e3; // m
  const double Bz0 = Bmax / (1+fMu*Z); // T
  if (r==0.0)
    return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, {{{ static_Bfield[0], static_Bfield[1], static_Bfield[2] + Bz0 }}});
  const double R = r/1e3; // m
  const double k = R*fMu/(1+fMu*Z);
  const double k2 = k*k;
  const double l = k2 * (3./8. - k2 * 5./16.);
  const double Bz = Bz0 * (1. - k2 * (0.5 + l));
  const double Br = Bz0 * k * (0.5 - l);
  return std::pair<StaticVector<3>, StaticVector<3>>(static_Efield, static_Bfield + StaticVector<3>(Br*x/r, Br*y/r, Bz));
}
