/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <complex>
#include "RF_Track.hh"
#include "quadrupole.hh"

void Quadrupole::track0(Particle &particle, double S, size_t start_step, size_t end_step, size_t number_of_steps, size_t thread ) const
{
  const double Brho = particle.Pc / particle.Q; // MV/c
  if (length==0.0) {
    const double k1L = strength * (end_step - start_step) / number_of_steps / Brho; // 1/m
    particle.xp -= k1L * particle.x; // mrad
    particle.yp += k1L * particle.y; // mrad
    return;
  }
  const double dS = length / number_of_steps; // m
  const double Kx = strength / length / Brho; // 1/m**2
  const auto init_matrix_elements = [] (double K, double length, double &C, double &ksqrt_S, double &S_over_ksqrt ) {
    if (K!=0.0) {
      const std::complex<double> ksqrt = sqrt(std::complex<double>(K)); // 1/m
      const std::complex<double> S = sin(ksqrt*length);
      C            = real(cos(ksqrt*length)); // 1
      ksqrt_S      = real(ksqrt*S); // 1/m
      S_over_ksqrt = real(S/ksqrt); // m
    } else {
      C            = 1.0; // 1
      ksqrt_S      = 0.0; // 1/m
      S_over_ksqrt = length; // m
    }
  };
  double Cx, Cy, ksqrt_Sx, ksqrt_Sy, Sx_over_ksqrt, Sy_over_ksqrt;
  init_matrix_elements( Kx, dS, Cx, ksqrt_Sx, Sx_over_ksqrt);
  init_matrix_elements(-Kx, dS, Cy, ksqrt_Sy, Sy_over_ksqrt);
  const double V = particle.get_beta(); // c
  for (size_t step=start_step; step<end_step; step++) {
    if (!is_particle_inside_aperture(particle)) {
      particle.lost_at(S + (double(step) + 0.5) * dS);
      break;
    }
    const double
      x  = particle.x, // mm
      y  = particle.y, // mm
      xp = particle.xp, // mrad
      yp = particle.yp; // mrad
    // transfer map
    particle.x = Cx * x + Sx_over_ksqrt * xp; // mm
    particle.y = Cy * y + Sy_over_ksqrt * yp; // mm
    particle.xp = -ksqrt_Sx * x + Cx * xp; // mrad
    particle.yp = -ksqrt_Sy * y + Cy * yp; // mrad
    particle.t += (dS * 1e3 + 0.5 * (0.5 * Kx * ((x*x)*(dS-Cx*Sx_over_ksqrt) -   (y*y)*(dS-Cy*Sy_over_ksqrt)) +
				     0.5 *    ((xp*xp)*(dS+Cx*Sx_over_ksqrt) + (yp*yp)*(dS+Cy*Sy_over_ksqrt)) -
				     (x*xp*(1.0-Cx*Cx) + y*yp*(1.0-Cy*Cy))) / 1e3) / V; // mm/c
  }
}
