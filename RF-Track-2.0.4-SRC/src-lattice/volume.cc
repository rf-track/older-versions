/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <cstdio>
#include <list>
#include <string>
#include <iomanip>

#include "volume.hh"
#include "RF_Track.hh"
#include "move_particle.hh"

void Volume::add_ref(std::shared_ptr<Element> element_ptr,
		     double x0, double y0, double z0, // m
		     double roll, double pitch, double yaw, // rad
		     const std::string &reference )
{
  const double L_mm = 1e3*element_ptr->get_length(); // mm
  // Tait-Bryan angles to accelerator convention
  const double rotX = pitch;
  const double rotY = yaw;
  const double rotZ = roll;
  Frame frame = Frame(1e3*Vector3d(x0, y0, z0), Rotation(rotZ, rotY, rotX)); // mm, rad 
  if (reference == "center") {
    frame *= Frame(Vector3d(0.0, 0.0, -0.5*L_mm));
  } else if (reference == "exit") {
    frame *= Frame(Vector3d(0.0, 0.0, -L_mm));
  } else if (reference != "entrance") {
    std::cerr << "error: argument 'reference' must be either 'entrance', 'center', or 'exit'\n";
  }
  if (getenv("DEBUG")) {
    std::cerr << "info: element entrance =" << frame * Vector3d(0.0, 0.0, 0.0) << std::endl;
    std::cerr << "info:         center =\t" << frame * Vector3d(0.0, 0.0, 0.5*L_mm) << std::endl;
    std::cerr << "info:         exit =\t"   << frame * Vector3d(0.0, 0.0, L_mm) << std::endl;
  }
  const double element_entrance = frame.get_origin()[2] / 1e3; // m
  const double element_exit = (frame * Vector3d(0.0, 0.0, L_mm))[2] / 1e3; // m
  const Frame element_frame = element_ptr->get_frame();
  if (element_entrance < s0) s0 = element_entrance;
  if (element_exit > s1) s1 = element_exit;
  element_ptr->zero_offsets();
  elements.push_back(Element_3d(frame * element_frame, element_ptr));
}

void Volume::add(std::shared_ptr<const Lattice> lattice_ptr,
		 double x0, double y0, double z0, // m
		 double roll, double pitch, double yaw, // rad
		 const std::string &reference )
{
  const double L_mm = 1e3*lattice_ptr->get_length(); // mm
  // Tait-Bryan angles to accelerator convention
  const double rotX = pitch;
  const double rotY = yaw;
  const double rotZ = roll;
  Frame frame = Frame(1e3*Vector3d(x0, y0, z0), Rotation(rotZ, rotY, rotX)); // mm, rad 
  if (reference == "center") {
    frame *= Frame(Vector3d(0.0, 0.0, -0.5*L_mm));
  } else if (reference == "exit") {
    frame *= Frame(Vector3d(0.0, 0.0, -L_mm));
  } else if (reference != "entrance") {
    std::cerr << "error: argument 'reference' must be either 'entrance', 'center', or 'exit'\n";
  }
  if (getenv("DEBUG")) {
    std::cerr << "info: lattice entrance =" << frame * Vector3d(0.0, 0.0, 0.0) << std::endl;
    std::cerr << "info:         center =\t" << frame * Vector3d(0.0, 0.0, 0.5*L_mm) << std::endl;
    std::cerr << "info:         exit =\t"   << frame * Vector3d(0.0, 0.0, L_mm) << std::endl;
  }
  for (size_t i=0; i<lattice_ptr->size(); i++) {
    const auto element_ptr = (*lattice_ptr)[i];
    const double EL_mm = 1e3*element_ptr->get_length(); // mm
    const double element_entrance = frame.get_origin()[2] / 1e3; // m
    const double element_exit = (frame * Vector3d(0.0, 0.0, EL_mm))[2] / 1e3; // m
    if (getenv("DEBUG")) {
      std::cerr << "info: +-> element entrance =\t" << frame * Vector3d(0.0, 0.0, 0.0) << std::endl;
      std::cerr << "info:             center =\t" << frame * Vector3d(0.0, 0.0, 0.5*EL_mm) << std::endl;
      std::cerr << "info:             exit =\t"   << frame * Vector3d(0.0, 0.0, EL_mm) << std::endl;
    }
    if (element_entrance < s0) s0 = element_entrance;
    if (element_exit > s1) s1 = element_exit;
    auto new_element_ptr = element_ptr->clone();
    new_element_ptr->zero_offsets();
    elements.push_back(Element_3d(frame * element_ptr->get_frame(), new_element_ptr)); // applies small misalignments from Element
    frame *= Frame(Vector3d(0.0, 0.0, EL_mm));
  }
}

void Volume::add(std::shared_ptr<const Volume> volume_ptr,
		 double x0, double y0, double z0, // m
		 double roll, double pitch, double yaw, // rad
		 const std::string &reference )
{
  const double L_mm = 1e3*volume_ptr->get_length(); // mm
  // Tait-Bryan angles to accelerator convention
  const double rotX = pitch;
  const double rotY = yaw;
  const double rotZ = roll;
  Frame frame = Frame(1e3*Vector3d(x0, y0, z0), Rotation(rotZ, rotY, rotX)); // mm, rad 
  if (reference == "center") {
    frame *= Frame(Vector3d(0.0, 0.0, -0.5*L_mm));
  } else if (reference == "exit") {
    frame *= Frame(Vector3d(0.0, 0.0, -L_mm));
  } else if (reference != "entrance") {
    std::cerr << "error: argument 'reference' must be either 'entrance', 'center', or 'exit'\n";
  }
  if (getenv("DEBUG")) {
    std::cerr << "info: volume entrance =" << frame * Vector3d(0.0, 0.0, 0.0) << std::endl;
    std::cerr << "info:        center =\t" << frame * Vector3d(0.0, 0.0, 0.5*L_mm) << std::endl;
    std::cerr << "info:        exit =\t"   << frame * Vector3d(0.0, 0.0, L_mm) << std::endl;
  }
  for (const auto &element_3d: volume_ptr->elements) {
    const auto element_ptr = element_3d.element_ptr;
    Frame new_frame = frame * element_3d.frame * element_ptr->get_frame(); // applies also small misalignments from Element
    const double EL_mm = 1e3*element_ptr->get_length(); // mm
    const double element_entrance = new_frame.get_origin()[2] / 1e3; // m
    const double element_exit = (new_frame * Vector3d(0.0, 0.0, EL_mm))[2] / 1e3; // m
    if (getenv("DEBUG")) {
      std::cerr << "info: +-> element entrance =\t" << frame * Vector3d(0.0, 0.0, 0.0) << std::endl;
      std::cerr << "info:             center =\t" << frame * Vector3d(0.0, 0.0, 0.5*EL_mm) << std::endl;
      std::cerr << "info:             exit =\t"   << frame * Vector3d(0.0, 0.0, EL_mm) << std::endl;
    }
    if (element_entrance < s0) s0 = element_entrance;
    if (element_exit > s1) s1 = element_exit;
    auto new_element_ptr = element_ptr->clone();
    new_element_ptr->zero_offsets();
    elements.push_back(Element_3d(new_frame, new_element_ptr));
  }
}

Volume &Volume::operator = (const Lattice &lattice )
{
  s0 = s1 = 0.0;
  elements.clear();
  transport_table.clear();
  bunch6d_at_s0_mm.clear();
  bunch6d_at_s1_mm.clear();
  double S_last = 0.0;
  for (size_t i=0; i<lattice.size(); i++) {
    auto element_ptr = lattice[i];
    add(element_ptr, 0, 0, S_last);
    S_last += element_ptr->get_length();
  }
  return *this;
}

int Volume::func(double t_mm, const double Y[], double dY[], void *params_ptr )
{
  // init parameters
  const Params &params = *(const Params *)(params_ptr);
  const double mass = params.mass;
  const double q = params.charge;
  const double s0_mm = params.s0_mm; // mm
  const double s1_mm = params.s1_mm; // mm
  const auto &self = *params.self;

  // system of equations for GSL
  // X_prime = V[0]; // c
  // Y_prime = V[1]; // c
  // S_prime = V[2]; // c
  // Px_prime = F[0]; // MeV/mm
  // Py_prime = F[1]; // MeV/mm
  // Pz_prime = F[2]; // MeV/mm

  if (Y[2]>=s0_mm && Y[2]<s1_mm && !self.is_point_inside_aperture(Y[0], Y[1])) {
    return GSL_EBADFUNC;
  }

  const auto &P = StaticVector<3>(Y[3], Y[4], Y[5]); // MeV/c
  const auto &V = P / hypot(mass,P); // c
  dY[0] = V[0]; // c
  dY[1] = V[1]; // c
  dY[2] = V[2]; // c
  // if (Y[2]<s0_mm || Y[2]>=s1_mm) { // if outside Volume
  //   dY[3] = 0.0; // MeV/mm
  //   dY[4] = 0.0; // MeV/mm
  //   dY[5] = 0.0; // MeV/mm
  //   return GSL_SUCCESS;
  // }

  const auto &field = self.get_field(Y[0], Y[1], Y[2], t_mm);
  const auto &Ef = field.first; // V/m
  const auto &Bf = field.second; // T
  if (gsl_isnan(Ef[0])) { // signal particle loss
    return GSL_EBADFUNC;
  }
  const auto &F = q * ((Bf[0] != 0.0 || Bf[1] != 0.0 || Bf[2] != 0.0) ? Ef + cross(V * C_LIGHT, Bf) : Ef) / 1e9; // MeV/mm
  dY[3] = F[0]; // MeV/mm
  dY[4] = F[1]; // MeV/mm
  dY[5] = F[2]; // MeV/mm
  return GSL_SUCCESS;
}

int Volume::jac(double t_mm, const double Y[], double *dfdy, double dfdt[], void *params_ptr )
{
  // init parameters
  const Params &params = *(const Params *)(params_ptr);
  const double mass = params.mass;
  const double q = params.charge;
  const double s0_mm = params.s0_mm; // mm
  const double s1_mm = params.s1_mm; // mm
  const auto &self = *params.self;
  const auto &elements = self.elements;

  // system of equations for GSL
  // X_prime = V[0]; // c
  // Y_prime = V[1]; // c
  // S_prime = V[2]; // c
  // Px_prime = F[0]/1e3; // MeV/mm
  // Py_prime = F[1]/1e3; // MeV/mm
  // Pz_prime = F[2]/1e3; // MeV/mm

  if (Y[2]>=s0_mm && Y[2]<s1_mm && !self.is_point_inside_aperture(Y[0], Y[1])) {
    return GSL_EBADFUNC;
  }

  const auto &X = Vector3d(Y[0], Y[1], Y[2]); // mm
  const auto &P = StaticVector<3>(Y[3], Y[4], Y[5]); // MeV/c
  const auto &E = hypot(mass,P); // MeV
  const auto &V = P / E; // c

  // derivative of force with time
  for (int i=0; i<6; i++)
    dfdt[i] = 0.0;
  
  // jacobian of force with Y
  gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy, 6, 6);
  gsl_matrix *m = &dfdy_mat.matrix;
  gsl_matrix_set_zero (m);
  
  const double inv_E3 = 1.0 / (E*E*E); 
  gsl_matrix_set (m, 0, 3, (mass*mass + P[1]*P[1] + P[2]*P[2]) * inv_E3); // dVx / dPx
  gsl_matrix_set (m, 0, 4, -P[0]*P[1] * inv_E3); // dVx / dPy
  gsl_matrix_set (m, 0, 5, -P[0]*P[2] * inv_E3); // dVx / dPz
  gsl_matrix_set (m, 1, 3, -P[1]*P[0] * inv_E3); // dVy / dPx
  gsl_matrix_set (m, 1, 4, (mass*mass + P[2]*P[2] + P[0]*P[0]) * inv_E3); // dVy / dPy
  gsl_matrix_set (m, 1, 5, -P[1]*P[2] * inv_E3); // dVy / dPz
  gsl_matrix_set (m, 2, 3, -P[2]*P[0] * inv_E3); // dVz / dPx
  gsl_matrix_set (m, 2, 4, -P[2]*P[1] * inv_E3); // dVz / dPy
  gsl_matrix_set (m, 2, 5, (mass*mass + P[0]*P[0] + P[1]*P[1]) * inv_E3); // dVz / dPz
    
  // if (Y[2]<s0_mm || Y[2]>=s1_mm) {
  //   return GSL_SUCCESS;
  // }

  for (const auto &element_3d: elements) {
    const Vector3d &X_ = X / element_3d.frame; // mm, in the element's reference frame
    // check if the particle is inside the element's aperture
    Element *element_ptr = element_3d.element_ptr.get();
    if (X_[2] >= 0.0 && X_[2] < element_3d.length_mm && !element_ptr->is_point_inside_aperture(X_[0], X_[1])) {
      return GSL_EBADFUNC;
    }
    // get E and B fields
    auto &&field = element_ptr->get_field(X_[0], X_[1], X_[2], t_mm);
    if (gsl_isnan(field.first[0])) { // trigger particle loss
      return GSL_EBADFUNC;
    }
    
    // in the element's frame
    field.first /= 1e9; // V/m -> MV/mm
    field.second *= C_LIGHT / 1e9; // T*c = V/m -> MV/mm
    const auto &Ef_ = field.first; // MV/mm
    const auto &Bf_ = field.second; // MV/mm

    auto &&jacobian = element_ptr->get_field_jacobian(X_[0], X_[1], X_[2], t_mm);
    jacobian.first /= 1e6; // V/m/mm -> MV/m/mm
    jacobian.second *= C_LIGHT / 1e6; // T/mm*c = V/m/mm -> MV/m/mm
    const auto &Ef_jac_ = jacobian.first; // MV/m/mm
    const auto &Bf_jac_ = jacobian.second; // MV/m/mm

    // to the lab's frame
    const auto &Ef = element_3d.frame.get_axes() * Ef_; // MeV/mm, rotate back to the lab's frame
    const auto &Bf = element_3d.frame.get_axes() * Bf_; // MeV/mm, rotate back to the lab's frame
    const auto &Ef_jac = element_3d.rotation_matrix * Ef_jac_; // MV/m/mm
    const auto &Bf_jac = element_3d.rotation_matrix * Bf_jac_; // MV/m/mm
    
    // in the lab's frame
    const auto &F = q * (Ef + cross(V, Bf)); // MeV/mm
    const auto &a = (F - dot(V,F)*V) / E; // c^2/mm, in the lab's frame

    for (int i=0; i<3; i++)
      dfdt[i] += a[i];
    
    (*gsl_matrix_ptr (m, 3, 4)) +=  q * Bf[2] * gsl_matrix_get (m, 0, 3); // dFx / dPy
    (*gsl_matrix_ptr (m, 3, 5)) += -q * Bf[1] * gsl_matrix_get (m, 2, 5); // dFx / dPz
    (*gsl_matrix_ptr (m, 4, 3)) += -q * Bf[2] * gsl_matrix_get (m, 1, 4); // dFy / dPx
    (*gsl_matrix_ptr (m, 4, 5)) +=  q * Bf[0] * gsl_matrix_get (m, 2, 5); // dFy / dPz
    (*gsl_matrix_ptr (m, 5, 3)) +=  q * Bf[1] * gsl_matrix_get (m, 0, 3); // dFz / dPx
    (*gsl_matrix_ptr (m, 5, 4)) += -q * Bf[0] * gsl_matrix_get (m, 1, 4); // dFz / dPy
    
    (*gsl_matrix_ptr (m, 3, 0)) += q * (Ef_jac[0][0] + V[1] * Bf_jac[2][0] - V[2] * Bf_jac[1][0]); // MeV/m/mm
    (*gsl_matrix_ptr (m, 3, 1)) += q * (Ef_jac[0][1] + V[1] * Bf_jac[2][1] - V[2] * Bf_jac[1][1]); // MeV/m/mm
    (*gsl_matrix_ptr (m, 3, 2)) += q * (Ef_jac[0][2] + V[1] * Bf_jac[2][2] - V[2] * Bf_jac[1][2]); // MeV/m/mm
    (*gsl_matrix_ptr (m, 4, 0)) += q * (Ef_jac[1][0] + V[2] * Bf_jac[0][0] - V[0] * Bf_jac[2][0]); // MeV/m/mm
    (*gsl_matrix_ptr (m, 4, 1)) += q * (Ef_jac[1][1] + V[2] * Bf_jac[0][1] - V[0] * Bf_jac[2][1]); // MeV/m/mm
    (*gsl_matrix_ptr (m, 4, 2)) += q * (Ef_jac[1][2] + V[2] * Bf_jac[0][2] - V[0] * Bf_jac[2][2]); // MeV/m/mm
    (*gsl_matrix_ptr (m, 5, 0)) += q * (Ef_jac[2][0] + V[0] * Bf_jac[1][0] - V[1] * Bf_jac[0][0]); // MeV/m/mm
    (*gsl_matrix_ptr (m, 5, 1)) += q * (Ef_jac[2][1] + V[0] * Bf_jac[1][1] - V[1] * Bf_jac[0][1]); // MeV/m/mm
    (*gsl_matrix_ptr (m, 5, 2)) += q * (Ef_jac[2][2] + V[0] * Bf_jac[1][2] - V[1] * Bf_jac[0][2]); // MeV/m/mm
  }

  return GSL_SUCCESS;
}

std::pair<StaticVector<3>, StaticVector<3>> Volume::get_field(double x, double y, double z, double t ) const
{
  std::pair<StaticVector<3>, StaticVector<3>> retval(0.0, 0.0);
  auto &Esum = retval.first; // V/m
  auto &Bsum = retval.second; // T
  for (const auto &element_3d: elements) {
    const Vector3d &X_ = Vector3d(x, y, z) / element_3d.frame; // mm, position in the element's reference frame
    // check if the particle is inside the element's aperture
    Element *element_ptr = element_3d.element_ptr.get();
    if (X_[2] >= 0.0 && X_[2] < element_3d.length_mm && !element_ptr->is_point_inside_aperture(X_[0], X_[1])) {
      Esum = StaticVector<3>(GSL_NAN);
      Bsum = StaticVector<3>(GSL_NAN);
      break;
    }
    // cumulate fields
    const auto &field = element_ptr->get_field(X_[0], X_[1], X_[2], t);
    auto &Ef = field.first; // V/m
    auto &Bf = field.second; // T
    if (gsl_isnan(Ef[0])) { // signal particle loss
      Esum = StaticVector<3>(GSL_NAN);
      Bsum = StaticVector<3>(GSL_NAN);
      break;
    }
    if (Ef[0] != 0.0 || Ef[1] != 0.0 || Ef[2] != 0.0) Esum += element_3d.frame.get_axes() * Ef; // V/m
    if (Bf[0] != 0.0 || Bf[1] != 0.0 || Bf[2] != 0.0) Bsum += element_3d.frame.get_axes() * Bf; // T
  }
  return retval;
}

Bunch6dT Volume::track(Bunch6dT bunch, const TrackingOptions &to )
{
  transport_table.clear();
  bunch6d_at_s0_mm.clear();
  bunch6d_at_s1_mm.clear();

  // initialize auxiliary variables
  const double s0_mm = s0 * 1e3; // mm, start of beamline
  const double s1_mm = s1 * 1e3; // mm, end of beamline

  // apply a negative drift to bring S_max to s0_mm
  if (to.backtrack_at_entrance) {
    double dt_max = -std::numeric_limits<double>::infinity();
    for (size_t i=0; i<bunch.size(); i++) {
      const auto &particle = bunch[i];
      if (particle && bunch.t>=particle.t0) {
	const double Vz = particle.get_Vx_Vy_Vz()[2]; // c
	const double dt = (particle.S - s0_mm) / Vz; // mm/c
	if (dt > dt_max)
	  dt_max = dt;
      }
    }
    if (dt_max != -std::numeric_limits<double>::infinity()) {
      for (size_t i=0; i<bunch.size(); i++) {
	auto &particle = bunch[i];
	if (particle && bunch.t>=particle.t0) {
	  const auto dX = particle.get_Vx_Vy_Vz() * dt_max; // mm
	  particle.X -= dX[0]; // mm
	  particle.Y -= dX[1]; // mm
	  particle.S -= dX[2]; // mm
	}
      }
      bunch.t -= dt_max; // mm/c
    }
  }
  
  // max duration of the integration, or 0.0 for tracking until all particles left the volume
  const double dt_max_mm = gsl_isinf(to.t_max_mm) ? 0.0 : (to.t_max_mm - bunch.t); // mm/c
  
  if (dt_max_mm<0.0) {
    if (to.verbosity>0)
      std::cout << "\ninfo: total number of integration steps = 0";
    return bunch;
  }

  const double dt_mm = [&] () -> double {
    double dt_mm = to.dt_mm;
    if (to.sc_dt_mm) dt_mm = std::min(to.sc_dt_mm, dt_mm);
    if (to.wp_dt_mm) dt_mm = std::min(to.wp_dt_mm, dt_mm);
    if (to.tt_dt_mm) dt_mm = std::min(to.tt_dt_mm, dt_mm);
    return dt_mm;
  } ();

  const size_t sc_nsteps = size_t(floor(to.sc_dt_mm / dt_mm));
  const size_t wp_nsteps = size_t(floor(to.wp_dt_mm / dt_mm));
  const size_t tt_nsteps = size_t(floor(to.tt_dt_mm / dt_mm));
  const size_t nsteps    = size_t(floor(dt_max_mm / dt_mm));

  // initialize auxiliary integration variables
  std::vector<Params> params(RFT::number_of_threads);
  for (auto &params_: params) {
    params_.s0_mm = s0_mm; // mm, start of beamline
    params_.s1_mm = s1_mm; // mm, end of beamline
    params_.self = this; // from volume
  }

  // init GSL ODE
  Parallel_ODE_Solver pos;
  pos.set_odeint_algorithm(to.odeint_algorithm.c_str());
  std::vector<gsl_odeiv2_system> sys;
  if (pos.use_gsl()) {
    sys.resize(RFT::number_of_threads);
    for (size_t i=0; i<RFT::number_of_threads; i++)
      sys[i] = { func, jac, 6, &params[i] };
    pos.init_gsl_drivers(sys);
  }

  std::vector<bool> outside_boundaries(bunch.size(), false); // use to ingore particles outside the boundaries

  // check if there are particles that need to be tracked
  auto do_particles_need_to_be_tracked = [&] () { // true: continue tracking; false: end tracking
    for (size_t i=0; i<bunch.size(); i++) {
      const auto &particle = bunch[i];
      if (particle && particle.S>=s0_mm && particle.S<s1_mm) {
	return true;
      }
    }
    return false;
  };

  if (tt_nsteps)
    transport_table.append_bunch_info(bunch);

  // tracking starts
  
  // main loop
  bool gsl_error = false;
  size_t iterations_cnt = 0;
  size_t sc_iterations_cnt = 0;
  std::list<Particle> particles_at_s0_mm;
  std::list<Particle> particles_at_s1_mm;
  const size_t nloop = [&] () { // number of steps per iteration loop, i.e. between one space-charge step and the next
    size_t xx_nsteps = sc_nsteps;
    if (wp_nsteps) xx_nsteps = xx_nsteps ? std::min(xx_nsteps, wp_nsteps) : wp_nsteps;
    if (tt_nsteps) xx_nsteps = xx_nsteps ? std::min(xx_nsteps, tt_nsteps) : tt_nsteps;
    if (xx_nsteps) return xx_nsteps;
    if (nsteps)    return nsteps;
    return std::max(size_t(floor((s1_mm - s0_mm) / dt_mm)), size_t(100));
  } ();

  size_t wp_index = 0;
  if (wp_nsteps != 0) {
    const bool gzip = to.wp_gzip;
    std::stringstream filename;
    if (gzip) filename << std::string("gzip -9 >");
    filename << to.wp_basename << '.' << std::setw(8) << std::setfill('0') << wp_index++ << ".txt";
    if (gzip) filename << ".gz";
    if (FILE *file = gzip ? popen(filename.str().c_str(), "w") : fopen(filename.str().c_str(), "w")) {
      fprintf(file, "# t = %g mm/c\n", bunch.t); 
    for (size_t i=0; i<bunch.size(); i++) {
      const auto &particle = bunch[i];
	if (particle) {
	  fprintf(file, "%.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g\n",
		  particle.X,    // mm
		  particle.Px,   // MeV/c
		  particle.Y,    // mm
		  particle.Py,   // MeV/c
		  particle.S,    // mm
		  particle.Pz,   // MeV/c
		  particle.mass, // MeV/c^2
		  particle.Q,    // e+
		  particle.N);   //
	}
      }
      if (gzip)
	pclose(file);
      else
	fclose(file);
    } else {
      std::cerr << "error: cannot open output file '" << filename.str() << "'\n";
    }
  }
  
  MatrixNd sc_force;
  bool loop;
  do {

    if (to.verbosity>0)
      std::cout << "\33[2K\rinfo: tracking in progress, t = " << bunch.t << " mm/c" << std::flush;
    
    struct ParticleSelector_exists : public SpaceCharge::ParticleSelector {
      const double &t_mm; // mm/c
      bool operator () (const ParticleT &p ) const { return p && t_mm>=p.t0; }
      ParticleSelector_exists(const double &t_mm_ ) : t_mm(t_mm_) {}
    } particle_exists(bunch.t);
    
    // first step of space charge (half kick)
    if (sc_nsteps && iterations_cnt==0) {
    
      if (to.verbosity>1)
	std::cout << " (applying space-charge kick #" << sc_iterations_cnt++ << ')' << std::flush;

      RFT::SC_engine->compute_force(sc_force, bunch, particle_exists);
      const double dt = (nloop * dt_mm) / 2e3; // m/c, applies half step
      for (size_t i=0; i<bunch.size(); i++) {
	auto &particle = bunch[i];
	if (particle_exists(particle)) {
	  const auto dP = StaticVector<3>(sc_force[i]) * dt; // MeV/c
	  particle.Px += dP[0]; // MeV/c
	  particle.Py += dP[1]; // MeV/c
	  particle.Pz += dP[2]; // MeV/c
	}
      }
    }
    
    // do tracking
    auto drift_particle = [] (ParticleT &particle, const double &dt_mm ) {
      const auto &dX = particle.get_Vx_Vy_Vz() * dt_mm; // mm
      particle.X += dX[0]; // mm
      particle.Y += dX[1]; // mm
      particle.S += dX[2]; // mm
    };

    auto track_parallel = [&] (size_t thread, size_t start, size_t end ) {
      if (pos.use_analytic()) { // use analytic
	for (size_t i=start; i<end; i++) {
	  ParticleT &particle = bunch[i];
	  if (particle) {
	    for (size_t iter=0; iter<nloop; iter++) {
	      double t0_mm = bunch.t + iter * dt_mm; // mm/c
	      double t1_mm = t0_mm + dt_mm; // mm/c
	      if (particle.t0 > t1_mm) // particle won't come to existance within this time step
		continue;
	      if (particle.t0 > t0_mm) { // particle is created in this time step
		t0_mm = particle.t0; // mm/c
	      }
	      if (particle.S>=s0_mm && particle.S<s1_mm && !is_point_inside_aperture(particle.X,particle.Y)) {
		particle.lost_at(t0_mm);
		break;
	      }
	      if (particle.S<s0_mm || particle.S>=s1_mm) { // particle already exists but it's still upstream of the tracking volume
		drift_particle(particle, t1_mm - t0_mm); // mm
		continue;
	      }
	      const auto &field = get_field(particle.X, particle.Y, particle.S, t0_mm);
	      const auto &Ef = field.first; // V/m
	      const auto &Bf = field.second; // T
	      if (gsl_isnan(Ef[0])) { // signal particle loss
		particle.lost_at(t0_mm);
		break;
	      }
	      move_particle_through_EBfield(particle, Ef, Bf, t1_mm - t0_mm);
	    }
	  }
	}
      } else if (pos.use_leapfrog()) { // use leapfrog
	for (size_t i=start; i<end; i++) {
	  ParticleT &particle = bunch[i];
	  if (particle) {
	    params[thread].mass = particle.mass;
	    params[thread].charge = particle.Q;
	    double dY[6], Y[6] = {
	      particle.X, // mm
	      particle.Y, // mm
	      particle.S, // mm
	      particle.Px, // MeV/c
	      particle.Py, // MeV/c
	      particle.Pz  // MeV/c
	    };
	    for (size_t iter=0; iter<nloop; iter++) {
	      double t0_mm = bunch.t + iter * dt_mm; // mm/c
	      double t1_mm = t0_mm + dt_mm; // mm/c
	      if (particle.t0 > t1_mm) // particle won't come to existance within this time step
		continue;
	      if (particle.t0 > t0_mm) { // particle is created in this time step, drift it to t1_mm
		t0_mm = particle.t0;
	      }
	      if (func(t0_mm, Y, dY, &params[thread]) == GSL_EBADFUNC) { // particle already exists it hits the aperture
		particle.lost_at(t0_mm);
		break;
	      }
	      // particle exists and is inside the tracking volume
	      const double dt_mm_ = t1_mm - t0_mm;
	      const auto E = hypot(particle.mass, Y[3], Y[4], Y[5]); // MeV, total energy
	      const auto &F = StaticVector<3>(dY[3], dY[4], dY[5]); // MeV/mm
	      const auto &v = StaticVector<3>(dY[0], dY[1], dY[2]); // c
	      const auto &a = (F - dot(v,F)*v) / E; // c^2/mm
	      Y[0] += (dY[0] + 0.5 * a[0] * dt_mm_) * dt_mm_; // X
	      Y[1] += (dY[1] + 0.5 * a[1] * dt_mm_) * dt_mm_; // Y
	      Y[2] += (dY[2] + 0.5 * a[2] * dt_mm_) * dt_mm_; // S
	      Y[3] += dY[3] * dt_mm_; // Px
	      Y[4] += dY[4] * dt_mm_; // Py
	      Y[5] += dY[5] * dt_mm_; // Pz
	    }
	    // update particle
	    particle.X  = Y[0]; // mm
	    particle.Y  = Y[1]; // mm
	    particle.S  = Y[2]; // mm
	    particle.Px = Y[3]; // MeV/c
	    particle.Py = Y[4]; // MeV/c
	    particle.Pz = Y[5]; // MeV/c
	  }
	}
      } else /* if (pos.use_gsl()) */ { // use GSL
	gsl_odeiv2_driver *driver = pos.get_gsl_driver(thread);
	for (size_t i=start; i<end; i++) {
	  ParticleT &particle = bunch[i];
	  if (particle) {
	    params[thread].mass = particle.mass;
	    params[thread].charge = particle.Q;
	    double Y[6] = {
	      particle.X, // mm
	      particle.Y, // mm
	      particle.S, // mm
	      particle.Px, // MeV/c
	      particle.Py, // MeV/c
	      particle.Pz  // MeV/c
	    };
	    gsl_odeiv2_driver_reset_hstart(driver, dt_mm);
	    for (size_t iter=0; iter<nloop; iter++) {
	      double t0_mm = bunch.t + iter * dt_mm; // mm/c
	      double t1_mm = t0_mm + dt_mm; // mm/c
	      if (particle.t0 > t1_mm) // particle won't come to existance within this time step
		continue;
	      if (particle.t0 > t0_mm) { // particle is created in this time step, drift it to t1_mm
		t0_mm = particle.t0;
	      }
	      const int status = gsl_odeiv2_driver_apply(driver, &t0_mm, t1_mm, Y);
	      if (status == GSL_EBADFUNC) { // particle already exists it hits the aperture
		particle.lost_at(t0_mm);
		break;
	      }
	      if (status != GSL_SUCCESS) { // gsl integrator gave an error
		gsl_error = true;
		break;
	      }
	    }
	    // update particle
	    particle.X  = Y[0]; // mm
	    particle.Y  = Y[1]; // mm
	    particle.S  = Y[2]; // mm
	    particle.Px = Y[3]; // MeV/c
	    particle.Py = Y[4]; // MeV/c
	    particle.Pz = Y[5]; // MeV/c
	  }
	}
      }
    };
    for_all(RFT::number_of_threads, bunch.size(), track_parallel);

    if (gsl_error) {
      std::cerr << "error: an error occurred integrating the equations of motion, consider reducing the integration step\n";
      break;
    }
    
    bunch.t += nloop * dt_mm;
    iterations_cnt += nloop;
    
    loop = [&] () { // does tracking need to continue?
      if (nsteps)
	return iterations_cnt < nsteps;
      return do_particles_need_to_be_tracked();
    } ();
    
    // apply space charge kick (or half if it's the last step)
    if (sc_nsteps != 0 && int(iterations_cnt/sc_nsteps) != int((iterations_cnt-nloop)/sc_nsteps)) {
      if (to.verbosity>1)
	std::cout << " (applying space-charge kick #" << sc_iterations_cnt++ << ')' << std::flush;
      RFT::SC_engine->compute_force(sc_force, bunch, particle_exists);
      const double dt = (nloop * dt_mm) / (loop ? 1e3 : 2e3); // m/c, the very last step applies half kick
      for (size_t i=0; i<bunch.size(); i++) {
	auto &particle = bunch[i];
	if (particle_exists(particle)) {
	  const auto dP = StaticVector<3>(sc_force[i]) * dt; // MeV/c
	  particle.Px += dP[0]; // MeV/c
	  particle.Py += dP[1]; // MeV/c
	  particle.Pz += dP[2]; // MeV/c
	}
      }
    }

    // check the boundaries populates the lists particles_at_s0_mm and _s1_mm
    for (size_t i=0; i<bunch.size(); i++) {
      if (outside_boundaries[i]==false) {
	auto &particleT = bunch[i];
	if (particleT && bunch.t>=particleT.t0) {
	  int particle_is_outside_boundaries = 0; // 0: no; -1: S<S0; +1: S>S1
	  StaticVector<3> V; // c
	  double dt;
	  if (particleT.S>=s1_mm) {
	    particle_is_outside_boundaries = +1;
	    V = particleT.get_Vx_Vy_Vz(); // c
	    const auto Vz = V[2]; // c
	    dt = (particleT.S - s1_mm) / Vz; // mm/c
	  } else if (particleT.S<s0_mm) {
	    particle_is_outside_boundaries = -1;
	    V = particleT.get_Vx_Vy_Vz(); // c
	    const auto Vz = V[2]; // c
	    dt = (particleT.S - s0_mm) / Vz; // mm/c
	  }
	  if (particle_is_outside_boundaries) {
	    outside_boundaries[i] = true;
	    const auto dX = V * dt; // mm
	    Particle particle;
	    particle.x  = particleT.X - dX[0];
	    particle.xp = particleT.Px * 1e3 / particleT.Pz;
	    particle.y  = particleT.Y - dX[1];
	    particle.yp = particleT.Py * 1e3 / particleT.Pz;
	    particle.t  = bunch.t - dt;
	    particle.Pc = particleT.get_Pc();
	    particle.mass = particleT.mass;
	    particle.Q  = particleT.Q;
	    particle.N  = particleT.N;
	    if (to.open_boundaries==false) {
	      particleT.X = particle.x;
	      particleT.Y = particle.y;
	      particleT.S = particle_is_outside_boundaries>0 ? s1_mm : s0_mm;
	      particleT.lost_at(particle.t);
	    }
	    if (particle_is_outside_boundaries>0) {
	      particles_at_s1_mm.push_back(particle);
	    } else {
	      particles_at_s0_mm.push_back(particle);
	    }
	  }
	}
      }
    }
    
    // watchpoint
    if (wp_nsteps != 0 && int(iterations_cnt/wp_nsteps) != int((iterations_cnt-nloop)/wp_nsteps)) {
      const bool gzip = to.wp_gzip;
      std::stringstream filename;
      if (gzip) filename << std::string("gzip -9 >");
      filename << to.wp_basename << '.' << std::setw(8) << std::setfill('0') << wp_index++ << ".txt";
      if (gzip) filename << ".gz";
      if (FILE *file = gzip ? popen(filename.str().c_str(), "w") : fopen(filename.str().c_str(), "w")) {
	fprintf(file, "# t = %g mm/c\n", bunch.t);
	for (size_t i=0; i<bunch.size(); i++) {
	  const auto &particle = bunch[i];
	  if (particle) {
	    fprintf(file, "%.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g %.15g\n",
		    particle.X,    // mm
		    particle.Px,   // MeV/c
		    particle.Y,    // mm
		    particle.Py,   // MeV/c
		    particle.S,    // mm
		    particle.Pz,   // MeV/c
		    particle.mass, // MeV/c^2
		    particle.Q,    // e+
		    particle.N);   //
	  }
	}
	if (gzip)
	  pclose(file);
	else
	  fclose(file);
      } else {
	std::cerr << "error: cannot open output file '" << filename.str() << "'\n";
      }
    }
    
    // transport table
    if (tt_nsteps != 0 && int(iterations_cnt/tt_nsteps) != int((iterations_cnt-nloop)/tt_nsteps)) {
      transport_table.append_bunch_info(bunch);
    }
    
  } while(loop);

  auto init_bunch_6d = [] (const std::list<Particle> &particles_list, double s_mm ) -> Bunch6d {
    MatrixNd particles_matrix (particles_list.size(), 9);
    size_t i = 0;
    for (auto const &particle: particles_list) {
      particles_matrix[i][0] = particle.x; // mm
      particles_matrix[i][1] = particle.xp; // mrad
      particles_matrix[i][2] = particle.y; // mm
      particles_matrix[i][3] = particle.yp; // mrad
      particles_matrix[i][4] = particle.t; // mm/c
      particles_matrix[i][5] = particle.Pc; // MeV/c
      particles_matrix[i][6] = particle.mass; // MeV/c/c
      particles_matrix[i][7] = particle.Q; // e+
      particles_matrix[i][8] = particle.N; // e+
      i++;
    }
    Bunch6d bunch_6d(particles_matrix);
    bunch_6d.S = s_mm / 1e3; // m
    return bunch_6d;
  };
  bunch6d_at_s0_mm = particles_at_s0_mm.size()>0 ? init_bunch_6d(particles_at_s0_mm, s0_mm) : Bunch6d();
  bunch6d_at_s1_mm = particles_at_s1_mm.size()>0 ? init_bunch_6d(particles_at_s1_mm, s1_mm) : Bunch6d();

  if (to.verbosity>0)
    std::cout << "\ninfo: total number of integration steps = " << iterations_cnt << std::endl;
  
  return bunch;
}
