/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#include <iostream>
#include <complex>
#include <array>
#include <cmath>
#include <thread>
#include <gsl/gsl_sys.h>

#include "RF_Track.hh"
#include "rf_field_map.hh"
#include "constants.hh"
#include "move_particle.hh"

template <typename COMPLEX_MESH_3D>
RF_FieldMap<COMPLEX_MESH_3D>::RF_FieldMap(const ComplexMesh3d &Ex,
					  const ComplexMesh3d &Ey,
					  const ComplexMesh3d &Ez,
					  const ComplexMesh3d &Bx,
					  const ComplexMesh3d &By,
					  const ComplexMesh3d &Bz,
					  double xa_, double ya_, /*, double za_, */ // m
					  double hx_, double hy_, double hz_, // m
					  double length_,
					  double frequency_,
					  double direction_,
					  double P_map_,
					  double P_actual_ ) : Nx(Ex.size1()),
							       Ny(Ex.size2()),
							       Nz(Ex.size3()),
							       disable_Efield(false),
							       disable_Bfield(false),
							       xa(xa_*1e3), // accepts m, stores mm
							       ya(ya_*1e3), // accepts m, stores mm
							       hx(hx_*1e3), // accepts m, stores mm
							       hy(hy_*1e3), // accepts m, stores mm
							       hz(hz_*1e3), // accepts m, stores mm
							       cylindrical(false),
							       z0(0.0), // mm
							       omega(2.0 * M_PI * (frequency_ / (C_LIGHT*1e3))), // frequency in Hz
							       direction(direction_ == 0.0 ? 0 : (direction_ > 0.0 ? +1 : -1)),
							       P_map(P_map_), // W
							       P_actual(P_actual_), // W
							       scale_factor_and_phase(sqrt(P_actual / P_map)),
							       phi(0.0),
							       t0_is_set(false),
							       t0(0.0),
							       static_Bfield(0.0, 0.0, 0.0)
{
  Efield.resize(Nx,Ny,Nz);
  Bfield.resize(Nx,Ny,Nz);
  for (size_t k=0; k<Nz; k++) {
    for (size_t i=0; i<Nx; i++) {
      for (size_t j=0; j<Ny; j++) {
	Efield.elem(i,j,k) = StaticVector<3,fftwComplex>(Ex.elem(i,j,k), Ey.elem(i,j,k), Ez.elem(i,j,k));
	Bfield.elem(i,j,k) = StaticVector<3,fftwComplex>(Bx.elem(i,j,k), By.elem(i,j,k), Bz.elem(i,j,k));
      }
    }
  }
  set_nsteps(Nz-1);
  set_length(length_);
  init_bounding_box();
}

template <typename COMPLEX_MESH_3D>
RF_FieldMap<COMPLEX_MESH_3D>::RF_FieldMap(double Ex, double Ey, double Ez,
					  const ComplexMesh3d &Bx,
					  const ComplexMesh3d &By,
					  const ComplexMesh3d &Bz,
					  double xa_, double ya_, /*, double za_, */ // m
					  double hx_, double hy_, double hz_, // m
					  double length_,
					  double frequency_,
					  double direction_,
					  double P_map_,
					  double P_actual_ ) :	Nx(Bx.size1()),
								Ny(Bx.size2()),
								Nz(Bx.size3()),
								disable_Efield(true),
								disable_Bfield(false),
								xa(xa_*1e3), // accepts m, stores mm
								ya(ya_*1e3), // accepts m, stores mm
								hx(hx_*1e3), // accepts m, stores mm
								hy(hy_*1e3), // accepts m, stores mm
								hz(hz_*1e3), // accepts m, stores mm
								cylindrical(false),
								z0(0.0), // mm
								omega(2.0 * M_PI * (frequency_ / (C_LIGHT*1e3))), // frequency in Hz
								direction(direction_ == 0.0 ? 0 : (direction_ > 0.0 ? +1 : -1)),
								P_map(P_map_), // W
								P_actual(P_actual_), // W
								scale_factor_and_phase(sqrt(P_actual / P_map)),
								phi(0.0),
								t0_is_set(false),
								t0(0.0),
								static_Bfield(0.0, 0.0, 0.0)
{
  Bfield.resize(Nx,Ny,Nz);
  for (size_t k=0; k<Nz; k++) {
    for (size_t i=0; i<Nx; i++) {
      for (size_t j=0; j<Ny; j++) {
	Bfield.elem(i,j,k) = StaticVector<3,fftwComplex>(Bx.elem(i,j,k), By.elem(i,j,k), Bz.elem(i,j,k));
      }
    }
  }
  set_nsteps(Nz-1);
  set_length(length_);
  init_bounding_box();
}

template <typename COMPLEX_MESH_3D>
RF_FieldMap<COMPLEX_MESH_3D>::RF_FieldMap(const ComplexMesh3d &Ex,
					  const ComplexMesh3d &Ey,
					  const ComplexMesh3d &Ez,
					  double Bx, double By, double Bz,
					  double xa_, double ya_, /*, double za_, */ // m
					  double hx_, double hy_, double hz_, // m
					  double length_,
					  double frequency_,
					  double direction_,
					  double P_map_,
					  double P_actual_ ) :	Nx(Ex.size1()),
								Ny(Ex.size2()),
								Nz(Ex.size3()),
								disable_Efield(false),
								disable_Bfield(true),
								xa(xa_*1e3), // accepts m, stores mm
								ya(ya_*1e3), // accepts m, stores mm
								hx(hx_*1e3), // accepts m, stores mm
								hy(hy_*1e3), // accepts m, stores mm
								hz(hz_*1e3), // accepts m, stores mm
								cylindrical(false),
								z0(0.0), // mm
								omega(2.0 * M_PI * (frequency_ / (C_LIGHT*1e3))), // frequency in Hz
								direction(direction_ == 0.0 ? 0 : (direction_ > 0.0 ? +1 : -1)),
								P_map(P_map_), // W
								P_actual(P_actual_), // W
								scale_factor_and_phase(sqrt(P_actual / P_map)),
								phi(0.0),
								t0_is_set(false),
								t0(0.0),
								static_Bfield(0.0, 0.0, 0.0)
{
  Efield.resize(Nx,Ny,Nz);
  for (size_t k=0; k<Nz; k++) {
    for (size_t i=0; i<Nx; i++) {
      for (size_t j=0; j<Ny; j++) {
	Efield.elem(i,j,k) = StaticVector<3,fftwComplex>(Ex.elem(i,j,k), Ey.elem(i,j,k), Ez.elem(i,j,k));
      }
    }
  }
  set_nsteps(Nz-1);
  set_length(length_);
  init_bounding_box();
}

template <typename COMPLEX_MESH_3D>
RF_FieldMap<COMPLEX_MESH_3D>::RF_FieldMap() : Nx(1),
					      Ny(1),
					      Nz(1),
					      disable_Efield(true),
					      disable_Bfield(true),
					      xa(0.0), // accepts m, stores mm
					      ya(0.0), // accepts m, stores mm
					      hx(1e3), // accepts m, stores mm
					      hy(1e3), // accepts m, stores mm
					      hz(1e3), // accepts m, stores mm
					      cylindrical(false),
					      z0(0.0), // mm
					      omega(0.0), // rad/(mm/c)
					      direction(0.0),
					      P_map(1.0), // W
					      P_actual(1.0), // W
					      scale_factor_and_phase(1.0),
					      phi(0.0),
					      t0_is_set(false),
					      t0(0.0),
					      static_Bfield(0.0, 0.0, 0.0)
{
  set_nsteps(0);
  set_length(0.0);
  init_bounding_box();
}

template <typename COMPLEX_MESH_3D>
void RF_FieldMap<COMPLEX_MESH_3D>::set_length(double length_ )  // accepts m
{
  const double z1_max = (Nz-1)*hz;
  if (length_<0.0) {
    z1 = z1_max;
  } else {
    z1 = z0 + length_*1e3;
    // if (z1>z1_max) z1 = z1_max;
  }
}

template <typename COMPLEX_MESH_3D>
void RF_FieldMap<COMPLEX_MESH_3D>::init_bounding_box()
{
  if (cylindrical) {
    x0 = std::numeric_limits<double>::infinity();
    y0 = std::numeric_limits<double>::infinity();
    rho_max_sqr = -std::numeric_limits<double>::infinity();
    double x1 = -std::numeric_limits<double>::infinity();
    double y1 = -std::numeric_limits<double>::infinity();
    for (size_t i=0; i<Nx; i++) {
      const double rho = xa + i * hx; // mm
      if (rho*rho > rho_max_sqr)
	rho_max_sqr = rho*rho;
      for (size_t j=0; j<Ny; j++) {
	const double theta = (ya + j * hy) / 1e3; // rad
	double x = rho * cos(theta); // mm
	double y = rho * sin(theta); // mm
	if (x < x0) x0 = x;
	if (x > x1) x1 = x;
	if (y < y0) y0 = y;
	if (y > y1) y1 = y;
      }
    }
    width  = x1 - x0; // mm
    height = y1 - y0; // mm
  } else {
    x0 = xa;
    y0 = ya;
    width  = (Nx-1) * hx;
    height = (Ny-1) * hy;
  }
}

template <typename COMPLEX_MESH_3D>
void RF_FieldMap<COMPLEX_MESH_3D>::set_cylindrical(bool c )
{
  if (cylindrical != c) {
    cylindrical = c;
    init_bounding_box();
    for (size_t k=0; k<Nz; k++) {
      for (size_t i=0; i<Nx; i++) {
	for (size_t j=0; j<Ny; j++) {
	  const double theta = (cylindrical ? +1 : -1) * (ya + j*hy) / 1e3; // rad
	  double c = cos(theta); // mm
	  double s = sin(theta); // mm
	  if (!disable_Efield) {
	    auto &E_ = Efield.elem(i,j,k);
	    const auto Er = E_[0]* c + E_[1]*s;
	    const auto Et = E_[0]*-s + E_[1]*c;
	    E_[0] = Er;
	    E_[1] = Et;
	  }
	  if (!disable_Bfield) {
	    auto &B_ = Bfield.elem(i,j,k);
	    const auto Br = B_[0]* c + B_[1]*s;
	    const auto Bt = B_[0]*-s + B_[1]*c;
	    B_[0] = Br;
	    B_[1] = Bt;
	  }
	}
      }
    }
  }
}

template <typename COMPLEX_MESH_3D>
std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> RF_FieldMap<COMPLEX_MESH_3D>::get_field_complex(double x /* mm */, double y /* mm */, double z /* mm */, double t  /* mm/c */ )
{
  // returned fields are in V/m and T
  if (z<0.0 || z>(z1-z0)) // outside element
    return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>(0.0, 0.0);
  z += z0;
  if (z<0.0 || z>z1)
    return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>(0.0, 0.0);
  x -= x0;
  y -= y0;
  struct { bool x, y; } mirror;
  {
    if ((mirror.x = x<0.0)) x = -x;
    if ((mirror.y = y<0.0)) y = -y;
    if (x >= 2*width)  x = fmod(x, 2*width);
    if (y >= 2*height) y = fmod(y, 2*height);
    if (x > width)  { mirror.x = !mirror.x; x = 2*width  - x; }
    if (y > height) { mirror.y = !mirror.y; y = 2*height - y; }
  }
  std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>> field;
  double _c, _s;
  if (cylindrical) {
    x += x0;
    y += y0;
    double rho_sqr = x*x+y*y; // mm^2
    if (rho_sqr>rho_max_sqr)
      return std::pair<StaticVector<3,fftwComplex>, StaticVector<3,fftwComplex>>(StaticVector<3,fftwComplex>(GSL_NAN, GSL_NAN, GSL_NAN),
										 StaticVector<3,fftwComplex>(GSL_NAN, GSL_NAN, GSL_NAN));
    const double rho = hypot(x,y); // mm
    const double theta = atan2(y,x); // rad
    const double _r = rho!=0.0 ? 1.0/rho : 0.0;
    _c = x*_r;
    _s = y*_r;
    x = rho - xa;
    y = theta*1e3 - ya; // mrad
    x /= hx;
    y /= hy;
    z /= hz;
    // do not change the order of the following lines...
    const double two_pi_hy = round(2e3*M_PI/hy);
    while (y<0.0) y += two_pi_hy; // no negative angles
    while (y>=Ny) y -= two_pi_hy;
    if (y<0.0) y = 0.0;
    if (disable_Efield) {
      field.first = StaticVector<3,fftwComplex>(0.0);
    } else {
      const auto &E_ = Efield(x, y, z);
      const auto Er = E_[0];
      const auto Et = E_[1];
      field.first[0] = Er*_c - Et*_s;
      field.first[1] = Er*_s + Et*_c;
      field.first[2] = E_[2];
    }
    if (disable_Bfield) {
      field.second = StaticVector<3,fftwComplex>(0.0);
    } else {
      const auto &B_ = Bfield(x, y, z);
      const auto Br = B_[0];
      const auto Bt = B_[1];
      field.second[0] = Br*_c - Bt*_s;
      field.second[1] = Br*_s + Bt*_c;
      field.second[2] = B_[2];
    }
  } else {
    x /= hx;
    y /= hy;
    z /= hz;
    field.first  = disable_Efield ? StaticVector<3,fftwComplex>(0.0) : Efield(x, y, z);
    field.second = disable_Bfield ? StaticVector<3,fftwComplex>(0.0) : Bfield(x, y, z);
  }
  // phase computation
  static std::mutex mutex;
  if (!t0_is_set) {
    if (mutex.try_lock()) {
      t0_is_set = true;
      t0 = t;
    } else {
      mutex.lock();
    }
    mutex.unlock();
  }
  const fftwComplex &f = [&] () {
    const double phase = direction * omega * (t - t0);
    const double c = cos(phase);
    const double s = sin(phase);
    return scale_factor_and_phase * fftwComplex(c,s); // complex
  } ();
  field.first  *= f;
  field.second *= f;
  // mirroring
  if (mirror.x) {
    field.first[0] = -field.first[0];
    field.second[1] = -field.second[1];
  }
  if (mirror.y) {
    field.first[1] = -field.first[1];
    field.second[0] = -field.second[0];
  }
  if (mirror.x != mirror.y) {
    field.second[2] = -field.second[2]; // Bz -> - Bz
  }
  // add static B field (e.g. solenoid)
  for(size_t i=0; i<3; i++) {
    if (static_Bfield[i]!=0.0) {
      field.second[i] += static_Bfield[i];
    }
  }
  if (disable_Efield && gsl_isnan(field.second[0].real)) {
    field.first = StaticVector<3,fftwComplex>(GSL_NAN);
  }
  return field;
}

template <typename COMPLEX_MESH_3D>
std::pair<StaticVector<3>,StaticVector<3>> RF_FieldMap<COMPLEX_MESH_3D>::get_field(double x /* mm */, double y /* mm */, double z /* mm */, double t /* mm/c */ )
{
  const auto &field = get_field_complex(x, y, z, t);
  return std::pair<StaticVector<3>, StaticVector<3>>(StaticVector<3>(real(field.first[0]),
								     real(field.first[1]),
								     real(field.first[2])),
						     StaticVector<3>(real(field.second[0]),
								     real(field.second[1]),
								     real(field.second[2])));
}

// template specializations
template class RF_FieldMap<TMesh3d_LINT<StaticVector<3,fftwComplex>>>;
template class RF_FieldMap<TMesh3d_CINT<StaticVector<3,fftwComplex>>>;
