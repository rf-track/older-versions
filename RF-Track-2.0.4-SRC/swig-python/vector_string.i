/*
** This file is part of RF-Track.
**
** Author and contact:  Andrea Latina <andrea.latina@cern.ch>
**                      BE-ABP Group
**                      CERN
**                      CH-1211 GENEVA 23
**                      SWITZERLAND
**
** Copyright (C) 2022 CERN, Geneva, Switzerland. All rights not expressly
** granted are reserved.
**
** See the COPYRIGHT file at the top-level directory of this distribution.
*/

#if defined(SWIGPYTHON)

// Vector of strings as input argument
%typemap(in) const std::vector<std::string> & {
  if (PyList_Check($input)) {
    int size = PyList_Size($input);
    if (($1 = new std::vector<std::string>(size))) {
      std::vector<std::string> &ret = *$1;
      for (size_t i=0; i<size; i++) {
	ret[i] = std::string(PyString_AsString(PyList_GetItem($input, i)));
      }
    }
  } else if (PyString_Check($input)) {
    if (($1 = new std::vector<std::string>(1))) {
      (*$1)[0] = std::string(PyString_AsString($input));
    }
  }
  Py_DECREF($input);
}

%typemap(freearg) const std::vector<std::string> & {
  if ($1) delete $1;
}

%typemap(typecheck) const std::vector<std::string> & {
  $1 = PyList_Check($input) || PyString_Check($input) ? 1 : 0;
}

%typemap(argout,noblock=1) const std::vector<std::string> & {
}

// Vector of strings as output argument
%typemap(in, numinputs=0) std::vector<std::string> & (std::vector<std::string> temp ) {
  $1 = &temp;
}

%typemap(argout) std::vector<std::string> & {
  if (PyObject *res = PyList_New($1->size())) {
    for (size_t i=0; i<$1->size(); i++) {
      PyList_SetItem(res, i, PyString_FromString((*$1)[i].c_str()));
    }
    $result = SWIG_Python_AppendOutput($result, res);
  }
}

%typemap(freearg,noblock=1) std::vector<std::string> & {
}

#endif
