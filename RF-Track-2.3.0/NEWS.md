## Changelog
### What is new in version 2.3.0
* Introduced new Beam and BeamT models for multi-bunch simulations
* Optimized multi-bunch effects for multi-bunch beam simulations
* Modified the transport table to support multi-bunch beams
* Added the RF_TRACK_NOSPLASH environment variable
* Implemented element misalignment handling in Volume
* Added scaling error support to the Bpm element
* Enhanced Lattice::autophase() to automatically set the magnetic strength of magnets
* Improved detection of the solenoid fringe field extension in Volume
* Enhanced the 3d solenoid field modelling using multiple thin sheets of current
* Introduced a new on-the-fly BeamLoading implementation compatible with Beam (thanks to Javier Olivares Herrador)
* Enhanced long-range wakefields to accept tapered modes along the structure
* Added generalized Gaussian distribution to Generator
* Added the displaced() method to Bunch6d and Bunch6dT
* Added Screen element support to Lattice and Volume
* Implemented the fast multipole method to significantly speed up the SpaceCharge_Field() element
* Various minor bug fixes and improvements

### What was new in version 2.2.4
* Ensured compatibility with numpy v2.0.0

### What was new in version 2.2.3
* Minor bugfixes and improvements
* Improved python packaging

### What was new in version 2.2.2
* Lost particles are now stored in the Lattice or Volume in the laboratory reference frame
* Bug fixes and improvements in the Python interface
* Bug fix when appending a Volume into a Lattice
* Renamed some Lattice's methods ( vary_corrector_strengths -> vary_correctors_strengths )
* Added methods to facilitate beam-based alignment in Lattice
* Fixed problem with backtracking on screens in Volume
* Improved backtracking on screens in Volume

### What was new in version 2.2.1
* Added particle ID
* Added name to elements
* Improved element accessibility in Lattice and Volume
* Exposed mean_Px and mean_Py in Bunch6d_info and Lattice transport table
* Volume::set_length() allows setting an arbitrary length
* Added element Pillbox_Cavity as generic TM_m1p cavity
* Bug fixes and improvements in Lattice and Volume

### What was new in version 2.2.0
* Volume is fully integrated as an Element within Lattice
* Volume::s0 and Volume::s1 are now 3d reference frames that can connect Lattice beamlines with arbitrary orientations
* Renamed ParticleT.S as ParticleT.Z to avoid confusion
* Removed automatic projections from Bunch6dT to Bunch6d in get_phase_space()
* Lattice can now import MAD-X Twiss files directly
* Self-consistent BeamLoading effects in TW structures (many thanks to Javier Olivares Herrador)
* Added Energy Straggling effect in Absorber element (many thanks to Bernd Michael Stechauner)
* Volume::set_tt_nsteps(N) works in Lattice(), distributing N screens in Volume along the reference trajectory
* Added Bunch6d_QR, Quasi-Random bunch creation from Twiss parameters
* New hard-scattering model (many thanks to Bernd Michael Stechauner)
* Improved 'analytic' integration in Lattice
* Added Yoshida integrator 4th order (beware, rk2 is better)
* Improved beam <-> screens intersection in Volume tracking

### What was new in version 2.1.6
* Implemented 'circular' and 'rectangular' shapes in the element Absorber
* Improved the way longitudinal emittance is handled in both Bunch6d and Bunch6dT
* Now Volume can track directly Bunch6d
* Now, Transport Table in Volume can also include future particles by default
* Implemented Response matrix calculation and Automatic Orbit correction
* Improved transport table in Volume to optionally ignore particles outside volume
* Updated autophase to ignore elements whose t0 is already set
* Added Multiple Coulomb Scattering and energy loss
* Introduced ASTRA-like Generator (many thanks to Avni Aksoy)
* Introduced quasi-random sequences generator
* Added to transport table rmax99 and rmax90, the envelope at 90% and 99% of rmax
* Fixed a bug in thin-lens Sbend
* Added disp_z to BunchInfo

### What was new in version 2.1.5
* Added element Solenoid to both Volume and Lattice
* Added particle lifetime for muons simulation
* Added Incoherent synchrotron radiation in two versions: quantum and average
* Added new element SpaceCharge_Field()
* Fixed SWIG problems with typemaps
* Bug fix in Multipole element (many thanks to Ewa Oponowicz)

### What was new in version 2.1.4
* Added dispersion information to bunch get_info() and transport_table
* Now Element::get_field() accepts vectors for X, Y, Z, and T
* Added velocity slicing in Space-charge PIC calculation
* Added Toroidal Harmonics element

### What was new in version 2.1.3
* Added polarisation to Thomson scattering
* Implemented end fields in GenericFieldMap
* Implemented asymmetric laser beam in Compton Scattering module
* Added new element Multipole and new CollectiveEffect MultipoleKick

### What was new in version 2.1.2
* Added autophasing of RF elements
* Added back-tracking
* Added Long-range wakefields

### What was new in version 2.1
* Added the possibility to have collective effects in both Lattice() and Volume()
* Added short-range wakefield effects
* Added new element RF_FieldMap_2d() optimized for cylindrically symmetric fields
* Added new element LaserBeam() for the simulation of Compton backscattering

### What was new in version 2.0.4
* Changed the way TrackingOptions.t_max_mm works, now indefinite tracking is achieved with t_max_mm = Inf, and no longer t_max_mm = 0
* Added element Static_Magnetic_FieldMap_2d for cylindrically symmetric fields
* Improved default field's Jacobian for Elements
* Added support for SDDS files (writing)
* Added element SBend for dS tracking
* Added external elements
* Added Python interface
* Reimplemented misalignments in Bunch6d tracking, full 3d rotations
* Added element Volume
* Added element Coil

### What was new in version 2.0
* Reorganization of classes and files
* New FieldMap elements with extension _LINT and _CINT for linear and cubic interpolation
* Added element RF_Field_1d with a reconstruction of the 3d E and B fields from the on-axis electric field
* Added magnetic element Adiabatic Matching Device
* Bunch6d::get_phase_space and Bunch6dT::get_phase_space now use the first particle as the reference particle
