Download area for RF-Track

# RF-Track

RF-Track is a tracking code developed at CERN for the design and optimisation of photoinjectors, electron linacs, positron sources, muon beamlines, cooling channels and proton or ion linacs. RF-Track implements space charge and beam effects, short and long range wake fields, incoherent synchrotron radiation and multiple Coulomb scattering. Other collective effects are currently being implemented.

RF-Track can simulate beam generation at photocathodes and Inverse Compton Scattering between charged particles and laser beams, allowing integrated start-to-end simulations of compact electron linacs, such as Inverse Compton Scattering sources, from cathode to X-ray in one go.

RF-Track offers excellent flexibility and fast simulation speed. It can transport beams of particles of any mass and charge, even mixed, solving fully relativistic equations of motion. In addition, it can simulate bounched or continuous beams and transport them through 1D, 2D, or 3D field maps of oscillating radio-frequency electromagnetic fields or through conventional elements.

RF-Track is written in modern, optimised, and parallel C++ and uses the scripting languages Octave and Python as user interfaces.