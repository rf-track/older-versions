RF_Track;

G = Bunch6dT_Generator();
G.cathode = false;                        % cathode, true or false
G.dist_pz = "g";                          % longitudinal energy and momentum distribution
G.sig_ekin = 1e-3;                        % keV rms value of the energy spread
G.le = 1e-3;                              % keV width of the energy distribution.
G.ref_ekin = 10e-6;                       % MeV energy of reference particle
G.q_total = 1e-3;                         % nC bunch charge
% G.phi_eff = 4.25;                       % eV effective work function for Fermi-Dirac distribution
% G.e_photon = 4.66;                      % eV photon energy for Fermi-Dirac distribution.
G.dist_z = "g";                           % longitudinal particle distribution
G.sig_z = 1;                              % mm rms value of the bunch length
% G.lt = 3e-3;                            % ns emission time
% G.rt = 3e-4;                            % rise time of the bunch; only for plateau distribution.
% G.sig_t = 3e-3;                         % ns
% G.dist_y = "uniform-ellipsoid";
% G.dist_x = "radial-parabola";
G.dist_y = "rpa";                         % radial-parabola
% G.dist_x = "un";                        % uniform
G.sig_x = 0.2;                            % mm rms horizontal size
G.sig_y = 0.4;                            % mm rms vertical size
G.dist_px = "rga";                        % radial-parabola
G.dist_py = "g";                          % gaussian
G.sig_px = 0.0002;                        % keV/c rms value of the horizontal momentum distribution
G.sig_py = 0.0005;                        % keV/c rms value of the vertical momentum distribution
G.cor_px = 0;                             % mrad, x-px correlation

%% Create bunch
Npart = 100000;                           % number of particles
B = Bunch6dT(G, Npart);
M = B.get_phase_space();

%% Plot
scatter(M(:,1), M(:,2))
xlabel('X [mm]')
ylabel('Px [MeV/c]')
