RF_Track;

%% create standing wave structure
a1 = 1.0; % V/m max gradient
freq = 3e9; % Hz
lambda = RF_Track.clight / freq; % m, wavelength
l_cell = lambda/2 + 0; % m, length of the full SW cell
n_cells = 1; % number of cells
             % a positive sign indicates a start from the beginning of the cell
             % a negative sign indicates a start from the middle of the cell

A = [ 0 0 0 0 4.6828e+07 ]';

PB = Pillbox_Cavity(A, freq, l_cell, n_cells);



PB.set_t0(0.0);

%% plot Ez along the structure, in time
T_period = RF_Track.s / freq; % mm/c
T_axis = linspace(0, T_period, 64); % mm/c
Z_axis = linspace(0, PB.get_length()*1e3, 64); % mm

Xa = linspace(-120, 120, 101); % mm
Ya = linspace(-120, 120, 103); % mm
[X,Y] = meshgrid(Xa, Ya);

R = hypot(X,Y); % mm

O = zeros(size(X(:)));
I = ones(size(X(:)));

for t=T_axis

    [E,B] = PB.get_field(X(:), Y(:), I, I*t);

    Ex = reshape(E(:,1), size(X));
    Ey = reshape(E(:,2), size(X));
    Ez = reshape(E(:,3), size(X));
    Bx = reshape(B(:,1), size(X));
    By = reshape(B(:,2), size(X));
    Bz = reshape(B(:,3), size(X));

    Ez(R>120) = nan;
    
    surf(Xa, Ya, (Ez)); shading interp
    xlim([ Xa(1) Xa(end) ]);
    ylim([ Ya(1) Ya(end) ]);
    zlim([ -2e7 2e7 ]);
    xlabel('X (mm)')
    ylabel('Y (mm)')
    shading flat
    title('E_z (V/m)')

    drawnow;
end

print -dpng -F:16 plot_Ez.png
