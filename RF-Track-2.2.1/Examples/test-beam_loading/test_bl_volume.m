clear all; clc; close all 
RF_Track;

%Test Beam Loading - Provide rho & Volume
% Example based on CLIC PETS - See: https://clicdp.web.cern.ch/content/conceptual-design-report

c = RF_Track.clight / 1e6 ; % mm/ns 
f0 = 11.9936 * 1e9; % Hz (1/ns)
VG = 0.453; % c
QQ = 7200;
RHO = 2294; % Ohm/m
phaseadvance = pi/2; % rad
Ltotal = 212.45; % mm
N0 = 34;

% Bunch specification 
Iaverage = 101; % A
fb = 11.994*1e9; % Hz Frequency of repetition of the bunches
charge = Iaverage/fb; % C
particles_bunch = round(charge*RF_Track.C); 
Nbunches = 244*12; 
sigma = 1; % mm/c  

%%% CONSTRUCTOR PERFORMANCE
% A) STEADY STATE CONSTRUCTOR 
BL_steady = BeamLoading(N0, f0, VG, QQ, RHO, phaseadvance, -1, particles_bunch , fb); 

G_steady = BL_steady.get_G_steady()/1e6; % [MV/m] 
N = length(G_steady); 
z = linspace(0, Ltotal, N); % [mm] 

figure(1)
plot(z,G_steady)
xlabel('z [mm]', 'FontSize', 15)
ylabel(' G [MV/m]', 'FontSize', 15)
title('Beam induced field (STEADY)' , 'FontSize', 15)


% B) TRANSIENT STATE CONSTRUCTOR 
tic
BL = BeamLoading(N0, f0, VG, QQ, RHO, phaseadvance, -1, particles_bunch , fb, Nbunches); 
toc

G = BL.get_G()/1e6; % [MV/m]

figure(2) 
tplot = [0:(0.5*c): 2*c]; % [mm/c]
indexx = ceil(tplot / (z(2) - z(1)) )+1; 
plot(z,G(:,1),'b','LineWidth',3)
for s = indexx(2:end-1)
  hold on 
  plot(z,G(:,s), '--r', 'LineWidth', 2)
endfor
plot(z,G(:,end), 'r', 'LineWidth', 3)
xlabel('z [mm]', 'FontSize', 15)
ylabel(' G [MV/m]', 'FontSize', 15)
title('Beam induced field (TRANSIENT)' , 'FontSize', 15)
h = legend('t = 0.0', 't = 0.5 ns', 't = 1.0 ns', 't = 1.5 ns', 't = 2.0 ns' ,'Location', 'EastOutside' ); 
set(h, 'FontSize',15)
set(gca, "linewidth", 2, "Fontsize", 12);


%%% TRACKING PERFORMANCE
%% Create TimeDependent_Element
D = TimeDependent_Element ( Ltotal / 1000 );

%% Attach BL effect
D.add_collective_effect(BL);

disp('there')

%% bunch
X0 = XP = Y0 = YP = zeros(1000,1);
T = 12 / (fb / 1e9) * c + randn(1000,1); % [mm/c] 
P = 2400 * ones(1000,1); % [MeV/c] 

B0 = Bunch6d(RF_Track.electronmass, charge * RF_Track.C, -1, [X0 XP Y0 YP T P] );
B0 = Bunch6dT(B0); 
P0 = Bunch6d(RF_Track.electronmass, charge * RF_Track.C, -1, [ 0 0 0 0 0 2400 ] );
P0 = Bunch6dT(P0); 

M0 = B0.get_phase_space();

%% lattice
V = Volume();
Npets = 5; 

for s = 1:Npets  
    V.add(D, 0, 0,(s-1)*Ltotal/1000, 'entrance');
end

TO = TrackingOptions();
TO.cfx_dt_mm = 1;
TO.tt_dt_mm = 1;

max_energy = V.autophase(P0,TO)

% perform tracking
tic
B1 = V.track(B0, TO);
toc 
M1 = B1.get_phase_space();


figure(3)
scatter(T/c, M1(:,6),'r')
title('Bunch energy after 5 PETS with BL','FontSize',15)
grid on 
xlabel('t_i_n_j [ns]','FontSize',15)
ylabel('E [MeV]', 'FontSize', 15) 
set(gca, 'FontSize',12,'LineWidth',2)

