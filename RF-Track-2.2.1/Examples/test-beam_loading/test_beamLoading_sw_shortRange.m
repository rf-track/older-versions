clear all; close all; clc;
%% Load RF-Track
RF_Track;
RF_Track.number_of_threads(1); 

%% Load data 
Edata = load('GUN_3GHz_2.6_CELL_AXIS.SF7');  
z = Edata(:,1) * 1000; % [mm]                                        
Ez = Edata(:,2); % [V/m]
max_field = 80e6; % [V/m]
Ez = Ez / max(Ez) * max_field; % [V/m]                                          

c = RF_Track.clight*1e-6; % [mm/ns] 
freq = 2.997; % [GHz]                                               
w = 2*pi*freq/c; % [c/mm]
Lcell = pi / w; % [mm]
Ncells = 2.6; 
Ltotal = range(z); % [mm]
initialphid =  -41.0; % [deg]
initialphi = initialphid * pi/180; % [rad] 
initialt0 = 0.0; % [mm/c]

%% Structure data
Q_factor = 14500;%                                                     
Rshunt = 50e6; %[Ohm/m]                                         
rho = Rshunt/Q_factor; % [Ohm/m]
dz = z(2) - z(1); % [mm]

% Bunch specifications
Q_pC = 285; 
Qbunch = Q_pC  * RF_Track.pC; % [e] 
Nbunches = 200;

%% Define train of bunches
X1 = PX1 = Y1 = PY1 = S1 = zeros(Nbunches,1);
P1 = 0.01 * ones(Nbunches,1); % [MeV/c]
Q1 = -1 * ones(Nbunches,1); 
N1 = Qbunch * ones(Nbunches,1) * exp(-w*w/2); 
MASS1 = RF_Track.electronmass * ones(Nbunches, 1); % [MeV/c2]

fb = freq/2; % [GHz] 
tinj1 = (0:1:(Nbunches - 1 ))/fb*c; % [mm/c] 

B0t = Bunch6dT([X1 PX1 Y1 PY1 S1 P1 MASS1 Q1 N1 tinj1']); 
M0t = B0t.get_phase_space();

%% Let's add a last bunch to study this in detail
Nparticles = 100; % Number of macroparticles
tinj2 = Nbunches/fb*c + randn(Nparticles,1) ; 

X2 = PX2 = Y2 = PY2 = S2 = zeros(Nparticles,1);
P2 = 0.01 * ones(Nparticles,1); % [MeV/c]
Q2 = -1 * ones(Nparticles,1); 
N2 = Qbunch/Nparticles * ones(Nparticles,1); 
MASS2 = RF_Track.electronmass * ones(Nparticles, 1); % [MeV/c2]

% Concatenate last bunch to train
X = [X1; X2]; % [mm]
PX = [PX1; PX2]; % [MeV/c]
Y = [Y1; Y2]; % [mm]
PY = [PY1; PY2]; % [MeV/c]
S = [S1; S2]; % [mm]
P = [P1; P2]; % [MeV/c]
Q = [Q1; Q2]; % [e]
N = [N1; N2]; % [-]
MASS = [MASS1; MASS2]; % [MeV/c/c]
tinj = [tinj1'; tinj2]; % [mm/c]

B0t = Bunch6dT([X PX Y PY S P MASS Q N tinj]); 
M0t = B0t.get_phase_space();

%% RF-Fieldmap
Gun  = RF_FieldMap_1d_CINT(Ez', dz/1000, Ltotal/1000, freq*1e9, +1);
Gun.set_t0(0.0);
Gun.set_phid(initialphid);


%% Build constructor 
tic
BL = BeamLoadingSW(Gun, Q_factor, rho, Ncells, RF_Track.electronmass, -1);  
toc

TT = BL.get_TT1(); 
TT2 = BL.get_TT2(); 
Gun.add_collective_effect(BL);

%% lattice
V = Volume();
V.add(Gun, 0, 0, 0);

% Tracking time-step
dt = 1/fb*c / 340; 

% Comparison with and without beam-loading
E = zeros(Nbunches + Nparticles,2); % [MeV]
for bool = [0 1]

	% Tracking options
	O = TrackingOptions();
	O.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
	O.odeint_epsabs = 1e-6; % required accuracy
	O.dt_mm =1*dt; % mm/c, integration step size

	O.tt_dt_mm = 10; % mm, tabular average quantities every tt_dt_mm
	O.cfx_dt_mm = bool * dt;

	% Tracking
	tic
	display('Tracking under Beam Loading effect... '); 
	B1t = V.track(B0t, O);
	toc

	% Retrieve final energy
	M1t = B1t.get_phase_space(); 
	E(:,bool+1) = M1t(:,end); % [MeV]
endfor

% Plot results
figure(1)
plot( M1t(:,5) , E(:,1), '.b','MarkerSize', 10)
hold on
plot( M1t(:,5) , E(:,2), '.r','MarkerSize', 10)
xlabel('t [mm/c]','FontSize',15)
ylabel('E [MeV]','FontSize',15)
title('Long range energy distribution','FontSize',15)
h = legend('Without beam loading','With beam loading','Location','southeast');
set(h,'FontSize',15)
set(gca, 'FontSize', 15, 'LineWidth', 1)


figure(2)
select = (Nbunches + 1): 1: (Nbunches + Nparticles); 
scatter(M1t(select,5), E(select,1),'b')
hold on
scatter(M1t(select,5), E(select,2),'r')
xlabel('t [mm/c]','FontSize',15)
ylabel('E [MeV]','FontSize',15)
title('Energy distribution for bunch #201','FontSize',15)
h = legend('Without beam loading','With beam loading','Location','southeast');
set(h,'FontSize',15)
set(gca, 'FontSize', 15, 'LineWidth', 1)

% Influence of beam loading vs rf-curvature
E_mean1 = mean(E(select,1)) % [MeV]
E_mean2 = mean(E(select,2)) % [MeV]
E_loss = E_mean1 - E_mean2 % [MeV]

E_spread1 = std(E(select,1))
E_spread2 = std(E(select,2))
E_spread = E_spread2 - E_spread1  % [MeV]
