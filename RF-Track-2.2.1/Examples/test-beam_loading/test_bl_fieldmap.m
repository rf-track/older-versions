clear all; close all; clc
RF_Track;
c = RF_Track.clight / 1e6; 

% Test Beam Loading - Provide fieldmap
% Example based on CLIC's main linac. See https://clicdp.web.cern.ch/content/conceptual-design-report

addpath('~/beam-loading/scripts/Implementation/')
load efield.dat.gz;

Ez = squeeze(Ez(1,1,:)); % [V/m] 
L = (length(Ez) - 1) * hz; % m
freq = 11.994 * 1e9; % Hz

% RF-element
TWS  = RF_FieldMap_1d_CINT(Ez, hz, L, freq, -1);

% Beam-Loading required arguments
Pmap = 4; % W
VG = [1.65, 1.2, 0.83]/100; % c 
QQ = [5536, 5635, 5738]; 
RHO = [14587 16220 17954]; % Ohm/m 
initialphi = 0.0; 
initialt0 = 0.0;  
phaseadvance = 2*pi/3; % rad

% Bunch specification 
charge = 600e-12; % C
particles_bunch = charge*RF_Track.C; % e
Nbunches = 312; 
sigma = 0.29979246; % mm/c  
fb = freq / 6; % Hz - Frequency of repetition of the bunches

%% Steady-state
BL_steady = BeamLoading(TWS, Pmap, VG, QQ, phaseadvance, -1 , particles_bunch, fb);

% Some information
G_steady = BL_steady.get_G_steady() / 1e6; % MV/m 
N = length(G_steady); 
Ltotal = BL_steady.get_Ltotal() * 1e3; % mm 
zplot = linspace(0,Ltotal,N); 

figure(1)
plot(zplot, G_steady)
xlabel('z [mm]', 'FontSize', 15)
ylabel(' Eb (synch) [MV/m]', 'FontSize', 15)
title('Beam induced field (STEADY)' , 'FontSize', 15)

% B) TRANSIENT STATE CONSTRUCTOR 
tic
BL = BeamLoading(TWS, Pmap, VG, QQ, phaseadvance, -1 , particles_bunch, fb, Nbunches);
toc 
G = BL.get_G()/1e6; % MV/m

figure(4) 
tplot = [0:(15*c):(75*c)]; % mm/c
indexx = ceil(tplot /0.833) +1; 
plot(zplot,G(:,1),'b','LineWidth',3)
for s = indexx(2:end-1)
  hold on
  plot(zplot, G(:,s),'--r','LineWidth',2) 
endfor
plot(zplot, G(:,end), 'r', 'LineWidth',3)
grid on
xlabel('z [mm]', 'FontSize', 15)
ylabel(' G [MV/m]', 'FontSize', 15)
title('Beam induced field (TRANSIENT)' , 'FontSize', 15)
h = legend('t = 0.0', 't = 15 ns', 't = 30 ns', 't = 45 ns', 't = 60 ns',  't = 75 ns', 'Location', 'EastOutside' )
set(h, 'FontSize',15)
set(gca, "linewidth", 1, "Fontsize", 12);
