clear all; close all; clc;
%% Load RF-Track
RF_Track;

%% Load data 
Edata = load('GUN_3GHz_2.6_CELL_AXIS.SF7');  
z = Edata(:,1) * 1000; % [mm]                                        
Ez = Edata(:,2); % [V/m]
max_field = 80e6; % [V/m]
Ez = Ez / max(Ez) * max_field; % [V/m]                                          

c = RF_Track.clight*1e-6; % [mm/ns] 
freq = 2.997; % [GHz]                                               
w = 2*pi*freq/c; % [c/mm]
Lcell = pi / w; % [mm]
Ncells = 2.6; 
Ltotal = range(z); % [mm]
initialphid =  -41.0; % [deg]
initialphi = initialphid * pi/180; % [rad] 
initialt0 = 0.0; % [mm/c]

%% Structure data
Q = 14500;%                                                     
Rshunt = 50e6; %[Ohm/m]                                         
rho = Rshunt/Q; % [Ohm/m]
dz = z(2) - z(1); % [mm]

%% Build constructor 
tic
BL = BeamLoadingSW(Ez, dz/1000, freq*1e9, Q, rho, Ncells, initialphi, initialt0, RF_Track.electronmass, -1);  
toc

TT = BL.get_TT1(); 
TT2 = BL.get_TT2(); 

%% Plots

[Nbeta, Nphase, Ndump] = size(TT);
TT_full = TT(:,:,1); % Time transit factor for undumped field
TT2_full = TT2(:,:,1); 
betaplot = linspace(0,1,Nbeta); 

f1 = figure(1);
plot(betaplot, TT_full(:, ceil(Nphase/2) ),'LineWidth', 3)
hold on
plot(betaplot, TT_full(:, ceil(Nphase/2) - 3), 'LineWidth', 3)
plot(betaplot, TT_full(:, ceil(Nphase/2) + 3), 'LineWidth', 3)
xlabel('\beta','FontSize',15)
ylabel('T ','FontSize',15)
title(' Time transit factor Cell #1 ','FontSize', 15)
h = legend(' t_0 = 0.0 mm/c', 't_0 = - 4.5 mm/c', 't_0 = + 4.5 mm/c', 'Location','EastOutside'); 
set(h, 'FontSize',15)
set(gca, 'FontSize',15,'LineWidth',1)
print(f1,'-djpeg','test_tt.jpeg')

f2 = figure(2);
plot(betaplot, TT2_full(:, ceil(Nphase/2) ),'LineWidth', 3)
hold on
plot(betaplot, TT2_full(:, ceil(Nphase/2) - 3), 'LineWidth', 3)
plot(betaplot, TT2_full(:, ceil(Nphase/2) + 3), 'LineWidth', 3)
xlabel('\beta','FontSize',15)
ylabel('T ','FontSize',15)
title(' Time transit factor Cell #2 ','FontSize', 15)
h = legend(' t_0 = 30.0 mm/c', 't_0 = 25.5 mm/c', 't_0 = + 34.5 mm/c', 'Location','EastOutside'); 
set(h, 'FontSize',15)
set(gca, 'FontSize',15,'LineWidth',1)


TT_t0 = squeeze( TT(:,ceil(Nphase/2),:)); % Time transit factor for fixed t0
TT2_t0 = squeeze( TT2(:, ceil(Nphase/2) + 3 ,:)); 
dumplot = linspace(0,0.8,Ndump);

f4 = figure(4); 
plot(dumplot*100, TT_t0(1,:), 'LineWidth', 2)
hold on
plot(dumplot*100, TT_t0(ceil(Nbeta/2),:), 'LineWidth', 2)
plot(dumplot*100, TT_t0(end,:), 'LineWidth', 2)
xlabel('Dump [%]', 'FontSize', 15)
ylabel('T [-]', 'FontSize', 15)
title('Time transit factor @ CELL #1 dump evolution (t_0 = 0)')
h = legend('\beta = 0', '\beta = 0.5', '\beta ~ 1','Location','South');
set(h,'FontSize',12)
set(gca, 'FontSize', 15, 'LineWidth', 1)

f5 = figure(5); 
plot(dumplot*100, TT2_t0(ceil(Nbeta/2),:), 'LineWidth', 2)
hold on
plot(dumplot*100, TT2_t0(ceil(Nbeta/2) + 3,:) , 'LineWidth', 2)
plot(dumplot*100, TT2_t0(end,:), 'LineWidth', 2)
xlabel('Dump [%]', 'FontSize', 15)
ylabel('T [-]', 'FontSize', 15)
title('Time transit factor @ CELL #2 dump evolution (t_0 = 34.5 mm/c)')
h = legend('\beta = 0.5', ' \beta = 0.65 ', '\beta ~ 1','Location','South');
axis([0, 80, 0, 0.80])
set(h,'FontSize',12)
set(gca, 'FontSize', 15, 'LineWidth', 1)

