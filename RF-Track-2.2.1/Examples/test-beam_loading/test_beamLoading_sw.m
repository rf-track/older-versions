clear all; close all; clc;
%% Load RF-Track
RF_Track;

%% Load data 
Edata = load('GUN_3GHz_2.6_CELL_AXIS.SF7');  
z = Edata(:,1) * 1000; % [mm]                                        
Ez = Edata(:,2); % [V/m]
max_field = 80e6; % [V/m]
Ez = Ez / max(Ez) * max_field; % [V/m]                                          

c = RF_Track.clight*1e-6; % [mm/ns] 
freq = 2.997; % [GHz]                                               
w = 2*pi*freq/c; % [c/mm]
Lcell = pi / w; % [mm]
Ncells = 2.6; 
Ltotal = range(z); % [mm]
initialphid =  -69.0; % [deg]
initialphi = initialphid * pi/180; % [rad] 
initialt0 = 0.0; % [mm/c]

%% Structure data
Q_factor = 14500;%                                                     
Rshunt = 50e6; %[Ohm/m]                                         
rho = Rshunt/Q_factor; % [Ohm/m]
dz = z(2) - z(1); % [mm]

%Bunch specifications
Q_pC = 1500; 
Qbunch = Q_pC  * RF_Track.pC; % [e] 
Nbunches = 200;

%% Define train
X = PX = Y = PY = S = zeros(Nbunches,1);
P = 6e-6 * ones(Nbunches,1); % [MeV/c]
Q = -1 * ones(Nbunches,1); 
N = Qbunch * ones(Nbunches,1); 
MASS = RF_Track.electronmass * ones(Nbunches, 1); % [MeV/c2]

fb = freq / 2.0; % [GHz] 
tinj = (0:1:(Nbunches - 1 ))/fb*c; % [mm/c] 

B0t = Bunch6dT([X PX Y PY S P MASS Q N tinj']); 
M0t = B0t.get_phase_space();

%% RF-Fieldmap
Gun  = RF_FieldMap_1d_CINT(Ez, dz/1000, Ltotal/1000, freq*1e9, +1);
Gun.set_t0(0.0);
Gun.set_phid(initialphid);


%% Build constructor 
tic
BL = BeamLoadingSW(Gun, Q_factor, rho, Ncells, RF_Track.electronmass, -1);  
toc
Gun.add_collective_effect(BL);

TT = BL.get_TT1(); % Time transit factor in cell 1
TT2 = BL.get_TT2(); % Time transit factor in cell 2,3,...


%% lattice
figure(1)
clf ; hold on
for nsteps = [50 100 200 300 500 700 800 1000 ]
    V = Volume();
    V.add(Gun, 0, 0, 0);

    dt = RF_Track.ns * (1 / fb) / nsteps; % mm/c

    O = TrackingOptions();
    O.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45', 'leapfrog', 'analytic'
    O.odeint_epsabs = 1e-6; % required accuracy
    O.dt_mm = 1*dt; % mm/c, integration step size
    O.tt_dt_mm = 10; % mm, tabular average quantities every tt_dt_mm
    O.cfx_dt_mm = 1*dt;

    tic
    display('Tracking under Beam Loading effect... '); 
    B1t = V.track(B0t, O);
    toc

    M1t = B1t.get_phase_space(); 
    E = M1t(:,end); % [MeV]

    plot( 1:1:Nbunches, E, '.','MarkerSize', 10, 'displayname', sprintf('%d', nsteps));
    xlabel('Bunches','FontSize',15)
    ylabel('E [MeV]','FontSize',15)
    set(gca, 'FontSize', 15, 'LineWidth', 1)
    legend
    drawnow
end


