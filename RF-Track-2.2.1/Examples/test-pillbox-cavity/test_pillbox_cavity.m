RF_Track;

%% create standing wave structure
a1 = 1.0; % V/m max gradient
freq = 12e9; % Hz
lambda = RF_Track.clight / freq; % m, wavelength
l_cell = lambda/2 + 0; % m, length of the full SW cell
n_cells = 1; % number of cells
             % a positive sign indicates a start from the beginning of the cell
             % a negative sign indicates a start from the middle of the cell

figure(1)
clf ; hold on

A = a1;
for i=1:4
    
    PB = Pillbox_Cavity(A, freq, l_cell, n_cells);
    PB.set_t0(0.0);

    %% plot Ez along the structure, in time
    T_period = RF_Track.s / freq; % mm/c
    T_axis = linspace(0, T_period, 64); % mm/c
    Z_axis = linspace(0, PB.get_length()*1e3, 64); % mm
    
    E_ = [];
    B_ = [];
    for z=Z_axis
        [E,B] = PB.get_field(0, 0, z, 0);
        E_ = [ E_ E ];
        B_ = [ B_ B ];
    end

    figure(1)
    plot(Z_axis, E_(3,:), 'displayname', sprintf('TM_{01%d}', i-1));
    xlabel('Z (mm)');
    ylabel('E_z (V/m)');
    axis([ 0 Z_axis(end) -a1*1.2 a1*1.2 ]);
    
    for t=T_axis
        E_ = [];
        B_ = [];
        for z=Z_axis
            [E,B] = PB.get_field(0, 0, z, t);
            E_ = [ E_ E ];
            B_ = [ B_ B ];
        end
        figure(2)
        plot(Z_axis, E_(3,:));
        title(sprintf('TM_{01%d} ; t = %2.f%% of period', i-1, t*100/T_period));
        xlabel('Z (mm)');
        ylabel('E_z (V/m)');
        axis([ 0 Z_axis(end) -a1*1.2 a1*1.2 ]);
        drawnow;
    end

    Xa = linspace(-5, 5, 101);
    Ya = linspace(-5, 5, 103);
    [X,Y] = meshgrid(Xa,Ya);
    
    O = zeros(size(X(:)));
    I = ones(size(X(:)));
    
    [E,B] = PB.get_field(X(:), Y(:), I*l_cell*1e3/3, I*T_period);
    
    Ex = reshape(E(:,1), size(X));
    Ey = reshape(E(:,2), size(X));
    Ez = reshape(E(:,3), size(X));
    Bx = reshape(B(:,1), size(X));
    By = reshape(B(:,2), size(X));
    Bz = reshape(B(:,3), size(X));
    
    figure(3)
    pcolor(Xa, Ya, Ez);
    xlim([ -5 5 ]);
    ylim([ -5 5 ]);
    xlabel('X (mm)')
    ylabel('Y (mm)')
    shading flat
    title('E_z (V/m)')
    
    A = [ 0, A ];

end
figure(1)
legend
