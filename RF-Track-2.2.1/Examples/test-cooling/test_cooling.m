%% Load RF_Track
RF_Track;

%% Cooler parameters
Cooler.L = 3; % m
Cooler.B = 0.07; % T
Cooler.r0 = 25; % mm, electron beam radius

%% Electron beam parameters
Electrons.Ne = 8.27e13; % electron number density #/m^3
Electrons.V = 0.09464747107415794; % 0.2% less than the ion speed
Electrons.gamma = 1 / sqrt(1 - Electrons.V**2); %
Electrons.P = RF_Track.electronmass * Electrons.V * Electrons.gamma;

%% Build cooler
E = ElectronCooler(Cooler.L, Cooler.r0/1e3, Cooler.r0/1e3);
E.set_Q(-1);
E.set_electron_mesh(16, 16, 16, Electrons.Ne, 0, 0, Electrons.V);
E.set_static_Bfield(0.0, 0.0, Cooler.B);
E.set_temperature(0.01, 0.001);
E.compute_cooling_force();

%% Ion beam
Ions.A = 208; % Lead
Ions.Q = 54; % ion charge
Ions.mass = Ions.A * RF_Track.protonmass; % MeV/c^2
Ions.Np = 6e10/54; % total number of ions per simulated bunch
Ions.V = Electrons.V; % same as the electrons
Ions.P = Ions.mass * Ions.V / sqrt(1-Ions.V^2); % MeV/c

%% Create RF-Track beam B0
N_particles = 10000; % number of macroparticles per simulated bunch
O = zeros(N_particles, 1);
Z = 25 * pi * 1e3 / Ions.V * rand(N_particles, 1); % mm/c
P_spread = Ions.P * 4e-3 * (rand(N_particles, 1) - 0.5); % MeV/c
P = Ions.P + P_spread; % MeV/c
B0 = Bunch6d(Ions.mass, Ions.Np, Ions.Q, [ O O O O Z P ]);

%% Simulate a 1000 turns
format long

V_ele = Electrons.V

figure(1)
hist(B0.get_phase_space('%Pc'), 100);
xlabel('P [MeV/c]');
title('before cooling');
mean_Vz_before = mean(B0.get_phase_space('%Vz'))

N_turns = 1000;
for n=1:N_turns
    E.track(B0);
end

figure(2)
hist(B0.get_phase_space('%Pc'), 100);
xlabel('P [MeV/c]');
title('after cooling');
mean_Vz_after = mean(B0.get_phase_space('%Vz'))
