RF_Track;

%% bunch
Q_nC = 4; % nC
sigma_z = 0.1; % mm
Pspread = 1; % permil, momentum spread
Pref = 200; % MeV/c

Twiss = Bunch6dT_twiss();
Twiss.emitt_x = 10; % mm.mrad
Twiss.emitt_y = 10; % mm.mrad
Twiss.beta_x = 1; % m
Twiss.beta_y = 1; % m
Twiss.alpha_x = 0.01; %
Twiss.alpha_y = 0.01; % 
Twiss.sigma_z = sigma_z; % mm/c
Twiss.sigma_d = Pspread; % permil

N_particles = 10000; % macro-particles
B0 = Bunch6dT (RF_Track.electronmass, Q_nC * RF_Track.nC, +1, Pref, Twiss, N_particles);
M0 = B0.get_phase_space();

%% Solenoid
L = 1; % m
Bz = 0.5; % T
R = 0.10; % m
S = Solenoid(L, Bz, R); % m,T,m

%% Drift 
Ez = 15e6; % V/m
D = Drift(10); % m
D.set_static_Efield(0, 0, Ez); % 

%% 
V = Volume();
for i=0:5
    V.add(S, 0, 0, i*1.5, 'center'); % element, X, Y, Z (in m)
end
V.add(D, 0, 0, 0);

%% Tracking options
TO = TrackingOptions();
TO.dt_mm = 10; % mm/c
TO.odeint_algorithm = 'rk2'; % 'rkf45', 'leapfrog', ...
TO.tt_dt_mm = 100; % mm/c, track the emittance every tt_dt_mm steps

%% Plot Bz
Za = linspace(-4, 10, 1000); % m
Bz = [];

for Z = Za
    [E,B] = V.get_field(0, 0, Z*1e3, 0); % x,y,z,t (mm, mm/c)
    Bz = [ Bz ; B(3) ];
end

figure(1);
clf
plot(Za, Bz)
xlabel('S [m]')
ylabel('B [T]');

%% Tracking
tic
B1 = V.track(B0, TO);
toc

M1 = B1.get_phase_space(); % x [mm] Px [MeV/c] y Py S Pz 
% M1 = B1.get_phase_space('%X %xp %Y %yp %S %P'); % x [mm] Px [MeV/c]   y Py S Pz 
TT = V.get_transport_table('%mean_Z %emitt_x %emitt_y %emitt_4d %sigma_X %sigma_Y');

figure(2);
clf ; hold on
xlabel('x [mm]');
ylabel('Px [MeV/c]');
scatter(M0(:,1), M0(:,2));
scatter(M1(:,1), M1(:,2));
legend('initial beam', 'final beam');

figure(3);
clf ; hold on
xlabel('S [mm]');
ylabel('Pz [MeV/c]');
scatter(M0(:,5), M0(:,6));
scatter(M1(:,5), M1(:,6));
legend('initial beam', 'final beam');

figure(4);
clf ; hold on
plot(TT(:,1)/1e3, TT(:,2));
plot(TT(:,1)/1e3, TT(:,3));
plot(TT(:,1)/1e3, TT(:,4));
xlabel('S [m]');
ylabel('emitt [mm.mrad]');
legend('emitt x', 'emitt y', 'emitt 4d');

figure(5);
clf ; hold on
plot(TT(:,1)/1e3, TT(:,5));
plot(TT(:,1)/1e3, TT(:,6));
xlabel('S [m]');
ylabel('sigma [mm]');
legend('sigma x', 'sigma y');