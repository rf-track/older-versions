RF_Track;

%% bunch
Q_nC = 4; % nC
sigma_t = 0.1; % mm/c
Pspread = 1; % permil, momentum spread
Pref = 200; % MeV/c

Twiss = Bunch6d_twiss();
Twiss.emitt_x = 10; % mm.mrad
Twiss.emitt_y = 10; % mm.mrad
Twiss.beta_x = 1; % m
Twiss.beta_y = 1; % m
Twiss.alpha_x = 0.01; %
Twiss.alpha_y = 0.01; % 
Twiss.sigma_t = sigma_t; % mm/c
Twiss.sigma_d = Pspread; % permil

N_particles = 10000; % macro-particles
B0 = Bunch6d (RF_Track.electronmass, Q_nC * RF_Track.nC, +1, Pref, Twiss, N_particles);
M0 = B0.get_phase_space();

%% Solenoid
L = 1; % m
Bz = 0.5; % T
R = 0.10; % m
S = Solenoid(L, Bz, R); % m,T,m
S.set_tt_nsteps(20);

%% Drift 
Ez = 15e6; % V/m
D = Drift(0.1); % m
D.set_static_Efield(0, 0, Ez); % 

%% 
L = Lattice();
for i=0:5
    L.append(S);
    L.append(D);
end

%% Plot Bz
Za = linspace(-4, 10, 1000); % m
Bz = [];

for Z = Za
    [E,B] = L.get_field(0, 0, Z*1e3, 0); % x,y,z,t (mm, mm/c)
    Bz = [ Bz ; B(3) ];
end

figure(1);
clf
plot(Za, Bz)
xlabel('S [m]')
ylabel('B [T]');

%% Tracking
tic
B1 = L.track(B0);
toc

M1 = B1.get_phase_space('%x %xp %y %yp %dt %P');
TT = L.get_transport_table('%S %emitt_x %emitt_y %emitt_4d %sigma_x %sigma_y');

figure(2);
clf ; hold on
xlabel('x [mm]');
ylabel('Px [MeV/c]');
scatter(M0(:,1), M0(:,2));
scatter(M1(:,1), M1(:,2));
legend('initial beam', 'final beam');

figure(3);
clf ; hold on
xlabel('dt [mm/c]');
ylabel('P [MeV/c]');
scatter(M0(:,5), M0(:,6));
scatter(M1(:,5), M1(:,6));
legend('initial beam', 'final beam');

figure(4);
clf ; hold on
plot(TT(:,1), TT(:,2));
plot(TT(:,1), TT(:,3));
plot(TT(:,1), TT(:,4));
xlabel('S [m]');
ylabel('emitt [mm.mrad]');
legend('emitt x', 'emitt y', 'emitt 4d');

figure(5);
clf ; hold on
plot(TT(:,1), TT(:,5));
plot(TT(:,1), TT(:,6));
xlabel('S [m]');
ylabel('sigma [mm]');
legend('sigma x', 'sigma y');