clear all
close all

%% load RF-Track
RF_Track;

%% Define reference particle, and rigidity
Part.mass = RF_Track.electronmass; % MeV/c^2
Part.P = 100; % MeV/c
Part.Q = -1; % e

%% Bunch
global B0
N = 10000;
X = randn(N,1);
Y = randn(N,1);
O = zeros(N,1);
I = ones (N,1);
B0 = Bunch6d(Part.mass, 0.0, Part.Q, [ X O Y O O I*Part.P ]);

%% MultipleCoulombScattering as an external effect
sc_size = get(0,'screensize') .* [1 1 0.6 0.4 ] + 10;

F = figure(1, "position", sc_size);
set (F, "color", get(0, "defaultuicontrolbackgroundcolor"))

init_val=[1];
PP=intract_plot.create_ui (init_val);
PP.fcn=@(init_val) my_function(init_val);

guidata (F, PP)

intract_plot.update (F,  true);
