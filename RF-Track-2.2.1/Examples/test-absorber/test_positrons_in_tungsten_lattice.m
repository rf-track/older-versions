%% load RF-Track
RF_Track;

nsteps = 100;

%% Volume
A = Absorber(0.020, 'tungsten');
A.set_cfx_nsteps(nsteps);

figure(1)
clf ; hold on
for K = 10:10:100

    %% Define reference particle, and rigidity
    Part.mass = RF_Track.electronmass; % MeV/c^2
    Part.P = sqrt(2*K*Part.mass+K^2); % MeV/c
    Part.Q = +1; % e
    
    %% Bunch
    N = 10000;
    X = randn(N,1);
    Y = randn(N,1);
    O = zeros(N,1);
    I = ones (N,1);
    B0 = Bunch6d(Part.mass, 0.0, Part.Q, [ 0 0 0 0 0 Part.P ]);
    
    %% Tracking
    L = Lattice();
    L.append(A);
   
    tic
    B1 = L.track(B0);
    T = L.get_transport_table('%S %sigma_x %sigma_x %mean_K');
    toc

    plot(T(:,1)*1e3, T(:,4), '*-');
    axis([ 0 20 0 100 ])
    xlabel('depth [mm]');
    ylabel('<K> [MeV]');
    grid on
    drawnow
end
title('Positrons in Tunsgten', 'fontsize', 12);
