%% load RF-Track
RF_Track;

nsteps = 100;

%% Volume
A = Absorber(0.020, 'tungsten');

V = Volume();
V.add(A, 0, 0, 0, 'entrance');

figure(1)
clf ; hold on
for K = 10:10:100

    %% Define reference particle, and rigidity
    Part.mass = RF_Track.electronmass; % MeV/c^2
    Part.P = sqrt(2*K*Part.mass+K^2); % MeV/c
    Part.Q = +1; % e
    
    %% Bunch
    N = 10000;
    X = randn(N,1);
    Y = randn(N,1);
    O = zeros(N,1);
    I = ones (N,1);
    B0 = Bunch6dT(Part.mass, 0.0, Part.Q, [ 0 0 0 0 0 Part.P ]);
    
    %% Tracking
    O = TrackingOptions();
    O.dt_mm = 10; % mm/c
    O.tt_dt_mm = V.get_length() * 1e3 / nsteps; % mm/c
    O.cfx_dt_mm = V.get_length() * 1e3 / nsteps; % mm/c
    
    tic
    B1 = V.track(B0, O);
    T = V.get_transport_table('%mean_Z %sigma_X %sigma_Y %mean_K');
    toc

    plot(T(:,1), T(:,4), '*-');
    axis([ 0 20 0 100 ])
    xlabel('depth [mm]');
    ylabel('<K> [MeV]');
    grid on
    drawnow
end
title('Positrons in Tunsgten', 'fontsize', 12);
