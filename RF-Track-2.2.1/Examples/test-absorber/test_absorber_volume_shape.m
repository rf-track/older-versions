%% load RF-Track
RF_Track;

%% Define reference particle, and rigidity
Part.mass = RF_Track.electronmass; % MeV/c^2
Part.P = 100; % MeV/c
Part.Q = -1; % e

%% Bunch
N = 10000;
R = sqrt(rand(N,1));
T = 360*rand(N,1);
X = R.*cosd(T);
Y = R.*sind(T);
O = zeros(N,1);
I = ones (N,1);
B0 = Bunch6dT(Part.mass, 0.0, Part.Q, [ X O Y O O I*Part.P ]);

%% Volume
A = Absorber(0.1, 'air');
A.set_shape('circular', 5e-4);

V = Volume();
V.add(A, 0, 0, 0, 'entrance');

figure(1)
clf ; hold on
for nsteps = [ 10 50 100 500 1000 ]

    O = TrackingOptions();
    O.dt_mm = 10; % mm/c
    O.tt_dt_mm = V.get_length() * 1e3 / nsteps; % mm/c
    O.cfx_dt_mm = V.get_length() * 1e3 / nsteps; % mm/c

    tic
    B1 = V.track(B0, O);
    T = V.get_transport_table('%mean_Z %sigma_X %sigma_Y');
    toc

    plot(T(:,1)/1e3, T(:,2), '*-', 'displayname', sprintf('nsteps = %d', nsteps));
    axis([ 0 0.1 0 1 ])
    xlabel('S [m]');
    ylabel('\sigma_X [mm]');
    title('Volume', 'fontsize', 12);
    legend('location', 'northwest');
    grid on
    drawnow
end

figure(2);
clf ; hold on
M1 = B1.get_phase_space();
scatter3(M1(:,5), M1(:,1), M1(:,3))
xlabel('S [mm]');
ylabel('X [mm]');
zlabel('Y [mm]');
title('Circular Absorber', 'fontsize', 12);
pause
[X,Y,Z] = cylinder([ 5e-1 5e-1 ]);
Z(1,:) = min(M1(:,5));
Z(2,:) = max(M1(:,5));
surf(Z,X,Y);
