function L = angular_momentum(M0)
  r = [ M0(:,1) M0(:,3) M0(:,5) ];
  P = [ M0(:,2) M0(:,4) M0(:,6) ];
  L = zeros(size(r));
  for i=1:size(r,1)
      L(i,:) = cross(r(i,:), P(i,:));
  end
end