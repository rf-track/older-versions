addpath('../..');

RF_Track;

C = Coils(0.4, ... % m, length
          1.0, ... % T, on-axis field
          0.1); % m, radius

C.set_nsteps(10);
C.set_odeint_algorithm("rk2");

B0 = Bunch6d(RF_Track.electronmass, 1, -1, [ 1 .5 1 .1 0 1000 ] / 10);
% B0 = Bunch6dT(RF_Track.electronmass, 1, -1, [ 1 0 0 0 0 100 ]);

L = Lattice();
L.append(C);

%{
O = TrackingOpts();
O.dt_mm = 1 ; % mm/c
O.nsteps = 0; % if 0, tracks all the way to the end of the lattice
O.space_charge_nsteps = 0; % if != 0, applies a space charge kick every space_charge_nsteps steps
O.backtrack_at_entrance = false; % bring the foremost particle to S=0 before strarting to track
O.odeint_algorithm = "rk2"; % pick your favorite algorithm
%}

T = [];
for i=1:100
    B0 = L.track(B0);
    % B0 = L.track(B0, O); % bunch 6dT
    M = B0.get_phase_space();
    T = [ T ; M hypot(M(1), M(2), M(3), M(4)) ];
    % M = B0.get_phase_space("%X %Px %Y %Py %Pz");
    % B0 = Bunch6dT(RF_Track.electronmass, 1, -1, [ M(1) M(2) M(3) M(4) 0 M(5) ]);
end
    
figure(1)
plot(T(:,7))

figure(2)
clear Bz
for i=1:int32(C.get_length()*1e3); [E,B] = C.get_field(0,0,i,0); Bz(i) = B(3); end;
plot(Bz);
