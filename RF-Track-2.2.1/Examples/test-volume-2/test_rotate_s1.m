%% load RF-Track
RF_Track;

%% Define reference particle, and rigidity
Part.mass = RF_Track.electronmass; % MeV/c^2
Part.P = 100; % MeV/c
Part.Q = -1; % e

%% Bunch
N = 1000;
R = sqrt(rand(N,1));
T = rand(N,1)*360;
X = R.*cosd(T);
Y = R.*sind(T);
O = zeros(N,1);
I = ones (N,1);
B0 = Bunch6d(Part.mass, 0.0, Part.Q, [ X O Y O O I*Part.P ]);

for angled = -80:5:80

    %% Volume
    V = Volume();
    V.set_s1(0, 0,  0.2, 0, 0, deg2rad(angled));
        
    %% Lattice
    L = Lattice();
    L.append(V);

    tic
    B1 = L.track(B0);
    toc

    M1 = B1.get_phase_space();
    scatter(M1(:,1), M1(:,3), '.');
    xlabel('x [mm]');
    ylabel('y [mm]');
    axis([ -6 6 -6 6 ]);
    title(sprintf('angle = %d deg', angled), 'fontsize', 12);
    grid on
    drawnow
    
    pause(0.1)
end
pause