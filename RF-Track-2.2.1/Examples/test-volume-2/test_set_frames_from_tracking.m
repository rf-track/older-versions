RF_Track;

%% Define reference particle, and rigidity
Part.mass = RF_Track.electronmass; % MeV/c^2
Part.P = 100; % MeV/c
Part.Q = -1; % e

P0 = Bunch6d(Part.mass, 0.0, Part.Q, [ 0 0 0 0 0 Part.P ]);

%% Volume with static vertical field
V = Volume();
V.set_static_Bfield(0, 0.5, 0);
V.odeint_algorithm = "rk2";
V.tt_dt_mm = 10;

% set s1, 1 m of path length downstram of s0
V.set_s1_from_s0(P0, 1);

% set s0, 2 m of path length upstram of s1
V.set_s0_from_s1(P0, 2);

% track the particle from s0 to s1
P1 = V.track(P0);

% plot
T = V.get_transport_table('%mean_X %mean_Z');
plot(T(:,2), T(:,1));
xlabel('Z [mm]');
ylabel('X [mm]');

disp ('press any key to continue...');
pause