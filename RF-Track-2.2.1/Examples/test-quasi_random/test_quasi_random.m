%% load RF-Track
RF_Track;

%% Pseudo-random vs. quasi-random bunch
PR = rand (800,2);
QR = qrand(800,2);

figure(1);
subplot(1,2,1);
scatter(PR(:,1), PR(:,2));
title('Pseudo-random');

subplot(1,2,2);
scatter(QR(:,1), QR(:,2));
title('Quasi-random');

%% Pseudo-random vs. quasi-random bunch
Part.mass = RF_Track.electronmass; % MeV/c^2
Part.P = 100; % MeV/c
Part.Q = -1; % e+

%% Bunch
Nparticles = 50000;
Twiss = Bunch6d_twiss();
Twiss.emitt_x = 10; % mm.mrad normalised emittance
Twiss.emitt_y = 10; % mm.mrad
Twiss.beta_x = 3; % m
Twiss.beta_y = 3; % m
Twiss.alpha_x = -0.5;
Twiss.alpha_y = -0.5;

B0_PR = Bunch6d    (Part.mass, 0.0, Part.Q, Part.P, Twiss, Nparticles);
B0_QR = Bunch6d_QR (Part.mass, 0.0, Part.Q, Part.P, Twiss, Nparticles);

figure(2)
subplot(1,2,1)
scatter(B0_PR.get_phase_space('%x'), B0_PR.get_phase_space('%xp'), 0.8, '.');
title('Pseudo-random bunch');
xlabel('x [mm]');
ylabel('x'' [mrad]');
axis([ -2 2 -0.8 0.8 ]);

subplot(1,2,2)
scatter(B0_QR.get_phase_space('%x'), B0_QR.get_phase_space('%xp'), 0.8, '.');
title('Quasi-random bunch');
xlabel('x [mm]');
ylabel('x'' [mrad]');
axis([ -2 2 -0.8 0.8 ]);
