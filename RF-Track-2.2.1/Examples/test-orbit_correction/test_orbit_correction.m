%% load RF-Track

addpath('../../')

RF_Track;

%% Define reference particle, and rigidity
Part.mass = RF_Track.protonmass; % MeV/c^2
Part.P = 100; % MeV/c
Part.Q = 1; % e+
B_rho = Part.P / Part.Q; % MV/c, reference rigidity

%% FODO cell with Lcell = 2 m, Lq = 24 cm, Ld = 76 cm, mu = 90 degress
Lcell = 2; % m
Lquad = 0.1; % m
mu = 90; % deg

% focusing params
k1L = sind(mu/2) / (Lcell/4); % 1/m
strength = k1L * B_rho; % MeV/m
gradient = strength / Lquad * 1e6 / RF_Track.clight; % T/m = V/m/m/c

%% Lattice
Qf = Quadrupole(Lquad/2,  strength/2);
Qd = Quadrupole(Lquad, -strength);
D  = Drift(Lcell/2 - Lquad);

C = Corrector();
B = Bpm();

FODO = Lattice();
FODO.append(Qf);
FODO.append(B);
FODO.append(D);
FODO.append(C);
FODO.append(Qd);
FODO.append(B);
FODO.append(D);
FODO.append(C);
FODO.append(Qf);

%% Tracking
Ncells = 10;
L = Lattice();
for i=1:Ncells
    L.append(FODO);
end

%% Bunch
Nparticles = 10000;
Twiss = Bunch6d_twiss();
Twiss.emitt_x = 10; % mm.mrad normalised emittance
Twiss.emitt_y = 10; % mm.mrad
Twiss.beta_x = Lcell * (1 + sind(mu/2)) / sind(mu); % m
Twiss.beta_y = Lcell * (1 - sind(mu/2)) / sind(mu); % m
Twiss.alpha_x = 0;
Twiss.alpha_y = 0;
P0 = Bunch6d(Part.mass, RF_Track.nC, Part.Q, [ 0 0 0 0 0 Part.P ]);
B0 = Bunch6d(Part.mass, RF_Track.nC, Part.Q, Part.P, Twiss, Nparticles);

%% Tracking
tic
B1 = L.track(B0);
toc

B = L.get_bpm_readings();

%% Simulate 40 random machines and correct them with global orbit correction

% rms offsets
sigmaX = 0.1; % mm
sigmaXP = 0.1; % mrad
sigmaROLL = 0.1; % mrad
sigmaBPMS = 0.01; % mm

% compute response matrix and plot it
R = L.get_response_matrix(P0, 0.001);

figure(1);
surf(R);
xlabel('X-Y correctors')
ylabel('X-Y bpms')
title('Response matrix [mm / (T*mm)]')
drawnow;

BU = zeros(size(B));
BO = zeros(size(B));

L.set_bpm_resolution(sigmaBPMS);

%% Simulate just 1 machine (use more if statistics is needed!)
Nmachines = 1;
for i=1:Nmachines
    printf('Machine %d/%d\r', i, Nmachines);
    
    L.scatter_elements('quadrupole', sigmaX, sigmaX, 0.0, sigmaROLL, sigmaXP, sigmaXP, 'center');
    L.scatter_elements('bpm', sigmaX, sigmaX, 0.0, sigmaROLL, sigmaXP, sigmaXP, 'center');
    L.reset_correctors();

    % plain tracking
    L.track(B0);
    BU += L.get_bpm_readings();
    
    % orbit correction
    L.orbit_correction(R, B0);
    BO += L.get_bpm_readings();
end
printf('\n');

BU /= Nmachines;
BO /= Nmachines;

%% Plot the emittance evolution
figure(2);
clf ; hold on
plot(BU(:,1), 'displayname', 'X - uncorrected');
plot(BU(:,2), 'displayname', 'Y - uncorrected');
plot(BO(:,1), 'linewidth', 2, 'displayname', 'X - orbit-corrected');
plot(BO(:,2), 'linewidth', 2, 'displayname', 'Y - orbit-corrected');
legend('location', 'northwest', 'box', 'off');
xlabel('bpm [#]');
ylabel('bpm reading [mm]');