RF_Track;

%% create standing wave structure
a0 = 1e6; % V/m, principal Fourier coefficient
freq = 12e9; % Hz
ph_adv = 2*pi/3; % radian, phase advance per cell
n_cells = 60; % number of cells, negative sign indicates a start from the beginning of the cell

TW = TW_Structure(a0, 0, freq, ph_adv, n_cells);
TW.set_phid(0.0);

L = Lattice();
for i=1:10
    L.append(TW);
end

P0 = Bunch6dT (RF_Track.electronmass, 0, -1, [ 0 0 0 0 0 100 ]);

V = Volume();
V.dt_mm = 1;
V.odeint_algorithm = 'rk2';
V.add(L, 0, 0, 0);

P_final = V.autophase(P0)

P1 = V.track(P0);
P1.get_phase_space()
