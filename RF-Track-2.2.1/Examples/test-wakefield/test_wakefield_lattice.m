RF_Track;

%% define wakefield
SRWF = ShortRangeWakefield(0.002, 0.005, 0.006); % a,g,l

D = Drift(1);
D.add_collective_effect(SRWF);
D.set_cfx_nsteps(10);

%% bunch
sigma_z = 0.1; % mm
Pspread = 0.001; % permil, momentum spread
Pref = 50; % MeV/c

Twiss = Bunch6d_twiss();
Twiss.sigma_t = sigma_z; % mm/c
Twiss.sigma_d = Pspread; % permil

B0 = Bunch6d(RF_Track.electronmass, RF_Track.nC, -1, Pref, Twiss, 1000);
M0 = B0.get_phase_space();

%% lattice
L = Lattice();
L.append(D);

%% main loop
figure(1); clf ; hold on
figure(2); clf ; hold on
for X = linspace(-1, 1, 15)

    M0(:,1) = X;
    B0.set_phase_space(M0);
    
    %% track
    B1 = L.track(B0);
    
    %% do plots
    M1 = B1.get_phase_space('%x %xp %y %yp %dt %P');
    
    figure(1)
    scatter3(M1(:,5), M1(:,1), M1(:,2))

    figure(2)
    scatter3(M1(:,5), M1(:,1), M1(:,6))
end

figure(1)
xlabel('\Delta{t} [mm/c]');
ylabel('x [mm]');
zlabel('x'' [mrad]');

figure(2)
xlabel('\Delta{t} [mm/c]');
ylabel('x [mm]');
zlabel('P [MeV/c]');
