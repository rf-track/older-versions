function M0e = init_electrons(S0, N, offsetY)
RF_Track;
% S0 = mm initial longitudinal coordinate
% N = 10000 % number of macro-particles
% offsetY, initial vertical offset, in mm 

St = 44e-3; % mm
Sd = -5; % permill, uniform distribution

mass = RF_Track.electronmass;
Pref = 1500e3; % MeV/c
beta_gamma = Pref / mass;

beta_star_x = 0.007; % m
beta_star_y = 0.000068; % m
Twiss = Bunch6d_twiss();
Twiss.emitt_x = 660e-3; % mm.mrad
Twiss.emitt_y = 20e-3; % mm.mrad
Twiss.beta_x = beta_star_x; % m
Twiss.beta_y = beta_star_y; % m
Twiss.sigma_t = St; % mm/c (negative means uniform)
Twiss.sigma_d = Sd; % permil, momentum spread

B0e = Bunch6d (mass, 5.2e9, -1, Pref, Twiss, N);
M0e = B0e.get_phase_space('%X %Px %Y %Py %Z %Pz %m %Q %N');
xp = M0e(:,2) ./ M0e(:,6); % mrad
yp = M0e(:,4) ./ M0e(:,6); % mrad
M0e(:,1) += xp * S0; % drift back 
M0e(:,3) += yp * S0; % drift back 
M0e(:,5) += S0; %% moves the bunch at the beginning of the cell

M0e(:,3) += offsetY; % mm
