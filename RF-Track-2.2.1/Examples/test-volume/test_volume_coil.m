RF_Track;

%% Two coils
Cm = Coil(0.0, -1.0, 0.3); % L length [m],
                           % B field at the center of the coil [T],
                           % R radius [m]
Cp = Coil(0.0, +1.0, 0.3);

%% Volume
V = Volume();

% add the two coils
V.add(Cm, 0, 0, -0.5);
V.add(Cp, 0, 0,  0.5);

%% plot Bz
S_axis = linspace(-1, 1, 100); % m
Bz = [];
for S = S_axis
    [E,B] = V.get_field(0, 0, S * 1e3, 0);
    Bz = [ Bz ; B(3) ];
end

plot(S_axis, Bz);
xlabel('S [m]');
ylabel('B_z [T]');
