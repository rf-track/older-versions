setenv("GNUTERM","X11");
addpath('../../');

%% Load RF_Track
RF_Track;

ra = (0:3) / 1e3; % m
ta = [ 0 pi/2 ]; % rad
za = (0:5) / 1e3; % m

Nr = length(ra);
Nt = length(ta);
Nz = length(za);

Er = zeros(Nr, Nt, Nz);
Et = zeros(Nr, Nt, Nz);
Ez = zeros(Nr, Nt, Nz);

for i=1:Nr
    Er(i,:,:) = (i-1); % fills with a radial field
end

[Ra,Ta,Za] = ndgrid(ra,ta,za);

Ex = Er .* cos(Ta); % RF-Track wants Ex and Ey...
Ey = Er .* sin(Ta);

FLD = RF_FieldMap (Ex, Ey, Ez, ...
                   0, 0, 0, ...
                   ra(1), ...
                   ta(1), ...
                   ra(2) - ra(1), ...
                   ta(2) - ta(1), ...
                   za(2) - za(1), ...
                   za(end), ...
                   0, ...
                   0);

FLD.set_cylindrical(true);

disp("Purely radial field:\n");

for theta = [ 0 90 180 270 360 ]
    theta
    X = ra * cosd(theta) * 1e3; % mm
    Y = ra * sind(theta) * 1e3; % mm
    for i=1:length(ra)
        [E,B] = FLD.get_field(X(i), Y(i), 2, 0);
        disp(E(:)');
    end
    disp("");
end
