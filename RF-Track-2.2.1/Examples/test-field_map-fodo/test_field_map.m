%% load RF-Track

addpath('../../')
RF_Track;

%% load the field and create the lattice
mass = RF_Track.protonmass; % MeV/c^2
P0c = 100; % MeV/c
Q = 1;

% FODO cell with Lq = 24 cm, Ld = 76 cm (Lcell = 200 cm), mud = 90 degress
%
Lcell = 2; % m
Lquad = 0.24; % m
mud = 90; % deg

k1L = sind(mud/2) / (Lcell/4); % 1/m
strength = k1L * P0c / Q; % MeV/m
gradient = strength / Lquad * 1e6 / RF_Track.clight; % T/m = V/m/m/c

Nx = 41;
Ny = 41;
Nz = 10;
W = H = 0.40; % m
L = Lquad; % m
Xa = linspace(-W/2, W/2, Nx);
Ya = linspace(-H/2, H/2, Ny);
Za = linspace(0, L, Nz);
hx = W / (Nx-1); % m
hy = H / (Ny-1); % m
hz = L / (Nz-1); % m

[X,Y,Z] = ndgrid(Xa, Ya, Za);
Bx = gradient * Y; % T
By = gradient * X; % T
Bz = zeros(Nx, Ny, Nz); % T

Qf = RF_FieldMap(0, 0, 0, ...
                 Bx, By, Bz, ...
                 -W/2, ...
                 -H/2, ...
                 hx, ...
                 hy, ...
                 hz, ...
                 Lquad/2, 0, 0);
Qf.set_odeint_algorithm('rkf45');
Qf.set_nsteps(100);
%Qf = Quadrupole(Lquad/2, strength/2);
Qf.set_tt_nsteps(10);

Qd = RF_FieldMap(0, 0, 0, ...
                 -Bx, -By, Bz, ...
                 -W/2, ...
                 -H/2, ...
                 hx, ...
                 hy, ...
                 hz, ...
                 Lquad/2, 0, 0);
Qd.set_odeint_algorithm('rkf45');
Qd.set_nsteps(100);
%Qd = Quadrupole(Lquad/2, -strength/2);
Qd.set_tt_nsteps(10);

D = Drift(Lcell/2 - Lquad);
D.set_tt_nsteps(50);

N_particles = 10000;
BunchT = Bunch6d_twiss();
BunchT.emitt_x = 1; % mm.mrad
BunchT.emitt_y = 1; % mm.mrad
BunchT.beta_x = 3.253752685769096;
BunchT.beta_y = 0.733817214348110;
B0 = Bunch6d(mass, 0.0, Q, P0c, BunchT, N_particles, -1);

L = Lattice();
L.append(Qf);
L.append(D);
L.append(Qd);
L.append(Qd);
L.append(D);
L.append(Qf);

B = [];
T = [];
X = [];
B1 = B0;
for i=1:5
    B1 = L.track(B1);
    E = P0c / mass * det(cov(B0.get_phase_space('%x %xp %y %yp')))**0.25;
    T = [ T ; L.get_transport_table('%S %emitt_x %emitt_y')(1,:) E ];
    B = [ B ; L.get_transport_table('%S %beta_x %beta_y') ];
    X = L.get_transport_table('%S %mean_x %mean_y');
end

%% Plots
figure(1)
clf
plot(T(:,1), T(:,2), 'b*-', T(:,1), T(:,3), 'r*-', T(:,1), T(:,4), 'g*-');
legend('emitt x', 'emitt y', 'emitt 4d');
xlabel('S [m]');
ylabel('emitt [mm.mrad]');

figure(2)
clf
plot(B(:,1), B(:,2), 'b-', B(:,1), B(:,3), 'r-');
legend('beta x', 'beta y');
xlabel('S [m]');
ylabel('beta [m]');

figure(3)
clf
plot(X(:,1), X(:,2), 'b*-', X(:,1), X(:,3), 'r*-');
legend('mean x', 'mean y');
xlabel('S [m]');
ylabel('X/Y [m]');

figure(4)
clf
hold on
scatter(B0.get_phase_space('%x'), B0.get_phase_space('%Px'));
scatter(B1.get_phase_space('%x'), B1.get_phase_space('%Px'), 'r');
title('initial and final horizontal phase space');
xlabel('x [mm]');
ylabel('P_x [MeV/c]');

figure(5)
clf
hold on
scatter(B0.get_phase_space('%y'), B0.get_phase_space('%Py'));
scatter(B1.get_phase_space('%y'), B1.get_phase_space('%Py'), 'r');
title('initial and final vertical phase space');
xlabel('y [mm]');
ylabel('P_y [MeV/c]');
