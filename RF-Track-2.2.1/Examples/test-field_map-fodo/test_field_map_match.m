%% load RF-Track

addpath('../../')

RF_Track;

%% load the field and create the lattice
P0c = 100; % MeV/c
Q = 10;

% FODO cell with Lq = 24 cm, Ld = 76 cm (Lcell = 200 cm), mu = 90 degress
%
Lcell = 2; % m
Lquad = 0.24; % m
mu = 90; % deg

k1L = sind(mu/2) / (Lcell/4); % 1/m
strength = k1L * P0c / Q; % MeV/m
gradient = strength / Lquad * 1e6 / RF_Track.clight; % T/m = V/m/m/c

% double Bx = gradient / C_LIGHT T/m = V/m/m/c
% double By = gradient / C_LIGHT T/m = V/m/m/c

Nx = 41;
Ny = 41;
Nz = 4;
W = H = 0.40; % m
L = Lquad; % m
Xa = linspace(-W/2, W/2, Nx);
Ya = linspace(-H/2, H/2, Ny);
Za = linspace(0, L, Nz);
hx = W / (Nx-1); % m
hy = H / (Ny-1); % m
hz = L / (Nz-1); % m

[X,Y,Z] = ndgrid(Xa, Ya, Za);
Bx = gradient * Y; % T
By = gradient * X; % T
Bz = zeros(Nx, Ny, Nz); % T
Ex = Ey = Ez = zeros(Nx, Ny, Nz); % number of cells 

Qf = RF_FieldMap(Ex, Ey, Ez, ...
                      Bx, By, Bz, ...
                      -W/2, ...
                      -H/2, ...
                      hx, ...
                      hy, ...
                      hz, ...
                      Lquad/2, 0, 0);
Qf.set_odeint_algorithm('rk2');
Qf.set_nsteps(10);
%Qf = Quadrupole(Lquad/2, strength/2);
Qf.set_tt_nsteps(10);

Qd = RF_FieldMap(Ex,  Ey, Ez, ...
                      -Bx, -By, Bz, ...
                      -W/2, ...
                      -H/2, ...
                      hx, ...
                      hy, ...
                      hz, ...
                      Lquad/2, 0, 0);
Qd.set_odeint_algorithm('rk2');
Qd.set_nsteps(10);
%Qd = Quadrupole(Lquad/2, -strength/2);
Qd.set_tt_nsteps(10);

D = Drift(Lcell/2 - Lquad);
D.set_tt_nsteps(50);

Bunch.sigmaz = 1; % mm
Bunch.P_spread = 0;
Bunch.mass = RF_Track.protonmass;

global FODO
FODO = Lattice();
FODO.append(Qf);
FODO.append(D);
FODO.append(Qd);
FODO.append(Qd);
FODO.append(D);
FODO.append(Qf);

function f = constrain(X, a, b)
    X = X ./ (1+abs(X));
    f = ((b-a)*X+(a+b))/2;
endfunction

function M = merit(X)
    RF_Track;
    global FODO
    mass = RF_Track.protonmass;
    P0c = 100; % MeV/c
    Q = 10;
    N_particles = 100000;
    BunchT = Bunch6d_twiss();
    BunchT.emitt_x = 1; % mm.mrad
    BunchT.emitt_y = 1; % mm.mrad
    BunchT.beta_x = constrain(X(1), 0, 5); % m
    BunchT.beta_y = constrain(X(2), 0, 5); % m
    B0 = Bunch6d(mass, 0.0, Q, P0c, BunchT, N_particles, -1);
    B1 = FODO.track(B0);
    I1 = B1.get_info();
    M = (BunchT.beta_x - I1.beta_x)**2 + ...
        (BunchT.beta_y - I1.beta_y)**2 + ...
        I1.alpha_x**2 + ...
        I1.alpha_y**2
    
    I1.alpha_x
    I1.alpha_y
endfunction

X = fminsearch(@merit, [ 0 0 ] );

format long
beta_x = constrain(X(1), 0, 5) % m
beta_y = constrain(X(2), 0, 5) % m
