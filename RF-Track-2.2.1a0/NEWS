What is new in development version 2.2.1 (not yet released)
* Added particle ID
* Volume::set_length() allows to set an arbitrary length

What was new in version 2.2.0:

* Volume is fully integrated as an Element within Lattice
* Volume::s0 and Volume::s1 are now 3D reference frames that can connect Lattice beamlines with arbitrary orientations
* Remamed ParticleT.S as ParticleT.Z to avoid confusion
* Removed automatic projections from Bunch6dT to Bunch6d in get_phase_space()
* Lattice can now read MAD-X Twiss files directly
* Self-consistent BeamLoading effects in TW structures (Javier Olivares Herrador)
* Added Energy Straggling effect in Absorber element
* V.set_tt_nsteps(N) works in Lattice(), distributing N screens in Volume along the reference trajectory
* Added Bunch6d_QR, Quasi-Random bunch creation from Twiss parameters
* New hard-scattering model (Bernd Michael Stechauner)
* Improved in 'analytic' integration in Lattice
* Added Yoshida integrator 4th order
* Improved beam <-> screens intersection in Volume's tracking

What was new in version 2.1.6:

* Implemented 'circular' and 'rectangular' shapes in element Absorber
* Improved the what longitudinal emittance is handled in both Bunch6d and Bunch6dT
* Now Volume can track directly Bunch6d
* Now Transport Table in Volume can include also future particles by default
* Implemented Response matrix calculation and Automatic Orbit correction
* Improved transport table in Volume to optionally ignore particles outside volume
* Updated autophase to ignore elements whose t0 is already set
* Added Multiple Coulomb Scattering and energy loss
* Introduced ASTRA-like Generator 
* Introduced quasi-random sequences generator
* Added to transport table rmax99 and rmax90, the envelope at 90% and 99% of rmax
* Fixed a bug in thin-lens Sbend
* Added disp_z to BunchInfo

What was new in version 2.1.5:

* Added element Solenoid to both Volume and Lattice
* Added particle lifetime
* Added Incoherent synchrotron radiation as a collective effect
* Added new element SpaceCharge_Field()
* Fixed SWIG problems with typemaps
* Fix in Multipole element (many thanks to Ewa Oponowicz)

What was new in version 2.1.4:

* Added dispersion to bunch get_info() and transport_table
* Now Element::get_field() accepts vectors for X, Y, Z, and T
* Added velocity slicing in Space-charge PIC calculation
* Added Torodial Harmonics element

What was new in version 2.1.3:

* Added polarization to Thomson scattering
* Implemented end fields in GenericFieldMap
* Implemented asymmetric laser beam in Compton Scattering module
* Added new element Multipole, and new CollectiveEffect MultipoleKick

What was new in version 2.1.2:

* Added autophasing of RF elements
* Added back-tracking
* Added short- and long-range wakefields

 What was new in version 2.1:

* Added the possibility to have collective effects in both Lattice() and Volume()
* Added short-range wakefield effects
* Added new element RF_FieldMap_2d() optimized for cylindrically symmetric fields
* Added new element LaserBeam() to simulate Compton back scattering

What was new in verison 2.0.4:

* Changed the way TrackingOptions.t_max_mm works, now indefinite tracking is achieved with t_max_mm = Inf, and no longer t_max_mm = 0
* Added element Static_Magnetic_FieldMap_2d for cylindrically symmetric fields
* Improved default field's Jacobian for Elements
* Added support for SDDS files (writing)
* Added element SBend for dS tracking
* Added external elements
* Added Python interface
* Reimplemented misalignments in Bunch6d tracking, using Frame()
* Added element Volume
* Added element Coil

What was new in version 2.0:

* Reorganisation of classes and files
* New FieldMap elements with extensions _LINT and _CINT for different  interpolation
* Added element RF_Field_1d with reconstruction of the 3d E and B fields from the on-axis electric field
* Added magnetic element Adiabatic Matching Device
* Bunch6d::get_phase_space and Bunch6dT::get_phase_space now use the first particle as reference particle
