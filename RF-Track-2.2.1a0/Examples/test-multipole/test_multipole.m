RF_Track;

%% define particle
Pref = 50; % MeV/c
Q = 1; % e

B0 =  Bunch6d (RF_Track.electronmass, 0, Q, [ 1 0 -1 0 0 Pref ]);
B0T = Bunch6dT(RF_Track.electronmass, 0, Q, [ 1 0 -1 0 0 Pref ]);

%% define multipole (Qadurpole + sextupole)
P_over_Q = Pref / Q; % MV/c

k1L = 1.5; % 1/m, quadrupole
k2L = -3; % 1/m^2, sextupole

Lmult = 1; % m

M = Multipole(Lmult);
M.set_strengths([ 0 k1L k2L ] * P_over_Q);
M.set_nsteps(100);
M.set_tt_nsteps(100);

%% Lattice test
L = Lattice();
L.append(M);

%% tracking
B1 = L.track(B0);

T_lattice = L.get_transport_table('%S %mean_x %mean_y %mean_xp %mean_yp');

figure(1)
plot(T_lattice(:,1), T_lattice(:,2), ...
     T_lattice(:,1), T_lattice(:,3), ...
     T_lattice(:,1), T_lattice(:,4), ...
     T_lattice(:,1), T_lattice(:,5));
legend('x', 'y', 'xp', 'yp');
xlabel('S [m]');
title('Lattice (thick multipole)');

print -dpng plot_lattice_thick.png

%% Volume test
O = TrackingOptions();
O.tt_dt_mm = 1; % mm/c

V = Volume();
V.add(M, 0, 0, 0);

B1T = V.track(B0T, O);
T_volume = V.get_transport_table('%mean_S %mean_X %mean_Y %mean_Px %mean_Py %mean_Pz');
T_xp = T_volume(:,4) ./ T_volume(:,6) * 1e3; % mrad
T_yp = T_volume(:,5) ./ T_volume(:,6) * 1e3; % mrad

figure(2)
plot(T_volume(:,1) / 1e3, T_volume(:,2), ...
     T_volume(:,1) / 1e3, T_volume(:,3), ...
     T_volume(:,1) / 1e3, T_xp, ...
     T_volume(:,1) / 1e3, T_yp);
legend('x', 'y', 'xp', 'yp');
xlabel('S [m]');
title('Volume (thick multipole)');

print -dpng plot_volume_thick.png

%% Lattice 100 thin multipoles test
Nslices = 100;

M.set_length(0)
M.set_strengths([ 0 k1L k2L ] * P_over_Q / Nslices);

L = Lattice();
for i=1:Nslices
    L.append (M);
    L.append (Drift (Lmult / Nslices));
end

B1 = L.track(B0);

T_lattice = L.get_transport_table('%S %mean_x %mean_y %mean_xp %mean_yp');

figure(3)
plot(T_lattice(:,1), T_lattice(:,2), ...
     T_lattice(:,1), T_lattice(:,3), ...
     T_lattice(:,1), T_lattice(:,4), ...
     T_lattice(:,1), T_lattice(:,5));
legend('x', 'y', 'xp', 'yp');
xlabel('S [m]');
title('Lattice (100 thin multipoles)');
print -dpng plot_lattice_thin.png
