#include <iostream>

int main()
{
  const double G = 1.2; // T/m, gradient
  double x, y, z, t; // mm
  while (std::cin >> x >> y >> z >> t) {
    double Bx = G * y / 1000.0; // convert Y from mm to m
    double By = G * x / 1000.0; // convert X from mm to m
    std::cout << "0 0 0 " << Bx << ' ' << By << " 0\n";
  }
  std::cerr << "exiting quadrupole\n";
  return 0;
}
