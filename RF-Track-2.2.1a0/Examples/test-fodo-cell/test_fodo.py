## Load RF-Track
import RF_Track as RFT
import numpy as np

## Bunch parameters
mass = RFT.electronmass # MeV/c^2
charge = -1 # single-particle charge, in units of e
population = 1e10 # number of real particles per bunch
Pc = 5 # reference momentum, MeV/c
B_rho = Pc / charge # MV/c, reference rigidity

## FODO cell paramters
Lcell = 2 # m
Lquad = 0 # m
Ldrift = Lcell/2 - Lquad # m
mu = np.pi/2 # rad
k1L = np.sin(mu/2) / (Lcell/4) # 1/m
strength = k1L * B_rho # MeV/m

# Setup the elements
Qf = RFT.Quadrupole(Lquad/2, strength/2)
QD = RFT.Quadrupole(Lquad, -strength)
Dr = RFT.Drift(Ldrift)
Dr.set_tt_nsteps(100)

# Setup the lattice
FODO = RFT.Lattice()
FODO.append(Qf)
FODO.append(Dr)
FODO.append(QD)
FODO.append(Dr)
FODO.append(Qf)

## Define Twiss parameters
Twiss = RFT.Bunch6d_twiss()
Twiss.emitt_x = 0.001 # mm.mrad, normalized emittances
Twiss.emitt_y = 0.001 # mm.mrad
Twiss.alpha_x = 0.0
Twiss.alpha_y = 0.0
Twiss.beta_x = Lcell * (1 + np.sin(mu/2)) / np.sin(mu) # m, matched beta
Twiss.beta_y = Lcell * (1 - np.sin(mu/2)) / np.sin(mu) # m

## Create the bunch
B0 = RFT.Bunch6d(mass, population, charge, Pc, Twiss, 10000)

## Perform tracking
B1 = FODO.track(B0)

## Make plots
import matplotlib.pyplot as plt

## Retrieve the Twiss plot and the phase space
T = FODO.get_transport_table('%S %beta_x %beta_y')
plt.figure(1)
plt.plot(T[:,0], T[:,1], 'b-', linewidth=2, label='beta x')
plt.plot(T[:,0], T[:,2], 'r-', linewidth=2, label='beta y')
plt.legend(loc="upper left")
plt.xlabel('S [m]')
plt.ylabel('beta [m]')
plt.show()

## Retrieve the phase space and plot x-xp
M = B1.get_phase_space('%x %xp')

plt.figure(2)
plt.scatter(M[:,0], M[:,1])
plt.xlabel('x [mm]')
plt.ylabel('x'' [mrad]')
plt.show()
