RF_Track;

D = Drift(0.1);
L = Lattice();
for i=1:10; L.append(D); end

% set pitch angle
L.set_angles(0.0, pi/4, 0.0, 'center');

% nested misalignments
L_ = Lattice();
L_.append(L);

