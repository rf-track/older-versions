RF_Track;

%% beam, 1.5 TeV/c electrons
mass = RF_Track.electronmass;
A = load('testdist.dat.gz');
P = A(:,1)*1e3;
x = A(:,2)/1e3;
y = A(:,3)/1e3;
z = A(:,4)/1e3;
xp = A(:,5)/1e3;
yp = A(:,6)/1e3;

%% Create the bunch
B0 = Bunch6d(mass, 4e9, -1, [ x xp y yp z P ]);

%% Sequence
P_over_Q = 1500e3 / -1;
QF1 = Quadrupole (3.25668132, P_over_Q, 0.04046334564);
QD0 = Quadrupole (2.732532, P_over_Q, -0.1162098187);
DEC0 = Multipole (0.496824, P_over_Q, -[ 0 0 0 53.80000813 * 0.496824 ]);
DD0 = Multipole (0, P_over_Q, [ 0 0 0 0 33509754.04 ]);
SF1 = Sextupole(0.496824, P_over_Q, -12.17930621);
SD0 = Sextupole(0.496824, P_over_Q, 43.80804075);

if 1
    
    ISR = IncoherentSynchrotronRadiation(true);

    QF1.add_collective_effect(ISR);
    QF1.set_cfx_nsteps(50);

    SF1.add_collective_effect(ISR);
    SF1.set_cfx_nsteps(50);

    QD0.add_collective_effect(ISR);
    QD0.set_cfx_nsteps(50);

    SD0.add_collective_effect(ISR);
    SD0.set_cfx_nsteps(50);

    DEC0.add_collective_effect(ISR);
    DEC0.set_cfx_nsteps(50);

else
    
    QF1.set_nsteps(50);
    SF1.set_nsteps(50);
    QD0.set_nsteps(50);
    SD0.set_nsteps(50);
    DEC0.set_nsteps(50);
end

FFS = Lattice();
FFS.append(Drift(2));
FFS.append(SF1);
FFS.append(Drift(0.248412));
FFS.append(QF1);
FFS.append(Drift(2.48412));
FFS.append(DEC0);
FFS.append(Drift(0.248412));
FFS.append(SD0);
FFS.append(Drift(0.248412));
FFS.append(DD0);
FFS.append(QD0);
FFS.append(Drift(3.5026092));

B1 = FFS.track(B0);
M1 = B1.get_phase_space();

figure(1)
clf
subplot(1,2,1)
scatter(M1(:,1)*1e6, M1(:,2)*1e3, 0.1, '.');
xlabel('x [nm]');
ylabel('x'' [urad]');

subplot(1,2,2)
scatter(M1(:,3)*1e6, M1(:,4)*1e3, 0.1, '.');
xlabel('y [nm]');
ylabel('y'' [urad]');
