RF_Track;

%% beam, 100 GeV/c electron
mass = RF_Track.electronmass; % MeV/c^2
Pref = 100e3; % MeV/c
Eref = hypot(mass, Pref); % MeV
rho = 3026.42; % m, effective bending radius, LEP

%% A drift with By != 0.0
By = Pref / rho / 299.792458; % T, LEP field at 100 GeV beam energy
Circ = 2*pi*rho; % m, circumference

Bfield = Drift(3*rho); % larger than diameter
Bfield.set_static_Bfield(0, By, 0);

%% tracking
T = TrackingOptions();
T.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45', 'rk8pd'
T.odeint_epsabs = 1;
T.dt_mm = 100; % mm/c
T.t_max_mm =  4 * Circ * 1e3; % mm/c
T.cfx_dt_mm = 100; % X [mm] Px [MeV/c] Y [mm] Py [MeV/c] S [mm] Pz [MeV/c]
T.tt_dt_mm = 1000; % X [mm] Px [MeV/c] Y [mm] Py [MeV/c] S [mm] Pz [MeV/c]
T.verbosity = 0;

%% create beam
B0 = Bunch6dT([ -rho*1e3 0 0 0 0 Pref RF_Track.electronmass -1 1 0.0 ]);

%% ISR
ISR = IncoherentSynchrotronRadiation();

figure(1)
clf ; hold on

%% Volume
for i=1:2
    V = Volume();
    V.add(Bfield, 0, 0, 0, 'center');
    if i==2
        V.add_collective_effect(ISR);
    end
    
    %% Tracking
    B1 = V.track(B0, T);
    T1 = V.get_transport_table('%mean_S %mean_X %mean_Y %t %mean_P');    

    plot(T1(:,1) / 1e6, T1(:,2) / 1e6)

end
title('circular trajectory over 4 turns');
xlabel('x [km]');
ylabel('y [km]');
legend('without ISR', 'with ISR');

figure(2);
clf ; hold on

S = T1(:,4) * Pref / Eref / 1e3; % m
% 1 turn
t = T1 (S<Circ,4);
P = T1 (S<Circ,5);

S = S (S<Circ);
E = hypot(mass, P); % MeV
plot(S/1e3, E/1e3);

% analytic estimate
% 4pi/3 * electronradius (electronmass c^2)^3  * (E MeV)^4 / (rho m) = (1 / 11304194512464.615234375) MeV
dE = Eref^4 / rho / 11304194512464.615234375; % MeV, energy loss per turn

plot(S/1e3, ones(size(S)) * (Eref - dE) / 1e3);
xlabel('S [km]')
ylabel('E [GeV]');
title('Energy over one turn')
legend('simulation', 'analytic estimate');