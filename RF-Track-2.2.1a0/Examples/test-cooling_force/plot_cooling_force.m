addpath('../../');

pkg load image;

%% Load RF_Track

RF_Track;

setenv("DEBUG"); % triggers RF-Track debug information

%%%%%%%%%%%%%%

V_ele = 0.09;

Cooler.L = 3; % m
Cooler.B = 0.07; % T
Cooler.r0 = 25; % mm, electron beam radius

%% Electron beam parameters
Electrons.Ne = 8.27e13; % electron number density #/m^3
Electrons.V = V_ele; % 0.2% less than the ion speed
Electrons.gamma = 1 / sqrt(1 - Electrons.V**2); %
Electrons.P = RF_Track.electronmass * Electrons.V * Electrons.gamma;

Nx = 16;
Ny = 16;
Nz = 4;

E = ElectronCooler(Cooler.L, Cooler.r0/1e3, Cooler.r0/1e3);
E.set_Q(-1);
E.set_electron_mesh(Nx, Ny, Nz, Electrons.Ne, 0, 0, Electrons.V);
E.set_static_Bfield(0.0, 0.0, Cooler.B);
E.set_temperature(0.01, 0.001);
E.compute_cooling_force();

for force = { 'unmagnetized' , 'magnetized' }

    U = load(sprintf('cooling_force_%s.txt', force{}));

    Ur = sort(unique(U(:,1)));
    Ul = sort(unique(U(:,2)));
    Nr = length(Ur);
    Nl = length(Ul);
    
    Fr_int = reshape (U(:,3), Nl, Nr); % integrated force
    Fl_int = reshape (U(:,4), Nl, Nr);
    Fr_asy = reshape (U(:,5), Nl, Nr); % asymptotic force
    Fl_asy = reshape (U(:,6), Nl, Nr);
    Fr_rat = reshape (U(:,7), Nl, Nr); % asymptotic / integrated ratio
    Fl_rat = reshape (U(:,8), Nl, Nr);

    pcolor(Ur, Ul, Fr_int/1e6);
    shading flat;
    h = colorbar;
    set(h, 'fontsize', 10);
    set(get(h,'label'),'string','F_{\perp_{(integrated)}} [10^6/c^2]', 'fontsize', 16);
    if strcmp(force, 'magnetized')
        xlabel('U_\perp / \Delta_{||}', 'fontsize', 16);
        ylabel('U_{||} / \Delta_{||}', 'fontsize', 16);
    else
        xlabel('U_\perp / \Delta_{\perp}', 'fontsize', 16);
        ylabel('U_{||} / \Delta_{\perp}', 'fontsize', 16);
    end
    axis([ min(Ur) max(Ur) min(Ul) max(Ul) ]);

    print -dpng -r200 tmp.png ; system(sprintf('anytopnm tmp.png | pnmcrop | pnmtopng > plot_Fr_%s_int_2.png', force{}));

    pcolor(Ur, Ul, Fl_int/1e6);
    shading flat;
    h = colorbar;
    set(h, 'fontsize', 10);
    set(get(h,'label'),'string','F_{{||}_{(integrated)}} [10^6/c^2]', 'fontsize', 16);
    if strcmp(force, 'magnetized')
        xlabel('U_\perp / \Delta_{||}', 'fontsize', 16);
        ylabel('U_{||} / \Delta_{||}', 'fontsize', 16);
    else
        xlabel('U_\perp / \Delta_{\perp}', 'fontsize', 16);
        ylabel('U_{||} / \Delta_{\perp}', 'fontsize', 16);
    end
    axis([ min(Ur) max(Ur) min(Ul) max(Ul) ]);
    
    print -dpng -r200 tmp.png ; system(sprintf('anytopnm tmp.png | pnmcrop | pnmtopng > plot_Fl_%s_int_2.png', force{}));
    
end
system('rm -f tmp.png');