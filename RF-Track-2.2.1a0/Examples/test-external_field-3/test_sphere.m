%% prepare for test
system('make sphere');

RF_Track;
RF_Track.number_of_threads = 1;

%% Two coils
Sp = ExternalField(1, "./sphere 1 0.2",1);

%% Volume
V = Volume();

% set boundaries
V.set_s0(0);
V.set_s1(1);

% add the two coils
V.add(Sp, 0, 0, 0.5, "center");

%% Create a beam
N = 10000;
X = 1e3 * (rand(N,1) - 0.5);
Y = 1e3 * (rand(N,1) - 0.5);
O = zeros(N,1);
P = 100000 * ones(N,1);
B = [ X O Y O O P ];
B0 = Bunch6dT(RF_Track.electronmass, 1, -1, B);
O = TrackingOptions();
O.dt_mm = 1.0;
O.t_max = 200;
O.verbosity = 2;

B1 = V.track(B0, O);

L1 = B1.get_lost_particles();
scatter3(L1(:,5), L1(:,1), L1(:,3))
xlabel('Z [mm]');
ylabel('X [mm]');
zlabel('Y [mm]');