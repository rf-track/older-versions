#include <iostream>
#include <string>
#include <cmath>

int main(int argc, char *argv[] )
{
  if (argc!=3) {
    std::cerr << "usage: ./sphere L R\n\n"
      " L: element length [m]\n"
      " R: radius of sphere (with center in 0,0,L/2) [m]\n\n";
    return 1;
  }
  auto sqr = [] (double x ) { return x*x; };
  const double L = std::stod(argv[1]); // m
  const double R = std::stod(argv[2]); // m
  const double half_L = L*1e3 / 2; // mm
  const double R2 = R*R*1e6; // mm^2
  double x, y, z, t; // mm
  while (std::cin >> x >> y >> z >> t) {
    if (sqr(x)+sqr(y)+sqr(z-half_L)>R2) {
      std::cout << "0 0 0 0 0 0\n";
    } else {
      std::cout << "NaN NaN NaN NaN NaN NaN\n";
    }
  }
  return 0;
}
