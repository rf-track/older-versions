RF_Track;

%% beam, 100 MeV/c proton
Pz = 1000; % MeV/c
B0 = Bunch6dT([ 0 0 0 0 0 Pz RF_Track.protonmass +1 1 0.0 ]);

%% A drift with By != 0.0
rho = 1; % m, bending radius
By = Pz * 1e6 / rho  / RF_Track.clight; % T

Dp = Drift(1.5);
Dp.set_static_Bfield(0, By, 0);

%% tracking
T = TrackingOptions();
T.odeint_algorithm = 'rk2'; % pick your favorite algorithm, 'rk2', 'rkf45', 'rk8pd'
T.odeint_epsabs = 1e-6;
T.dt_mm = 10; % mm/c
T.wp_basename = "test";
T.wp_gzip = false;
T.wp_dt_mm = 100; % X [mm] Px [MeV/c] Y [mm] Py [MeV/c] S [mm] Pz [MeV/c]

clf
hold on
for angle = linspace(0, 2*pi, 17)

    %% Volume
    V = Volume();
    V.add(Dp, 0, 0, 1, angle, 0, 0);
    
    B1 = V.track(B0, T);
    
    % plots
    F = glob('test.*.txt');
    XYZ = [];
    for i=1:length(F)
        M = load(F{i});
        XYZ = [ XYZ ; M(1) M(3) M(5) ];
    end
    system('rm -f test.*.txt');
    
    plot3(-XYZ(:,1), XYZ(:,3), XYZ(:,2));
    drawnow;
    
end