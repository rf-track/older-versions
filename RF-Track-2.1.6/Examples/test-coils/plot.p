set term post eps enh color solid 22 lw 2

set key outside;
set key right top;
set xlabel 'X [mm]'
set ylabel 'Y [mm]'
set title '1000 turns'

set out 'plot_all.eps'

plot\
 'initial.dat'u 1:3 ti "Initial" w p,\
 'rk2_1000.dat' u 1:3 ti "rk2" w p,\
 'rkf45_1000.dat' u 1:3 ti "rkf45" w p,\
 'msadams_1000.dat' u 1:3 ti "msadams" w p
 
set out 'plot_rk_all.eps'

plot\
 'initial.dat'u 1:3 ti "Initial" w p,\
 'rk2_1000.dat' u 1:3 ti "rk2" w p,\
 'rkf45_1000.dat' u 1:3 ti "rkf45" w p
 
!for i in plot*eps; do epstopdf $i ${i%.eps}.pdf ; rm -f $i & done 
