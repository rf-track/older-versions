%%
%% CLIC 3 TeV beam-beam effects
%%

clear all
close all

RF_Track;


%% Simulation parameters
S0 = 0.2; % mm, initial distance from IP
D = 2*S0; % mm, simulation distance
D_step = 0.001; % mm, integration step

% init bunches
offsetY = 0; % mm, vertical offset
M0e = init_electrons (-S0, 10000, -offsetY);
M0p = init_positrons ( S0, 10000, +offsetY);

B0p = Bunch6dT(M0p);
B0e = Bunch6dT(M0e);

%% main loop
N_steps = D / D_step;
dt_mm = D_step; % mm/c, integration step

do_EE = 1; % simulate effect eletrons <- electrons
do_EP = 1; % simulate effect eletrons <- positrons
do_PP = 1; % simulate effect positrons <- positrons
do_PE = 1; % simulate effect positrons <- eletrons

do_plot = 1;
do_save = 0;

if do_save
    save('-text', '-zip', sprintf('electrons_%05d.txt.gz', 0), 'M0e');
    save('-text', '-zip', sprintf('positrons_%05d.txt.gz', 0), 'M0p');
end

E_SY = [ mean(M0e(:,[ 5 3 ])) ];
P_SY = [ mean(M0p(:,[ 5 3 ])) ];

for i = 1:N_steps
    
    printf('Step %d/%d ...\r', i, N_steps)

    Fep = Fee = zeros(B0e.size(), 3);
    Fpe = Fpp = zeros(B0p.size(), 3);
    
    if do_EE || do_PE ; SC_ele = SpaceCharge_Field (B0e, 32, 32, 32); end
    if do_EP || do_PP ; SC_pos = SpaceCharge_Field (B0p, 32, 32, 32); end
    
    if do_EE ; Fee = compute_force(B0e, SC_ele); end
    if do_EP ; Fep = compute_force(B0e, SC_pos); end
    
    if do_PP ; Fpp = compute_force(B0p, SC_pos); end
    if do_PE ; Fpe = compute_force(B0p, SC_ele); end
    
    B0e.apply_force(Fee + Fep, dt_mm);
    B0p.apply_force(Fpe + Fpp, dt_mm);
    
    %% Plots
    if (do_plot || do_save) && rem (i, 10) == 0
        M0e = B0e.get_phase_space();
        M0p = B0p.get_phase_space();
    end
    
    if do_plot && rem (i, 10) == 0
        figure(1)
        clf
        hold on
        scatter (M0e(:,5), M0e(:,3)*1e6);
        scatter (M0p(:,5), M0p(:,3)*1e6);
        axis([ -0.4 0.4 -15 15 ])
        xlabel ('S [mm]');
        ylabel ('Y [nm]');
        legend('electrons', 'positrons');
        grid
        drawnow
        print('-dpng', '-S800,600', '-F:4', sprintf('frame_%05d.png', i));
    end
    
    if do_save && rem (i, 10) == 0
        save('-text', '-zip', sprintf('electrons_%05d.txt.gz', i), 'M0e');
        save('-text', '-zip', sprintf('positrons_%05d.txt.gz', i), 'M0p');
    end
end

system('rm -f video.mp4');
system('ffmpeg -framerate 10 -pattern_type glob -i ''frame_*.png'' -r 20 video.mp4')
system('rm -f frame_*.png');
