set G 1.2 ; # T/m, gradient
while {[gets stdin line] >= 0} {
    set coords [split $line { }]
    set X [lindex $coords 0]
    set Y [lindex $coords 1]
    set Bx [expr $G * $Y / 1000.0] ; # convert Y from mm to m
    set By [expr $G * $X / 1000.0] ; # convert X from mm to m
    puts "0 0 0 $Bx $By 0"
}
