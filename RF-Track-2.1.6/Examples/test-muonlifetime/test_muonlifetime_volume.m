RF_Track;

%% Define reference particle, and rigidity
Part.mass = RF_Track.muonmass; % MeV/c^2
Part.P = 1000; % MeV/c
Part.Q = -1; % e

%% Bunch
N = 10000;
X = randn (N,1);
Y = randn (N,1);
O = zeros (N,1);
I = ones  (N,1);

B0 = Bunch6dT (Part.mass, 0.0, Part.Q, [ X O Y O O I*Part.P ]);
B0.set_lifetime (RF_Track.muonlifetime);

%% Volume
V = Volume();
V.set_s1 (20e3); % 20 km

O = TrackingOptions();
O.tt_dt_mm = 20e3; % mm/c, transport table, tracks average quantities every 20 m/c

tic
B1 = V.track(B0, O);
toc

T = V.get_transport_table ('%mean_S %N'); % transmission vs S

plot (T(:,1) / 1e6, T(:,2));
xlabel ('S [km]');
ylabel ('transmission [%]');

