#! /opt/local/bin/octave -q --persist
addpath('../../');

%% Load RF_Track

RF_Track;

Q_ele = 1e4;
Q_pro = 1e11;

%% define bunch
N_ele = 10000;
N_pro = 10000;

randn("seed", 1234);

B_ele = [ 2 + 0.3 * randn(N_ele,1), ... % x [mm]
	  0.0 * randn(N_ele,1), ... % Px [MeV/c]
	  -4 + 0.3 * randn(N_ele,1), ... % y [mm]
	  0.15 * ones(N_ele,1), ...  % Py [MeV/c]
	  0.1 * randn(N_ele,1), ... % Z [mm]
	  0.0 * ones(N_ele,1), ...  % Pz [MeV/c]
	  RF_Track.electronmass * ones(N_ele,1), ... % mass [MeV/c^2]
	  -1 * ones(N_ele,1), ...   % Q [e]
	  Q_ele / N_ele * ones(N_ele,1) ]; % N [#]

B_pro = [ 0.1 * randn(N_pro,1), ... % x
	  0.0 * randn(N_pro,1), ... % Px
	  0.1 * randn(N_pro,1), ... % y
	  0.0 * randn(N_pro,1), ... % Py
	  0.1 * randn(N_pro,1), ... % Z
	  0   * ones(N_pro,1),  ... % Pz
	  RF_Track.protonmass * ones(N_pro,1), ... % mass
	  +1 * ones(N_pro,1), ...   % Q
	  Q_pro / N_pro * ones(N_pro,1) ]; % N

Nx = 32; % mesh for PIC calculation
Ny = 32;
Nz = 32;

SC = SpaceCharge_PIC_FreeSpace(Nx,Ny,Nz);

N_tests = 5; % takes the average of 5 tests
N_frames = 60;
T = [];
for RF_Track.number_of_threads = 1:RF_Track.max_number_of_threads
  time_per_frame = 0.0;
  for n=1:N_tests
    B0 = Bunch6dT( [ B_ele ; B_pro ] );
    for i=1:N_frames
      tic
      F = SC.compute_force(B0);
      B0.apply_force(F, 0.4);
      time_per_frame += toc;
    end
  end
  time_per_frame /= N_frames * N_tests;
  T = [ T ; time_per_frame ];
  disp([ RF_Track.number_of_threads time_per_frame ]);
end

save(sprintf('T_%s_%s_%s.txt', uname.sysname, uname.machine, uname.nodename), '-text', 'T');

plot(T*1e3)
xlabel('number of cores');
ylabel('simulation time [ms]');
print -dpng plot_performance.png
