%% load RF-Track
RF_Track;

%% Define reference particle, and rigidity
Part.mass = RF_Track.electronmass; % MeV/c^2
Part.P = 100; % MeV/c
Part.Q = -1; % e

%% Bunch
N = 10000;
X = randn(N,1);
Y = randn(N,1);
O = zeros(N,1);
I = ones (N,1);
B0 = Bunch6d(Part.mass, 0.0, Part.Q, [ X O Y O O I*Part.P ]);

%% 1 meter of air
A = Absorber(1.0, 'air');
A.disable_log_term();

figure(1)
clf ; hold on
for nsteps = [ 10 50 100 500 1000 ] % test different number of steps for convergence

    %% Set the nunmber of steps
    A.set_cfx_nsteps(nsteps);

    %% Lattice
    L = Lattice();
    L.append(A);

    tic
    B1 = L.track(B0);
    T = L.get_transport_table('%S %sigma_x %sigma_y');
    toc

    plot(T(:,1), T(:,2), '*-', 'displayname', sprintf('nsteps = %d', nsteps));
    axis([ 0 1 0 5 ])
    xlabel('S [m]');
    ylabel('\sigma_X [mm]');
    title('Lattice', 'fontsize', 12);
    legend('location', 'northwest');
    grid on
    drawnow
end
pause
