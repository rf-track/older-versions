RF_Track;

%% create standing wave structure
a0 = 1.0; % V/m, principal Fourier coefficient
freq = 12e9; % Hz
ph_adv = 2*pi/3; % radian, phase advance per cell
n_cells = 3; % number of cells, negative sign indicates a start from the beginning of the cell

TW = TW_Structure(a0, 0, freq, ph_adv, n_cells);

%% plot Ez along the structure, in time
T_period = RF_Track.s / freq; % mm/c
T_axis = linspace(0, T_period, 64); % mm/c
Z_axis = linspace(0, TW.get_length()*1e3, 64); % mm
for t=T_axis
    E_ = [];
    B_ = [];
    for z=Z_axis
        [E,B] = TW.get_field(0, 0, z, t);
        E_ = [ E_ E ];
        B_ = [ B_ B ];
    end
    plot(Z_axis, E_(3,:));
    title(sprintf('t = %2.f%% of period', t*100/T_axis(end)));
    xlabel('Z [mm]');
    ylabel('E_z [V/m]');
    axis([ 0 Z_axis(end) -a0*1.2 a0*1.2 ]);
    drawnow;
end


